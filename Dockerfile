#first stage building angular image
FROM node:14.18-alpine3.14 as build
RUN mkdir -p /app
WORKDIR /app

COPY package.json /app/
COPY package-lock.json /app/
RUN npm -g install npm@6.14.15
RUN npm install

COPY . /app/
RUN npm run build:dev


#deploy on tomcat

FROM tomcat:8.0-alpine


RUN mkdir -p /usr/local/tomcat/webapps/crm-kyc

COPY --from=build /app/dist/CRM-KYC /usr/local/tomcat/webapps/crm-kyc

ENTRYPOINT ["catalina.sh", "run"]



#docker build . -t crmkycapp
#docker run -d -p 80:8080 crmkycapp


