import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Console } from 'console';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';
import { Subscription } from 'rxjs';
import { HabilitationModel } from './main/models';
import { HabilitationService } from './main/services';
import {RouteInfoModel,MENU_ITEMS} from 'src/app/layouts/menu/menu-items'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  title = 'MANAR CRM/KYC';
  menu:RouteInfoModel[]=MENU_ITEMS;
  onPermissionChangeSubscription: Subscription = null;

  constructor(
    private translate: TranslateService,  
    private permissionsService: NgxPermissionsService,
    private roleService:NgxRolesService,
    private habilitationService: HabilitationService,) {
    this.translate.use(this.getLanguage());
    this.onPermissionChangeSubscription = this.permissionsService.permissions$.subscribe((event) => { this.givePermissionMenuItems(); });
    this.givePermissionMenuItems();
  }
  ngOnInit(): void {
   //this.loadPermissions();
  }

  private getLanguage(): string {
    if (localStorage.getItem('langue')===null) {
      localStorage.setItem('langue', this.translate.getBrowserLang());
    }
    return localStorage.getItem('langue');
  }


  /*** LOAD PERMISSIONS */
  loadPermissions() {
    this.habilitationService.getHabalitation()
      .subscribe((habilitations: HabilitationModel) => {
         let permissions: string[] = [];
        if (habilitations) {
          permissions.push(habilitations.role["name"]);
          habilitations.permissions.map(p=>permissions.push(p.name));
          this.permissionsService.loadPermissions(permissions);

      };

});
}

/** GIVE PERMISSION MENU ITEMS */
givePermissionMenuItems() {
  this.menu.forEach(item => this.givePermissionMenuItem(item));
}

/** GIVE PERMISSION MENU ITEM */
givePermissionMenuItem(menuItem: RouteInfoModel) {
  if (menuItem.children != null) {
    menuItem.children.forEach(item => this.givePermissionMenuItem(item));
  }
  if (menuItem.data && menuItem.data.permissions) {
  
    this.permissionsService.hasPermission(menuItem.data.permissions).
      then(response => {

        response ? menuItem.hidden = false : menuItem.hidden = true;
      });
  } else if  (menuItem.data && menuItem.data.only) {

    this.permissionsService.hasPermission(menuItem.data.permissions).
      then(response => {
        response ? menuItem.hidden = false : menuItem.hidden = true;
      });
  } else if  (menuItem.data && menuItem.data.oneOf) {

    this.permissionsService.hasPermission(menuItem.data.oneOf).
      then(response => {
        response ? menuItem.hidden = false : menuItem.hidden = true;
      });
  }
  
  else {
    menuItem.hidden = true;
  }
}

}
