import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'ago'
})
export class AgoPipe implements PipeTransform {

  transform(value: Date | undefined, limit?: string): string {
    if (!value) {
      return '';
    }

    let date = new Date(value);

    let seconds = Math.abs(Math.floor(((new Date()).getTime() - date.getTime()) / 1000));
    let interval = seconds / 31536000;

    if (interval > 1) return this.genAgo(interval) + " a";

    interval = seconds / 2592000;
    if (interval > 1) return this.genAgo(interval) + " m";

    interval = seconds / 86400;
    if (interval > 1 ) return this.genAgo(interval) + " j";

    interval = seconds / 3600;
    if (interval > 1) return this.genAgo(interval) + " h";

    interval = seconds / 60;
    return this.genAgo(interval) + " min";
  }

  genAgo(value: any): any {
    return Math.floor(value);
  }

}
