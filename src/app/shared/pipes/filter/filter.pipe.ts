import { CurrencyPipe, DatePipe, DecimalPipe, PercentPipe } from '@angular/common';
import { Inject, Pipe, PipeTransform } from '@angular/core';
import { ColumnFilteredEventModel, DataTableColumnsFormat } from 'src/app/shared/models';
import { CommonUtil } from './../../utils/common.util';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  constructor(private decimalPipe: DecimalPipe,
    private currencyPipe: CurrencyPipe,
    private percentPipe: PercentPipe,
    private datePipe: DatePipe,
    @Inject('pipeParameters') private pipeParameters: any) { }

  transform(items: any[], filter: ColumnFilteredEventModel): any[] {

    if (!items || !filter) return items;

    return items.filter(item => {

      let result = true;

      for (let filterColumn of filter.columns) {

        if (!item.hasOwnProperty(filterColumn.columnName)) continue;

        let compar: boolean = null;

        if (filterColumn.value === null || filterColumn.value === '') {
          compar = true;

        } else if (item[filterColumn.columnName] === null) {
          compar = false;

        } else {

          switch (filterColumn.format) {

            case DataTableColumnsFormat.CURRENCY:
              let numberCurrencyFormat = this.currencyPipe.transform(item[filterColumn.columnName], this.pipeParameters.currency.currencyCode, 'symbol', this.pipeParameters.currency.digitsInfo);
              compar = CommonUtil.removeSpaceFromStr(numberCurrencyFormat).toLowerCase().startsWith(CommonUtil.removeSpaceFromStr(filterColumn.value).toLowerCase());
              break

            case DataTableColumnsFormat.NUMBER:
              let numberDecimalFormat = this.decimalPipe.transform(item[filterColumn.columnName], this.pipeParameters.number.digitsInfo);
              compar = CommonUtil.removeSpaceFromStr(numberDecimalFormat).toLowerCase().startsWith(CommonUtil.removeSpaceFromStr(filterColumn.value).toLowerCase());
              break

            case DataTableColumnsFormat.PERCENT:
              let numberPercentFormat = this.percentPipe.transform(item[filterColumn.columnName] / 100, this.pipeParameters.percent.digitsInfo);
              compar = CommonUtil.removeSpaceFromStr(numberPercentFormat).toLowerCase().startsWith(CommonUtil.removeSpaceFromStr(filterColumn.value).toLowerCase());
              break

            case DataTableColumnsFormat.DATE:
              let strDateFormat = this.datePipe.transform(item[filterColumn.columnName], this.pipeParameters.date.format);
              compar = CommonUtil.removeSpaceFromStr(strDateFormat).toLowerCase().includes(CommonUtil.removeSpaceFromStr(filterColumn.value).toLowerCase());
              break

            case DataTableColumnsFormat.STRING:
              compar = item[filterColumn.columnName].toLowerCase().includes(filterColumn.value.toLowerCase());
              break

            default:
              if (typeof item[filterColumn.columnName] === 'number') {
                compar = item[filterColumn.columnName] === +filterColumn.value;
              } else if (typeof item[filterColumn.columnName] === 'string') {
                compar = item[filterColumn.columnName].toLowerCase().includes(filterColumn.value.toLowerCase());
              } else {
                compar = item[filterColumn.columnName] === filterColumn.value;
              }

          }

        }

        if (compar) {
          if (!filter.strict) {
            return true;
          }
        } else {
          result = false;
        }

      }

      return result;
    });

  }

}
