import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtil } from '../../utils/common.util';
import { ColumnSortedDirectionType } from '../../models';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(items: any[], property: string, direction: ColumnSortedDirectionType): any[] {
    if (!property || !direction) {
      return items;
    }
    return CommonUtil.sortData(items, direction, property);;
  }

}
