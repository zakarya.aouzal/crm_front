import { Directive, HostBinding, Input, OnChanges, ElementRef } from '@angular/core';

@Directive({
  selector: '[appNumberColor]'
})
export class NumberColorDirective implements OnChanges {

  @Input('appNumberColor')
  options: { apply: boolean, input: number } = null;

  @HostBinding('class')
  elementClass: string = null;

  constructor(private el: ElementRef) { }

  ngOnChanges() {

    let oldIcon = this.el.nativeElement.querySelector('.fa');
    let oldPositiveSymbol = this.el.nativeElement.querySelector('.positive-symbol');

    if (oldIcon) {
      oldIcon.remove();
    }

    if (oldPositiveSymbol) {
      oldPositiveSymbol.remove();
    }

    if (this.options && this.options.apply && this.options.input !== null) {

      if (this.options.input < 0) {

        this.elementClass = 'text-danger';
        
        this.el.nativeElement.insertAdjacentHTML('beforeend', '<em class="fa fa-arrow-down ml-1"></em>');

      } else if (this.options.input > 0) {

        this.elementClass = 'text-success';

        this.el.nativeElement.insertAdjacentHTML('afterbegin', '<span class="positive-symbol">+</span>');
        this.el.nativeElement.insertAdjacentHTML('beforeend', '<em class="fa fa-arrow-up ml-1"></em>');

      } else if (this.options.input === 0) {

        this.elementClass = 'text-warning';

      }

    }
  }

}
