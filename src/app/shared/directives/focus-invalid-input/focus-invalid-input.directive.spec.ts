import { FocusInvalidInputDirective } from './focus-invalid-input.directive';

describe('FocusInvalidInputDirective', () => {
  it('should create an instance', () => {
    let elRefMock = {
      nativeElement: document.createElement('div')
    };
    const directive = new FocusInvalidInputDirective(elRefMock);
    expect(directive).toBeTruthy();
  });
});
