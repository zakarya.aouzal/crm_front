import { ColumnFilteredEventModel } from 'src/app/shared/models';
import { Directive, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { FilterService } from '../../services/filter/filter.service';

@Directive({
  selector: '[appFilterTable]'
})
export class FilterTableDirective {

  constructor(private filterService: FilterService) { }

  @Output()
  filtered = new EventEmitter<ColumnFilteredEventModel>();

  private columnFiltredSubscription: Subscription = null;

  ngOnInit() {
    this.columnFiltredSubscription = this.filterService.columnFiltred$.subscribe(event => {
      this.filtered.emit(event);
    });
  }

  ngOnDestroy() {
    this.columnFiltredSubscription?.unsubscribe();
  }

}
