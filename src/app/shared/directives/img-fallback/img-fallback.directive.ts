import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appImgFallback]'
})
export class ImgFallbackDirective {

  @Input()
  appImgFallback: string;

  photoImg: string = './assets/img/avatar.png';

  constructor(private eRef: ElementRef) { }

  @HostListener('error')
  loadFallbackOnError() {
    const element: HTMLImageElement = <HTMLImageElement>this.eRef.nativeElement;
    element.src = this.appImgFallback || this.photoImg;
  }

}
