import { Directive, EventEmitter, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { SortService } from '../../services/sort/sort.service';

@Directive({
  selector: '[appSortTable]'
})
export class SortTableDirective {

  constructor(private sortService: SortService) { }

  @Output()
  sorted = new EventEmitter();

  private columnSortedSubscription: Subscription = null;

  ngOnInit() {
    this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
      this.sorted.emit(event);
    });
  }

  ngOnDestroy() {
    this.columnSortedSubscription?.unsubscribe();
  }

}
