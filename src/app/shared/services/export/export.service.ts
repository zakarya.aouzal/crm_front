import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import jsPDF from 'jspdf';
import autoTable, { ColumnInput, RowInput } from 'jspdf-autotable';
import * as XLSX from 'xlsx';
import { DataTableSettingsModel } from '../../models';


@Injectable()
export class ExportService {

  constructor() { }

  exportAsExcelFile(json: any[], settings: DataTableSettingsModel, sheetName: string, excelFileName: string): void {
    let data: Object[] = this.generateDataToExcelExport(json, settings);

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data);

    const sheet: { [sheet: string]: XLSX.WorkSheet } = {};
    sheet[sheetName] = worksheet;

    const workbook: XLSX.WorkBook = { Sheets: sheet, SheetNames: [sheetName] };

    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const blob: Blob = new Blob([excelBuffer]);

    FileSaver.saveAs(blob, excelFileName + '_export_' + new Date().getTime() + '.xlsx');
  }

  private generateDataToExcelExport(data: any[], settings: DataTableSettingsModel): Object[] {
    let result: Object[] = [];

    if (data && settings && settings.columns) {

      for (const item of data) {

        let obj: Object = {};

        settings.columns.forEach(column => {
          if (item.hasOwnProperty(column.name)) {
            obj[column.title] = item[column.name];
          }
        });

        result.push(obj);
      }

    }

    return result;
  }

  exportAsPDFFile(json: any[], settings: DataTableSettingsModel, title: string, pdfFileName: string) {

    const doc = new jsPDF();

    doc.text(title, 10, 10);

    let body: RowInput[] = json;
    let columns: ColumnInput[] = [];

    // columns
    settings.columns.forEach(column => {
      columns.push({ header: column.title, dataKey: column.name });
    });

    autoTable(
      doc,
      {
        body: body,
        columns: columns,
        headStyles: { fillColor: '#00a4e4' }
      }
    );

    doc.save(pdfFileName + '_export_' + new Date().getTime() + '.pdf');
  }

}
