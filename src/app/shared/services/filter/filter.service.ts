import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ColumnFilteredEventModel } from 'src/app/shared/models';

@Injectable()
export class FilterService {

  private columnFiltredSource = new Subject<ColumnFilteredEventModel>();
  columnFiltred$ = this.columnFiltredSource.asObservable();
  currentFilter: ColumnFilteredEventModel = null;

  constructor() { }

  columnFiltred(event: ColumnFilteredEventModel) {
    this.currentFilter = event;
    this.columnFiltredSource.next(event);
  }
}
