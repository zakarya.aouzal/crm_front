import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ColumnSortedEventModel } from '../../models/column-sorted-event.model';

@Injectable()
export class SortService {

  private columnSortedSource = new Subject<ColumnSortedEventModel>();
  columnSorted$ = this.columnSortedSource.asObservable();

  constructor() { }

  columnSorted(event: ColumnSortedEventModel) {
    this.columnSortedSource.next(event);
  }

}
