import { CommonModule, CurrencyPipe, DatePipe, DecimalPipe, PercentPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import {TranslateModule, TranslatePipe} from '@ngx-translate/core';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxSelectModule } from 'ngx-select-ex';
import { environment } from './../../environments/environment';
import { CardToolbarComponent } from './components/card-toolbar/card-toolbar.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DataTableComponent } from './components/table/data-table/data-table.component';
import { FilterColumnComponent } from './components/table/filter-column/filter-column.component';
import { FilterComponent } from './components/table/filter/filter.component';
import { PaginationComponent } from './components/table/pagination/pagination.component';
import { SearchComponent } from './components/table/search/search.component';
import { SortableColumnComponent } from './components/table/sortable-column/sortable-column.component';
import { FilterTableDirective } from './directives/filter-table/filter-table.directive';
import { FocusInvalidInputDirective } from './directives/focus-invalid-input/focus-invalid-input.directive';
import { NumberColorDirective } from './directives/number-color/number-color.directive';
import { SortTableDirective } from './directives/sort-table/sort-table.directive';
import { FilterPipe } from './pipes/filter/filter.pipe';
import { SortPipe } from './pipes/sort/sort.pipe';
import { TruncatePipe } from './pipes/truncate/truncate.pipe';
import { FilterService, SortService } from './services';
import { ExportService } from './services/export/export.service';
import { AgoPipe } from './pipes/ago/ago.pipe';
import { ImgFallbackDirective } from './directives/img-fallback/img-fallback.directive';
import { WatchlistProcessComponent } from './components/watchlist-process/watchlist-process.component';
import {NotAuthorizedComponent} from './components/not-authorized/not-authorized.component'
import { NgxPermissionsModule } from 'ngx-permissions';
@NgModule({
  declarations: [
    PageNotFoundComponent,
    NotAuthorizedComponent,
    CardToolbarComponent,
    DataTableComponent,
    SearchComponent,
    SortableColumnComponent,
    FilterColumnComponent,
    WatchlistProcessComponent,
    FilterPipe,
    SortPipe,
    TruncatePipe,
    FilterTableDirective,
    FocusInvalidInputDirective,
    NumberColorDirective,
    SortTableDirective,
    PaginationComponent,
    FilterComponent,
    AgoPipe,
    ImgFallbackDirective,
    //Graphs
  ],
  imports: [
    CommonModule,
    RouterModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatDialogModule,
    FormsModule,
    PaginationModule.forRoot(),
    NgxSelectModule,
    TranslateModule,
    NgxPermissionsModule
    
  ],
    exports: [
        CardToolbarComponent,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        DataTableComponent,
        NgxSelectModule,
        FocusInvalidInputDirective,
        TranslateModule,
        AgoPipe,
        TranslatePipe,
        ImgFallbackDirective,WatchlistProcessComponent,
        NgxPermissionsModule
    ],
  providers: [
    { provide: OWL_DATE_TIME_LOCALE, useValue: environment.defaultLanguage },
    FilterService,
    SortService,
    DatePipe,
    DecimalPipe,
    CurrencyPipe,
    PercentPipe,
    ExportService
  ]
})
export class SharedModule { }
