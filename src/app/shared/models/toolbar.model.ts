export enum ToolbarOption {
    DownloadAsExcel = 'DownloadAsExcel',
    DownloadAsPDF = 'DownloadAsPDF',
    Refresh = 'Refresh'
}