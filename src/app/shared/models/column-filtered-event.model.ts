import { DataTableColumnsFormat } from '../models';

export class ColumnFilteredEventModel {
  columns: ColumnFilteredColumnsModel[];
  strict: boolean;

  constructor() {
    this.columns = [];
  }
}

export class ColumnFilteredColumnsModel {
  columnName: string;
  value: any;
  format?: DataTableColumnsFormat;

  constructor() { }
}
