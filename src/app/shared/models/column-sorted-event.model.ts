export interface ColumnSortedEventModel {
  direction: ColumnSortedDirectionType;
  property: string;
}

export enum ColumnSortedDirectionType {
  ASC = 'ASC',
  DESC = 'DESC'
}