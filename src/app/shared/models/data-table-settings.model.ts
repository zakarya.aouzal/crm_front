export class DataTableSettingsModel {
    columns: DataTableColumnsModel[];
    actions?: DataTableActions[];
    clickable?: boolean;
    selectable?: boolean;

    constructor() { }
}

export class DataTableColumnsModel {
    name: string;
    title: string;
    format?: DataTableColumnsFormat;
    link?: string;
    linkParams?: { [propName: string]: any };
    hidden?: boolean;
    data?: any;
    enum?: Array<{value: string,text: string}>;
    style?: {
        colors?: { colorValue: string; conditionValue: any; }[]
        orientation?: string,
        textColor?: string,
        backgroundColor?: string
    };

    constructor() { }
}

export enum DataTableActions {
    VIEW = 'VIEW',
    EDIT = 'EDIT',
    DOWNLOAD = 'DOWNLOAD',
    ROUTER = 'ROUTER',
    CANCEL = 'CANCEL',
    DELETE = 'DELETE',
    EXPORT = 'EXPORT'
}

export enum DataTableColumnsFormat {
    CURRENCY = 'CURRENCY',
    NUMBER = 'NUMBER',
    DATE = 'DATE',
    STRING = 'STRING',
    PERCENT = 'PERCENT',
    ENUM = 'ENUM',
    PROGRESS_BAR = 'PROGRESS_BAR'
}

export class DataTableOutputActionsModel<T> {
    item: T;
    actionType: DataTableActions;

    constructor() { }
}
