import { ColumnSortedDirectionType } from '../models';
import numeral from 'numeral';

export class CommonUtil {

    static getRandomNumberBetween(min: number, max: number, isDecimal: boolean = false): number {
        const randomNumber = Math.random() * (max - min + 1) + min;
        if (isDecimal) {
            return randomNumber;
        } else {
            return Math.floor(randomNumber);
        }
    }

    static sortData(items: any[], direction: ColumnSortedDirectionType, property: string): any[] {
        const isAsc = direction === ColumnSortedDirectionType.ASC;
        return items.sort((a, b) => this.compare(a[property], b[property], isAsc));
    }

    static compare(a: any, b: any, isAsc: boolean): number {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    static removeSpaceFromStr(str: string): string {
        return str.replace(/\s/g, '');
    }


    static format(value: number, options: any = {}): string {
        return new Intl.NumberFormat('ma-MA', {
            style: options.style || 'decimal',
            currency: options.currency || 'MAD',
            minimumFractionDigits: !isNaN(options.minimumFractionDigits) ? options.minimumFractionDigits : 2,
            maximumFractionDigits: !!options.maximumFractionDigits ? options.maximumFractionDigits : 2,
        }).format(value);
    }


    static getChartFormedDataDevise(data: any, kname: string) {
        const array: Array<any> = [];
        Object.keys(data).forEach(key => {
            array.push({
                value: data[key][kname],
                name: key,
                devise: data[key]['devise'],
            });
        });
        return array;
    }

    static _n(number): string {
        return !!number ? numeral(number).format('0.0a') : '0';
    }

    static getMinMax(data: Array<any>): any {
        return {
            min: Math.min.apply(Math, data.map(o => o.value)),
            max: Math.max.apply(Math, data.map(o => o.value)),
        };
    }



    static getDate(dateStr?: string) : Date{
    if(!!dateStr) {
      dateStr = dateStr.toString();
      const dateParts = dateStr.split('/');
      return new Date(+dateParts[2],+dateParts[1],+dateParts[0]);
    }
    return new Date();
  }





}
