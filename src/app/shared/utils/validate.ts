import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

/** 
 * Check if START_DATE after END_DATE 
 */
export function endDateLessThanStartDateValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const startDateControl = control.get('startDate');
        const endDateControl = control.get('endDate');
        if (startDateControl && endDateControl && startDateControl.value && endDateControl.value) {
            let startDateValue: Date = new Date(startDateControl.value);
            let endDateValue: Date = new Date(endDateControl.value);
            if (endDateValue.getTime() < startDateValue.getTime()) {
                return { endDateLessThanStartDate: true }
            }
        }
        return null;
    }
}