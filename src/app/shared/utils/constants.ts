export class Constants {

     static readonly PREFERRED_PAGES_URL: Map<string, string> = new Map([
        ['Alertes', '/pages/alert/settings'],
        ['EnvirenementOPCVM', '/pages/portefeuille/opcvm'],
        ['FicheValeurBourse', '/pages/marche/fiche-valeur'],
        ['FicheValeurOPCVM', '/pages/marche/fiche-opcvm'],
        ['HistoriqueOperations', '/pages/portefeuille/historique/operations'],
        ['HistoriqueOrdres', '/pages/portefeuille/historique/ordres'],
        ['HistoriquePosition', '/pages/portefeuille/positions'],
        ['Indices', '/pages/marche/indices'],
        ['Lexique', '/pages/services/lexiques'],
        ['Marche', '/pages/marche/resume'],
        ['MarchesDroits', '/pages/marche/resume'],
        ['MarchesEnDirect', '/pages/marche-direct/instrument/valeurs'],
        ['PasserOrdreBourse', '/pages/passer-ordre'],
        ['PortefeuilleValorise', '/pages/portefeuille/valorise'],
        ['Preferences', '/pages/services/preference'],
        ['Publications', '/pages/services/publications']
    ]);

}