declare namespace jasmine {
    interface Matchers<T> {
        toBeIn(expected: any, expectationFailOutput?: any): boolean;
    }
}