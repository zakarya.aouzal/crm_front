import MatchersUtil = jasmine.MatchersUtil;
import CustomMatcherFactories = jasmine.CustomMatcherFactories;
import CustomEqualityTester = jasmine.CustomEqualityTester;
import CustomMatcher = jasmine.CustomMatcher;
import CustomMatcherResult = jasmine.CustomMatcherResult;

export const CustomMatchers: CustomMatcherFactories = {
    toBeIn: function (util: MatchersUtil, customEqualityTester: CustomEqualityTester[]): CustomMatcher {
        return {
            compare: function (actual: any, expected: any): CustomMatcherResult {
                return {
                    pass: expected.some(function (item) { return item === actual; }),
                    message: actual + ' is not in ' + expected
                }
            }
        }
    }
};