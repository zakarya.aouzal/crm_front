import {Component, Inject, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {
  AccountPMModel,
  AccountPPModel,
  AccountType,
  StatusWatchlistPM,
  StatusWatchlistPP
} from "src/app/main/models";
import {DataTableSettingsModel, ToolbarOption} from "src/app/shared/models";
import {WATCHLIST_PM_LIST_SETTINGS, WATCHLIST_PP_LIST_SETTINGS} from "./watchlist-list.settings";
import {AccountService} from "src/app/main/services";
import {ExportService} from "src/app/shared/services";
import { ACCOUNT_PP_LIST_SETTINGS } from 'src/app/main/pages/clients-management/account/account-list/account-list.settings';

@Component({
  selector: 'app-watchlist-process',
  templateUrl: './watchlist-process.component.html',
  styleUrls: ['./watchlist-process.component.scss']
})
export class WatchlistProcessComponent implements OnInit,OnChanges {

  constructor(private fb: FormBuilder,
              private toastr: ToastrService,
              private accountService: AccountService,
              private exportService: ExportService,
             ) {
  }
  ngOnChanges(changes: SimpleChanges): void {
    // if(changes){
    //   this.
    // }
  }

  @Input()
  set _data(value:{ accountPP: AccountPPModel, accountPM: AccountPMModel,accountType: AccountType, watchlistStatusList: StatusWatchlistPP[] | StatusWatchlistPM[] }){
    this.data=value;

    switch (this.data?.accountType) {
      case AccountType.PP:
        this.settings = WATCHLIST_PP_LIST_SETTINGS;
        break;
      case AccountType.PM:
        this.settings = WATCHLIST_PM_LIST_SETTINGS;
        break;
    }
  }

  data:{ accountPP: AccountPPModel, accountPM: AccountPMModel,accountType: AccountType, watchlistStatusList: StatusWatchlistPP[] | StatusWatchlistPM[] };
  
  settings: DataTableSettingsModel = null;

  loading: boolean = false;

  ngOnInit(): void {

    
  }

  /** RETURN IF PROFILE IS WATCHLIST RISK **/
  isRisque(): boolean { // if length=0 then NR, if length>0 then R
    return this.data?.watchlistStatusList?.length>0;
  }

  /** UPDATE PROFIL RISK RESULT **/
  onSubmit(): void {
    this.loading = true;

    switch (this.data?.accountType) {
      case AccountType.PP:
        const cpp: any = {
          ...this.data?.accountPP,
          profilRisque: this.isRisque()?'R':'NR'
        }
        this.accountService.updateAccountPP(cpp)
          // .pipe(finalize(() => this.closePopup(true)))
          .subscribe(value => this.toastr.success('Profil mis à jour'),
            error => this.toastr.error('Erreur pendant la màj du profil'));
        break;
      case AccountType.PM:
        const cpm: any = {
          ...this.data?.accountPM,
          profilRisque: this.isRisque()?'R':'NR'
        }
        this.data.accountPM.profilRisque = this.isRisque()?'R':'NR';
        this.accountService.updateAccountPM(cpm)
          // .pipe(finalize(() => this.closePopup(true)))
          .subscribe(value => this.toastr.success('Profil mis à jour'),
            error => this.toastr.error('Erreur pendant la màj du profil'));
        break;
    }
  }

  // closePopup(reload: boolean): void {
  //   this.dialogRef.close(reload);
  // }

  /** ON ACTION TOOLBAR */
  onActionToolbar(event: ToolbarOption) {
    switch (event) {
      case ToolbarOption.DownloadAsExcel:
        this.exportService.exportAsExcelFile(this.data?.watchlistStatusList, ACCOUNT_PP_LIST_SETTINGS, 'Liste des compte PP', 'comptes-pp');
        break;
      case ToolbarOption.DownloadAsPDF:
        this.exportService.exportAsPDFFile(this.data?.watchlistStatusList, ACCOUNT_PP_LIST_SETTINGS, 'Liste des compte PP', 'comptes-pp');
        break;
    }
  }
}
