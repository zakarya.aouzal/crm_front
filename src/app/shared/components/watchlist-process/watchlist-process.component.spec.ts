import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WatchlistProcessComponent } from './watchlist-process.component';

describe('WatchlistProcessComponent', () => {
  let component: WatchlistProcessComponent;
  let fixture: ComponentFixture<WatchlistProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatchlistProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WatchlistProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
