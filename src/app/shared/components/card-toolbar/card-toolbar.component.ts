import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ToolbarOption } from '../../models';

@Component({
  selector: 'app-card-toolbar',
  templateUrl: './card-toolbar.component.html',
  styleUrls: ['./card-toolbar.component.scss']
})
export class CardToolbarComponent implements OnInit {

  @Input()
  collapseTarget: string = null;

  @Input()
  options: ToolbarOption[] = [];

  @Output()
  action: EventEmitter<ToolbarOption> = new EventEmitter<ToolbarOption>();

  toolbarOption = ToolbarOption;

  constructor() { }

  ngOnInit(): void { }

  onAction(option: ToolbarOption) {
    this.action.emit(option);
  }

}
