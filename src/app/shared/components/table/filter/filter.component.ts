import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  @Input()
  selectedItem: number = null;

  @Output()
  filterChanged: EventEmitter<any> = new EventEmitter();

  filters: any[] = [];

  constructor() { }

  ngOnInit(): void {
    this.filters = [
      { value: 5, label: '5' },
      { value: 10, label: '10' },
      { value: 25, label: '25' },
      { value: 50, label: '50' },
      { value: 100, label: '100' }
    ];
  }

  onFilterChanged() {
    this.filterChanged.emit(this.selectedItem);
  }

}
