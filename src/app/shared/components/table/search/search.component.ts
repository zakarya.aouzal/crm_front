import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, OnDestroy } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  @Output()
  searched: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('input')
  inputElementRef: ElementRef;

  keyword: any = null;

  inputSubscription: Subscription = null;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.inputSubscription = fromEvent(this.inputElementRef.nativeElement, 'keyup')
      .pipe(debounceTime(500))
      .subscribe(() => {
        this.searched.emit(this.keyword);
      });
  }

  onSearch() {
    this.searched.emit(this.keyword);
  }

  ngOnDestroy() {
    this.inputSubscription?.unsubscribe();
  }

}
