import { ColumnFilteredEventModel } from 'src/app/shared/models';
import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { FilterService } from 'src/app/shared/services/filter/filter.service';

@Component({
  selector: 'app-filter-column',
  templateUrl: './filter-column.component.html',
  styleUrls: ['./filter-column.component.scss']
})
export class FilterColumnComponent implements OnInit, OnDestroy, AfterViewInit {

  @Input()
  columnName: string = null;

  @ViewChild("input")
  inputElementRef: ElementRef;

  keyword: any = null;

  filter: ColumnFilteredEventModel = new ColumnFilteredEventModel();

  inputSubscription: Subscription = null;

  constructor(private filterService: FilterService) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.inputSubscription = fromEvent(this.inputElementRef.nativeElement, 'keyup')
      .pipe(debounceTime(500))
      .subscribe(() => {

        if (this.filterService.currentFilter) {
          this.filter = this.filterService.currentFilter;
        }

        const filterFound = this.filter.columns.find(item => item.columnName === this.columnName);

        if (filterFound) {
          filterFound.value = this.keyword;
        } else {
          this.filter.columns.push({ columnName: this.columnName, value: this.keyword })
        }

        this.filterService.columnFiltred(this.filter);
      });
  }

  ngOnDestroy(): void {
    this.inputSubscription?.unsubscribe();
  }
}
