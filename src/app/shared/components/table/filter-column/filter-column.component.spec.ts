import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FilterService } from './../../../services/filter/filter.service';
import { FilterColumnComponent } from './filter-column.component';


describe('FilterColumnComponent', () => {
  let component: FilterColumnComponent;
  let fixture: ComponentFixture<FilterColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FilterColumnComponent],
      providers: [FilterService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
