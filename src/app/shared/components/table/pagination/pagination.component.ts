import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input()
  totalItems: number = null;

  @Input()
  numberOfItems: number = null;

  @Input()
  totalPages: number = null;

  @Input()
  maxSize: number = null;

  @Input()
  disabled: boolean = null;

  @Input()
  align: boolean = null;

  @Input()
  firstText: string = null;

  @Input()
  lastText: string = null;

  @Input()
  itemsPerPage: number = null;

  @Input()
  boundaryLinks: boolean = null;

  @Output()
  pageChanged: EventEmitter<number> = new EventEmitter();

  currentPage: number = null;
  
  constructor() { }

  ngOnInit(): void { }

  onPageChanged(event: any): void {
    this.currentPage = event.page;
    this.pageChanged.emit(event.page);
  }

}
