import { Component, HostListener, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ColumnSortedDirectionType } from 'src/app/shared/models';
import { SortService } from 'src/app/shared/services/sort/sort.service';

@Component({
  selector: '[app-sortable-column]',
  templateUrl: './sortable-column.component.html',
  styleUrls: ['./sortable-column.component.scss']
})
export class SortableColumnComponent implements OnInit, OnDestroy {

  @Input('app-sortable-column')
  columnName: string = null;

  @Input('sort-direction')
  direction: ColumnSortedDirectionType = null;

  private columnSortedSubscription: Subscription = null;

  @HostListener('click')
  sort() {
    this.direction = this.direction === ColumnSortedDirectionType.ASC ? ColumnSortedDirectionType.DESC : ColumnSortedDirectionType.ASC;
    this.sortService.columnSorted({ property: this.columnName, direction: this.direction });
  }

  constructor(private sortService: SortService) { }

  ngOnInit() {
    // subscribe to sort changes so we can react when other columns are sorted
    this.columnSortedSubscription = this.sortService.columnSorted$.subscribe(event => {
      // reset this column's sort direction to hide the sort icons
      if (this.columnName !== event.property) { // the second incdicator
        this.direction = null;
      }
    });
  }

  ngOnDestroy() {
    this.columnSortedSubscription?.unsubscribe();
  }

}
