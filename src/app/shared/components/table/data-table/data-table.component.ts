import { ChangeDetectorRef, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ColumnFilteredEventModel, ColumnSortedEventModel, DataTableActions, DataTableColumnsModel, DataTableOutputActionsModel, DataTableSettingsModel } from 'src/app/shared/models';
import { FilterService, SortService } from 'src/app/shared/services';
import { DataTableDialogComponent } from './data-table-dialog/data-table-dialog.component';
import {FilterPipe} from "../../../pipes/filter/filter.pipe";
import { CommonUtil } from 'src/app/shared/utils/common.util';
import { GestionUtilisateursComponent } from 'src/app/main/pages/users-management/gestion-utilisateurs/gestion-utilisateurs.component';
import { UsersWebModel } from 'src/app/main/models';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  providers: [SortService, FilterService, FilterPipe]
})
export class DataTableComponent implements OnInit, OnDestroy {

  @Input()
  set settings(value: DataTableSettingsModel) {
    this._settings = value;
    this.displayedColumns = [];
    if (this._settings && this._settings.columns) {
      this.displayedColumns = this.MenuTranslate(this._settings.columns.filter(column => column.hidden !== true ));
    }
  }

  @Input()
  set items(data: any[]) {
   // let data:any=this.formatData(value)
    this.filteredColumns = null;
    this.sortedColumn = null;
    this._originalItems = data;
    this._items = data;
    this.paginate();
   

  }
  

  

  @Input()
  enableLoading: boolean = false;

  @Input()
  enableSearch: boolean = true;

  @Input()
  isRecap:boolean=false;

  @Input()
  enableActions:boolean=true;

  @Input()
  enableFilterColumn: boolean = true;

  @Output()
  itemSelected = new EventEmitter<any>();

  @Output()
  itemActionVisible=new EventEmitter<any>();

  @Output()
  itemChecked = new EventEmitter<{ item: any, isChecked: boolean }>();

  @Output()
  action = new EventEmitter<DataTableOutputActionsModel<any>>();

  /** ITEMS */
  _items: any[] = null;
  _originalItems: any[] = null;
  paginatedItems: any[] = null;
  Visible: any[] = null;

  /** SETTINGS */
  _settings: DataTableSettingsModel = null;

  /** SORTED COLUMN  */
  sortedColumn: ColumnSortedEventModel = null;

  /** FILTERED COLUMN */
  filteredColumns: ColumnFilteredEventModel = null;

  /** SUBSCRIPTION */
  matDialogCloseSubscription: Subscription = null;

  /** DISPLAYED COLUMNS */
  displayedColumns: DataTableColumnsModel[] = [];

   visible: UsersWebModel []= [];

  /*** PAGINATION */
  @Input()
  itemsPerPage: number = 10;
  currentPage: number = 1;

  constructor(public dialog: MatDialog,
              @Inject('pipeParameters') public pipeParameters: any,
              private cdRef: ChangeDetectorRef,
              private translateService: TranslateService,
              private filterPipe: FilterPipe) { }

  ngOnInit(): void {

  }

  ngAfterContentChecked() {
    this.cdRef.detectChanges();
  }

  ngOnDestroy(): void {
    if (this.matDialogCloseSubscription) {
      this.matDialogCloseSubscription?.unsubscribe();
    }
  }


  /** TRANSLATE COLUMNS */
  MenuTranslate(setting: DataTableColumnsModel[]): DataTableColumnsModel[] {
    setting.forEach(item => {
      item.title = item.data ? this.translateService.instant(item.data.i18nValue) : item.title;  
    })
    return setting;
  }


  //format data

  formatData(data:any[]):any[]{

    this._settings.columns.forEach(item => {
      data[item.name] = item.data.format ?  CommonUtil.format(data[item.name],{minimumFractionDigits:item.data.format.minimumFractionDigits,maximumFractionDigits:item.data.format.maximumFractionDigits}) : data[item.name];   
    })
    return data;

  }

  /** ON SORTED */
  onSort(event: ColumnSortedEventModel): void {
    this.sortedColumn = event;
  }

  /** ON FILTER */
  onFilter(event: ColumnFilteredEventModel): void {
    const filter: ColumnFilteredEventModel = Object.assign({}, event);
    const hasFilter: boolean = filter.columns.filter(col => col.value !== '')?.length > 0;
    this.currentPage = 1;
    this._items = this._originalItems;
    if (hasFilter) {
      filter.strict = true;
      filter.columns.forEach(column => {
        const settingFound = this.displayedColumns.find(item => item.name === column.columnName);
        if (settingFound) {
          column.format = settingFound.format;
        }
      });
      this._items = this.filterPipe.transform(this._items, filter);
    }
    this.paginate();
  }

  /** ON SEARCH */
  onSearch(event: any): void {
    const filter: ColumnFilteredEventModel = new ColumnFilteredEventModel();
    filter.strict = false;
    for (const column of this.displayedColumns) {
      filter.columns.push({
        columnName: column.name,
        value: event,
        format: column.format
      });
    }
    this.filteredColumns = filter;
  }

  /** ON SELECT ROW */
  onSelectItem(item: any): void {
    this.itemSelected.emit(item);
  }

  /** ON FILTER CHANGE */
  onFilterChanged(event: number) {
    this.itemsPerPage = event;
    this.paginate();
  }

  /** PAGINATE */
  paginate() {
    if (this._items && this.currentPage && this.itemsPerPage) {
      let start: number = (this.currentPage - 1) * this.itemsPerPage;
      let end: number = this.currentPage * this.itemsPerPage;
      this.paginatedItems = this._items.slice(start, end);
    }
  }

  /** ON CLICK ACTION */
  onAction(item: any, actionType: DataTableActions) {
    const outputEventModel: DataTableOutputActionsModel<any> = {
      ...new DataTableOutputActionsModel(),
      item: item,
      actionType: actionType
    }
    this.action.emit(outputEventModel);
  }

  /** OPEN DIALOG */
  openDialog(item: any, actionType: DataTableActions): void {
    const selectedItem = item;
    const selectedActionType = actionType;
    const dialogRef = this.dialog.open(DataTableDialogComponent, {
      data: { item: item, actionType: actionType, settings: this._settings },
      width: '600px'
    });
    this.matDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        /** ON CONFIRM ACTION */
        this.onAction(selectedItem, selectedActionType);
      }
    });
  }

  /** MAP LINK PARAMS WITH ITEM */
  mapLinkParams(item: any, linkParams: { [propName: string]: any; }) {
    if (!item || !linkParams) {
      return null;
    }
    let queryParams = {};
    for (let linkParamName in linkParams) {
      let linkParamValue = linkParams[linkParamName];
      queryParams[linkParamName] = item[linkParamValue];
    }
    return queryParams;
  }

  /** OUTPUT ITEM SELECTION OR UNSELECTION */
  onCheckedItem(item: any, isChecked: boolean): void {
    this.itemChecked.emit({ item: item, isChecked: isChecked });
  }

  /** GET COLUMN STYLE */
  getColumnStyle(column: DataTableColumnsModel, item: any): Object {
    let style: Object = {};
    if (item && column?.style?.colors) {
      const selectedColor = column.style.colors.find(x => x.conditionValue === item[column.name]);
      if (selectedColor) {
        style['color'] = selectedColor.colorValue;
      }
    }
    if (item && column?.style?.orientation) {
      const selectedStyle = column.style.orientation;
      if (selectedStyle) {
        style['text-align'] = selectedStyle;
      }
    }
    if (item && column?.style?.backgroundColor) {
      const selectedStyle = column.style.backgroundColor;
      if (selectedStyle) {
        style['background-color'] = selectedStyle;
      }
    }
    if (item && column?.style?.textColor) {
      const selectedStyle = column.style.textColor;
      if (selectedStyle) {
        style['color'] = selectedStyle;
      }
    }
    return style;
  }

  extractEnumValue(value: string, enumList: Array<{value: string,text: string}>): string {
    if (value) {
      return enumList.find((en) => value===en.value).text;
    }
    return '';
  }

 
   
   onVisible( item : any){
     console.log('item1', item);
      item.actionVisible=!item.actionVisible;
      console.log(item.actionVisible)
      
      console.log('item2', item);
     
   }
   
}
