import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataTableActions } from 'src/app/shared/models/data-table-settings.model';
import { DataTableSettingsModel } from './../../../../models';

@Component({
  selector: 'app-data-table-dialog',
  templateUrl: './data-table-dialog.component.html',
  styleUrls: ['./data-table-dialog.component.scss']
})
export class DataTableDialogComponent {

  item: any = null;
  actionType: DataTableActions = null;
  settings: DataTableSettingsModel = null;

  constructor(public dialogRef: MatDialogRef<DataTableDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { item: any, actionType: DataTableActions, settings: DataTableSettingsModel },
    @Inject('pipeParameters') public pipeParameters: any) {

    if (data) {
      this.item = data.item;
      this.actionType = data.actionType;
      this.settings = data.settings;
    }
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
