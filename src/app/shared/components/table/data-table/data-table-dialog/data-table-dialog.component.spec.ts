import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from './../../../../../../environments/environment';
import { DataTableDialogComponent } from './data-table-dialog.component';


describe('DataTableDialogComponent', () => {

  let component: DataTableDialogComponent;
  let fixture: ComponentFixture<DataTableDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DataTableDialogComponent],
      imports: [MatDialogModule],
      providers: [
        { provide: 'pipeParameters', useValue: environment.pipeParameters },
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
