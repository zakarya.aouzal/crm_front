import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MENU_ITEMS, RouteInfoModel } from './../menu/menu-items';



@Component({
  selector: 'app-second-navbar',
  templateUrl: './second-navbar.component.html',
  styleUrls: ['./second-navbar.component.scss']
})
export class SecondNavbarComponent implements OnInit, OnDestroy {

  menuItem: RouteInfoModel = null;

  routerSubscription: Subscription = null;

  selectedMenuItem: RouteInfoModel = null;

  constructor(private route: ActivatedRoute, private router: Router) {

    this.routerSubscription = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        this.menuItem = null;
        const data = this.route.snapshot.firstChild.data;
        if (data) {
          this.menuItem = MENU_ITEMS.find(menuItem => menuItem.id === data.routeId);
        }
      })

  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    if (this.routerSubscription) {
      this.routerSubscription?.unsubscribe();
    }
  }

}
