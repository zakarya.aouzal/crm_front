import { RoleEnum } from "src/app/main/models";

export interface RouteInfoModel {
    id?: string;
    title: string;
    path: string;
    icon?: string;
    data?: any;
    hidden?:boolean;
    children?: RouteInfoModel[];
}

export const MENU_ITEMS: RouteInfoModel[] = [
    // {
    //     id: 'dashboard',
    //     title: 'Dashboard',
    //     path: '/pages/dashboard',
    //     icon: 'ri-home-4-line',
    //     data:{
    //         i18nValue:"MENU.DASHBOARD"
    //     }
    // },
    // GESTION ACTIVITES
    {
        id: 'activity-management',
        title: 'Activités',
        path: '/pages/activity-management/activity-consultation',
        icon: 'ri-lightbulb-flash-line',
        data:{
            i18nValue:"MENU.GESTION_ACTIVITE",
            permissions: [],
        }
    },
    // GESTION CARNET D'ORDRE
    {
      id: 'carnet-ordre-management',
      title: 'Carnet dordre',
      path: '/pages/carnet-ordre-management/carnet-ordre-management-consultation',
      //path: '/pages/carnet-ordre-management/carnet-ordre-management-consultation',
      icon: 'ri-funds-line',
      data:{
        i18nValue:"MENU.GESTION_CARNET_D'ORDRE",
        permissions: [],
      }
    },
    // GESTION PROSPECTS
    {
        id: 'prospect-management',
        title: 'Prospect',
        path: '/pages/prospects-management',
        icon: 'ri-user-location-line',
        data:{
            i18nValue:'MENU.GESTION_PROSPECT',
            permissions: [],
        },
        children: [
        {
                title: 'Consultation prospects',
                path: '/pages/prospects-management/prospect-consultation',
                data:{
                    i18nValue:'SUB_MENU.CONSULTATION_PROSPECT',
                    permissions: [],
                },
            },
            {
                title: 'Validation prospects',
                path: '/pages/prospects-management/prospect-validation',
                data:{
                    i18nValue:'SUB_MENU.VALIDATION_PROSPECT',
                    permissions: [RoleEnum.CONTROLEUR],
                },
            }
         ]
    },
    // GESTION CLIENTS
     {
        id: 'client-management',
        title: 'Clients',
        path: '/pages/clients-management',
        icon: 'ri-user-follow-line',
        data:{
            i18nValue:'MENU.GESTION_CLIENT',
            permissions: [],
        },
        children: [
            // {
            //     title: 'Nouveau compte',
            //     path: '/pages/accounts-management/new-account',
            //     data:{
            //         i18nValue:'SUB_MENU.NOUVEAU_COMPTE'
            //     },
            // },
             {
                title: 'Consultation client',
                path: '/pages/clients-management/client-consultation',
                data:{
                    i18nValue:'SUB_MENU.CONSULTATION_CLIENT',
                    permissions: [],
                },
            },
             {
                title: 'Vérification des clients',
                path: '/pages/clients-management/clients-verification',
                data: {
                  i18nValue:'SUB_MENU.CLIENT_VERIFICATION',
                  permissions: [],
                },
            },

        ]
    },
    // GESTION CONTACTS
    {
        id: 'contact-management',
        title: 'contacts',
        path: '/pages/contacts-management',
        icon: 'ri-contacts-line',
        data:{
            i18nValue:'MENU.GESTION_CONTACT',
            permissions: [],
        },
        children: [

            {
                title: 'Nouveau contact',
                path: '/pages/contacts-management/new-contact',
                data:{
                    i18nValue:'SUB_MENU.NOUVEAU_CONTACT',
                    permissions: [],
                },
            }, {
                title: 'Consultation contact',
                path: '/pages/contacts-management/contact-consultation',
                data:{
                    i18nValue:'SUB_MENU.CONSULTATION_CONTACT',
                    permissions: [],
                },
            }
        ]
    },
    // KWC
    // {
    //     id: 'crm',
    //     title: 'KYC',
    //     path: '/pages/crm',
    //     icon: 'ri-shield-user-line',
    //     data:{
    //         i18nValue:'MENU.KYC'
    //     },
    //     children: [
    //         {
    //             title: 'Contrôle & validation',
    //             path: '/pages/crm/control-validation',
    //             data:{
    //                 i18nValue:'SUB_MENU.CONTROL_VALIDATION'
    //             },
    //             children: [
    //                 {
    //                     title: 'Contrôle modification',
    //                     path: '/pages/crm/control-validation/modification',
    //                     data:{
    //                         i18nValue:'SUB_MENU.CONTROL_MODIFICATION'
    //                     },
    //                 }, {
    //                     title: 'Traitement whatch list',
    //                     path: '/pages/crm/control-validation/traitement',
    //                     data:{
    //                         i18nValue:'SUB_MENU.TRAITEMENT_WATCH_LIST'
    //                     },
    //                 }, {
    //                     title: 'Export contact',
    //                     path: '/pages/crm/control-validation/export',
    //                     data:{
    //                         i18nValue:'SUB_MENU.EXPORT_CONTACT'
    //                     },
    //                 }
    //             ]
    //         }, {
    //             title: 'Traitement lab',
    //             path: '/pages/crm/traitment-lab',
    //             data:{
    //                 i18nValue:'SUB_MENU.TRAITEMENT_LAB'
    //             },
    //             children: [
    //                 {
    //                     title: 'Consultation lab',
    //                     path: '/pages/crm/traitment-lab/consultation',
    //                     data:{
    //                         i18nValue:'SUB_MENU.CONSULT_LAB'
    //                     },
    //                 }, {
    //                     title: 'Rapport lab',
    //                     path: '/pages/crm/traitment-lab/report',
    //                     data:{
    //                         i18nValue:'SUB_MENU.RAPPORT_LAB'
    //                     },
    //                 }
    //             ]
    //         },

    //     ]
    // },

    // GESTION GED
    {
        title: 'GED',
        path: '/pages/documents-management/new-document',
        icon: 'ri-folder-settings-line',
        data:{
            i18nValue:'MENU.GED',
            permissions: [],
        }
    },
    // POSITIONS
    {
        title: 'Positions',
        path: '/pages/kyc/position-consultation',
        icon: 'ri-focus-2-line',
        data:{
            i18nValue:'MENU.POSITION',
            permissions: [],
        },
    },
    // OPERATION
    {
        title: 'Opérations',
        path: '/pages/kyc/operation-consultation',
        icon:'ri-exchange-line',
        data:{
            i18nValue:'MENU.OPERATION',
            permissions: [],
        },
    },

    // ASSET MANAGEMENT
    {
        title: 'Asset',
        path: '/pages/asset',
        icon:'ri-stock-fill',
        data:{
            i18nValue:'MENU.ASSET',
            permissions: [],
        },

        children: [
            {
                title: "Détail VL",
                path: "/pages/asset/opcvm",
                data: {
                    i18nValue: "SUB_MENU.OPCVM",
                    permissions: [],
                },
            }, {
                title: "Detail titres",
                path: "/pages/asset/details-fond",
                data: {
                    i18nValue: "SUB_MENU.DETAILS",
                    permissions: [],
                },
            }, {
                title: "Ratios AMMC",
                path: "/pages/asset/ammc",
                data: {
                    i18nValue: "SUB_MENU.AMMC",
                    permissions: [],
                },
            },
        ],
    },

    // users management
    {
        title: 'Users',
        path: '/pages/users',
        icon:'ri-settings-4-fill',
        data:{
            i18nValue:'MENU.USERS',
            permissions: [],
        },

        children: [
            {
                title: "Gestion des utilisateurs",
                path: "/pages/users/gestion-utilisateurs",
                data: {
                    i18nValue: "SUB_MENU.GESTION_UTILISATEURS",
                    permissions: [],
                },
            }, {
                title: "Gestion des permissions ",
                path: "/pages/users/gestion-permissions",
                data: {
                    i18nValue: "SUB_MENU.PERMISSIONS",
                    permissions: [],
                },
            },
            
        ],
    }


];
