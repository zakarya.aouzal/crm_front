import {Component, OnInit} from '@angular/core';

import {LayoutService} from '../../services/layout.service';
import {MENU_ITEMS} from '../menu-items';
import {Alerte} from "../../../main/models";
import { KeycloakService } from 'src/app/core/services/security/keycloak.service';


@Component({
  selector: 'app-horizontal-menu',
  templateUrl: './horizontal-menu.component.html',
  styleUrls: ['./horizontal-menu.component.scss']
})
export class HorizontalMenuComponent implements OnInit {

  menuItems = MENU_ITEMS;

  enableDarkMode: boolean = false;
  alertesList: Alerte[];

  constructor(private layoutService: LayoutService, private keycloakService: KeycloakService) { }


  ngOnInit(): void {
  }

  /** ON SWITCH MODE */
  onSwitchToDarkMode(value: boolean) {
    this.enableDarkMode = value;
    this.layoutService.switchToDarkMode(value);
  }


  logOut(){
    // this.keycloakService.keycloak.logout();

  }

}
