import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Alerte} from "../../../main/models";
import {MatDialog} from "@angular/material/dialog";
import {Subscription} from "rxjs";
import {ExpiredDocumentComponent} from "../../../main/pages/clients-management/account/expired-document/expired-document.component";
import {AccountService} from "../../../main/services";

@Component({
  selector: 'app-alertes-list',
  templateUrl: './alertes-list.component.html',
  styleUrls: ['./alertes-list.component.scss']
})
export class AlertesListComponent implements OnInit, OnDestroy {

  @Input()
  alertesList: Alerte[] = [];

  dialogCloseSubscription: Subscription = null;

  constructor(private matDialog: MatDialog,
              private accountService: AccountService) { }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.dialogCloseSubscription?.unsubscribe();
  }

  openAlert(type: string, alerte: Alerte) {
    switch (type) {
      // case 'ExpiredDocument': {
      //   this.dialogCloseSubscription = this.matDialog.open(ExpiredDocumentComponent, {
      //     panelClass: 'activity-popup',
      //     data: {alerte}
      //   }).afterClosed().subscribe(() => {});
      //   break;
      // }
      case 'Autre': {
        break;
      }
    }
  }
}
