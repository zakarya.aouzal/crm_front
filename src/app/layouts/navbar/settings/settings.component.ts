import { Component, OnInit } from '@angular/core';
import {KeycloakService} from "../../../core/services/security/keycloak.service";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(private keycloakService: KeycloakService) { }

  ngOnInit(): void {
  }

  /** SIGN OUT */
  signOut() {
    // this.keycloakService.keycloak.logout();
  }
}
