import {Component, OnInit} from '@angular/core';
import {KeycloakService} from 'src/app/core/services/security/keycloak.service';
import {LayoutService} from '../services/layout.service';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  enableDarkMode: boolean = false;
  currentLang: string;

  constructor(private layoutService: LayoutService,
              private keycloakService: KeycloakService,
              private translate: TranslateService) {
  }

  ngOnInit(): void {
    this.currentLang = this.getLanguage();
    this.translate.use(this.currentLang);
  }

  /** ON SWITCH MODE */
  onSwitchToDarkMode(value: boolean) {
    this.enableDarkMode = value;
    this.layoutService.switchToDarkMode(value);
  }

  private getLanguage(): string {
    if (localStorage.getItem('langue')===null) {
      localStorage.setItem('langue', this.translate.getBrowserLang());
    }
    return localStorage.getItem('langue');
  }
  switchLang(): void {
    this.currentLang = (this.currentLang==='fr')?'en':'fr';
    localStorage.setItem('langue', this.currentLang);
    location.reload();
  }
}
