import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';

@Injectable()
export class LayoutService {

  private renderer: Renderer2;

  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
   }

  switchToDarkMode(enableDarkMode: boolean) {
    const element = document.querySelector('html');
    if (enableDarkMode) {
      this.renderer.addClass(element, 'dark-layout');
    } else {
      this.renderer.removeClass(element, 'dark-layout');
    }
  }
}
