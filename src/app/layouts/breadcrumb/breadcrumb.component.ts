import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { ActivationEnd, Event, NavigationEnd, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { buffer, filter, map, pluck } from "rxjs/operators";

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

  @Input()
  breadcrumbType: 'second-navbar' | 'navbar' = 'second-navbar';

  // List
  bcLoadedData: any[] = [];
  bcForDisplay: any[] = [];

  // Subscription
  routerSubscription: Subscription = null;

  constructor(private router: Router) {
    const navigationEnd$ = this.router.events.pipe(filter((ev: Event) => ev instanceof NavigationEnd));

    /**
    * Here we subscribe to all navigation events, in order to update
    * our route "data", which we will use for the breadcrumb visualization.
    *
    * Than we filter the events emitted from the `router` in such way, that we are only
    * taking action upon a completed router event (in other words we are subscribing only for `ActivationEnd`
    * events)
    *
    * We use pluck to get only the required breadcrumb data
    *
    * The buffer operator accumulates all `data` coming from the router and emits the accumulated data once
    * when a `NavigationEnd` event passes trough `navigationEnd$`
    *
    * The `map` in the end is used to reverse the collected data, in order to convert it to more logical
    * sequence. Without the revers first we will get the data for the current route, after that for it's parent
    * and so on (that is how the ng router works).
    */
    this.routerSubscription = this.router.events
      .pipe(
        filter((ev: Event) => ev instanceof ActivationEnd),
        pluck("snapshot"),
        pluck("data"),
        buffer(navigationEnd$),
        map((bcData: any[]) => bcData.reverse())
      )
      .subscribe(data => {
        this.bcLoadedData = data;
        this.bcForDisplay = this.bcLoadedData
          .filter(root => root.breadcrumb !== undefined)
          .reduce((rootAcc, rootElement) => {
            return [...rootAcc, rootElement.breadcrumb];
          }, []);
      });
  }

  ngOnInit(): void { }

  ngOnDestroy(): void {
    if (this.routerSubscription) {
      this.routerSubscription?.unsubscribe();
    }
  }

}
