import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { MENU_ITEMS, RouteInfoModel } from '../menu/menu-items';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menuItems = MENU_ITEMS;

  constructor(public router: Router,private translateService:TranslateService) { }

  ngOnInit(): void {
   this. MenuTranslate(this.menuItems);
  }

  MenuTranslate(menu:RouteInfoModel[]): RouteInfoModel[]{
    menu.forEach(item => {
      item.title = this.translateService.instant(item.data.i18nValue);
      item.children?this.MenuTranslate(item.children):null;
    })
    return menu;
  }

}
