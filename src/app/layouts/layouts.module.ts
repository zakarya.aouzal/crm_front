import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { FooterComponent } from './footer/footer.component';
import { HorizontalMenuComponent } from './menu/horizontal-menu/horizontal-menu.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SecondNavbarComponent } from './second-navbar/second-navbar.component';
import { LayoutService } from './services/layout.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SettingsComponent } from './navbar/settings/settings.component';
import {SharedModule} from "../shared/shared.module";
import { AlertesListComponent } from './navbar/alertes-list/alertes-list.component';
import {AccountService} from "../main/services";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    FooterComponent,
    HorizontalMenuComponent,
    SecondNavbarComponent,
    BreadcrumbComponent,
    SidebarComponent,
    NavbarComponent,
    SettingsComponent,
    AlertesListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    FooterComponent,
    HorizontalMenuComponent,
    SecondNavbarComponent,
    BreadcrumbComponent,
    SidebarComponent,
    NavbarComponent
  ],
  providers: [
    LayoutService,
    AccountService
  ]
})
export class LayoutsModule { }
