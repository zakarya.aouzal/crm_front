import {HttpClient, HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from 'src/environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CoreModule} from 'src/app/core/core.module'
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import localeDe from '@angular/common/locales/fr';
import {registerLocaleData} from '@angular/common';
import { NgxPermissionsModule } from 'ngx-permissions';
import { HabilitationService } from './main/services';
import { NgxEchartsModule } from 'ngx-echarts';


registerLocaleData(localeDe, 'fr-FR');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-right',
      progressBar: true,
      progressAnimation: 'increasing',
      preventDuplicates: true
    }),
    CoreModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    }),

    NgxPermissionsModule.forRoot(),
    
   
  ],
  providers: [
    ToastrService,
    
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    {provide: 'pipeParameters', useValue: environment.pipeParameters},
    {provide: 'photoControl', useValue: environment.photoControl}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}



