import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { UrlSerializer } from '@angular/router';
import { GlobalErrorHandler } from './error/global-error-handler';
import { KeycloakAuthGuard } from './guard/keycloak-auth.guard';
import { KeycloakInterceptor } from './interceptor/keycloak.interceptor';
import { KeycloakService } from './services/security/keycloak.service';
import { LowerCaseUrlSerializer } from './services/serializer/lower-case-url-serializer';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    KeycloakService,
    /*KeycloakAuthGuard,
    {
      provide: APP_INITIALIZER,
      useFactory: keycloakFactory,
      multi: true,
      deps: [KeycloakService],
    }
    , {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakInterceptor,
      multi: true
    },*/
     {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    }, {
      provide: UrlSerializer,
      useClass: LowerCaseUrlSerializer
    }
  ]
})
export class CoreModule {
}

/** INITIALIZE KEYCLOAK */
function keycloakFactory(keycloak: KeycloakService) {
  return () => keycloak.init();
}
