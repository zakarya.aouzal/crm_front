export interface ResponseModel {
    valid: boolean;
    code: string;
    message: ReturnMessageCode;
}

export interface ResponseFileModel {
    valid: boolean;
    messageCode: ReturnMessageCode;
    detail: string;
}

export enum ReturnMessageCode {
    CREATED = "created",
    UPDATED = "updated",
    DELETED = "deleted",
    UPDATED_ON_CONTROLE = "deleted-on-controle",
    NOT_EXISTED = "not-existed",
    TRANSACTION_FAILED = "transaction-failed",
    TRANSACTION_FAILED_ON_CONTROLE = "transaction-failed",
    EXCEPTION_RAISED = "exception-raised"
}