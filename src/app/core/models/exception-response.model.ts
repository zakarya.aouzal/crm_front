export class ExceptionResponseModel {
    errorCode: string;
    errorMessage: string;
    field: string;

    constructor() { }
}