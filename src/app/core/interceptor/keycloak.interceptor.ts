import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { KeycloakService } from './../services/security/keycloak.service';

@Injectable()
export class KeycloakInterceptor implements HttpInterceptor {

  constructor(private keycloakService: KeycloakService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    // if (this.keycloakService.keycloak.authenticated) {
    //   request = request.clone({
    //     setHeaders: {
    //       Authorization: 'Bearer ' + this.keycloakService.keycloak.token
    //     }
    //   })
    // }

    return next.handle(request);
  }

}
