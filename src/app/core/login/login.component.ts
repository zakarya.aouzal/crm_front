import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.default';
import { ReferenceService } from '../../main/services';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    // Form group
    formGroup: FormGroup = null;

  constructor(private fb: FormBuilder,private service:ReferenceService,private router: Router) { }

  ngOnInit(): void {
    this.createFormGroup();
  }


   /** CREATE FORM GROUP */
   createFormGroup() {
    this.formGroup = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
     
    });
  }


  /** SUBMIT EVENT */
  onSubmit() {
 
    if (this.formGroup && this.formGroup.valid) {
      const obj = this.generatePayload();
        // this.service.login(obj).subscribe(response => {
        //   if(response){
        //     this.router.navigateByUrl('/pages');
        //   }
        // })
      
    }
  }


  /** GENERATE PAYLOAD */
  generatePayload(): any {
       return {
        username: this.formGroup.get('username').value,
        password: this.formGroup.get('password').value,  
        }

  }

}
