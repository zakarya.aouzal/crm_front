import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { KeycloakService } from './../services/security/keycloak.service';

@Injectable()
export class KeycloakAuthGuard implements CanActivate {

  constructor(private keycloakService: KeycloakService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // if (this.keycloakService.keycloak.authenticated) {
    //   return true;
    // } else {
    //   this.keycloakService.keycloak.login();
    //   return false;
    // }
    return true;

  }

}
