import { Injectable, Injector } from '@angular/core';
import * as Keycloak from 'keycloak-js';
import { KeycloakInstance } from 'keycloak-js';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Injectable()
export class KeycloakService {

  constructor(private injector: Injector) { }

  private get toastrService(): ToastrService {
    return this.injector.get(ToastrService);
  }

  keycloak: KeycloakInstance = null;

  init() {
    // ********* INIT SERVICE KEYCLOAK *********

    this.keycloak = Keycloak({
      url: environment.keycloak.config.url,
      realm: environment.keycloak.config.realm,
      clientId: environment.keycloak.config.clientId
    });

    return new Promise<boolean>((resolve, reject) => {

      // ********* INITTIALIZING... ********
 

      // INIT KEYCLOAK
      this.keycloak.init({
        onLoad: 'login-required',
        checkLoginIframe: environment.keycloak.keycloakOptions.checkLoginIframe
      //  enableLogging: environment.keycloak.enableLogging

      }).then((authenticated) => {
        
        // if (!authenticated) {
        //   this.keycloak.redirectUri = environment.keycloak.defaultRedirectUrl;
        // }

        resolve(authenticated);
      }, ((error: Keycloak.KeycloakError) => {
        console.error('FAILED TO AUTHENTICATE : ', error);
        reject(error);
      }));

      // ********* REFRESH TOKEN IF EXPIRED  *********
      this.keycloak.onTokenExpired = () => {
        // Update the token when will last less than x seconds
        // return true if TOKEN WAS SUCCESSFULLY REFRESHED else  TOKEN IS STILL VALID
        this.keycloak.updateToken(5)
          .catch(error => {
            console.error('FAILED TO REFRESH THE TOKEN, OR THE SESSION HAS EXPIRE : ', error);
            this.toastrService.info('Votre session a expiré. Veuillez vous reconnecter', null, { timeOut: 5000 });
            setTimeout(() => {
              this.keycloak.logout();
            }, 5000);
          })
      }

    });

  }

}
