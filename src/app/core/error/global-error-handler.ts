import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ExceptionResponseModel } from '../models';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  constructor(private injector: Injector, private translateService: TranslateService) { }

  private get toastrService(): ToastrService {
    return this.injector.get(ToastrService);
  }

  handleError(errorResponse: any): any {

    console.error(errorResponse);

    if (errorResponse && errorResponse.error) {

      if (errorResponse.error.validationMessages && Array.isArray(errorResponse.error.validationMessages)) {

        let validationMessages: ExceptionResponseModel[] = errorResponse.error.validationMessages;

        validationMessages.forEach(item => {

          if (item.errorCode) {

            this.toastrService.warning(this.translateService.instant(`exceptionResponse.${item.errorCode}`));

          } else if (item.errorMessage) {

            this.toastrService.warning(item.errorMessage);

          } else {

            this.handleErrorStatus(errorResponse);
          }
        });

      } else if (errorResponse.error.errorCode) {

        this.toastrService.warning(this.translateService.instant(`exceptionResponse.${errorResponse.error.errorCode}`));

      } else if (errorResponse.error.errorMessage) {

        this.toastrService.warning(errorResponse.error.errorMessage);

      } else {

        this.handleErrorStatus(errorResponse);
      }

    }

  }

  handleErrorStatus(errorResponse: any) {
    // handle 401 Unauthorized and 403 Forbidden
    if (errorResponse.status === 0 || errorResponse.status === 401 || errorResponse.status === 403) {
      this.toastrService.error("The authentication session has expired or the user is not authorised !");
    }

    // handle 4xx Client errors
    else if (errorResponse.status >= 400 && errorResponse.status < 500) {
      this.toastrService.error("The request is incorrect !");
    }

    // handle 5xx Server error
    else if (errorResponse.status >= 500) {
      this.toastrService.error("The server is not responding. Contact the administrator !");
    }
  }

}