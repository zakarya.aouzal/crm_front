import {AbstractControl} from '@angular/forms';

export class CarnetOrdreModel {

   numOrdre: string ;
   codeClient: string ;
   descriptionClient: string ;
   sens: string ;
   titre: string ;
   descriptionTitre: string ;
   quantite: number ;
   cours: number ;
   dateOrdre: Date ;
   dateExpiration: Date;
   constructor() {
   }
};

export class CarnetOrdreFilterModel {
   typeFonds: string ;
   fond: string ;
   sens: string ;
   typeClient: string;
   client: string ;
   date : string ;
   constructor() {
   }
}

export class OrdreModel {
  client: string ;
  compteEspece: string ;
  fond : string ;
  budget: number ;
  quantite: number;
  vl : number ;
  sens: string ;
  constructor() {
  }
}
