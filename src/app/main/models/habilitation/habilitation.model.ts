export class HabilitationModel {
    

    role: RoleModel;
    permissions:PermissionModel[]

    constructor() {     
        this.permissions = [];
    }
}

export class RoleModel {

    code: number;
    name: string;
    description: string;

    constructor() { }
}

export class PermissionModel {

    code: number;
    name: string;
    description: string;
    active: string;

    constructor() { }
}