export class OperationModel {

  numOperation: string;
  portefeuille: string;
  descriptionPortf: string;
  poste: string;
  fonds: string;
  descriptionFonds: string;
  dateValeur: string;
  quantite: number;
  cours: number;
  montantBrut: number;
  frais: number;
  taxes: number;
  montantNet: number;
  pmv: number;

  constructor() {
  }

}

export class OperationFilterModel {

  dateDebut?: string;
  dateFin?: string;
  entite?: string;
  portefeuille?: string;
  limit:string;

  constructor() {
  }

}


