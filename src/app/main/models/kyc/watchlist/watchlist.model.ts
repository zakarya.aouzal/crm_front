export class IStatusWatchlist {
  code: string;
  code_wl: string;
  similtitude: number;
  statut_wl: string;
  code_liste: string;
}

export class StatusWatchlistPP extends IStatusWatchlist {
  nom: string;
  prenom: string;
  date_naissance: string;
  num_pi: string;
  nationalite: string;
}

export class StatusWatchlistPM extends IStatusWatchlist {
  raison_sociale: string;
  pays: string;
}

export class WatchlistProcessDTO {
  critere: SimiltitudeDTO;
  codeCompte: string;
}

export class SimiltitudeDTO {
  flagTrait: string;
  flag: boolean;
  codeListe: string;
}
