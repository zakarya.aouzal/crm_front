export class CommentModel {
  code?: number;
  ticketCode: number;
  affectation?: string;
  commentaire: string;
  operateurSaisie?: string;
  dateCreation?: Date;
  dateMaj?: Date;
}

