import {ActivityGlobalFilterModel} from "../history-activity/history-activity.model";

export class IncidentModel {
  code?: number;
  objet: string;
  description: string;
  type: string;
  origine: string;
  compte: string;
  contact: string;
  statut: string;
  priorite: PrioriteIncident;
  dateDeclaration?: Date;
  dateMaj?: Date;

  gestionnaire?: string;
  operateurSaisie?: string;
  operateurModification?: string;


}

export class IncidentFilterModel extends ActivityGlobalFilterModel {
  priorite: PrioriteIncident;
  dateDeclaration: Date;

}

export class HistoIncident {
  code: number;
  mtable: string;
  mcode: number;
  colonne: string;
  ancienneValeur: string;
  nouvelleValeur: string;
  dateModification: Date;
  operateurModification: string;

  constructor() {
  }
}

export enum PrioriteIncident {
  HAUTE = 'HAUTE',
  MOYENNE = 'MOYENNE',
  BASSE = 'BASSE'
}
