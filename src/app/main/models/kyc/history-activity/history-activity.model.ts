import { IncidentModel, OpportuniteModel } from '../..';
import {AccountType} from '../account/account.model';
import {EventModel} from '../event/event.model';
import {TaskModel} from '../task/task.model';

export interface HistoryActivityModel {
  type: String;
  date: Date;
  task: TaskModel;
  event: EventModel;
  opportunite: OpportuniteModel;
  incident: IncidentModel;
}

export class ActivityGlobalFilterModel {
  // global fields
  typeCompte?: AccountType;
  compte?: string;
  open?: boolean;

  // common fields
  type?: string;
  status?: string;
  gestionnaire?: string;

  constructor() {
  }
}
