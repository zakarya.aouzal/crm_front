import {ActivityGlobalFilterModel} from "../history-activity/history-activity.model";

export class EventPayloadModel {
    code?: any;
    subject: string;
    type: string;
    description: string;
    location: string;
    status: EventStatus;
    start_date: Date;
    end_date: Date;
    compte_id: string;
    contact_id: string;

    constructor() { }
}

export enum EventStatus {
    TO_DO = "TO_DO",
    IN_PROGRESS = "IN_PROGRESS",
    CLOSED = "CLOSED"
}

export interface EventModel {
    name: string;
    account_type: string;
    code: string;
    subject: string;
    description: string;
    location: string;
    status: string;
    priority: number;
    start_date: Date;
    end_date: Date;
    operateur_saisie: string;
    operateur_modification: string;
    contact_id: string;
    compte_id: string;
}

export class EventFilterModel extends ActivityGlobalFilterModel {
    dateDebut: Date;
    dateFin: Date;

}
