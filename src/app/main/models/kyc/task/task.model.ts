import {ActivityGlobalFilterModel} from "../history-activity/history-activity.model";

export class TaskPayloadModel {
    code?: any;
    subject: string;
    type: string;
    description: string;
    status: string;
    due_date: Date;
    compte_id: string;
    contact_id: string;

    constructor() { }
}

export interface TaskModel {
    name: string;
    account_type: string;
    type: string;
    code: string;
    subject: string;
    description: string;
    status: string;
    priority: number;
    due_date: Date;
    operateur_saisie: string;
    operateur_modification: string;
    contact_id: string;
    compte_id: string;
}

export class TaskFilterModel extends ActivityGlobalFilterModel {
    dateCreation: Date;

}
