export class PositionMoyModel {
  entite: string;
  portefeuille: string;
  descriptionPortf: string;
  fonds: string;
  descriptionFonds: string;
  totalSous: number;
  totalRac: number;
  totalPmv: number;
  totalFdg: number;
  actMoy: number;
}

export class PositionSousRachatModel {
  entite: string;
  portefeuille: string;
  descriptionPortef: string;
  fonds: string;
  descriptionFonds: string;
  categorieFonds: string;
  stock: number;
  dateVL: Date;
  vl: number;
  valorisation: number;
  pmup: number;
  pmvLatentes: number;
}

export class PositionMoyFilterModel {
  dateDebut?: Date;
  dateFin?: Date;
  entite?: string;
  portefeuille?: string;
  limit?:string;

  constructor() {
  }
}

export class PositionSousRachatFilterModel {
  position?: Date;
  entite?: string;
  portefeuille?: string;
  limit?:string;
  constructor() {
  }
}
