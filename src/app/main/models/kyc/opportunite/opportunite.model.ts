import {ActivityGlobalFilterModel} from "../history-activity/history-activity.model";

export enum OpportuniteStatus {
    TO_DO = "TO_DO",
    IN_PROGRESS = "IN_PROGRESS",
    CLOSED = "CLOSED"
}

export class OpportuniteModel {
  code?: number;
  subject: string;
  description: string;
  status: string;
  probabilite: number;
  estimation: number;
  type: string;
  contact_id: string;
  compte_id: string;
  due_date: Date;

  dateModification?: Date;
  dateInsert?: Date;
  operateur_saisie?: string;
  operateur_modification?: string;
}

export class OpportuniteFilterModel extends ActivityGlobalFilterModel {
    dateCreation: Date;
}
