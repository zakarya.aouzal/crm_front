export class IncidentModel {
  code?: number;
  objet: string;
  description: string;
  type: string;
  origine: string;
  compte: string;
  contact: string;
  statut: string;
  priorite: string;
  dateDeclaration?: Date;
  dateMaj?: Date;

  gestionnaire?: string;
  operateurSaisie?: string;
  operateurModification?: string;
}
