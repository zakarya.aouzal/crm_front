export class ContactModel {
  code: string;
  codePostale: string;
  email: string;
  mobile1: string;
  mobile2: string;
  numTaxePro: string;
  operateurModification: string;
  operateurSaisie: string;
  profilRisque: string;
  adresse1: string;
  adresse2: string;
  tribImmatricule: string;
  ville: string;

  affected?: boolean;
}

export class ContactPPModel extends ContactModel {
  code: string;
  nom: string;
  prenom: string;
  civilite: string;
  ville: string;
  email: string;
  codePostale: string;
  dateNaissance: Date;
  dateEmiPI: Date;
  dateExiPI: Date;
  emetteurPI: string;
  adresse1: string;
  adresse2: string;
  mobile1: string;
  mobile2: string;
  nationalite: string;
  nomPrenomMere: string;
  nomPrenomPere: string;
  numPI: string;
  numTaxePro: string;
  operateurModification: string;
  operateurSaisie: string;
  paysResidence: string;
  pieceIdentite: string;
  profilRisque: string;
  rcRn: string;
  flagActif: string;
  flagValidation: string;
  statutControle: string;
  statutFatca: string;
  statutInterv: string;
  statutPPE: string;
  statutWL: string;
  tribImmatricule: string;
  typeResidence: string;
}

export class ContactPMModel extends ContactModel {
  code: string;
  email: string;
  fax: string;
  flagActif: string;
  adresse1: string;
  adresse2: string;
  agrementDelivrance: string;
  categorie: string;
  codePostale: string;
  flagValidation: string;
  formeJuridique: string;
  ice: string;
  idFiscal: string;
  mobile1: string;
  mobile2: string;
  numTaxePro: string;
  operateurModification: string;
  operateurSaisie: string;
  ville: string;
  pays: string;
  profilRisque: string;
  raisonSociale: string;
  rc: string;
  secteurActivite: string;
  siteWeb: string;
  statutControle: string;
  statutFatca: string;
  statutInterv: string;
  statutPPE: string;
  statutWL: string;
  dateInsertion: Date;
  tribImmatricule: string;
  nom:string;
}
