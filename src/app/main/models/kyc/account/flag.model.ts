export enum Flag {
    CONSULTATION = "N",
    VALIDATION= "O",
}

export enum FlagExport {
  NotExported = "N",
  Exported = "O",
}
