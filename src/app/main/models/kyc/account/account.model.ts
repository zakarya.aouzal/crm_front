import {Flag, FlagExport} from "./flag.model";
import {ContactModel} from "./contact.model";

export class AccountFilterModel {
    code: string;
    raisonSociale: string;
    nom: string;
    prenom: string;
    gestionnaire: string;
    operateur_saisie: string;
    dateInsert: Date;
    mobile1: string;
    paysResidence: string;
    ville: string;
    flag: Flag;
    flagExport: FlagExport;

    constructor() { }
}

export enum AccountType {
    PP = 'PP',
    PM = 'PM'
}

export enum ContactType {
    PP = 'PP',
    PM = 'PM'
}

export interface AccountPMModel {
    code: string;
    description?:string;
    raisonSociale: string;
    rc: string;
    idFiscal: string;
    ice: string;
    statutWL: string;
    mobile1: string;
    siteWeb: string;
    formeJuridique: string;
    adresse1: string;
    email: string;
    codePostale: string;
    gestionnaire: string;
    autoriteAgrement: string;
    tribImmatricule: string;
    numTaxePro: string;
    categorie: string;
    secActivite: string;
    produitSouhaites: string;
    niveauInv: string;
    typeProduit:string;
    secteurSouhaites: string;
    horizonPlacement: string;
    profilInvestissement: string;
    objectifInvestissement: string;
    paysResidenceFiscale: string;
    pays: string;
    compteBancaireUSA: string;
    adresseUSA: string;
    villeFiscaleUSA: string;
    idFiscalUSA: string;
    ligneTelephoneUSA: string;
    giin: string;
    typeCompte: string;
    capaciteFinanciere: string;
    resultatNet: number;
    fixe:string;
    profilRisque: string;
    onboarding_status:string;
    flagExport:string;
    motifRejet:string;
}

export interface AccountPPModel {
    code: string;
    nom: string;
    prenom: string;
    description?:string;
    dateNaissance: string;
    numPI: string;
    mobile1: string;
    mobile2: string;
    codePostale: string;
    secActivite: string;
    civilite: string;
    profession: string;
    nomPrenomMere: string;
    nomPrenomPere: string;
    paysResidence: string;
    nationalite: string;
    ville: string;
    adresse1: string;
    adresse2: string;
    email: string;
    pieceIdentite: string;
    emetteurPI: string;
    dateEmiPI: Date;
    dateExpPI: Date;
    tribImmatricule: string;
    rc_rn: string;
    numTaxePro: string;
    revenuAnnuel: string;
    origineFonds: string;
    niveauInv: string;
    produitSouhaites: string;
    horizonPlacement: string;
    typeProduit:string;
    gestionnaire: string;
    operateur_saisie: string;
    secteurSouhaites: string;
    objectifInvestissement: string;
    profilInvestissement: string;
    nationaliteUSA: string;
    compteBancaireUSA: string;
    adresseUSA: string;
    greenCardUSA: string;
    idFiscalUSA: string;
    ligneTelephoneUSA: string;
    typeCompte: string;
    statutWL: string;
    capaciteFinanciere: string;
    profilRisque: string;
    dateOfBirth:string ;
    fatherFullName:string ;
    motherFullName:string;
    residenceCountry:string ;
    typeResidence:string;
    onboarding_status:string;
    flagExport:string;
    motifRejet:string;
}

export class AccountPPPayloadModel {
    code: string;
    nom: string;
    prenom: string;
    dateNaissance: Date;
    mobile1: string;
    mobile2: string;
    civilite: string;
    codePostale: string;
    nomPrenomMere: string;
    nomPrenomPere: string;
    paysResidence: string;
    nationalite: string;
    ville: string;
    adresse1: string;
    adresse2: string;
    gestionnaire: string;
    email: string;
    pieceIdentite: string;
    numPI: string;
    emetteurPI: string;
    dateEmiPI: Date;
    dateExpPI: Date;
    tribImmatricule: string;
    rc_rn: string;
    numTaxePro: string
    profession: string;
    secActivite: string;
    capaciteFinanciere: string;
    origineFonds: string;
    niveauInv: string;
    produitSouhaites: string;
    horizonPlacement: string;
    typeProduit:string;
    secteurSouhaites: string;
    objectifInvestissement: string;
    profilInvestissement: string;
    nationaliteUSA: string;
    compteBancaireUSA: string;
    adresseUSA: string;
    greenCardUSA: string;
    idFiscalUSA: string;
    ligneTelephoneUSA: string;
    typeCompte: string;
    profilRisque: string;
    dateOfBirth:string ;
    fatherFullName:string ;
    motherFullName:string;
    residenceCountry:string ;
    country:string ;
    city:string ;
    typeResidence:string;
    revenuAnnuel:string;
    statutWL: string;


    constructor() { }
}

export class AccountPMPayloadModel {
    code: string;
    categorie: string;
    formeJuridique: string;
    ice: string;
    adresse1: string;
    mobile1: string;
    email: string;
    codePostale: string;
    siteWeb: string;
    gestionnaire: string;
    autoriteAgrement: string;
    idFiscal: string;
    rc: string;
    numTaxePro: string;
    paysResidenceFiscale: string;
    pays: string;
    villeFiscaleUSA: string;
    giin: string;
    capaciteFinanciere: string;
    raisonSociale: string;
    fixe:string;
    statutWL: string;

    constructor() { }

  adresseUSA: string;
  compteBancaireUSA: string;
  horizonPlacement: string;
  idFiscalUSA: string;
  ligneTelephoneUSA: string;
  niveauInv: string;
  objectifInvestissement: string;
  produitSouhaites: string;
  profilInvestissement: string;
  profilRisque: string;
  resultatNet: number;
  secActivite: string;
  secteurSouhaites: string;
  tribImmatricule: string;
  typeCompte: string;
  typeProduit: string;
}

export interface ContactAccountModel {
    code: string;
    codeContact: string;
    codeCompte: string;
    description_pp:string;
    description_pm:string;
    email: string;
    mobile: string;
    descriptionContact: string;
    role: string;
    qualite: string;
    dateDebutQualite: Date;
    dateFinQualite: Date;
    pourcentageCapital: number;
    pourcentageDroitVote: number;
}

export class ContactFilterModel {
    accountType: AccountType;
    accountCode: string;

    constructor() { }
}


export class CompteContactPayload {
    code?: string;
    codeContact: string;
    codeCompte: string;
    descriptionContact: string;
    role: string;
    qualite: string;
    dateDebutQualite: Date;
    dateFinQualite: Date;
    pourcentageCapital: number;
    pourcentageDroitVote: number;

    constructor() { }
}

export interface AttachmentAccountModel {
    num_doc: number;
    fileName: string;
    documentName: string;
    description: string;
    filePath: string;
    compte: string;
    compteType: number;
    documentType: string;
    visibility: string;
    gestionnaire: string;
    dateArchivage: Date;
}

export class RegisterAccountPayloadModel {

    accountType: AccountType;
    accountPP: AccountPPPayloadModel;
    accountPM: AccountPMPayloadModel;
    contactAccounts: CompteContactPayload[];

    selectedContacts: ContactModel[];

    constructor() { }
}

export class CompteMap {
  comptesPP: AccountPPModel[];
  comptesPM: AccountPMModel[];
}

export class Alerte {
  title: string;
  description: string;
  type: 'ExpiredDocument' | 'Autre';
  date: Date;
}

export class CompteAlerte extends Alerte {
  code: string;
  accountType: AccountType;
  pieceIdentite: string;
  numPI: string;
  emetteurPI: string;
  dateEmiPI: Date;
  dateExpPI: Date;
}

export class CompteEspModel{
    banque:string;
    ville:string;
    agence:string;
    compte_esp :string;
    depositaire:string;
    compte_titre:string;

}
