
export class DetailFondsModel{
 

    classe: string ;
    descriptionTitre: string ;
    codeIsin: string ;
    depositaire: string ;
    depot: number ;
    actif: number ;
    repo: number ;
    pret: number ;
    emprunt: number ;

    constructor() {
    }

    
}

export class DetailFondsFilterModel{

    client?:string;
    fond?:string;
    date_vl?:string;

    constructor() {
    }

}