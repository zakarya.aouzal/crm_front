
export class RatiosAMMCModel {
  date_vl : string ;
  groupe: string ;
  indicateur : string ;
  reference : string ;
  valeur_reference : string ;
  minimum : string ;
  maximum : string ;
  valeur : string ;
  echelle : string
}

export class RatiosAMMCFilterModel {
  fond :string ;
  date_debut : string ;
  date_fin : string ;
  indicateur : string[] ;

}
