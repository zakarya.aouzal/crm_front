export interface DashboardModel {
    annee: number;
    mois: number;
    pays: string;
    statutWl: string;
    total: number;
}

export interface AccountPotentialAdviceModel {
    conseiller: string;
    revenu: number;
}

export interface AccountActivityArea {
    secteurActivite: string;
    total: number;
}

export interface AccountRepartitionByCatArea {
    titre_cat: string;
    titre_valeur: string;
    valo: number;
    poidsvalo: number;
}

export interface AccountRepartitionByValArea {
    titre: string;
    titre_desc: string;
    valo: number;
    pdr: number;
    pmv: number;
}