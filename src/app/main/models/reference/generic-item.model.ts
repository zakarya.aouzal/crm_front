export interface GenericItemModel {
    code: string;
    value: string;
}

export interface DateVLItemModel {
    DATE_VL: string;
    FORMAT: string;
}

export interface DateVLModel {
  date_vl: string;
  format: string;
}
