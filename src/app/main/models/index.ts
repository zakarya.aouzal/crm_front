export * from './dashboard/dashboard.model';
export * from './kyc/account/account.model';
export * from './kyc/account/contact.model';
export * from './kyc/incident/incident.model';
export * from './kyc/incident/comment.model';
export * from './kyc/opportunite/opportunite.model';
export * from './kyc/event/event.model';
export * from './kyc/watchlist/watchlist.model';
export * from './kyc/history-activity/history-activity.model';

export * from './kyc/operation/operation.model';
export * from './kyc/position/position.model';
export * from './kyc/task/task.model';
export * from './kyc/opportunite/opportunite.model';
export * from './reference/generic-item.model';
export * from './kyc/account/flag.model'
export * from './habilitation/habilitation.model'
export * from './habilitation/habilitation.enum'
export * from './asset/opcvm.model'
export * from './asset/dataPortfValorise.model'
export * from './asset/detail-fonds.model'
export * from './users/gestion-utilisateurs.model';




