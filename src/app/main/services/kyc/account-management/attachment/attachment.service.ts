import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AttachmentAccountModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';


@Injectable()
export class AttachmentService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getListAttachmentsByAccount(accountCode: string): Observable<AttachmentAccountModel[]> {
    const url = `${this.apiEndPoint}/ged/compte/${accountCode}/documents`;
    return this.http.get<AttachmentAccountModel[]>(url);
  }

  getAttachment(attachmentCode: number): Observable<Blob> {
    const url = `${this.apiEndPoint}/ged/documents/${attachmentCode}`;
    return this.http.get(url, { responseType: 'blob' });
  }

  getListAttachmentsByIncident(incidentCode: number): Observable<AttachmentAccountModel[]> {
    const url = `${this.apiEndPoint}/ged/incident/${incidentCode}/documents`;
    return this.http.get<AttachmentAccountModel[]>(url);
  }

}
