import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AttachmentAccountModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { AttachmentService } from './attachment.service';

fdescribe('AttachmentService', () => {
  let service: AttachmentService;
  let httpTestingController: HttpTestingController;
  let apiEndPoint = environment.apiEndPoint;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AttachmentService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AttachmentService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('\'getListAttachmentsByAccount\' should return Observable match the right data', () => {
    const accountPPListMock: AttachmentAccountModel[] = [];
    const accountCode: string = 'PP_1002';
    let result: AttachmentAccountModel[] = null;

    service.getListAttachmentsByAccount(accountCode)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/ged/compte/${accountCode}/documents`
    });

    req.flush(accountPPListMock);

    expect(result).toEqual(accountPPListMock);

  });

  it('\'getAttachment\' should return Observable match the right data', () => {
    const attachmentMock: Blob = new Blob([''], { type: 'text/html' });
    const attachmentCode: number = 1;
    let result: Blob = null;

    service.getAttachment(attachmentCode)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/ged/document/${attachmentCode}`
    });

    req.flush(attachmentMock);

    expect(result).toEqual(attachmentMock);

  });

});
