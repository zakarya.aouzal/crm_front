import { Injectable } from '@angular/core';
import {StatusWatchlistPM, StatusWatchlistPP, WatchlistProcessDTO} from "../../../../models";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../../../environments/environment";

@Injectable()
export class WatchlistService {

  private watchlistApi = `${environment.apiEndPoint}/watch-lists`;

  constructor(private httpClient: HttpClient) { }

  traiterComptePPSimiltitudeByCode(dto: WatchlistProcessDTO): Observable<StatusWatchlistPP[]> {
    const url = `${this.watchlistApi}/similtitude/pp`;
    return this.httpClient.post<StatusWatchlistPP[]>(url, dto);
  }

  traiterComptePMSimiltitudeByCode(dto: WatchlistProcessDTO): Observable<StatusWatchlistPM[]> {
    const url = `${this.watchlistApi}/similtitude/pm`;
    return this.httpClient.post<StatusWatchlistPM[]>(url, dto);
  }
}
