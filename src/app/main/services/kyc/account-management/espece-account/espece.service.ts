import { Injectable } from '@angular/core';
import {environment} from "../../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {CommentModel, CompteEspModel} from "../../../../models";
import {ResponseModel} from "../../../../../core/models";

@Injectable()
export class CompteEspService {

  private apiEndPoint = environment.apiEndPoint;
  private mockResult :CompteEspModel[]=[
    {
      "banque" : "UCM",
      "ville" : "null",
      "agence" : "UCM",
      "compte_esp" : "*****",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "UCM",
      "ville" : "null",
      "agence" : "UCM",
      "compte_esp" : "*****",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP07",
      "compte_esp" : "1323056950570010",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP03",
      "compte_esp" : "1323058169040017",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770217",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770332",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770308",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "2111151412770078",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DSGMB",
      "ville" : "null",
      "agence" : "ASGMB02",
      "compte_esp" : "000 096 000 903 8412",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DCNCA",
      "ville" : "null",
      "agence" : "ACNCA01",
      "compte_esp" : "Z 200 387 M 651",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DCIH",
      "ville" : "null",
      "agence" : "ACIH02",
      "compte_esp" : "907 840 322 100 2000",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "2111151412770003",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770209",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770316",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770290",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP02",
      "compte_esp" : "1323051412770621",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    },
    {
      "banque" : "DBCP",
      "ville" : "null",
      "agence" : "ABCP04",
      "compte_esp" : "1323018623660057",
      "depositaire" : "M0110",
      "compte_titre" : "CMR01"
    }
  ]

  constructor(private http: HttpClient) { }



  loadListEspecesAccounts(Code: string): Observable<CompteEspModel[]> {
    // const url = `${this.apiEndPoint}/crm/tickets/${incidentCode}/comments`;
    // return this.http.get<CommentModel[]>(url);
    return of(this.mockResult);
  }

}
