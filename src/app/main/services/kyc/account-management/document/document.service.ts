import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { GenericItemModel } from 'src/app/main/models';

@Injectable()
export class DocumentService {

  private apiEndPoint = environment.apiEndPoint;
  constructor(private httpClient: HttpClient) {
  }

  getDoc(type:string): Observable<GenericItemModel[]> {
    return this.httpClient.get<GenericItemModel[]>( `${this.apiEndPoint}/KYCListeDocGed/${type}`);
    
  }

  uploadZip(zip: any): Observable<any> {

    // TO DO
    let formData = new FormData();
    formData.append('uploadFile', zip, "docs.zip");
   
        return this.httpClient.post<any>("", formData);
    
}

download(id): Observable<any> {
  //TO DO
  return this.httpClient.get("this.url", {
      responseType: 'blob'
  });
}
}
