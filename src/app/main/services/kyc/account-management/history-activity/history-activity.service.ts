import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HistoryActivityModel } from 'src/app/main/models';
import { SearchDto } from 'src/app/main/pages/activity-management/activities-history/activities-history.component';

import { environment } from 'src/environments/environment';

@Injectable()
export class HistoryActivityService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }
  
  getPreviousHistoryList(searchDto:SearchDto): Observable<HistoryActivityModel[]> {
    const url = `${this.apiEndPoint}/crm/history-activity/previous`;
    return this.http.post<HistoryActivityModel[]>(url,searchDto);
  }

  getNextHistoryList(searchDto:SearchDto): Observable<HistoryActivityModel[]> {
    const url = `${this.apiEndPoint}/crm/history-activity/next`;
    return this.http.post<HistoryActivityModel[]>(url,searchDto);
  }

  
}
