import { TestBed } from '@angular/core/testing';

import { CompteContactService } from './compte-contact.service';

describe('CompteContactService', () => {
  let service: CompteContactService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompteContactService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
