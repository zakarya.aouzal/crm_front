import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../../../environments/environment';
import { ResponseModel } from '../../../../../core/models';
import { CompteContactPayload } from '../../../../models';

@Injectable()
export class CompteContactService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  insertCompteContact(payload: CompteContactPayload[]): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/compte-contacts`;
    return this.http.post<ResponseModel>(url, payload);
  }


}
