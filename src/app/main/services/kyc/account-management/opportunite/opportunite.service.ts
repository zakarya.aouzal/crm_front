import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseModel} from 'src/app/core/models';
import {
  ActivityGlobalFilterModel,
  GenericItemModel,
  OpportuniteModel
} from 'src/app/main/models';
import {environment} from 'src/environments/environment';

@Injectable()
export class OpportuniteService {

  private apiEndPoint = `${environment.apiEndPoint}`;

  constructor(private http: HttpClient) {
  }

  create(dto: OpportuniteModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/opportunite`;
    return this.http.post<ResponseModel>(url, dto);
  }

  getNextOpportunites(): Observable<OpportuniteModel[]> {
    const url = `${this.apiEndPoint}/crm/opportunite/date/next`;
    return this.http.get<OpportuniteModel[]>(url);
  }

  getPreviousOpportunites(): Observable<OpportuniteModel[]> {
    const url = `${this.apiEndPoint}/crm/opportunite/date/previous`;
    return this.http.get<OpportuniteModel[]>(url);
  }

  getAll(): Observable<OpportuniteModel[]> {
    const url = `${this.apiEndPoint}/crm/opportunite`;
    return this.http.get<OpportuniteModel[]>(url);
  }

  getByCode(code: number): Observable<OpportuniteModel> {
    const url = `${this.apiEndPoint}/crm/opportunite/${code}`;
    return this.http.get<OpportuniteModel>(url);
  }

  fetchAllByFilter(filter: ActivityGlobalFilterModel): Observable<OpportuniteModel[]> {
    const url = `${this.apiEndPoint}/crm/opportunite/filter`;
    return this.http.post<OpportuniteModel[]>(url, filter);
  }

  update(dto: OpportuniteModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/opportunite`;
    return this.http.put<ResponseModel>(url, dto);
  }

  deleteOpportunite(code: number): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/opportunite/${code}`;
    return this.http.delete<ResponseModel>(url);
  }

  getOpportuniteStatus(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/opportunite/status`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getOpportuniteTypes(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/opportunite/types`;
    return this.http.get<GenericItemModel[]>(url);
  }

}
