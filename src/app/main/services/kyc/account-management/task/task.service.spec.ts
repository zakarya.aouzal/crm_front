import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ResponseModel } from 'src/app/core/models';
import { TaskPayloadModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { TaskService } from './task.service';


fdescribe('TaskService', () => {
  let service: TaskService;
  let httpTestingController: HttpTestingController;
  let apiEndPoint = environment.apiEndPoint;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaskService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TaskService);;
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('\'addTask\' should return Observable match the right data', () => {

    const responseMock: ResponseModel = {  
      valid: true,
      code: '225',
      message: null
    };
    const payload: TaskPayloadModel = null;
    let result: ResponseModel = null;

    service.addTask(payload)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${apiEndPoint}/crm/tasks`
    });

    req.flush(responseMock);

    expect(req.request.body).toEqual(payload);
    expect(result).toEqual(responseMock);

  });

});
