import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from 'src/app/core/models';
import {ActivityGlobalFilterModel, GenericItemModel, TaskModel, TaskPayloadModel} from 'src/app/main/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class TaskService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  addTask(payload: TaskPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/tasks`;
    return this.http.post<ResponseModel>(url, payload);
  }

  getTaskByCode(code: any): Observable<TaskModel> {
    const url = `${this.apiEndPoint}/crm/tasks/${code}`;
    return this.http.get<TaskModel>(url);
  }

  updateTask(payload: TaskPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/tasks`;
    return this.http.put<ResponseModel>(url, payload);
  }

  deleteTask(taskCode: string): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/tasks/${taskCode}`;
    return this.http.delete<ResponseModel>(url);
  }

  getNextTasks(): Observable<TaskModel[]> {
    const url = `${this.apiEndPoint}/crm/tasks/date/next`;
    return this.http.get<TaskModel[]>(url);
  }

  getPreviousTasks(): Observable<TaskModel[]> {
    const url = `${this.apiEndPoint}/crm/tasks/date/previous`;
    return this.http.get<TaskModel[]>(url);
  }

  getListOfTaskTypes(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/tasks/types`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListOfTaskStatus(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/tasks/status`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getTasksListByFilter(filter: ActivityGlobalFilterModel): Observable<TaskModel[]> {
    const url = `${this.apiEndPoint}/crm/tasks/filter`;
    return this.http.post<TaskModel[]>(url, filter);
  }

}
