import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {ResponseFileModel, ResponseModel} from 'src/app/core/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class GedService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  uploadFilesForNewAccount(formData: FormData, accountCode: string): Observable<ResponseFileModel> {
    const url = `${this.apiEndPoint}/ged/comptes/documents?compte=${accountCode}`;
    return this.http.post<ResponseFileModel>(url, formData);
  }

  uploadDocuments(dto: string, files: Array<File>): Observable<ResponseFileModel> {
    const url = `${this.apiEndPoint}/ged/documents`;
    const formData = new FormData();
    formData.append("dto", dto);
    for (let item of files) {
      formData.append("files", item);
    }
    return this.http.post<ResponseFileModel>(url, formData);
  }
  //   /crm/comptes/{compte}/incidents

  uploadFilesForIncident(incident: string, files: Array<File>): Observable<ResponseFileModel> {
    const url = `${this.apiEndPoint}/ged/incidents/documents`;
    const formData = new FormData();
    formData.append("incident", incident);
    for (let item of files) {
      formData.append("files", item);
    }
    return this.http.post<ResponseFileModel>(url, formData);
  }

}
