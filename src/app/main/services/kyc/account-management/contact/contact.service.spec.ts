import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ContactAccountModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { ContactService } from './contact.service';


fdescribe('ContactService', () => {
  let service: ContactService;
  let httpTestingController: HttpTestingController;
  let apiEndPoint = environment.apiEndPoint;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ContactService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('\'getListContactsByAccount\' should return Observable match the right data', () => {
    const contactsListMock: ContactAccountModel[] = [];

    const accountCode: string = 'PP_1002';
    let result: ContactAccountModel[] = null;

    service.getListContactsByAccount(accountCode)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/compte-contacts/comptes/${accountCode}/contacts`
    });

    req.flush(contactsListMock);

    expect(result).toEqual(contactsListMock);

  });

});
