import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {ContactAccountModel, ContactFilterModel, ContactPMModel, ContactPPModel} from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import {ResponseModel} from '../../../../../core/models';

@Injectable()
export class ContactService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getContactsPP(): Observable<ContactPPModel[]> {
    const url = `${this.apiEndPoint}/contacts/pp`;
    return this.http.get<ContactPPModel[]>(url);
  }

  getContactsPM(): Observable<ContactPMModel[]> {
    const url = `${this.apiEndPoint}/contacts/pm`;
    return this.http.get<ContactPMModel[]>(url);
  }

  getListContactsByAccount(accountCode: string): Observable<ContactAccountModel[]> {
    const url = `${this.apiEndPoint}/compte-contacts/comptes/${accountCode}/contacts`;
    return this.http.get<ContactAccountModel[]>(url);
  }


  getContactPPByCode(code: string): Observable<ContactPPModel> {
    const url = `${this.apiEndPoint}/contacts/pp/${code}`;
    return this.http.get<ContactPPModel>(url);
  }

  getContactPMByCode(code: string): Observable<ContactPMModel> {
    const url = `${this.apiEndPoint}/contacts/pm/${code}`;
    return this.http.get<ContactPMModel>(url);
  }

  getListContactPMByCode(codeCompte: string): Observable<ContactPMModel[]> {
    const url = `${this.apiEndPoint}/contacts/pm/all/${codeCompte}`;
    return this.http.get<ContactPMModel[]>(url);
  }

  getListContactPPByCode(codeCompte: string): Observable<ContactPMModel[]> {
    const url = `${this.apiEndPoint}/contacts/pp/all/${codeCompte}`;
    return this.http.get<ContactPMModel[]>(url);
  }

  insertContactPP(dto: ContactPPModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/contacts/pp`;
    return this.http.post<ResponseModel>(url, dto);
  }

  insertContactPM(dto: ContactPMModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/contacts/pm`;
    return this.http.post<ResponseModel>(url, dto);
  }

  updateContactPP(dto: ContactPPModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/contacts/pp`;
    return this.http.put<ResponseModel>(url, dto);
  }

  updateContactPM(dto: ContactPMModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/contacts/pm`;
    return this.http.put<ResponseModel>(url, dto);
  }

}
