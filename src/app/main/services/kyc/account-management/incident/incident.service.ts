import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {ActivityGlobalFilterModel, GenericItemModel, HistoIncident, IncidentModel} from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { ResponseModel } from "../../../../../core/models";

@Injectable()
export class IncidentService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getListOfIncidents(accountCode: string): Observable<IncidentModel[]> {
    const url = `${this.apiEndPoint}/crm/comptes/${accountCode}/incidents`;
    return this.http.get<IncidentModel[]>(url);
  }

  getIncidentByCode(incidentCode: number): Observable<IncidentModel> {
    const url = `${this.apiEndPoint}/crm/incidents/${incidentCode}`;
    return this.http.get<IncidentModel>(url);
  }

  createIncident(dto: IncidentModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/incidents`;
    return this.http.post<ResponseModel>(url, dto);
  }

  updateIncident(dto: IncidentModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/incidents`;
    return this.http.put<ResponseModel>(url, dto);
  }

  getListOfTypeTickets(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/incidents/types`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListOfTicketsStatus(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/incidents/status`;
    return this.http.get<GenericItemModel[]>(url);
  }

  fetchAllByFilter(filter: ActivityGlobalFilterModel): Observable<IncidentModel[]> {
    const url = `${this.apiEndPoint}/crm/incidents/filter`;
    return this.http.post<IncidentModel[]>(url, filter);
  }

  getListOfTicketsHistory(incidentCode: number): Observable<HistoIncident[]> {
    const url = `${this.apiEndPoint}/crm/incidents/${incidentCode}/histo`;
    return this.http.get<HistoIncident[]>(url);
  }

  deleteIncidentByCode(incidentCode: number): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/incidents/${incidentCode}`;
    return this.http.delete<ResponseModel>(url);
  }

}
