import { Injectable } from '@angular/core';
import {environment} from "../../../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CommentModel} from "../../../../models";
import {ResponseModel} from "../../../../../core/models";

@Injectable()
export class CommentService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  insertComment(dto: CommentModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/comments`;
    return this.http.post<ResponseModel>(url, dto);
  }

  getCommentsByTicket(incidentCode: number): Observable<CommentModel[]> {
    const url = `${this.apiEndPoint}/crm/tickets/${incidentCode}/comments`;
    return this.http.get<CommentModel[]>(url);
  }

}
