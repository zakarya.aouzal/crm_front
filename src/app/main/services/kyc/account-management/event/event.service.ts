import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from 'src/app/core/models';
import {
  ActivityGlobalFilterModel,
  EventModel,
  EventPayloadModel,
  GenericItemModel
} from 'src/app/main/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class EventService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  addEvent(payload: EventPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/events`;
    return this.http.post<ResponseModel>(url, payload);
  }

  updateEvent(payload: EventPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/events`;
    return this.http.put<ResponseModel>(url, payload);
  }

  getEventByCode(eventCode: string): Observable<EventPayloadModel> {
    const url = `${this.apiEndPoint}/crm/events/${eventCode}`;
    return this.http.get<EventPayloadModel>(url);
  }

  deleteEvent(eventCode: string): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/crm/events/${eventCode}`;
    return this.http.delete<ResponseModel>(url);
  }

  getNextEvents(): Observable<EventModel[]> {
    const url = `${this.apiEndPoint}/crm/events/date/next`;
    return this.http.get<EventModel[]>(url);
  }

  getPreviousEvents(): Observable<EventModel[]> {
    const url = `${this.apiEndPoint}/crm/events/date/previous`;
    return this.http.get<EventModel[]>(url);
  }

  getListOfEventTypes(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/events/types`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListOfEventStatus(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/events/status`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getEventsListByFilter(filter: ActivityGlobalFilterModel): Observable<EventModel[]> {
    const url = `${this.apiEndPoint}/crm/events/filter`;
    return this.http.post<EventModel[]>(url, filter);
  }

}
