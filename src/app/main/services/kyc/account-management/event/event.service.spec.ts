import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ResponseModel } from 'src/app/core/models';
import { EventPayloadModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { EventService } from './event.service';


fdescribe('EventService', () => {
  let service: EventService;
  let httpTestingController: HttpTestingController;
  let apiEndPoint = environment.apiEndPoint;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EventService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(EventService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('\'addEvent\' should return Observable match the right data', () => {

    const responseMock: ResponseModel = {
      valid: true,
      code: '225',
      message: null
    };
    const payload: EventPayloadModel = null;
    let result: ResponseModel = null;

    service.addEvent(payload)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${apiEndPoint}/crm/events`
    });

    req.flush(responseMock);

    expect(req.request.body).toEqual(payload);
    expect(result).toEqual(responseMock);

  });

});
