import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResponseModel } from "src/app/core/models";
import {
  AccountFilterModel,
  AccountPMModel,
  AccountPMPayloadModel,
  AccountPPModel,
  AccountPPPayloadModel,
  CompteAlerte, CompteMap,
  RegisterAccountPayloadModel
} from 'src/app/main/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class AccountService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getListAccountPP(filter: AccountFilterModel): Observable<AccountPPModel[]> {
    const url = `${this.apiEndPoint}/comptes/pp/filter`;
    return this.http.post<AccountPPModel[]>(url, filter).pipe(
      map(res=>{return this.transformePPData(res)})

    );
  }

  getListAccountPM(filter: AccountFilterModel): Observable<AccountPMModel[]> {
    const url = `${this.apiEndPoint}/comptes/pm/filter`;
    return this.http.post<AccountPMModel[]>(url, filter).pipe(
      map(res=>{return this.transformePMData(res)})

    );
  }

  getAccountPPByCode(accountCode: string): Observable<AccountPPModel> {
    const url = `${this.apiEndPoint}/comptes/pp/${accountCode}`;
    return this.http.get<AccountPPModel>(url);
  }

  RejectProspect(accountCode: string,accountType: string,motif: string): Observable<any> {
    const url = `${this.apiEndPoint}/comptes/reject`;
    const data = new FormData();
    data.set("code",accountCode);
    data.set("type",accountType);
    data.set("motif",motif);
    return this.http.post<void>(url,data);
  }


  ExportProspect(accountCode: string,accountType: string): Observable<any> {
    const url = `${this.apiEndPoint}/comptes/exportOne`;
    const data = new FormData();
    data.set("code",accountCode);
    data.set("type",accountType);

    return this.http.post<void>(url,data);
  }





  getAccountPMByCode(accountCode: string): Observable<AccountPMModel> {
    const url = `${this.apiEndPoint}/comptes/pm/${accountCode}`;
    return this.http.get<AccountPMModel>(url);
  }

  addAccountPP(payload: AccountPPPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/pp`;
    console.log(AccountPPPayloadModel.toString());
    return this.http.post<ResponseModel>(url, payload);
  }

  updateAccountPP(payload: AccountPPPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/pp`;
    return this.http.put<ResponseModel>(url, payload);
  }

  addAccountPM(payload: AccountPMPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/pm`;
    return this.http.post<ResponseModel>(url, payload);
  }

  updateAccountPM(payload: AccountPMPayloadModel): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/pm`;
    return this.http.put<ResponseModel>(url, payload);
  }

  registerAccount(payload: RegisterAccountPayloadModel ): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/regester`;
    return this.http.post<ResponseModel>(url, payload);
  }

  onboarding(payload: RegisterAccountPayloadModel ): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/onboarding`;
    return this.http.post<ResponseModel>(url, payload);
  }

  uploadPhoto(accountCode: string, photo: File): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/${accountCode}/photo`;
    const data = new FormData();
    data.set("photo", photo);
    return this.http.put<ResponseModel>(url, data);
  }

  getPhoto(accountCode: string): Observable<any> {
    const url = `${this.apiEndPoint}/comptes/${accountCode}/photo`;
    return this.http.get<any>(url);
  }


  transformePPData(data: AccountPPModel[]){
    data.forEach(element =>  element.description=element.code+": "+element.nom+" "+element.prenom)
    return data;
  }

  transformePMData(data: AccountPMModel[]){
    data.forEach(element =>  element.description=element.code+": "+element.raisonSociale)
    return data;
  }

  getExpiredDocumentsAccount(): Observable<CompteAlerte[]> {
    const url = `${this.apiEndPoint}/comptes/expired-documents`;
    return this.http.get<CompteAlerte[]>(url);
  }

  getAccountsToUpdate(): Observable<CompteMap> {
    const url = `${this.apiEndPoint}/comptes/accounts-update`;
    return this.http.get<CompteMap>(url);
  }

  sendMail(dto: any): Observable<ResponseModel> {
    const url = `${this.apiEndPoint}/comptes/notify-mail`;
    return this.http.put<ResponseModel>(url, dto);
  }

}
