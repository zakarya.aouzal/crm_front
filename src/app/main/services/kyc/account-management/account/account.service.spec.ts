import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AccountFilterModel, AccountPMModel, AccountPPModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { AccountService } from './account.service';


fdescribe('AccountService', () => {
  let service: AccountService;
  let httpTestingController: HttpTestingController;
  let apiEndPoint = environment.apiEndPoint;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(AccountService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('\'getListAccountPP\' should return Observable match the right data', () => {
    const accountPPListMock: AccountPPModel[] = [];
    const filter: AccountFilterModel = null;
    let result: AccountPPModel[] = null;

    service.getListAccountPP(filter)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${apiEndPoint}/comptes/pp/filter`
    });

    req.flush(accountPPListMock);

    expect(req.request.body).toEqual(filter);
    expect(result).toEqual(accountPPListMock);

  });

  it('\'getListAccountPM\' should return Observable match the right data', () => {
    const accountPMListMock: AccountPMModel[] = [];
    const filter: AccountFilterModel = null;
    let result: AccountPMModel[] = null;

    service.getListAccountPM(filter)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${apiEndPoint}/comptes/pm/filter`
    });

    req.flush(accountPMListMock);

    expect(req.request.body).toEqual(filter);
    expect(result).toEqual(accountPMListMock);

  });

  it('\'getAccountPPByCode\' should return Observable match the right data', () => {
    const accountPPMock: AccountPPModel = null;

    const accountCode: string = 'PM_1002';
    let result: AccountPPModel = null;

    service.getAccountPPByCode(accountCode)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/comptes/pp/${accountCode}`
    });

    req.flush(accountPPMock);
  
    expect(result).toEqual(accountPPMock);

  });

  it('\'getAccountPMByCode\' should return Observable match the right data', () => {
    const accountPMMock: AccountPMModel = null;

    const accountCode: string = 'PM_1002';
    let result: AccountPMModel = null;

    service.getAccountPMByCode(accountCode)
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/comptes/pm/${accountCode}`
    });

    req.flush(accountPMMock);
  
    expect(result).toEqual(accountPMMock);

  });

});
