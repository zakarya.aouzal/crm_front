import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OperationFilterModel, OperationModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class OperationsService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getListOperations(filter: OperationFilterModel): Observable<OperationModel[]> {
    const url = `${this.apiEndPoint}/crm/operation`;
    return this.http.post<OperationModel[]>(url,filter);
  }

  



}
