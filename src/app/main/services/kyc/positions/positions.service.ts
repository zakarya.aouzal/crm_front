import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  PositionMoyFilterModel,
  PositionMoyModel,
  PositionSousRachatFilterModel,
  PositionSousRachatModel
} from 'src/app/main/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class PositionsService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getListPositionsMoy(filter: PositionMoyFilterModel): Observable<PositionMoyModel[]> {
    const url = `${this.apiEndPoint}/crm/position/moy`;
    return this.http.post<PositionMoyModel[]>(url, filter);
  }

  getLisPositionsSousRachat(filter: PositionSousRachatFilterModel): Observable<PositionSousRachatModel[]> {
    const url = `${this.apiEndPoint}/crm/position/sous-rachat`;
    return this.http.post<PositionSousRachatModel[]>(url, filter);
  }


}
