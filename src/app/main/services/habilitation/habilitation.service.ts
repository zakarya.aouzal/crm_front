import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';
import { Observable, of, Subject } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { HabilitationModel, RoleModel } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class HabilitationService {

  private apiEndPoint = environment.apiEndPoint;

 
  constructor(private http: HttpClient,private roleService:NgxRolesService,) { }

  getHabalitation(): Observable<HabilitationModel> {
    const url = `${this.apiEndPoint}/habilitations`;
    return this.http.get<HabilitationModel>(url).pipe(

      );
  }




 



}
