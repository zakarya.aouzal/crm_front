import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountActivityArea,AccountRepartitionByCatArea,AccountRepartitionByValArea, AccountPMModel, AccountPPModel, DashboardModel, AccountPotentialAdviceModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';

@Injectable()
export class DashboardService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getTotalAccountPMByCountry(): Observable<DashboardModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pm/pays`;
    return this.http.get<DashboardModel[]>(url);
  }

  

  getTotalAccountPPByCountry(): Observable<DashboardModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pp/pays`;
    return this.http.get<DashboardModel[]>(url);
  }

  getTotalAccountPPByDate(): Observable<DashboardModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pp/accounts`;
    return this.http.get<DashboardModel[]>(url);
  }

  getTotalAccountPMByDate(): Observable<DashboardModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pm/accounts`;
    return this.http.get<DashboardModel[]>(url);
  }

  getRiskAccountPP(): Observable<DashboardModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pp/risque`;
    return this.http.get<DashboardModel[]>(url);
  }

  getRiskAccountPM(): Observable<DashboardModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pm/risque`;
    return this.http.get<DashboardModel[]>(url);
  }

  getPotentialAdvice(): Observable<AccountPotentialAdviceModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/potentiel/conseiller`;
    return this.http.get<AccountPotentialAdviceModel[]>(url);
  }

  getTotalAccountByActivityArea(): Observable<AccountActivityArea[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/accounts/sect-activite`;
    return this.http.get<AccountActivityArea[]>(url);
  }

  getTopAccountPP(): Observable<AccountPPModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pp/top`;
    return this.http.get<AccountPPModel[]>(url);
  }

  getTopAccountPM(): Observable<AccountPMModel[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/pm/top`;
    return this.http.get<AccountPMModel[]>(url);
  }

  //fiche client graphes

  getRepartitionByCategory(entite:String): Observable<AccountRepartitionByCatArea[]> {
    console.log(entite)
    const url = `${this.apiEndPoint}/crm/dashboard/client/repartition/category/${entite}`;
    return this.http.get<AccountRepartitionByCatArea[]>(url);
  }

  getRepartitionByValues(entite:String): Observable<AccountRepartitionByValArea[]> {
    const url = `${this.apiEndPoint}/crm/dashboard/client/repartition/values/${entite}`;
    return this.http.get<AccountRepartitionByValArea[]>(url);
  }
}
