import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {AccountPPModel, OperationFilterModel, OperationModel} from '../../models';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {CarnetOrdreFilterModel, CarnetOrdreModel, OrdreModel} from '../../models/carnet-ordre/carnet-ordre.model';

@Injectable({
  providedIn: 'root'
})
export class CarnetOrdreService {

  private apiEndPoint = environment.apiEndPoint;
  constructor(private http: HttpClient) { }


  
  getListCarnetOrdre(filter: CarnetOrdreFilterModel): Observable<CarnetOrdreModel[]>{
    const url = `${this.apiEndPoint}/carnetOrdre`;
    return this.http.post<CarnetOrdreModel[]>(url,filter);
  }

  passerOrdre(payload : OrdreModel) : Observable<any>{
    const url = `${this.apiEndPoint}/carnetOrdre/passerOrdre`;
    return this.http.post<OrdreModel>(url,payload);
  }
}

