import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import { DateVLItemModel, DetailFondsFilterModel, DetailFondsModel} from '../../models';

import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailFondsService {

  private apiEndPoint = environment.apiEndPoint;
  constructor(private http: HttpClient) { }
  
  getListDetailFonds(filter: DetailFondsFilterModel): Observable<DetailFondsModel[]>{
    const url = `${this.apiEndPoint}/detailFonds`;
    return this.http.post<DetailFondsModel[]>(url,filter);
  }

  getListDatesVL(fond?: string): Observable<DateVLItemModel[]> {
    let url = `${this.apiEndPoint}/opcvm/date_vl`;
    if (fond) {
      url = url.concat(`?fond=${fond}`);
    }
    return this.http.get<DateVLItemModel[]>(url);
  }

}
