import {OpcvmFilterModel, RatiosAMMCFilterModel} from '../../models';
import {Observable} from 'rxjs';
import {RatiosAMMCModel} from '../../models/asset/ratios-ammc.model';
import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RatiosAmmcService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) {
  }
  getRatiosAmmcDetails(payload: RatiosAMMCFilterModel): Observable<any>{
    let url = `${this.apiEndPoint}/ratiosAmmc/details_vl`;
    return this.http.post<RatiosAMMCModel[]>(url, payload);
  }
  getDataValMinMax(payload: RatiosAMMCFilterModel) : Observable<any>{
    let url = `${this.apiEndPoint}/ratiosAmmc/dataValMinMax`;
    return this.http.post<any>( url, payload );
  }
}
