import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {GenericItemModel, DateVLItemModel, RatiosAMMCFilterModel} from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { rootCertificates } from 'tls';

import { OpcvmFilterModel } from 'src/app/main/models';



@Injectable()
export class OpcvmService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }




  getListDatesVL(fond?: string): Observable<DateVLItemModel[]> {
    let url = `${this.apiEndPoint}/opcvm/date_vl`;
    if (fond) {
      url = url.concat(`?fond=${fond}`);
    }
    return this.http.get<DateVLItemModel[]>(url);
  }

  getVLDetails(payload: OpcvmFilterModel) {

    return this.http
      .get<Array<any>>(
        `${this.apiEndPoint}/opcvm/vldetail/?datevl=${payload.date_vl}&fond=${payload.fond}&client=${payload.client}`,
      );
  }


  getListDataPortfVal(payload:OpcvmFilterModel){
    return this.http
    .get<Array<any>>(
      `${this.apiEndPoint}/opcvm/portfvalorise/?datevl=${payload.date_vl}&fond=${payload.fond}&client=${payload.client}`,
    );
  }


  getListOperations(payload:OpcvmFilterModel){
    return this.http
    .get<Array<any>>(
      `${this.apiEndPoint}/opcvm/opencours/?datevl=${payload.date_vl}&fond=${payload.fond}&client=${payload.client}`,
    );
  }


  getPieChartClasseActif(payload: OpcvmFilterModel) {
    return this.http
      .get<Array<any>>(
        `${this.apiEndPoint}/opcvm/piechartactifparsousclasse?datevl=${payload.date_vl}&fond=${payload.fond}&client=${payload.client}`,
      );
  }
  getAxisChartEvolutionVL(payload: OpcvmFilterModel) {
    return this.http
      .get<Array<any>>(
        `${this.apiEndPoint}/opcvm/axischartevolutionvl?datevl=${payload.date_vl}&fond=${payload.fond}&client=${payload.client}`,
      );
  }


}
