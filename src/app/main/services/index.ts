export * from './dashboard/dashboard.service';
export * from './kyc/account-management/account/account.service';
export * from './kyc/account-management/attachment/attachment.service';
export * from './kyc/account-management/compte-contact/compte-contact.service';
export * from './kyc/account-management/contact/contact.service';
export * from './kyc/account-management/event/event.service';
export * from './kyc/account-management/ged/ged.service';
export * from './kyc/account-management/incident/incident.service';
export * from './kyc/account-management/incident/comment.service';
export * from './kyc/account-management/history-activity/history-activity.service';
export * from './kyc/account-management/espece-account/espece.service'
export * from './kyc/account-management/opportunite/opportunite.service';
export * from './kyc/account-management/task/task.service';
export * from './kyc/operations/operations.service';
export * from './kyc/positions/positions.service';
export * from './kyc/account-management/opportunite/opportunite.service';
export * from './reference/reference.service';
export * from './habilitation/habilitation.service';
export * from './asset/opcvm.service';
export * from './asset/detail-fonds.service';
export * from './users/gestion-utilisateurs.service';
export * from './users/gestion-permissions.service';



