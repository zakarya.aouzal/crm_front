import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { GenericItemModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { ReferenceService } from './reference.service';


fdescribe('ReferenceService', () => {
  let service: ReferenceService;
  let httpTestingController: HttpTestingController;
  let apiEndPoint = environment.apiEndPoint;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReferenceService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ReferenceService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('\'getListCountries\' should return Observable match the right data', () => {
    const countriesMock: GenericItemModel[] = [{
      code: 'MA',
      value: 'MAROC'
    }];

    let result: GenericItemModel[] = null;

    service.getPaysList()
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/pays`
    });

    req.flush(countriesMock);

    expect(result).toEqual(countriesMock);

  });

  it('\'getListAccountManager\' should return Observable match the right data', () => {
    const accountManagerMock: GenericItemModel[] = [{
      code: "SOS",
      value: "soufaine oubalkass"
    }];

    let result: GenericItemModel[] = null;

    service.getGestionnaireFronts()
      .subscribe(response => {
        result = response;
      });

    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${apiEndPoint}/gestionnaire-fronts`
    });

    req.flush(accountManagerMock);

    expect(result).toEqual(accountManagerMock);

  });

});
