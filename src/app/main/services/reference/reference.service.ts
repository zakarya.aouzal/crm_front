import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { GenericItemModel } from 'src/app/main/models';
import { environment } from 'src/environments/environment';
import { rootCertificates } from 'tls';

@Injectable({
  providedIn: 'root'
})
export class ReferenceService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getYesNoList(): Observable<GenericItemModel[]> {
    return of([{code: 'OUI', value: 'Oui'}, {code: 'NON', value: 'Non'}]);
  }

  getPaysList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/pays`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getTypeResidenceList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/types-residences`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getGestionnaireFronts(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/gestionnaire-fronts`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListPortefeuilles(entite?: string): Observable<GenericItemModel[]> {
    let url = `${this.apiEndPoint}/portefeuilles`;
    if (entite) {
      url = url.concat(`?entite=${entite}`);
    }
    return this.http.get<GenericItemModel[]>(url);
  }


  getListclient(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/clients`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListfonds(client?: string): Observable<GenericItemModel[]> {
    let url = `${this.apiEndPoint}/fonds`;
    if (client) {
      url = url.concat(`?client=${client}`);
    }
    return this.http.get<GenericItemModel[]>(url);
  }


  getListEntites(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/entites`;
    return this.http.get<GenericItemModel[]>(url);
  }


  getListGEDType(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/types-docs`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListStatuts(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/statuts`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListTypeTickets(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/crm/incidents/types`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListOrigines(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/origines`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListPriorites(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/priorites`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getListGEDPublicTypes():Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/KYCListeDocGedPublic`;
    return this.http.get<GenericItemModel[]>(url);
  }


  getListGEDPrivateType(type:string):Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/KYCListeDocGed/${type}`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getTypeProduits():Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/prod-types`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getCategories(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/categories`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getCategoriesInvList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/categories-inv`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getCivilites(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/civilites`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getClassifications(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/classifications`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getFormesJuridiques(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/formes-juridiques`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getHorizonPlacement(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/horizon_placement`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getNiveauInvList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/niveau-inv`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getObjectifInvList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/objectif-inv`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getOriginesFondsList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/origine-fonds`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getNationalites(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/tiers-nations`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getTypesControles(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/types-controles`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getTypesDocs(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/types-docs`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getSectActivitesList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/sec-activ`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getProfilInvList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/profil-inv`;
    return this.http.get<GenericItemModel[]>(url);
  }

  getUniversInvList(type: string): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/univers-invt`;
    const params = new HttpParams().set('type', type);
    return this.http.get<GenericItemModel[]>(url, {params});
  }

  getInstrumentsList(type: string, values: string): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/instruments`;
    const params = new HttpParams().set('type', type).set('values', values);
    return this.http.get<GenericItemModel[]>(url, {params});
  }

  getTypeFundsList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/fund-types`;
    return this.http.get<[]>(url);
  }

  getFundsList(): Observable<GenericItemModel[]>  {
    const url = `${this.apiEndPoint}/funds`;
    return this.http.get<[]>(url);
  }

  getSensList(): Observable<GenericItemModel[]>{
    const url = `${this.apiEndPoint}/sens`;
    return this.http.get<[]>(url);
  }

  getTypesClientList(): Observable<GenericItemModel[]>{
    const url = `${this.apiEndPoint}/typeClient`;
    return this.http.get<[]>(url);
  }

  getClientsList(): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/clients`;
    return this.http.get<[]>(url);
  }

  getIndicateursList(fond,dateDebut,dateFin): Observable<GenericItemModel[]> {
    const url = `${this.apiEndPoint}/indicateurs?fond=`+fond+"&dateDebut="+dateDebut+"&dateFin="+dateFin;
    return this.http.get<[]>(url);
  }

  getCompteEspeceList(client : string) : Observable<GenericItemModel[]>{
    const url = `${this.apiEndPoint}/compteEspece?client=`+client ;
    return this.http.get<[]>(url);
  }

  getInfoSousRachatTitre(client: string, fond: string) : Observable<any>{
    const url = `${this.apiEndPoint}/infoSousRachatTitre?client=`+client+"&fond="+fond ;
    return this.http.get<any>(url);
  }

  getFondsPasserOrdreList() : Observable<GenericItemModel[]>{
    const url = `${this.apiEndPoint}/fondsPasserOrdre` ;
    return this.http.get<[]>(url);
  }
}

