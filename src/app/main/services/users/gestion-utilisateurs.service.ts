import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ResponseModel } from 'src/app/core/models';
import { environment } from 'src/environments/environment';
import { UsersKeycloakModel, UsersKeycloakPayloadModel, UsersWebModel } from '../../models';




@Injectable()
export class UserService {

  private apiEndPoint = environment.apiEndPoint;

  constructor(private http: HttpClient) { }

  getUsersKeycloakList(): Observable<UsersKeycloakModel[]>{
    const url = `${this.apiEndPoint}/habilitations/KeycloakUsers`;
    return this.http.get<UsersKeycloakModel[]>(url);
  }

  getUsersWebList(): Observable<UsersWebModel[]>{
    const url = `${this.apiEndPoint}/habilitations/WebUsers`;
    return this.http.get<UsersWebModel[]>(url);
  }

  addUserItem(user: UsersWebModel): Observable<ResponseModel>{
    console.log('yeeeees',user)
    const url = `${this.apiEndPoint}/habilitations/AddWebUsers`;
    return this.http.post<ResponseModel>(url,user);
  }
  updateFlagExportUserItem(user: UsersKeycloakPayloadModel): Observable<ResponseModel>{
    console.log('yeeeees',user)
    const url = `${this.apiEndPoint}/habilitations/updateKeycloackUsers`;
    return this.http.put<ResponseModel>(url,user);
  }


  



  
  







}
