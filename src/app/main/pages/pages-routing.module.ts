import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { NotAuthorizedComponent } from 'src/app/shared/components/not-authorized/not-authorized.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { PermissionEnum, RoleEnum, RoleModel } from '../models';
import { ActivityGlobalComponent } from './activity-management/activites-consultation/activity-global/activity-global.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      // {
      //   path: 'activity-management',
      //   component: ActivityGlobalComponent,
      //   data: {
      //     breadcrumb: {
      //       label: 'BREADCRUMBS.PAGES.DASHBOARD'
      //     }
      //   }
      // },
      // {
      //   path: 'dashboard',
      //   component: DashboardComponent
      // },
      {
        path: 'prospects-management',
        loadChildren: () => import('./prospects-management/prospects-management.module').then(m => m.ProspectsManagementModule),
        },
      {
        path: 'carnet-ordre-management',
        loadChildren: () => import('./carnet-ordre-management/carnet-ordre/carnet-ordre.module').then(m => m.CarnetOrdreModule),
      },
      {
        path: 'contacts-management',
        loadChildren: () => import('./contacts-management/contacts-management.module').then(m => m.ContactsManagementModule),
      },
      {
        path: 'documents-management',
        loadChildren: () => import('./documents-management/documents-management.module').then(m => m.DocumentsManagementModule),
      },
      {
        path: 'clients-management',
        loadChildren: () => import('./clients-management/clients-management.module').then(m => m.ClientsManagementModule),
      },
       {
        path: 'activity-management',
        loadChildren: () => import('./activity-management/activity-management.module').then(m => m.ActivityManagementModule),
      }, {
        path: 'kyc',
        loadChildren: () => import('./kyc/kyc.module').then(m => m.KycModule),
      },
      
      {
        path: 'asset',
        loadChildren: () => import('./asset-management/asset.management.module').then(m => m.AssetManagementModule),
      },
      {
        path: 'users',
        loadChildren: () => import('./users-management/users-management.module').then(m => m.UsersManagementModule),
      },
      {
        path: '',
        redirectTo: 'activity-management',
        pathMatch: 'full'
      },
      {
        path: 'not-authorized',
        component: NotAuthorizedComponent
      },{
        path: '**',
        component: ActivityGlobalComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]

})
export class PagesRoutingModule { }
