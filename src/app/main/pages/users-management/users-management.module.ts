import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersManagementRoutingModule } from './users-management-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { GestionUtilisateursComponent } from './gestion-utilisateurs/gestion-utilisateurs.component';
import { GestionPermissionsComponent } from './gestion-permissions/gestion-permissions.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PermissionService, UserService } from '../../services';
import { GestionUtilisateursListComponent } from './gestion-utilisateurs/gestion-utilisateurs-list/gestion-utilisateurs-list.component';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from 'src/app/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { UsersRolesPermissionsComponent } from './users-roles-permissions/users-roles-permissions.component';

@NgModule({
  declarations: [GestionUtilisateursComponent, GestionPermissionsComponent, GestionUtilisateursListComponent, UsersRolesPermissionsComponent],
  imports: [
    CommonModule,
    UsersManagementRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    FormsModule,
    SharedModule,
  ],
  exports:[],
    providers:[
      UserService, PermissionService
    ]
})
export class UsersManagementModule { }
