import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/shared/components/page-not-found/page-not-found.component';
import { GestionPermissionsComponent } from './gestion-permissions/gestion-permissions.component';
import { GestionUtilisateursComponent } from './gestion-utilisateurs/gestion-utilisateurs.component';
import { UsersRolesPermissionsComponent } from './users-roles-permissions/users-roles-permissions.component';


const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: {
        label: 'BREADCRUMBS.CLIENT_MANAGEMENT.TITLE'
      }
    },
    children: [
      {
        path: 'gestion-utilisateurs',
        component: GestionUtilisateursComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.USERS.UTILISATEURS'
          }
        },
      },
      {
        path: 'roles-permissions',
        component: UsersRolesPermissionsComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.USERS.PERMISSIONS'
          }
        },
      }, {
        path: 'gestion-permissions',
        component: GestionPermissionsComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.USERS.PERMISSIONS'
          }
        },
      },
      {
        path: '',
        redirectTo: 'gestion-utilisateurs',
        pathMatch: 'full'
      }, {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UsersManagementRoutingModule { }
