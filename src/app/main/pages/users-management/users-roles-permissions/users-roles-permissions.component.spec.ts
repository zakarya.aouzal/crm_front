import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRolesPermissionsComponent } from './users-roles-permissions.component';

describe('UsersRolesPermissionsComponent', () => {
  let component: UsersRolesPermissionsComponent;
  let fixture: ComponentFixture<UsersRolesPermissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRolesPermissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRolesPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
