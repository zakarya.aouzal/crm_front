import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionUtilisateursListComponent } from './gestion-utilisateurs-list.component';

describe('GestionUtilisateursListComponent', () => {
  let component: GestionUtilisateursListComponent;
  let fixture: ComponentFixture<GestionUtilisateursListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionUtilisateursListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionUtilisateursListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
