import { Component, OnInit } from '@angular/core';
import { ResponseModel } from 'src/app/core/models';
import { UsersKeycloakModel, UsersKeycloakPayloadModel, UsersWebModel } from 'src/app/main/models';
import { UserService } from 'src/app/main/services';
import { DataTableOutputActionsModel, DataTableSettingsModel } from 'src/app/shared/models';
import {USERS_KEYCLOAK_LIST_SETTINGS, USERS_WEB_LIST_SETTINGS} from './gestion-utilisateurs-list-settings'
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-gestion-utilisateurs-list',
  templateUrl: './gestion-utilisateurs-list.component.html',
  styleUrls: ['./gestion-utilisateurs-list.component.scss']
})
export class GestionUtilisateursListComponent implements OnInit {
 
  userWebItem: UsersWebModel ;
  usersKeycloakSettings: DataTableSettingsModel = USERS_KEYCLOAK_LIST_SETTINGS;
  usersWebSettings: DataTableSettingsModel = USERS_WEB_LIST_SETTINGS;
  usersKeycloakList: UsersKeycloakModel[] = new Array<UsersKeycloakModel>(); 
  usersWebList: UsersWebModel[] = new Array<UsersWebModel>();
  redirect_URL:string='/pages/users/roles-permissions';

  constructor(private userService: UserService, private toastrService: ToastrService, private router: Router) { }

  ngOnInit(): void {
    this.loadUsersKeycloakList();
    this.loadUsersWebList();
  }

  loadUsersKeycloakList() { 
    this.userService.getUsersKeycloakList().subscribe(
      (value: any) => { this.usersKeycloakList = value ;
      console.log('utilisateurs Keycloak', this.usersKeycloakList) }
      
    );
  }

  loadUsersWebList() { 
    this.userService.getUsersWebList().subscribe(
      (value: any) => { this.usersWebList = value ;
      console.log('utilisateurs tpUtilisateur ', this.usersWebList) }
      
    );
  }

  onActionUserKeycloak(event: DataTableOutputActionsModel<UsersKeycloakModel>) {
    let user: UsersWebModel = null;
    let payloadModel: UsersKeycloakPayloadModel = null;
    if (event) {
      console.log('event', event)
      user = {
        code: event.item?.id,
        description: event.item?.first_name,
        fonction: "Back Office",
        groupe_portefeuille: "AppAdm",
        flag_actif: "N" ,
        trader: "N",
        risk  : "N",
        sales : "N",
        back  : "N",
        front : "O",
        canal : "Web",
        visibleAction : false 
      }
      payloadModel = {
        id : event.item.id,
        flag_export: "O",
      }
      if (!user) return;
      this.userService.addUserItem(user).subscribe((data:ResponseModel)=>{
        if(data.valid=true){
          this.loadUsersWebList() 
          this.userService.updateFlagExportUserItem(payloadModel).subscribe((data:ResponseModel)=>{
            if(data.valid=true){
              this.toastrService.success('L\'utilisateur a été exporté');
              this.loadUsersKeycloakList() 
            }
            else
            this.toastrService.success(' Utilisateur non exporté');
          })
        }

      })
      
    }
  }

 /** ON ACTION */
  onActionUserWeb(event: DataTableOutputActionsModel<UsersWebModel>) {
    if (event) {
      switch (event.actionType) {
       
        case 'VIEW':
          let id = event.item ? event.item.code : null;
          this.router.navigate([this.redirect_URL]);
          break;
      }
      // console.log('action', event)
    }
  }

 
  


  

 

  

  
 
  

  

}
