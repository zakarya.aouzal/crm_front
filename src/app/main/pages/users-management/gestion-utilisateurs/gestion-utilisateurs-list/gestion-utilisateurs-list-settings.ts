import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const USERS_KEYCLOAK_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'id',
      title: 'Id',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.USERS_KEYCLOAK_LIST.ID'
      }
    }, {
      name: 'first_name',
      title: 'First name',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.USERS_KEYCLOAK_LIST.FIRST_NAME'
      }
    }, {
      name: 'last_name',
      title: 'Last name',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.USERS_KEYCLOAK_LIST.LAST_NAME'
      }
    },
    
   

  ],actions: [DataTableActions.EXPORT]
}


export const USERS_WEB_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.USERS_WEB_LIST.CODE'
      }
    }, {
      name: 'description',
      title: 'Description',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.USERS_WEB_LIST.DESCRIPTION'
      }
    }
    
   

  ],actions: [DataTableActions.VIEW]
}
