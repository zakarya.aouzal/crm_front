import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { saveAs } from 'file-saver';
import * as JSZip from 'jszip';
import { GenericItemModel } from 'src/app/main/models';
import { DocumentService } from 'src/app/main/services/kyc/account-management/document/document.service';



@Component({
  selector: 'app-documents',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  documents:GenericItemModel[]=[]

  @Input()
  type: string;
  // @Input()
  // isBo: boolean;
  // @Input()
  // consult: boolean;
  // @Input()
  // isDraft : boolean;
  files1:Map<any, File | null> = new Map();
  
  zip: any;
  files: Map<any, File | null> = new Map();
  docsForm: FormGroup | any;
  allowedFiles: Array<string> = ['application/pdf', 'image/jpg', 'image/jpeg', 'image/png'];
  dirtyForm: boolean;
  constructor(private fb: FormBuilder,
    // private translateService:TranslateService,
    // private dialogService:DialogService,
    //private snackBarService:SnackBarService,
    private documentService: DocumentService) {
    this.docsForm = this.fb.group({
      docs: this.fb.group({})
    });

    this.files1.set(0,new File([],"file1.txt"));
    this.files1.set(1,new File([],"file2.txt"));
    this.files1.set(2,new File([],"file3.txt"));
  }

  ngOnInit() {
    this.dirtyForm= false;
    //KYCListeDocGedPP || KYCListeDocGedPM
    this.documentService.getDoc(this.type).subscribe(data => {
      this.documents = data;
      this.createdocsForm();
    })
  }

  createdocsForm() {
    for (let i = 0; i < this.documents!.length; i++) {
      this.docsForm.addControl("file" + i, this.fb.control(null,Validators.required))
    }
  }
  onFileSelected(event, i) {
  
    const file: File = event.target.files[0];
    if (!file || this.allowedFiles.indexOf(file.type) < 0 || file.size > 16000000) {
      this.docsForm.get("file" + i).setErrors({ required: true });
    }
    else {
      this.files.set(i, file);
    }
    this.dirtyForm=true;
  }
  remove(index) {
    this.docsForm.get("file" + index).setValue(null)
    this.files.set(index,null);
  }
  onClickInput(index) {

    this.docsForm.get("file" + index).markAsDirty()
  }
  onSubmit() {
    if (this.docsForm.valid) {
      var zip = new JSZip();
      this.files.forEach((f, i) => {
        if (f) {
          const name = this.documents[i]+ "." + f.name.split('.').pop();
          zip.file(name, f, { base64: true });
        }
      });
      zip.generateAsync({ type: "blob" })
        .then((content) => {
          this.documentService.uploadZip(content).subscribe(
            (msj) => {

              //TO DO
            },
            err => {
              //TO DO
            }
            )
        })
    }else{

      // document required message
 //     this.openDocumentsRequiredDialog();
    }
  }

  // openDocumentsRequiredDialog() {
  //   this.translateService.get('ERRORS_MESSAGES.DOCS_REQUIRED_FIELDS_REQUIRED')
  //     .subscribe(message => {
  //       this.dialogService.openConfirmationDialog(DialogType.WARNING, message).subscribe(result => {});
  //     });
  // }

  download(index) {
      this.documentService.download(this.documents[index]).subscribe(
        response => {
                let file = new Blob([response], { type: response.type });
                var fileURL = URL.createObjectURL(file);
                saveAs(fileURL, this.documents[index]);
          })

  }

  previewFile(index){
    var newWindowTab:any = window.open('', '_blank');

          let fileToPreview = this.files.get(index);
          if(fileToPreview){
            var fileURL = URL.createObjectURL(fileToPreview);
            newWindowTab.location.href=fileURL;

    }
  }


  getFormControlName(index) {
    return 'file' + index;
  }
}
