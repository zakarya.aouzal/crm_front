import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountService, GedService, ReferenceService} from "../../../services";
import {AccountPMModel, AccountPPModel, GenericItemModel} from "../../../models";
import {GedModel} from "../../../models/kyc/ged/ged.model";
import {finalize} from "rxjs/operators";
import {ToastrService} from "ngx-toastr";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import { AttachmentFormComponent } from '../attachment-form/attachment-form.component';

@Component({
  selector: 'app-new-document',
  templateUrl: './new-document.component.html',
  styleUrls: ['./new-document.component.scss']
})
export class NewDocumentComponent implements OnInit {

  gedFormGroup: FormGroup = null;

  // form
  showValidationsMsg: boolean = false;
  loading: boolean = false;

  gedTypes: GenericItemModel[] = [];

  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];

  @ViewChild(AttachmentFormComponent)
  attachementFormComponent: AttachmentFormComponent;

  confirmationModalRef: BsModalRef;

  selectedFiles: File[] = [];

  constructor(private formBuilder: FormBuilder,
              @Inject('pipeParameters') public pipeParameters: any,
              private gedService: GedService,
              private referenceService: ReferenceService,
              private accountService: AccountService,
              private modalService: BsModalService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.initFormGroup();
    this.loadListAccountPP();
    this.loadListAccountPM();
    this.loadGedPublicTypes();
  }

  /** CREATE FORM */
  initFormGroup(): void {
    this.gedFormGroup = this.formBuilder.group({
      visibility: ['public', [Validators.required]],
      compteType: ['PP'],
      compte: [''],
      documentType: ['', [Validators.required]],
      documentName: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });

    this.gedFormGroup.get('visibility').valueChanges.subscribe(value => {
      if (value === 'private') {
        this.gedFormGroup.controls['compte'].setValidators([Validators.required]);
      } else {
        this.gedFormGroup.controls['compte'].clearValidators();
      }
      this.gedFormGroup.controls['compte'].updateValueAndValidity();
    });
  }

  onVisibiltyChange(value: string): void {
    value == "public" ? this.loadGedPublicTypes() : this.loadGedPrivateTypes(this.gedFormGroup?.get('compteType')?.value);
  }

  onAccountTypeChange(value:string): void {
      this.loadGedPrivateTypes(value);
  }

  /** LOAD LIST OF ACCOUNTS PP */
  loadListAccountPP(): void {
    this.accountPPList = [];
    this.loading = true;
    this.accountService.getListAccountPP(null)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.accountPPList = response);
  }

  /** LOAD LIST OF ACCOUNTS PM */
  loadListAccountPM(): void {
    this.accountPMList = [];
    this.loading = true;
    this.accountService.getListAccountPM(null)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.accountPMList = response);
  }

  /** SUBMIT FORM */
  openSubmitModal(template): void {
    if (this.gedFormGroup?.invalid) {
      this.showValidationsMsg = true;
      return;
    }

    this.selectedFiles = this.attachementFormComponent.uploadedFiles;

    if (this.selectedFiles?.length === 0) {
      this.toastr.warning("Vous devez selectionnez au moins un fichier...");
      return;
    }

    this.confirmationModalRef = this.modalService.show(template);
  }

  /** UPLOAD PROCESS */
  onSubmit(): void {
    const dto: GedModel = {
      visibility: this.gedFormGroup?.get('visibility')?.value,
      documentType: this.gedFormGroup?.get('documentType')?.value,
      documentName: this.gedFormGroup?.get('documentName')?.value,
      description: this.gedFormGroup?.get('description')?.value
    };
    if (this.gedFormGroup?.get('visibility')?.value === 'private') {
      dto.compte = this.gedFormGroup?.get('compte')?.value;
      dto.compteType = this.gedFormGroup?.get('compteType')?.value;
    }

    this.loading = true;
    if (this.filesControlTest(this.selectedFiles)) {
      this.gedService.uploadDocuments(JSON.stringify(dto), this.selectedFiles)
        .pipe(finalize(() => this.loading = false))
        .subscribe(value => {
          if (value.valid) {
            this.toastr.success('Les fichier ont été ajoutés');
            this.attachementFormComponent.reset();
            this.gedFormGroup?.reset();
          } else {
            this.toastr.error('Il y avait une erreur pendant l\'ajout des fichiers : \n' + value.detail);
          }
        });
    } else {
      this.toastr.error('La taille totale des fichiers dépasse les 5mb');
    }
  }

  filesControlTest(files: Array<File>): boolean {
    let totalSize = 0;
    files.forEach(value => {
      totalSize += value.size;
    });
    return totalSize < (this.pipeParameters.ged.totalFilesSize * 1024 * 1024);
  }

  /** RESET FORM */
  onReset(): void {
    this.gedFormGroup.reset();
  }

  /** LOAD GED TYPES */
  loadGedPublicTypes(): void {
    this.referenceService.getListGEDPublicTypes()
      .subscribe(value => this.gedTypes = value);
  }

  loadGedPrivateTypes(type:string): void {
    this.referenceService.getListGEDPrivateType(type)
      .subscribe(value => this.gedTypes = value);
  }
}
