import {Component, Input, OnInit} from '@angular/core';
import * as FileSaver from 'file-saver';
import {AttachmentAccountModel} from 'src/app/main/models';
import {AttachmentService} from 'src/app/main/services';

@Component({
  selector: 'app-attachement',
  templateUrl: './attachement.component.html',
  styleUrls: ['./attachement.component.scss']
})
export class AttachementComponent implements OnInit {

  @Input()
  set accountCode(value: string) {
    this.loadAttachmentsByAccountCode(value);
  }

  @Input()
  set incidentCode(value: number) {
    if (value) {
      this.loadAttachmentsByIncidentCode(value);
    }
  }

  @Input()
  hideHeader: boolean = false;

  attachments: AttachmentAccountModel[] = [];

  constructor(private attachmentService: AttachmentService) {
  }

  ngOnInit(): void {
  }

  /** LOAD ATTACHMENTS BY ACCOUNT */
  loadAttachmentsByAccountCode(accountCode: string) {
    this.attachments = [];
    if (!accountCode) return;
    this.attachmentService.getListAttachmentsByAccount(accountCode)
      .subscribe(response => {
        this.attachments = response;
      })
  }

  /** LOAD ATTACHMENTS BY INCIDENT */
  loadAttachmentsByIncidentCode(incidentCode: number) {
    this.attachments = [];
    if (!incidentCode) return;
    this.attachmentService.getListAttachmentsByIncident(incidentCode)
      .subscribe(response => {
        this.attachments = response;
      });
  }

  /** DOWNLOAD ATTACHMENT */
  onDownload(attachement: AttachmentAccountModel) {
    if (attachement && attachement.num_doc && attachement.fileName) {
      this.attachmentService.getAttachment(attachement.num_doc)
        .subscribe(response => {
          if (response) {
            FileSaver.saveAs(response, `${attachement.fileName}`);
          }
        })
    }
  }



  /** GET FILE EXTENSION */
  getFileExtension(fineName: string): string {
    let fileType: string = null;
    const formatImage = ['jpeg', 'jpg', 'png'];
    const formatExcel = ['xlsx', 'xlsm', 'xlsb', 'xltx', 'xltm', 'xls', 'xlt', 'xls', 'xml', 'xml', 'xlam', 'xla', 'xlw', 'xlr'];
    const formatWord = ['doc', 'docm', 'docx', 'docx', 'dot', 'dotm', 'dotx'];
    if (fineName) {
      if (formatExcel.includes(fineName.split('.').pop().toLocaleLowerCase())) {
        fileType = 'excel';
      } else if (formatWord.includes(fineName.split('.').pop().toLocaleLowerCase())) {
        fileType = 'word';
      } else if (formatImage.includes(fineName.split('.').pop().toLocaleLowerCase())) {
        fileType = 'image';
      } else {
        fileType = fineName.split('.').pop().toLocaleLowerCase();
      }
    }
    return fileType;
  }

}
