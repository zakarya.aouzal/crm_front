import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/shared/components/page-not-found/page-not-found.component';
import { NewDocumentComponent } from './new-document/new-document.component';



const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: {
        label: 'BREADCRUMBS.DOCUMENT_MANAGEMENT.TITLE'
      }
    },
    children: [
      {
        path: 'new-document',
        component: NewDocumentComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.KYC.NEW_DOCUMENT'
          }
        },
      },
      {
        path: '',
        redirectTo: 'new-document',
        pathMatch: 'full'
      }, {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DocumentsManagementRoutingModule { }
