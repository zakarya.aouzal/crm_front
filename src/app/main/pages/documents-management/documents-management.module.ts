import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from '@iplab/ngx-file-upload';

import {BsModalService, ModalModule} from "ngx-bootstrap/modal";
import { SharedModule } from 'src/app/shared/shared.module';
import { AccountService, GedService, ReferenceService } from '../../services';
import { DocumentService } from '../../services/kyc/account-management/document/document.service';
import {DocumentsManagementRoutingModule} from './documents-management-routing.module'
import { NewDocumentComponent } from './new-document/new-document.component';
import  {AttachmentFormComponent} from './attachment-form/attachment-form.component';
import  {DocumentComponent} from './document/document.component';
import { AttachementComponent } from './attachement/attachement.component';
@NgModule({
  declarations: [
    AttachmentFormComponent,
    DocumentComponent,
    NewDocumentComponent,
    AttachementComponent
  ],
  imports: [
    CommonModule,
    DocumentsManagementRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule,
    ModalModule.forRoot()
  ],
  exports: [
    AttachmentFormComponent,
    DocumentComponent,
    NewDocumentComponent,
    AttachementComponent
  ],
  providers: [
    ReferenceService,
    BsModalService,
    DocumentService,
    AccountService,
    GedService
  ]
})
export class DocumentsManagementModule { }
