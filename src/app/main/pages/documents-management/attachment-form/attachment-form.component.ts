import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-attachment-form',
  templateUrl: './attachment-form.component.html',
  styleUrls: ['./attachment-form.component.scss']
})
export class AttachmentFormComponent implements OnInit {

  @Input()
  height: string = '100%';

   uploadedFiles: Array<File> = [];

  constructor() { }

  ngOnInit(): void {
  }

  reset(): void {
    this.uploadedFiles = [];
  }
}
