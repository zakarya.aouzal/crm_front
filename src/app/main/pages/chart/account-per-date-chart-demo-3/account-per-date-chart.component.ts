import { finalize } from 'rxjs/operators';
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ReplaySubject, Subscription } from 'rxjs';
import { DashboardModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-per-date-chart-demo-3',
  templateUrl: './account-per-date-chart.component.html',
  styleUrls: ['./account-per-date-chart.component.scss']
})
export class AccountPerDateChartDemo3Component implements OnInit, OnDestroy {

  // data
  totalAccountPPByDateList: DashboardModel[] = null;
  totalAccountPMByDateList: DashboardModel[] = null;

  // loading
  readonly loadingSubject = new ReplaySubject<{ loadingPP?: boolean; loadingPM?: boolean }>();
  loadingSubscription: Subscription = new Subscription();
  loadingPP: boolean = null;
  loadingPM: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadTotalAccountPPByDate();
    this.loadTotalAccountPMByDate();

    this.loadingSubscription = this.loadingSubject.subscribe(value => {
      if (value) {
        this.loadingPP = (value.loadingPP !== undefined) ? value.loadingPP : this.loadingPP;
        this.loadingPM = (value.loadingPM !== undefined) ? value.loadingPM : this.loadingPM;
      }
      if (this.loadingPP === false && this.loadingPM === false) {
        this.createChart();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription?.unsubscribe();
    }
  }

  /** LOAD TOTAL ACCOUNT PP BY DATE */
  loadTotalAccountPPByDate() {
    this.totalAccountPPByDateList = null;
    this.loadingSubject.next({ loadingPP: true });
    this.dashboardService.getTotalAccountPPByDate()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPP: false });
      }))
      .subscribe(response => {
        this.totalAccountPPByDateList = response;
      })
  }

  /** LOAD TOTAL ACCOUNT PM BY DATE */
  loadTotalAccountPMByDate() {
    this.totalAccountPMByDateList = null;
    this.loadingSubject.next({ loadingPM: true });
    this.dashboardService.getTotalAccountPMByDate()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPM: false });
      }))
      .subscribe(response => {
        this.totalAccountPMByDateList = response;
      })
  }

  /** CREATE ACCOUNT PER DATE CHART */
  createChart() {

    let categories: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let accountPPData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let accountPMData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    // create data
    if (this.totalAccountPPByDateList) {
      for (let totalAccountPPByDate of this.totalAccountPPByDateList) {
        if (totalAccountPPByDate.mois && totalAccountPPByDate.total) {
          let month: number = Number(totalAccountPPByDate.mois);
          accountPPData[month - 1] = accountPPData[month - 1] + totalAccountPPByDate.total;
        }
      }
    }

    if (this.totalAccountPMByDateList) {
      for (let totalAccountPM of this.totalAccountPMByDateList) {
        if (totalAccountPM.mois && totalAccountPM.total) {
          let month: number = Number(totalAccountPM.mois);
          accountPMData[month - 1] = accountPMData[month - 1] + totalAccountPM.total;
        }
      }
    }

    // Make monochrome colors
var pieColors = (function () {
  var colors = [],
      base = Highcharts.getOptions().colors[0],
      i;

  for (i = 0; i < 10; i += 1) {
      // Start out with a darkened base color (negative brighten), and end
      // up with a much brighter color
      colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

    // options
    let options: any =  {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie',
          height: 280,
      },
      title: {
          text: ''
      },
      subtitle: {
        text: 'Historic World Population by Region'
    },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      accessibility: {
          point: {
              valueSuffix: '%'
          }
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              colors: pieColors,
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                  distance: -50,
                  filter: {
                      property: 'percentage',
                      operator: '>',
                      value: 4
                  }
              }
          }
      },
      series: [{
          name: 'Share',
          data: [
              { name: 'Chrome', y: 61.41 },
              { name: 'Internet Explorer', y: 11.84 },
              { name: 'Firefox', y: 10.85 },
              { name: 'Edge', y: 4.67 },
              { name: 'Safari', y: 4.18 },
              { name: 'Other', y: 7.05 }
          ]
      }]
  };

  // create
  const element = this.el.nativeElement.querySelector('#account-per-date-chart-container');
  Highcharts.chart(element, options);


};

};
