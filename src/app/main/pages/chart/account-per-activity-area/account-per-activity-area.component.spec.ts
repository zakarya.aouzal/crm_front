import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPerActivityAreaComponent } from './account-per-activity-area.component';

describe('AccountPerActivityAreaComponent', () => {
  let component: AccountPerActivityAreaComponent;
  let fixture: ComponentFixture<AccountPerActivityAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPerActivityAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPerActivityAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
