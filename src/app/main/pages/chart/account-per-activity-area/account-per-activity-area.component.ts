import { finalize } from 'rxjs/operators';
import { Component, ElementRef, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { AccountActivityArea, AccountRepartitionByCatArea } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-per-activity-area',
  templateUrl: './account-per-activity-area.component.html',
  styleUrls: ['./account-per-activity-area.component.scss']
})
export class AccountPerActivityAreaComponent implements OnInit {


   // data
   accountRepartitionAreaList: AccountRepartitionByCatArea[] = null;

   // loading
   loading: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadRepartitionByCategory();
  }


  /** LOAD TOTAL ACCOUNT BY ACTIVITY AREA */
  loadRepartitionByCategory() {
    this.accountRepartitionAreaList = null;
    this.loading = true;
    this.dashboardService.getRepartitionByCategory("P0046")
      .pipe((finalize(() => {
        this.loading = false;
      })))
      .subscribe(response => {
        this.accountRepartitionAreaList = response;
        this.createChart();
      })
  }


  /** CREATE ACCOUNT REPARTITION CHART */
  createChart() {

    let data: [{}] = [{}];

    // create data
    if (this.accountRepartitionAreaList) {
      for (let accountActivityArea of this.accountRepartitionAreaList) {
        data.push({
          name: accountActivityArea.titre_valeur,
          y: accountActivityArea.valo,
          title:accountActivityArea.titre_cat,
      });
      }
    }

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 65,
          beta: 0
        },
        //backgroundColor: '#e4f0f6',
        
      },
      title: {
        text: this.translate.instant('DASHBOARD.COMPTE_NBR_SECT_ACTV'),
        style: {
          fontSize: '16px'
        }
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      tooltip: {
        pointFormat: '<b> Value:{point.y} ({point.percentage:.1f}%)</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: true,
            format: '{point.title}'
          },
          size:350
        }
      },
      series: [{
        type: 'pie',
        name: this.translate.instant('DASHBOARD.COMPTE_NMB'),
        data: data
      }]
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-activity-area-container');
    Highcharts.chart(element, options);
  }

}
