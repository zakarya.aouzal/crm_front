import { finalize } from 'rxjs/operators';
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ReplaySubject, Subscription } from 'rxjs';
import { DashboardModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-per-date-chart',
  templateUrl: './account-per-date-chart.component.html',
  styleUrls: ['./account-per-date-chart.component.scss']
})
export class AccountPerDateChartComponent implements OnInit, OnDestroy {

  // data
  totalAccountPPByDateList: DashboardModel[] = null;
  totalAccountPMByDateList: DashboardModel[] = null;

  // loading
  readonly loadingSubject = new ReplaySubject<{ loadingPP?: boolean; loadingPM?: boolean }>();
  loadingSubscription: Subscription = new Subscription();
  loadingPP: boolean = null;
  loadingPM: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadTotalAccountPPByDate();
    this.loadTotalAccountPMByDate();

    this.loadingSubscription = this.loadingSubject.subscribe(value => {
      if (value) {
        this.loadingPP = (value.loadingPP !== undefined) ? value.loadingPP : this.loadingPP;
        this.loadingPM = (value.loadingPM !== undefined) ? value.loadingPM : this.loadingPM;
      }
      if (this.loadingPP === false && this.loadingPM === false) {
        this.createChart();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription?.unsubscribe();
    }
  }

  /** LOAD TOTAL ACCOUNT PP BY DATE */
  loadTotalAccountPPByDate() {
    this.totalAccountPPByDateList = null;
    this.loadingSubject.next({ loadingPP: true });
    this.dashboardService.getTotalAccountPPByDate()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPP: false });
      }))
      .subscribe(response => {
        this.totalAccountPPByDateList = response;
      })
  }

  /** LOAD TOTAL ACCOUNT PM BY DATE */
  loadTotalAccountPMByDate() {
    this.totalAccountPMByDateList = null;
    this.loadingSubject.next({ loadingPM: true });
    this.dashboardService.getTotalAccountPMByDate()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPM: false });
      }))
      .subscribe(response => {
        this.totalAccountPMByDateList = response;
      })
  }

  /** CREATE ACCOUNT PER DATE CHART */
  createChart() {

    let categories: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let accountPPData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let accountPMData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    // create data
    if (this.totalAccountPPByDateList) {
      for (let totalAccountPPByDate of this.totalAccountPPByDateList) {
        if (totalAccountPPByDate.mois && totalAccountPPByDate.total) {
          let month: number = Number(totalAccountPPByDate.mois);
          accountPPData[month - 1] = accountPPData[month - 1] + totalAccountPPByDate.total;
        }
      }
    }

    if (this.totalAccountPMByDateList) {
      for (let totalAccountPM of this.totalAccountPMByDateList) {
        if (totalAccountPM.mois && totalAccountPM.total) {
          let month: number = Number(totalAccountPM.mois);
          accountPMData[month - 1] = accountPMData[month - 1] + totalAccountPM.total;
        }
      }
    }

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'line'
      },
      title: {
        text: this.translate.instant('DASHBOARD.COMPTE_MOIS_NBR'),
        style: {
          fontSize: '16px'
        }
      },
      subtitle: {
        text: null
      },
      xAxis: {
        categories: categories
      },
      yAxis: {
        title: {
          text: this.translate.instant('DASHBOARD.COMPTE_NMB'),
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [
        {
          name: 'Compte PP',
          type: 'line',
          data: accountPPData
        }, {
          name: 'Compte PM',
          type: 'line',
          data: accountPMData
        }],
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-date-chart-container');
    Highcharts.chart(element, options);
  }

}
