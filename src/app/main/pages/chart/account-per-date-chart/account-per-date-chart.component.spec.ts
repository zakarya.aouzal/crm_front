import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPerDateChartComponent } from './account-per-date-chart.component';

describe('AccountPerDateChartComponent', () => {
  let component: AccountPerDateChartComponent;
  let fixture: ComponentFixture<AccountPerDateChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPerDateChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPerDateChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
