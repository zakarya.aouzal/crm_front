import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ReplaySubject, Subscription } from 'rxjs';
import { AccountRepartitionByValArea, DashboardModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-account-repartition-value-chart',
  templateUrl: './account-repartition-value-chart.component.html',
  styleUrls: ['./account-repartition-value-chart.component.scss']
})
export class AccountRepartitionValueChartComponent implements OnInit {

    // data
    accountRepartitionAreaList: AccountRepartitionByValArea[] = null;

    // loading
    loading: boolean = null;



    @Input()
    set accountCode(value:string){
      if(value){
        this.loadRepartitionByValue(value);
      }

    }
  
    constructor(private dashboardService: DashboardService,
                private translate: TranslateService,
                private el: ElementRef) { }
  
    ngOnInit(): void {
      
    }

    emptyData():boolean{
      return this.accountRepartitionAreaList.length==0;
    }
  
    /** LOAD TOTAL ACCOUNT BY ACTIVITY AREA */
    loadRepartitionByValue(value:string) {
      this.accountRepartitionAreaList = null;
      this.loading = true;
      
      this.dashboardService.getRepartitionByValues(value)
        .pipe((finalize(() => {
          this.loading = false;
        })))
        .subscribe(response => {
          this.accountRepartitionAreaList = response;
          this.createChart();
        })
    }
  
  
    /** CREATE ACCOUNT REPARTITION CHART */
    createChart() {
  
      let pdr: [number] = [null];
      let pmv: [number] = [null];
      let categories:[String]=[null]
  
      // create data
      if (this.accountRepartitionAreaList) {
        pdr.pop();
        pmv.pop();
        categories.pop();
        for (let accountActivityArea of this.accountRepartitionAreaList) {
          
          pdr.push(accountActivityArea.pdr);
          pmv.push(accountActivityArea.pmv);
          categories.push(accountActivityArea.titre_desc);
        }
      }
  
      // options
      let options: any = {
        chart: {
          type: 'column',
          //backgroundColor: '#e4f0f6',
          exporting: {
            buttons: { 
                exportButton: {
                    enabled:false
                },
                printButton: {
                    enabled:false
                }
        
            }
        }
      },
  
      title: {
          text: 'Répartition Par Valeur '
      },
  
      xAxis: {
          categories: categories
      },
  
      yAxis: {
          allowDecimals: false,
          min: 0,
          title: {
              text: ''
          }
      },

      exporting: {
        buttons: { 
            exportButton: {
                enabled:false
            },
            printButton: {
                enabled:false
            }
    
        }
    },
  
      tooltip: {
          formatter: function () {
              return '<b>Série ' +this.series.name  +'Point '+ this.x + '</b><br/>' +
                  '<b>Valeur: ' + this.y + '</b><br/>' ;
                
          }
      },

      
  
      plotOptions: {
          column: {
              stacking: 'normal'
          }
      },
  
      series: [{
          name: 'PDR',
          data: pdr,
          stack: 'male'
      }, {
          name: 'PMV_Latente',
          data: pmv,
          stack: 'male'
      },]
      };
  
      // create
      const element = this.el.nativeElement.querySelector('#account-repartition-value-chart-container');
      Highcharts.chart(element, options);
    }
}
