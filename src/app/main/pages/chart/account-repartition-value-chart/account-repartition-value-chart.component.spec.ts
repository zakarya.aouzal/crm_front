import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountRepartitionValueChartComponent } from './account-repartition-chart.component';

describe('AccountRepartitionValueChartComponent', () => {
  let component: AccountRepartitionValueChartComponent;
  let fixture: ComponentFixture<AccountRepartitionValueChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountRepartitionValueChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountRepartitionValueChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
