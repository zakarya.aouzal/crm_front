import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DashboardService } from 'src/app/main/services';
import { SharedModule } from 'src/app/shared/shared.module';
import { AccountPerActivityAreaChartComponent } from './account-per-activity-area-chart/account-per-activity-area-chart.component';
import { AccountPerAdviceChartComponent } from './account-per-advice-chart/account-per-advice-chart.component';
import { AccountPerCountryChartComponent } from './account-per-country-chart/account-per-country-chart.component';
import { AccountPerDateChartComponent } from './account-per-date-chart/account-per-date-chart.component';
import { AccountRepartitionChartComponent } from './account-repartition-chart/account-repartition-chart.component';
import { AccountTopListComponent } from './account-top-list/account-top-list.component';
import { HighchartsUtils } from './chart.util';
import { DemoChartComponent } from './demo-chart/demo-chart.component';
import {AccountPerDateChartDemo1Component} from './account-per-date-chart-demo-1/account-per-date-chart.component'
import {AccountPerDateChartDemo2Component} from './account-per-date-chart-demo-2/account-per-date-chart.component'
import {AccountPerDateChartDemo3Component} from './account-per-date-chart-demo-3/account-per-date-chart.component'
import {AccountRepartitionValueChartComponent} from './account-repartition-value-chart/account-repartition-chart.component';
import { AccountPerActivityAreaComponent } from './account-per-activity-area/account-per-activity-area.component';



HighchartsUtils.setHighchartsFactory();
HighchartsUtils.setHighchartsOptions();

@NgModule({
  declarations: [
    DemoChartComponent,
    AccountPerCountryChartComponent,
    AccountPerDateChartComponent,
    AccountRepartitionChartComponent,
    AccountPerAdviceChartComponent,
    AccountPerActivityAreaChartComponent,
    AccountPerActivityAreaChartComponent,
    AccountTopListComponent,
    AccountPerDateChartDemo1Component,
    AccountPerDateChartDemo2Component,
    AccountPerDateChartDemo3Component,
    AccountRepartitionValueChartComponent,
    AccountPerActivityAreaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    
   
  ],
  exports: [
    AccountPerCountryChartComponent,
    AccountPerDateChartComponent,
    AccountRepartitionChartComponent,
    AccountPerAdviceChartComponent,
    AccountPerActivityAreaChartComponent,
    AccountTopListComponent,
    AccountPerDateChartDemo1Component,
    AccountPerDateChartDemo2Component,
    AccountPerDateChartDemo3Component,
    AccountRepartitionValueChartComponent,
    AccountPerActivityAreaComponent
  ],
  providers: [
    DashboardService
  ]
})
export class ChartModule { }
