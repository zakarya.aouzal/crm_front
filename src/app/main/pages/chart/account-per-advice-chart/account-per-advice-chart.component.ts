import { Component, ElementRef, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { finalize } from 'rxjs/operators';
import { AccountPotentialAdviceModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-per-advice-chart',
  templateUrl: './account-per-advice-chart.component.html',
  styleUrls: ['./account-per-advice-chart.component.scss']
})
export class AccountPerAdviceChartComponent implements OnInit {

  // data
  potentialAdviceList: AccountPotentialAdviceModel[] = null;

  // loading
  loading: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadPotentialAdvice();
  }

  /** LOAD POTENTIAL ADVICE */
  loadPotentialAdvice() {
    this.potentialAdviceList = null;
    this.loading = true;
    this.dashboardService.getPotentialAdvice()
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.potentialAdviceList = response;
        this.createChart();
      })
  }



  /** CREATE ACCOUNT PER COUNTRY CHART */
  createChart() {

    let categories: Set<string> = new Set<string>();
    let data: number[] = [];

    // create data
    if (this.potentialAdviceList) {
      for (let potentialAdvice of this.potentialAdviceList) {
        categories.add(potentialAdvice.conseiller);
        data.push(potentialAdvice.revenu);
      }
    }

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'bar'
      },
      title: {
        text: this.translate.instant('DASHBOARD.POTENTIAL_DHS'),
        style: {
          fontSize: '16px'
        }
      },
      xAxis: {
        categories: Array.from(categories.values()),
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Potentiel (DH)',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: null
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        enabled: false
      },
      credits: {
        enabled: false
      },
      series: [{
        type: 'bar',
        name: 'Potentiel',
        data: data
      }]
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-advice-chart-container');
    Highcharts.chart(element, options);
  }

}
