import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPerAdviceChartComponent } from './account-per-advice-chart.component';

describe('AccountPerAdviceChartComponent', () => {
  let component: AccountPerAdviceChartComponent;
  let fixture: ComponentFixture<AccountPerAdviceChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPerAdviceChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPerAdviceChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
