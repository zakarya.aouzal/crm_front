import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPerActivityAreaChartComponent } from './account-per-activity-area-chart.component';

describe('AccountPerActivityAreaChartComponent', () => {
  let component: AccountPerActivityAreaChartComponent;
  let fixture: ComponentFixture<AccountPerActivityAreaChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPerActivityAreaChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPerActivityAreaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
