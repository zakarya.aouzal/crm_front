import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountTopListComponent } from './account-top-list.component';

describe('AccountTopListComponent', () => {
  let component: AccountTopListComponent;
  let fixture: ComponentFixture<AccountTopListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountTopListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountTopListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
