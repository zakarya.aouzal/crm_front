import { Component, Inject, Input, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AccountPMModel, AccountPPModel, AccountType } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';

@Component({
  selector: 'app-account-top-list',
  templateUrl: './account-top-list.component.html',
  styleUrls: ['./account-top-list.component.scss']
})
export class AccountTopListComponent implements OnInit {

  @Input()
  set accountType(value: AccountType) {
    this._accountType = value;
    this.accountPPList = [];
    this.accountPMList = [];
    switch (this._accountType) {
      case AccountType.PP:
        this.loadTopAccountPP();
        break
      case AccountType.PM:
        this.loadTopAccountPM();
    }
  }

  _accountType: AccountType = null;

  // data
  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];

  // loading
  loading: boolean = false;

  constructor(private dashboardService: DashboardService, @Inject('pipeParameters') public pipeParameters: any) { }

  ngOnInit(): void { }

  /** LOAD TOP ACCOUNT PP */
  loadTopAccountPP() {
    this.accountPPList = [];
    this.loading = true;
    this.dashboardService.getTopAccountPP()
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.accountPPList = response)
  }

  /** LOAD TOP ACCOUNT PM */
  loadTopAccountPM() {
    this.accountPPList = [];
    this.loading = true;
    this.dashboardService.getTopAccountPM()
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.accountPMList = response);
  }

}
