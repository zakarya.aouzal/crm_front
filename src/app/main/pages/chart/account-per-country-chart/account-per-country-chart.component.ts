import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ReplaySubject, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { DashboardModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslatePipe, TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-per-country-chart',
  templateUrl: './account-per-country-chart.component.html',
  styleUrls: ['./account-per-country-chart.component.scss']
})
export class AccountPerCountryChartComponent implements OnInit, OnDestroy {





  // data
  totalAccountPPByCountryList: DashboardModel[] = null;
  totalAccountPMByCountryList: DashboardModel[] = null;

  // loading
  readonly loadingSubject = new ReplaySubject<{ loadingPP?: boolean; loadingPM?: boolean }>();
  loadingSubscription: Subscription = new Subscription();
  loadingPP: boolean = null;
  loadingPM: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadTotalAccountPPByCountry();
    this.loadTotalAccountPMByCountry();

    this.loadingSubscription = this.loadingSubject.subscribe(value => {
      if (value) {
        this.loadingPP = (value.loadingPP !== undefined) ? value.loadingPP : this.loadingPP;
        this.loadingPM = (value.loadingPM !== undefined) ? value.loadingPM : this.loadingPM;
      }
      if (this.loadingPP === false && this.loadingPM === false) {
        this.createChart();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription?.unsubscribe();
    }
  }

  /** LOAD TOTAL ACCOUNT PP BY COUNTRY */
  loadTotalAccountPPByCountry() {
    this.totalAccountPPByCountryList = null;
    this.loadingSubject.next({ loadingPP: true });
    this.dashboardService.getTotalAccountPPByCountry()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPP: false });
      }))
      .subscribe(response => {
        this.totalAccountPPByCountryList = response;
      })
  }

  /** LOAD TOTAL ACCOUNT PM BY COUNTRY */
  loadTotalAccountPMByCountry() {
    this.totalAccountPMByCountryList = null;
    this.loadingSubject.next({ loadingPM: true });
    this.dashboardService.getTotalAccountPMByCountry()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPM: false });
      }))
      .subscribe(response => {
        this.totalAccountPMByCountryList = response;
      })
  }

  /** CREATE ACCOUNT PER COUNTRY CHART */
  createChart() {

    let categories: Set<string> = new Set<string>();
    let accountPPData: number[] = [];
    let accountPMData: number[] = [];

    // create categories
    if (this.totalAccountPPByCountryList) {
      for (let totalAccountPPByCountry of this.totalAccountPPByCountryList) {
        categories.add(totalAccountPPByCountry.pays);
      }
    }

    if (this.totalAccountPMByCountryList) {
      for (let totalAccountPMByCountry of this.totalAccountPMByCountryList) {
        categories.add(totalAccountPMByCountry.pays);
      }
    }

    // create data
    for (let category of categories) {
      if (this.totalAccountPPByCountryList) {
        const total = this.totalAccountPPByCountryList.find(item => item.pays === category)?.total;
        accountPPData.push(total ? total : null);
      }
      if (this.totalAccountPMByCountryList) {
        const total = this.totalAccountPMByCountryList.find(item => item.pays === category)?.total;
        accountPMData.push(total ? total : null);
      }
    }

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'bar'
      },
      title: {
        text: this.translate.instant('DASHBOARD.COMPTE_STATS'),
        style: {
          fontSize: '16px'
        }
      },
      xAxis: {
        categories: Array.from(categories.values()),
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: this.translate.instant('DASHBOARD.COMPTE_NMB'),
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: null
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        type: 'bar',
        name: 'Compte PP',
        data: accountPPData
      }, {
        type: 'bar',
        name: 'Compte PM',
        data: accountPMData
      }]
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-country-chart-container');
    Highcharts.chart(element, options);
  }

}
