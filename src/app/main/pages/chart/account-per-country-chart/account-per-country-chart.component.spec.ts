import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPerCountryChartComponent } from './account-per-country-chart.component';

describe('AccountPerCountryChartComponent', () => {
  let component: AccountPerCountryChartComponent;
  let fixture: ComponentFixture<AccountPerCountryChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPerCountryChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPerCountryChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
