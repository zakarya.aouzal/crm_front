import { Component, ElementRef, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HighMaps from 'highcharts/highmaps.js';


@Component({
  selector: 'app-demo-chart',
  templateUrl: './demo-chart.component.html',
  styleUrls: ['./demo-chart.component.scss']
})
export class DemoChartComponent implements OnInit {

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
 //   this.createAccountPerCountry();
    this.createAccountPerCountryMap();
  //  this.createAccountPerDateChart();
  //  this.createAccountDivisionChart();


  }

  /** CREATE ACCOUNT PER DATE CHART */
  createAccountPerDateChart() {

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Nombre de compte crée par mois'
      },
      subtitle: {
        text: null
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Nombre de compte'
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: [
        {
          name: 'Compte PP',
          type: 'line',
          data: [7, 6, 9, 14, 18, 21, 25, 26, 23, 18, 13, 9]
        }, {
          name: 'Compte PM',
          type: 'line',
          data: [3, 4, 5, 8, 11, 15, 17, 16, 14, 10, 6, 4]
        }],
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-date-chart-container');
    Highcharts.chart(element, options);
  }

  /** CREATE ACCOUNT DIVISION CHART */
  createAccountDivisionChart() {

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        }
      },
      title: {
        text: 'Répartition des comptes PP Risqué / Non risqué'
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: true,
            format: '{point.name}'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Répartition des comptes PP',
        data: [
          ['Risqué', 45.0],
          ['Non risqué', 55.0]
        ]
      }]
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-division-chart-container');
    Highcharts.chart(element, options);
  }

  /** CREATE ACCOUNT PER COUNTRY */
  createAccountPerCountry() {

    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Nombre de compte PP/PM par pays'
      },
      xAxis: {
        categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Nombre de compte',
          align: 'high'
        },
        labels: {
          overflow: 'justify'
        }
      },
      tooltip: {
        valueSuffix: null
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
      },
      credits: {
        enabled: false
      },
      series: [{
        type: 'bar',
        name: 'Compte PP',
        data: [107, 31, 635, 203, 2]
      }, {
        type: 'bar',
        name: 'Compte PM',
        data: [133, 156, 947, 408, 6]
      }]
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-country-chart-container');
    Highcharts.chart(element, options);
  }


  data = [
    {
      "code3": "ABW",
      "name": "Aruba",
      "value": 582,
      "code": "AW"
    },
    {
      "code3": "AFG",
      "name": "Afghanistan",
      "value": 53,
      "code": "AF"
    },
    {
      "code3": "AGO",
      "name": "Angola",
      "value": 23,
      "code": "AO"
    },
    {
      "code3": "ALB",
      "name": "Albania",
      "value": 104,
      "code": "AL"
    },
    {
      "code3": "AND",
      "name": "Andorra",
      "value": 164,
      "code": "AD"
    },
    {
      "code3": "ARE",
      "name": "United Arab Emirates",
      "value": 110,
      "code": "AE"
    },
    {
      "code3": "ARG",
      "name": "Argentina",
      "value": 16,
      "code": "AR"
    },
    {
      "code3": "ARM",
      "name": "Armenia",
      "value": 102,
      "code": "AM"
    },
    {
      "code3": "ASM",
      "name": "American Samoa",
      "value": 278,
      "code": "AS"
    },
    {
      "code3": "ATG",
      "name": "Antigua and Barbuda",
      "value": 229,
      "code": "AG"
    },
    {
      "code3": "AUS",
      "name": "Australia",
      "value": 3,
      "code": "AU"
    },
    {
      "code3": "AUT",
      "name": "Austria",
      "value": 105,
      "code": "AT"
    },
    {
      "code3": "AZE",
      "name": "Azerbaijan",
      "value": 118,
      "code": "AZ"
    },
    {
      "code3": "BDI",
      "name": "Burundi",
      "value": 409,
      "code": "BI"
    },
    {
      "code3": "BEL",
      "name": "Belgium",
      "value": 374,
      "code": "BE"
    },
    {
      "code3": "BEN",
      "name": "Benin",
      "value": 96,
      "code": "BJ"
    },
    {
      "code3": "BFA",
      "name": "Burkina Faso",
      "value": 68,
      "code": "BF"
    },
    {
      "code3": "BGD",
      "name": "Bangladesh",
      "value": 1251,
      "code": "BD"
    },
    {
      "code3": "BGR",
      "name": "Bulgaria",
      "value": 65,
      "code": "BG"
    },
    {
      "code3": "BHR",
      "name": "Bahrain",
      "value": 1848,
      "code": "BH"
    },
    {
      "code3": "BHS",
      "name": "Bahamas, The",
      "value": 39,
      "code": "BS"
    },
    {
      "code3": "BIH",
      "name": "Bosnia and Herzegovina",
      "value": 68,
      "code": "BA"
    },
    {
      "code3": "BLR",
      "name": "Belarus",
      "value": 46,
      "code": "BY"
    },
    {
      "code3": "BLZ",
      "name": "Belize",
      "value": 16,
      "code": "BZ"
    },
    {
      "code3": "BMU",
      "name": "Bermuda",
      "value": 1307,
      "code": "BM"
    },
    {
      "code3": "BOL",
      "name": "Bolivia",
      "value": 10,
      "code": "BO"
    },
    {
      "code3": "BRA",
      "name": "Brazil",
      "value": 24,
      "code": "BR"
    },
    {
      "code3": "BRB",
      "name": "Barbados",
      "value": 662,
      "code": "BB"
    },
    {
      "code3": "BRN",
      "name": "Brunei Darussalam",
      "value": 80,
      "code": "BN"
    },
    {
      "code3": "BTN",
      "name": "Bhutan",
      "value": 20,
      "code": "BT"
    },
    {
      "code3": "BWA",
      "name": "Botswana",
      "value": 3,
      "code": "BW"
    },
    {
      "code3": "CAF",
      "name": "Central African Republic",
      "value": 7,
      "code": "CF"
    },
    {
      "code3": "CAN",
      "name": "Canada",
      "value": 3,
      "code": "CA"
    },
    {
      "code3": "CHE",
      "name": "Switzerland",
      "value": 211,
      "code": "CH"
    },
    {
      "code3": "CHL",
      "name": "Chile",
      "value": 24,
      "code": "CL"
    },
    {
      "code3": "CHN",
      "name": "China",
      "value": 146,
      "code": "CN"
    },
    {
      "code3": "CIV",
      "name": "Cote d'Ivoire",
      "value": 74,
      "code": "CI"
    },
    {
      "code3": "CMR",
      "name": "Cameroon",
      "value": 49,
      "code": "CM"
    },
    {
      "code3": "COD",
      "name": "Congo, Dem. Rep.",
      "value": 34,
      "code": "CD"
    },
    {
      "code3": "COG",
      "name": "Congo, Rep.",
      "value": 15,
      "code": "CG"
    },
    {
      "code3": "COL",
      "name": "Colombia",
      "value": 43,
      "code": "CO"
    },
    {
      "code3": "COM",
      "name": "Comoros",
      "value": 427,
      "code": "KM"
    },
    {
      "code3": "CPV",
      "name": "Cabo Verde",
      "value": 133.89,
      "code": "CV"
    },
    {
      "code3": "CRI",
      "name": "Costa Rica",
      "value": 95,
      "code": "CR"
    },
    {
      "code3": "CUB",
      "name": "Cuba",
      "value": 110,
      "code": "CU"
    },
    {
      "code3": "CUW",
      "name": "Curacao",
      "value": 359,
      "code": "CW"
    },
    {
      "code3": "CYM",
      "name": "Cayman Islands",
      "value": 253,
      "code": "KY"
    },
    {
      "code3": "CYP",
      "name": "Cyprus",
      "value": 126,
      "code": "CY"
    },
    {
      "code3": "CZE",
      "name": "Czech Republic",
      "value": 136,
      "code": "CZ"
    },
    {
      "code3": "DEU",
      "name": "Germany",
      "value": 236,
      "code": "DE"
    },
    {
      "code3": "DJI",
      "name": "Djibouti",
      "value": 40,
      "code": "DJ"
    },
    {
      "code3": "DMA",
      "name": "Dominica",
      "value": 98,
      "code": "DM"
    },
    {
      "code3": "DNK",
      "name": "Denmark",
      "value": 135,
      "code": "DK"
    },
    {
      "code3": "DOM",
      "name": "Dominican Republic",
      "value": 220,
      "code": "DO"
    },
    {
      "code3": "DZA",
      "name": "Algeria",
      "value": 17,
      "code": "DZ"
    },
    {
      "code3": "ECU",
      "name": "Ecuador",
      "value": 65,
      "code": "EC"
    },
    {
      "code3": "EGY",
      "name": "Egypt, Arab Rep.",
      "value": 96,
      "code": "EG"
    },
    {
      "code3": "ESP",
      "name": "Spain",
      "value": 92,
      "code": "ES"
    },
    {
      "code3": "EST",
      "name": "Estonia",
      "value": 31,
      "code": "EE"
    },
    {
      "code3": "ETH",
      "name": "Ethiopia",
      "value": 102,
      "code": "ET"
    },
    {
      "code3": "FIN",
      "name": "Finland",
      "value": 18,
      "code": "FI"
    },
    {
      "code3": "FJI",
      "name": "Fiji",
      "value": 49,
      "code": "FJ"
    },
    {
      "code3": "FRA",
      "name": "France",
      "value": 122,
      "code": "FR"
    },
    {
      "code3": "FRO",
      "name": "Faroe Islands",
      "value": 35,
      "code": "FO"
    },
    {
      "code3": "FSM",
      "name": "Micronesia, Fed. Sts.",
      "value": 149,
      "code": "FM"
    },
    {
      "code3": "GAB",
      "name": "Gabon",
      "value": 7,
      "code": "GA"
    },
    {
      "code3": "GBR",
      "name": "United Kingdom",
      "value": 271,
      "code": "GB"
    },
    {
      "code3": "GEO",
      "name": "Georgia",
      "value": 53,
      "code": "GE"
    },
    {
      "code3": "GHA",
      "name": "Ghana",
      "value": 123,
      "code": "GH"
    },
    {
      "code3": "GIB",
      "name": "Gibraltar",
      "value": 3440,
      "code": "GI"
    },
    {
      "code3": "GIN",
      "name": "Guinea",
      "value": 50,
      "code": "GN"
    },
    {
      "code3": "GMB",
      "name": "Gambia, The",
      "value": 201,
      "code": "GM"
    },
    {
      "code3": "GNB",
      "name": "Guinea-Bissau",
      "value": 64,
      "code": "GW"
    },
    {
      "code3": "GNQ",
      "name": "Equatorial Guinea",
      "value": 43,
      "code": "GQ"
    },
    {
      "code3": "GRC",
      "name": "Greece",
      "value": 83,
      "code": "GR"
    },
    {
      "code3": "GRD",
      "name": "Grenada",
      "value": 315,
      "code": "GD"
    },
    {
      "code3": "GRL",
      "name": "Greenland",
      "value": 5,
      "code": "GL"
    },
    {
      "code3": "GTM",
      "name": "Guatemala",
      "value": 154,
      "code": "GT"
    },
    {
      "code3": "GUM",
      "name": "Guam",
      "value": 301,
      "code": "GU"
    },
    {
      "code3": "GUY",
      "name": "Guyana",
      "value": 3,
      "code": "GY"
    },
    {
      "code3": "HKG",
      "name": "Hong Kong SAR, China",
      "value": 6987.24,
      "code": "HK"
    },
    {
      "code3": "HND",
      "name": "Honduras",
      "value": 81.44,
      "code": "HN"
    },
    {
      "code3": "HRV",
      "name": "Croatia",
      "value": 74.6,
      "code": "HR"
    },
    {
      "code3": "HTI",
      "name": "Haiti",
      "value": 393.59,
      "code": "HT"
    },
    {
      "code3": "HUN",
      "name": "Hungary",
      "value": 108.41,
      "code": "HU"
    },
    {
      "code3": "IDN",
      "name": "Indonesia",
      "value": 144.14,
      "code": "ID"
    },
    {
      "code3": "IMN",
      "name": "Isle of Man",
      "value": 146.91,
      "code": "IM"
    },
    {
      "code3": "IND",
      "name": "India",
      "value": 445.37,
      "code": "IN"
    },
    {
      "code3": "IRL",
      "name": "Ireland",
      "value": 68.95,
      "code": "IE"
    },
    {
      "code3": "IRN",
      "name": "Iran, Islamic Rep.",
      "value": 49.29,
      "code": "IR"
    },
    {
      "code3": "IRQ",
      "name": "Iraq",
      "value": 85.66,
      "code": "IQ"
    },
    {
      "code3": "ISL",
      "name": "Iceland",
      "value": 3.35,
      "code": "IS"
    },
    {
      "code3": "ISR",
      "name": "Israel",
      "value": 394.92,
      "code": "IL"
    },
    {
      "code3": "ITA",
      "name": "Italy",
      "value": 206.12,
      "code": "IT"
    },
    {
      "code3": "JAM",
      "name": "Jamaica",
      "value": 266.05,
      "code": "JM"
    },
    {
      "code3": "JOR",
      "name": "Jordan",
      "value": 106.51,
      "code": "JO"
    },
    {
      "code3": "JPN",
      "name": "Japan",
      "value": 348.35,
      "code": "JP"
    },
    {
      "code3": "KAZ",
      "name": "Kazakhstan",
      "value": 6.59,
      "code": "KZ"
    },
    {
      "code3": "KEN",
      "name": "Kenya",
      "value": 85.15,
      "code": "KE"
    },
    {
      "code3": "KGZ",
      "name": "Kyrgyz Republic",
      "value": 31.7,
      "code": "KG"
    },
    {
      "code3": "KHM",
      "name": "Cambodia",
      "value": 89.3,
      "code": "KH"
    },
    {
      "code3": "KIR",
      "name": "Kiribati",
      "value": 141.23,
      "code": "KI"
    },
    {
      "code3": "KNA",
      "name": "St. Kitts and Nevis",
      "value": 210.85,
      "code": "KN"
    },
    {
      "code3": "KOR",
      "name": "Korea, Rep.",
      "value": 525.7,
      "code": "KR"
    },
    {
      "code3": "KWT",
      "name": "Kuwait",
      "value": 227.42,
      "code": "KW"
    },
    {
      "code3": "LAO",
      "name": "Lao PDR",
      "value": 29.28,
      "code": "LA"
    },
    {
      "code3": "LBN",
      "name": "Lebanon",
      "value": 587.16,
      "code": "LB"
    },
    {
      "code3": "LBR",
      "name": "Liberia",
      "value": 47.9,
      "code": "LR"
    },
    {
      "code3": "LBY",
      "name": "Libya",
      "value": 3.58,
      "code": "LY"
    },
    {
      "code3": "LCA",
      "name": "St. Lucia",
      "value": 291.83,
      "code": "LC"
    },
    {
      "code3": "LIE",
      "name": "Liechtenstein",
      "value": 235.41,
      "code": "LI"
    },
    {
      "code3": "LKA",
      "name": "Sri Lanka",
      "value": 338.11,
      "code": "LK"
    },
    {
      "code3": "LSO",
      "name": "Lesotho",
      "value": 72.59,
      "code": "LS"
    },
    {
      "code3": "LTU",
      "name": "Lithuania",
      "value": 45.78,
      "code": "LT"
    },
    {
      "code3": "LUX",
      "name": "Luxembourg",
      "value": 224.72,
      "code": "LU"
    },
    {
      "code3": "LVA",
      "name": "Latvia",
      "value": 31.51,
      "code": "LV"
    },
    {
      "code3": "MAC",
      "name": "Macao SAR, China",
      "value": 20405.57,
      "code": "MO"
    },
    {
      "code3": "MAF",
      "name": "St. Martin (French part)",
      "value": 591.65,
      "code": "MF"
    },
    {
      "code3": "MAR",
      "name": "Morocco",
      "value": 79.04,
      "code": "MA"
    },
    {
      "code3": "MCO",
      "name": "Monaco",
      "value": 19249.5,
      "code": "MC"
    },
    {
      "code3": "MDA",
      "name": "Moldova",
      "value": 108.06,
      "code": "MD"
    },
    {
      "code3": "MDG",
      "name": "Madagascar",
      "value": 42.79,
      "code": "MG"
    },
    {
      "code3": "MDV",
      "name": "Maldives",
      "value": 1425.85,
      "code": "MV"
    },
    {
      "code3": "MEX",
      "name": "Mexico",
      "value": 65.61,
      "code": "MX"
    },
    {
      "code3": "MHL",
      "name": "Marshall Islands",
      "value": 294.81,
      "code": "MH"
    },
    {
      "code3": "MKD",
      "name": "Macedonia, FYR",
      "value": 82.52,
      "code": "MK"
    },
    {
      "code3": "MLI",
      "name": "Mali",
      "value": 14.75,
      "code": "ML"
    },
    {
      "code3": "MLT",
      "name": "Malta",
      "value": 1366.93,
      "code": "MT"
    },
    {
      "code3": "MMR",
      "name": "Myanmar",
      "value": 80.98,
      "code": "MM"
    },
    {
      "code3": "MNE",
      "name": "Montenegro",
      "value": 46.27,
      "code": "ME"
    },
    {
      "code3": "MNG",
      "name": "Mongolia",
      "value": 1.95,
      "code": "MN"
    },
    {
      "code3": "MNP",
      "name": "Northern Mariana Islands",
      "value": 119.62,
      "code": "MP"
    },
    {
      "code3": "MOZ",
      "name": "Mozambique",
      "value": 36.66,
      "code": "MZ"
    },
    {
      "code3": "MRT",
      "name": "Mauritania",
      "value": 4.17,
      "code": "MR"
    },
    {
      "code3": "MUS",
      "name": "Mauritius",
      "value": 622.4,
      "code": "MU"
    },
    {
      "code3": "MWI",
      "name": "Malawi",
      "value": 191.89,
      "code": "MW"
    },
    {
      "code3": "MYS",
      "name": "Malaysia",
      "value": 94.92,
      "code": "MY"
    },
    {
      "code3": "NAM",
      "name": "Namibia",
      "value": 3.01,
      "code": "NA"
    },
    {
      "code3": "NCL",
      "name": "New Caledonia",
      "value": 15.15,
      "code": "NC"
    },
    {
      "code3": "NER",
      "name": "Niger",
      "value": 16.32,
      "code": "NE"
    },
    {
      "code3": "NGA",
      "name": "Nigeria",
      "value": 204.21,
      "code": "NG"
    },
    {
      "code3": "NIC",
      "name": "Nicaragua",
      "value": 51.1,
      "code": "NI"
    },
    {
      "code3": "NLD",
      "name": "Netherlands",
      "value": 505.5,
      "code": "NL"
    },
    {
      "code3": "NOR",
      "name": "Norway",
      "value": 14.34,
      "code": "NO"
    },
    {
      "code3": "NPL",
      "name": "Nepal",
      "value": 202.18,
      "code": "NP"
    },
    {
      "code3": "NRU",
      "name": "Nauru",
      "value": 652.45,
      "code": "NR"
    },
    {
      "code3": "NZL",
      "name": "New Zealand",
      "value": 17.82,
      "code": "NZ"
    },
    {
      "code3": "OMN",
      "name": "Oman",
      "value": 14.3,
      "code": "OM"
    },
    {
      "code3": "PAK",
      "name": "Pakistan",
      "value": 250.63,
      "code": "PK"
    },
    {
      "code3": "PAN",
      "name": "Panama",
      "value": 54.27,
      "code": "PA"
    },
    {
      "code3": "PER",
      "name": "Peru",
      "value": 24.82,
      "code": "PE"
    },
    {
      "code3": "PHL",
      "name": "Philippines",
      "value": 346.51,
      "code": "PH"
    },
    {
      "code3": "PLW",
      "name": "Palau",
      "value": 46.75,
      "code": "PW"
    },
    {
      "code3": "PNG",
      "name": "Papua New Guinea",
      "value": 17.85,
      "code": "PG"
    },
    {
      "code3": "POL",
      "name": "Poland",
      "value": 124.01,
      "code": "PL"
    },
    {
      "code3": "PRI",
      "name": "Puerto Rico",
      "value": 384.59,
      "code": "PR"
    },
    {
      "code3": "PRK",
      "name": "Korea, Dem. People’s Rep.",
      "value": 210.69,
      "code": "KP"
    },
    {
      "code3": "PRT",
      "name": "Portugal",
      "value": 112.72,
      "code": "PT"
    },
    {
      "code3": "PRY",
      "name": "Paraguay",
      "value": 16.93,
      "code": "PY"
    },
    {
      "code3": "PSE",
      "name": "West Bank and Gaza",
      "value": 756.07,
      "code": "PS"
    },
    {
      "code3": "PYF",
      "name": "French Polynesia",
      "value": 76.56,
      "code": "PF"
    },
    {
      "code3": "QAT",
      "name": "Qatar",
      "value": 221.34,
      "code": "QA"
    },
    {
      "code3": "ROU",
      "name": "Romania",
      "value": 85.62,
      "code": "RO"
    },
    {
      "code3": "RUS",
      "name": "Russian Federation",
      "value": 8.81,
      "code": "RU"
    },
    {
      "code3": "RWA",
      "name": "Rwanda",
      "value": 483.08,
      "code": "RW"
    },
    {
      "code3": "SAU",
      "name": "Saudi Arabia",
      "value": 15.01,
      "code": "SA"
    },
    {
      "code3": "SDN",
      "name": "Sudan",
      "value": 16.66,
      "code": "SD"
    },
    {
      "code3": "SEN",
      "name": "Senegal",
      "value": 80.05,
      "code": "SN"
    },
    {
      "code3": "SGP",
      "name": "Singapore",
      "value": 7908.72,
      "code": "SG"
    },
    {
      "code3": "SLB",
      "name": "Solomon Islands",
      "value": 21.42,
      "code": "SB"
    },
    {
      "code3": "SLE",
      "name": "Sierra Leone",
      "value": 102.47,
      "code": "SL"
    },
    {
      "code3": "SLV",
      "name": "El Salvador",
      "value": 306.21,
      "code": "SV"
    },
    {
      "code3": "SMR",
      "name": "San Marino",
      "value": 553.38,
      "code": "SM"
    },
    {
      "code3": "SOM",
      "name": "Somalia",
      "value": 22.82,
      "code": "SO"
    },
    {
      "code3": "SRB",
      "name": "Serbia",
      "value": 80.7,
      "code": "RS"
    },
    {
      "code3": "STP",
      "name": "Sao Tome and Principe",
      "value": 208.24,
      "code": "ST"
    },
    {
      "code3": "SUR",
      "name": "Suriname",
      "value": 3.58,
      "code": "SR"
    },
    {
      "code3": "SVK",
      "name": "Slovak Republic",
      "value": 112.94,
      "code": "SK"
    },
    {
      "code3": "SVN",
      "name": "Slovenia",
      "value": 102.53,
      "code": "SI"
    },
    {
      "code3": "SWE",
      "name": "Sweden",
      "value": 24.36,
      "code": "SE"
    },
    {
      "code3": "SWZ",
      "name": "Swaziland",
      "value": 78.09,
      "code": "SZ"
    },
    {
      "code3": "SXM",
      "name": "Sint Maarten (Dutch part)",
      "value": 1175.56,
      "code": "SX"
    },
    {
      "code3": "SYC",
      "name": "Seychelles",
      "value": 205.82,
      "code": "SC"
    },
    {
      "code3": "SYR",
      "name": "Syrian Arab Republic",
      "value": 100.37,
      "code": "SY"
    },
    {
      "code3": "TCA",
      "name": "Turks and Caicos Islands",
      "value": 36.74,
      "code": "TC"
    },
    {
      "code3": "TCD",
      "name": "Chad",
      "value": 11.48,
      "code": "TD"
    },
    {
      "code3": "TGO",
      "name": "Togo",
      "value": 139.85,
      "code": "TG"
    },
    {
      "code3": "THA",
      "name": "Thailand",
      "value": 134.79,
      "code": "TH"
    },
    {
      "code3": "TJK",
      "name": "Tajikistan",
      "value": 62.94,
      "code": "TJ"
    },
    {
      "code3": "TKM",
      "name": "Turkmenistan",
      "value": 12.05,
      "code": "TM"
    },
    {
      "code3": "TLS",
      "name": "Timor-Leste",
      "value": 85.32,
      "code": "TL"
    },
    {
      "code3": "TON",
      "name": "Tonga",
      "value": 148.78,
      "code": "TO"
    },
    {
      "code3": "TTO",
      "name": "Trinidad and Tobago",
      "value": 266.07,
      "code": "TT"
    },
    {
      "code3": "TUN",
      "name": "Tunisia",
      "value": 73.4,
      "code": "TN"
    },
    {
      "code3": "TUR",
      "name": "Turkey",
      "value": 103.31,
      "code": "TR"
    },
    {
      "code3": "TUV",
      "name": "Tuvalu",
      "value": 369.9,
      "code": "TV"
    },
    {
      "code3": "TZA",
      "name": "Tanzania",
      "value": 62.74,
      "code": "TZ"
    },
    {
      "code3": "UGA",
      "name": "Uganda",
      "value": 206.9,
      "code": "UG"
    },
    {
      "code3": "UKR",
      "name": "Ukraine",
      "value": 77.69,
      "code": "UA"
    },
    {
      "code3": "URY",
      "name": "Uruguay",
      "value": 19.68,
      "code": "UY"
    },
    {
      "code3": "USA",
      "name": "United States",
      "value": 35.32,
      "code": "US"
    },
    {
      "code3": "UZB",
      "name": "Uzbekistan",
      "value": 74.87,
      "code": "UZ"
    },
    {
      "code3": "VCT",
      "name": "St. Vincent and the Grenadines",
      "value": 281.14,
      "code": "VC"
    },
    {
      "code3": "VEN",
      "name": "Venezuela, RB",
      "value": 35.79,
      "code": "VE"
    },
    {
      "code3": "VGB",
      "name": "British Virgin Islands",
      "value": 204.41,
      "code": "VG"
    },
    {
      "code3": "VIR",
      "name": "Virgin Islands (U.S.)",
      "value": 307.17,
      "code": "VI"
    },
    {
      "code3": "VNM",
      "name": "Vietnam",
      "value": 304.99,
      "code": "VN"
    },
    {
      "code3": "VUT",
      "name": "Vanuatu",
      "value": 22.18,
      "code": "VU"
    },
    {
      "code3": "WSM",
      "name": "Samoa",
      "value": 68.95,
      "code": "WS"
    },
    {
      "code3": "YEM",
      "name": "Yemen, Rep.",
      "value": 52.25,
      "code": "YE"
    },
    {
      "code3": "ZAF",
      "name": "South Africa",
      "value": 46.18,
      "code": "ZA"
    },
    {
      "code3": "ZMB",
      "name": "Zambia",
      "value": 22.32,
      "code": "ZM"
    },
    {
      "code3": "ZWE",
      "name": "Zimbabwe",
      "value": 41.75,
      "code": "ZW"
    }
  ];

  data2: [string, number][] = [
    ['fo', 0],
    ['um', 1],
    ['us', 2],
    ['jp', 3],
    ['sc', 4],
    ['in', 5],
    ['fr', 6],
    ['fm', 7],
    ['cn', 8],
    ['pt', 9],
    ['sw', 10],
    ['sh', 11],
    ['br', 12],
    ['ki', 13],
    ['ph', 14],
    ['mx', 15],
    ['es', 16],
    ['bu', 17],
    ['mv', 18],
    ['sp', 19],
    ['gb', 20],
    ['gr', 21],
    ['as', 22],
    ['dk', 23],
    ['gl', 24],
    ['gu', 25],
    ['mp', 26],
    ['pr', 27],
    ['vi', 28],
    ['ca', 29],
    ['st', 30],
    ['cv', 31],
    ['dm', 32],
    ['nl', 33],
    ['jm', 34],
    ['ws', 35],
    ['om', 36],
    ['vc', 37],
    ['tr', 38],
    ['bd', 39],
    ['lc', 40],
    ['nr', 41],
    ['no', 42],
    ['kn', 43],
    ['bh', 44],
    ['to', 45],
    ['fi', 46],
    ['id', 47],
    ['mu', 48],
    ['se', 49],
    ['tt', 50],
    ['my', 51],
    ['pa', 52],
    ['pw', 53],
    ['tv', 54],
    ['mh', 55],
    ['cl', 56],
    ['th', 57],
    ['gd', 58],
    ['ee', 59],
    ['ag', 60],
    ['tw', 61],
    ['bb', 62],
    ['it', 63],
    ['mt', 64],
    ['vu', 65],
    ['sg', 66],
    ['cy', 67],
    ['lk', 68],
    ['km', 69],
    ['fj', 70],
    ['ru', 71],
    ['va', 72],
    ['sm', 73],
    ['kz', 74],
    ['az', 75],
    ['tj', 76],
    ['ls', 77],
    ['uz', 78],
    ['ma', 79],
    ['co', 80],
    ['tl', 81],
    ['tz', 82],
    ['ar', 83],
    ['sa', 84],
    ['pk', 85],
    ['ye', 86],
    ['ae', 87],
    ['ke', 88],
    ['pe', 89],
    ['do', 90],
    ['ht', 91],
    ['pg', 92],
    ['ao', 93],
    ['kh', 94],
    ['vn', 95],
    ['mz', 96],
    ['cr', 97],
    ['bj', 98],
    ['ng', 99],
    ['ir', 100],
    ['sv', 101],
    ['sl', 102],
    ['gw', 103],
    ['hr', 104],
    ['bz', 105],
    ['za', 106],
    ['cf', 107],
    ['sd', 108],
    ['cd', 109],
    ['kw', 110],
    ['de', 111],
    ['be', 112],
    ['ie', 113],
    ['kp', 114],
    ['kr', 115],
    ['gy', 116],
    ['hn', 117],
    ['mm', 118],
    ['ga', 119],
    ['gq', 120],
    ['ni', 121],
    ['lv', 122],
    ['ug', 123],
    ['mw', 124],
    ['am', 125],
    ['sx', 126],
    ['tm', 127],
    ['zm', 128],
    ['nc', 129],
    ['mr', 130],
    ['dz', 131],
    ['lt', 132],
    ['et', 133],
    ['er', 134],
    ['gh', 135],
    ['si', 136],
    ['gt', 137],
    ['ba', 138],
    ['jo', 139],
    ['sy', 140],
    ['mc', 141],
    ['al', 142],
    ['uy', 143],
    ['cnm', 144],
    ['mn', 145],
    ['rw', 146],
    ['so', 147],
    ['bo', 148],
    ['cm', 149],
    ['cg', 150],
    ['eh', 151],
    ['rs', 152],
    ['me', 153],
    ['tg', 154],
    ['la', 155],
    ['af', 156],
    ['ua', 157],
    ['sk', 158],
    ['jk', 159],
    ['bg', 160],
    ['qa', 161],
    ['li', 162],
    ['at', 163],
    ['sz', 164],
    ['hu', 165],
    ['ro', 166],
    ['ne', 167],
    ['lu', 168],
    ['ad', 169],
    ['ci', 170],
    ['lr', 171],
    ['bn', 172],
    ['iq', 173],
    ['ge', 174],
    ['gm', 175],
    ['ch', 176],
    ['td', 177],
    ['kv', 178],
    ['lb', 179],
    ['dj', 180],
    ['bi', 181],
    ['sr', 182],
    ['il', 183],
    ['ml', 184],
    ['sn', 185],
    ['gn', 186],
    ['zw', 187],
    ['pl', 188],
    ['mk', 189],
    ['py', 190],
    ['by', 191],
    ['cz', 192],
    ['bf', 193],
    ['na', 194],
    ['ly', 195],
    ['tn', 196],
    ['bt', 197],
    ['md', 198],
    ['ss', 199],
    ['bw', 200],
    ['bs', 201],
    ['nz', 202],
    ['cu', 203],
    ['ec', 204],
    ['au', 205],
    ['ve', 206],
    ['sb', 207],
    ['mg', 208],
    ['is', 209],
    ['eg', 210],
    ['kg', 211],
    ['np', 212]
  ];

  showDataLabels: boolean = false;

  /** CREATE ACCOUNT PER COUNTRY MAP */
  createAccountPerCountryMap() {
/*
    const worldMap = require('@highcharts/map-collection/custom/world.geo.json');

      // this.data.forEach(item => {
      //   item.value = Math.floor(item.value);
      // });

    // options
    let options: HighMaps.Options = {

      chart: {
        map: worldMap
      },

      title: {
        text: 'Nombre de compte par pays'
      },

      mapNavigation: {
        enabled: true,
        buttonOptions: {
          verticalAlign: 'bottom'
        }
      },

      colorAxis: {
        min: 0
      },

      tooltip: {
        backgroundColor: 'none',
        borderWidth: 0,
        shadow: false,
        useHTML: true,
        padding: 0,
        pointFormat: '<span class="f32"><span class="flag {point.properties.hc-key}">' +
          '</span></span> {point.name}<br>' +
          '<span style="font-size:30px">{point.value}</span>',
        positioner: function () {
          return { x: 0, y: 250 };
        }
      },

      series: [{
        type: 'map',
        data: this.data2,
        name: 'Random data',
        states: {
          hover: {
            color: '#BADA55'
          }
        }, dataLabels: {
          enabled: this.showDataLabels,
          formatter: function () {
            return this.point.name;
          }
        },
      }]

    };

    // create
    const element = this.el.nativeElement.querySelector('#account-per-country-map-chart-container');
    HighMaps.mapChart(element, options);
*/
  }

}
