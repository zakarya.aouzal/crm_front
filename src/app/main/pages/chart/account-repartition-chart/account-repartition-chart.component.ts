import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ReplaySubject, Subscription } from 'rxjs';
import { DashboardModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-repartition-chart',
  templateUrl: './account-repartition-chart.component.html',
  styleUrls: ['./account-repartition-chart.component.scss']
})
export class AccountRepartitionChartComponent implements OnInit, OnDestroy {

  // data
  riskAccountPPList: DashboardModel[] = null;
  riskAccountPMList: DashboardModel[] = null;

  // loading
  readonly loadingSubject = new ReplaySubject<{ loadingPP?: boolean; loadingPM?: boolean }>();
  loadingSubscription: Subscription = new Subscription();
  loadingPP: boolean = null;
  loadingPM: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadRiskAccountPP();
    this.loadRiskAccountPM();
    this.loadingSubscription = this.loadingSubject.subscribe(value => {
      if (value) {
        this.loadingPP = (value.loadingPP !== undefined) ? value.loadingPP : this.loadingPP;
        this.loadingPM = (value.loadingPM !== undefined) ? value.loadingPM : this.loadingPM;
      }
      if (this.loadingPP === false && this.loadingPM === false) {
        this.createChart();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription?.unsubscribe();
    }
  }

  /** LOAD RISK ACCOUNT PP */
  loadRiskAccountPP() {
    this.riskAccountPPList = null;
    this.loadingSubject.next({ loadingPP: true });
    this.dashboardService.getRiskAccountPP()
      .subscribe(response => {
        this.riskAccountPPList = response;
        this.loadingSubject.next({ loadingPP: false });
      })
  }

  /** LOAD RISK ACCOUNT PM */
  loadRiskAccountPM() {
    this.riskAccountPMList = null;
    this.loadingSubject.next({ loadingPM: true });
    this.dashboardService.getRiskAccountPM()
      .subscribe(response => {
        this.riskAccountPMList = response;
        this.loadingSubject.next({ loadingPM: false });
      })
  }

  /** CREATE ACCOUNT REPARTITION CHART */
  createChart() {

    let riskAccountData: number = 0;
    let noRiskAccountData: number = 0;

    // create data
    if (this.riskAccountPPList) {

      for (let riskAccountPP of this.riskAccountPPList) {

        if (riskAccountPP.total) {
          if (riskAccountPP.statutWl === 'R') {
            riskAccountData = riskAccountData + riskAccountPP.total;
          } else if (riskAccountPP.statutWl === 'NR') {
            noRiskAccountData = noRiskAccountData + riskAccountPP.total;
          }
        }

      }

    }

    if (this.riskAccountPMList) {

      for (let riskAccountPM of this.riskAccountPMList) {

        if (riskAccountPM.total) {
          if (riskAccountPM.statutWl === 'R') {
            riskAccountData = riskAccountData + riskAccountPM.total;
          } else if (riskAccountPM.statutWl === 'NR') {
            noRiskAccountData = noRiskAccountData + riskAccountPM.total;
          }
        }

      }

    }


    // options
    let options: Highcharts.Options = {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
          beta: 0
        }
      },
      title: {
        text: this.translate.instant('DASHBOARD.REPARTITION_RISK'),
        style: {
          fontSize: '16px'
        }
      },
      accessibility: {
        point: {
          valueSuffix: '%'
        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          depth: 35,
          dataLabels: {
            enabled: true,
            format: '{point.name}'
          }
        }
      },
      series: [{
        type: 'pie',
        name: 'Répartition des comptes',
        data: [
          ['Risqué', riskAccountData],
          ['Non risqué', noRiskAccountData]
        ]
      }]
    };

    // create
    const element = this.el.nativeElement.querySelector('#account-repartition-chart-container');
    Highcharts.chart(element, options);
  }

}
