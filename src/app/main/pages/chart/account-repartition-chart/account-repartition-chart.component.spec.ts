import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountRepartitionChartComponent } from './account-repartition-chart.component';

describe('AccountRepartitionChartComponent', () => {
  let component: AccountRepartitionChartComponent;
  let fixture: ComponentFixture<AccountRepartitionChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountRepartitionChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountRepartitionChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
