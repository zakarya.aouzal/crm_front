import { finalize } from 'rxjs/operators';
import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ReplaySubject, Subscription } from 'rxjs';
import { DashboardModel } from 'src/app/main/models';
import { DashboardService } from 'src/app/main/services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-account-per-date-chart-demo-2',
  templateUrl: './account-per-date-chart.component.html',
  styleUrls: ['./account-per-date-chart.component.scss']
})
export class AccountPerDateChartDemo2Component implements OnInit, OnDestroy {

  // data
  totalAccountPPByDateList: DashboardModel[] = null;
  totalAccountPMByDateList: DashboardModel[] = null;

  // loading
  readonly loadingSubject = new ReplaySubject<{ loadingPP?: boolean; loadingPM?: boolean }>();
  loadingSubscription: Subscription = new Subscription();
  loadingPP: boolean = null;
  loadingPM: boolean = null;

  constructor(private dashboardService: DashboardService,
              private translate: TranslateService,
              private el: ElementRef) { }

  ngOnInit(): void {
    this.loadTotalAccountPPByDate();
    this.loadTotalAccountPMByDate();

    this.loadingSubscription = this.loadingSubject.subscribe(value => {
      if (value) {
        this.loadingPP = (value.loadingPP !== undefined) ? value.loadingPP : this.loadingPP;
        this.loadingPM = (value.loadingPM !== undefined) ? value.loadingPM : this.loadingPM;
      }
      if (this.loadingPP === false && this.loadingPM === false) {
        this.createChart();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loadingSubscription) {
      this.loadingSubscription?.unsubscribe();
    }
  }

  /** LOAD TOTAL ACCOUNT PP BY DATE */
  loadTotalAccountPPByDate() {
    this.totalAccountPPByDateList = null;
    this.loadingSubject.next({ loadingPP: true });
    this.dashboardService.getTotalAccountPPByDate()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPP: false });
      }))
      .subscribe(response => {
        this.totalAccountPPByDateList = response;
      })
  }

  /** LOAD TOTAL ACCOUNT PM BY DATE */
  loadTotalAccountPMByDate() {
    this.totalAccountPMByDateList = null;
    this.loadingSubject.next({ loadingPM: true });
    this.dashboardService.getTotalAccountPMByDate()
      .pipe(finalize(() => {
        this.loadingSubject.next({ loadingPM: false });
      }))
      .subscribe(response => {
        this.totalAccountPMByDateList = response;
      })
  }

  /** CREATE ACCOUNT PER DATE CHART */
  createChart() {

    let categories: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let accountPPData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let accountPMData: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    // create data
    if (this.totalAccountPPByDateList) {
      for (let totalAccountPPByDate of this.totalAccountPPByDateList) {
        if (totalAccountPPByDate.mois && totalAccountPPByDate.total) {
          let month: number = Number(totalAccountPPByDate.mois);
          accountPPData[month - 1] = accountPPData[month - 1] + totalAccountPPByDate.total;
        }
      }
    }

    if (this.totalAccountPMByDateList) {
      for (let totalAccountPM of this.totalAccountPMByDateList) {
        if (totalAccountPM.mois && totalAccountPM.total) {
          let month: number = Number(totalAccountPM.mois);
          accountPMData[month - 1] = accountPMData[month - 1] + totalAccountPM.total;
        }
      }
    }

    // options
    let options: any = {
      chart: {
          type: 'bar',
          height: 280,
      },
      title: {
          text: ''
      },
      subtitle: {
          text: 'Historic World Population by Region'
      },
      xAxis: {
          categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
          title: {
              text: null
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Population (millions)',
              align: 'high'
          },
          labels: {
              overflow: 'justify'
          }
      },
      tooltip: {
          valueSuffix: ' millions'
      },
      plotOptions: {
          bar: {
              dataLabels: {
                  enabled: false
              }
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          backgroundColor:
              Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
          shadow: true
      },
      credits: {
          enabled: false
      },
      series: [{
          name: 'Year 1800',
          data: [107, 31]
      }, {
          name: 'Year 1900',
          data: [133, 156]
      }, {
          name: 'Year 2000',
          data: [814, 841]
      }, {
          name: 'Year 2016',
          data: [1216, 1001]
      }]
  };
    // create
    const element = this.el.nativeElement.querySelector('#account-per-date-chart-container');
    Highcharts.chart(element, options);
  }

}
