import * as Highcharts from 'highcharts';
import * as HighMaps from 'highcharts/highmaps.js';

export class HighchartsUtils {

    /** SET HIGHCHARTS FACTORY */
    static setHighchartsFactory() {
        let hc = require("highcharts");
        let hhm = require("highcharts/highmaps.js");
        const hcd = require("highcharts/highcharts-3d");
        const exp = require("highcharts/modules/exporting");
        const hm = require("highcharts/highcharts-more");
        const st = require("highcharts/modules/stock");
        const dn = require("highcharts/modules/drilldown");
        const tn = require("highcharts/modules/timeline");
        const sk = require("highcharts/modules/sankey");
        const on = require("highcharts/modules/organization");

        hcd(hc);
        exp(hc);
        exp(hhm);
        st(hc);
        hm(hc);
        dn(hc);
        tn(hc);
        sk(hc);
        on(hc);
    }

    /** SET HIGHCHARTS LANG */
    static setHighchartsOptions() {
        let options: Highcharts.Options = {
            credits: {
                enabled: false
            },
            lang: {
                printChart: 'Imprimer',
                thousandsSep: ',',
                months: [
                    'Janvier', 'Février', 'Mars', 'Avril',
                    'Mai', 'Juin', 'Juillet', 'Août',
                    'Septembre', 'Octobre', 'Novembre', 'Décembre'
                ],
                weekdays: [
                    'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
                    'Jeudi', 'Vendredi', 'Samedi'
                ],
                shortMonths: ['Jan', 'Fév', 'Mars', 'Avr', 'Mai', 'Jui', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
                downloadPNG: 'Télécharger image PNG',
                downloadJPEG: 'Télécharger image JPEG',
                downloadPDF: 'Télécharger image PDF',
                downloadSVG: 'Télécharger image SVG',
                drillUpText: '◁ Retour',
                viewFullscreen: 'Voir en plein écran'
            }
        };
        Highcharts.setOptions(options);
        HighMaps.setOptions(options);
    }

}
