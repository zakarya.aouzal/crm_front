import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const POSITION_MOY_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
      {
        name: 'entite',
        title: 'Entite',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.ENTITE'
        }
      }, {
        name: 'portefeuille',
        title: 'Portefeuille',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.PORTFEUILLE'
        }
      }, {
        name: 'descriptionPortf',
        title: 'Description',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.DESCRIPTION'
        }
      },
      {
        name: 'descriptionFonds',
        title: 'Fonds',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.DESCRIPTION_FONDS'
        }
      },
      {
        name: 'totalSous',
        title: 'Total Souscription',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.TOTAL_SOUS'
        }
      },
      {
        name: 'totalRac',
        title: 'Total Rachat',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.TOTAL_RACH'
        }
      },
      {
        name: 'totalPmv',
        title: 'Total Pmv',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.TOTAL_PMV'
        }
      },
      {
        name: 'totalFdg',
        title: 'Total Fdg',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.TOTAL_FDG'
        }
      },
      {
        name: 'actMoy',
        title: 'Act Moyen',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_M_LIST.ACT_MOYEN'
        }
      }
    ], actions: [DataTableActions.VIEW]
};
