import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionMoyListComponent } from './position-moy-list.component';

describe('PositionMoyListComponent', () => {
  let component: PositionMoyListComponent;
  let fixture: ComponentFixture<PositionMoyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionMoyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionMoyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
