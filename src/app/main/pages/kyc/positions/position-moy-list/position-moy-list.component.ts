import {Component, Input, OnInit} from '@angular/core';
import {PositionMoyFilterModel, PositionMoyModel} from "../../../../models";
import {DataTableOutputActionsModel, DataTableSettingsModel, ToolbarOption} from "../../../../../shared/models";
import {PositionsService} from "../../../../services";
import {Router} from "@angular/router";
import {ExportService} from "../../../../../shared/services";
import {finalize} from "rxjs/operators";
import {POSITION_MOY_LIST_SETTINGS} from "./position-moy-list.settings";

@Component({
  selector: 'app-position-moy-list',
  templateUrl: './position-moy-list.component.html',
  styleUrls: ['./position-moy-list.component.scss']
})
export class PositionMoyListComponent implements OnInit {

  @Input()
  set filter(value: { filterBody: PositionMoyFilterModel }) {

    if (!value) return;
    this._filter = value.filterBody;
    if (this._filter) {
     // this.settings = POSITION_MOY_LIST_SETTINGS;
      this.loadListPositions(this._filter)
    }
  }

  settings: DataTableSettingsModel = POSITION_MOY_LIST_SETTINGS;
  positionList: PositionMoyModel[] = [];

  _filter: PositionMoyFilterModel = null;

  loading: boolean = false;

  constructor(private operationsService: PositionsService, private router: Router,
              private exportService: ExportService) { }

  ngOnInit(): void {
  }

  /** LOAD LIST OF POSITIONS */
  loadListPositions(filter: PositionMoyFilterModel): void {
    this.positionList = [];
    if (!filter) return;
    this.loading = true;
    this.operationsService.getListPositionsMoy(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.positionList = response;
      });
  }



  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<PositionMoyModel>): void {
    if (event) {
      switch (event.actionType) {
        case 'VIEW':
          let id = event.item ? event.item.entite : null;
          this.router.navigate(['/pages/kyc/position-details',id]);
          break;
      }
    }
  }

  /** ON ACTION TOOLBAR */
  onActionToolbar(event: ToolbarOption): void {
    switch (event) {
      case ToolbarOption.DownloadAsExcel:
        this.exportService.exportAsExcelFile(this.positionList, POSITION_MOY_LIST_SETTINGS, 'Liste des operations', 'operations') ;
        break;
      case ToolbarOption.DownloadAsPDF:
        this.exportService.exportAsPDFFile(this.positionList, POSITION_MOY_LIST_SETTINGS, 'Liste des operations', 'operations');
        break;
    }
  }

}
