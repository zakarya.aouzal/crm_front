import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionSousRachatListComponent } from './position-sous-rachat-list.component';

describe('PositionSousRachatListComponent', () => {
  let component: PositionSousRachatListComponent;
  let fixture: ComponentFixture<PositionSousRachatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionSousRachatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionSousRachatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
