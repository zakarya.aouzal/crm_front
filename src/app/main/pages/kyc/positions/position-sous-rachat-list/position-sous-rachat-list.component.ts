import {Component, Input, OnInit} from '@angular/core';
import {
  PositionSousRachatFilterModel,
  PositionSousRachatModel
} from "../../../../models";
import {DataTableOutputActionsModel, DataTableSettingsModel, ToolbarOption} from "../../../../../shared/models";
import {PositionsService} from "../../../../services";
import {Router} from "@angular/router";
import {ExportService} from "../../../../../shared/services";
import {finalize} from "rxjs/operators";
import {POSITION_SOUS_RACHAT_LIST_SETTINGS} from "./position-sous-rachat-list.settings";

@Component({
  selector: 'app-position-sous-rachat-list',
  templateUrl: './position-sous-rachat-list.component.html',
  styleUrls: ['./position-sous-rachat-list.component.scss']
})
export class PositionSousRachatListComponent implements OnInit {

  @Input()
  set filter(value: { filterBody: PositionSousRachatFilterModel }) {
    if (!value) return;
    this._filter = value.filterBody;
    if (this._filter) {
     // this.settings = POSITION_SOUS_RACHAT_LIST_SETTINGS;
      this.loadListPositions(this._filter);
    }
  }

  @Input()
  set enableActions(value: boolean) {
    this._enableActions = value;
  }

  _enableActions: boolean = true;

  settings: DataTableSettingsModel = POSITION_SOUS_RACHAT_LIST_SETTINGS;
  positionList: PositionSousRachatModel[] = [];

  _filter: PositionSousRachatFilterModel = null;

  loading: boolean = false;

  constructor(private operationsService: PositionsService, private router: Router,
              private exportService: ExportService) { }

  ngOnInit(): void {
  }

  /** LOAD LIST OF POSITIONS */
  loadListPositions(filter: PositionSousRachatFilterModel): void {
    this.positionList = [];
    if (!filter) return;
    this.loading = true;
    this.operationsService.getLisPositionsSousRachat(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.positionList = response;
      });
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<PositionSousRachatModel>) {
    if (event) {
      switch (event.actionType) {
        case 'VIEW':
          let id = event.item ? event.item.entite : null;
          this.router.navigate(['/pages/kyc/position-details',id]);
          break;
      }
    }
  }

  /** ON ACTION TOOLBAR */
  onActionToolbar(event: ToolbarOption) {
    switch (event) {
      case ToolbarOption.DownloadAsExcel:
        this.exportService.exportAsExcelFile(this.positionList, POSITION_SOUS_RACHAT_LIST_SETTINGS, 'Liste des operations', 'operations') ;
        break;
      case ToolbarOption.DownloadAsPDF:
        this.exportService.exportAsPDFFile(this.positionList, POSITION_SOUS_RACHAT_LIST_SETTINGS, 'Liste des operations', 'operations');
        break;
    }
  }

}
