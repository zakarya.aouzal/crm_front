import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const POSITION_SOUS_RACHAT_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
      {
        name: 'entite',
        title: 'Entite',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.ENTITE'
        }
      }, {
        name: 'portefeuille',
        title: 'Portefeuille',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.PORTFEUILLE'
        }
      }, {
        name: 'descriptionPortef',
        title: 'Description',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.DESCRIPTION'
        }
      },
      // {
      //   name: 'descriptionFonds',
      //   title: 'Desc Fonds',
      //   format: DataTableColumnsFormat.STRING,
      //   data: {
      //     i18nValue:'COLUMNS.POSITION_SR_LIST.DESCRIPTION_FONDS'
      //   }
      // },
      {
        name: 'fonds',
        title: 'Fonds',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.FONDS'
        }
      },
      {
        name: 'categorieFonds',
        title: 'Categorie du Fonds',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.CATEGORIE_FONDS'
        }
      },
      {
        name: 'stock',
        title: 'Stock',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.STOCK'
        }
      },
      {
        name: 'dateVL',
        title: 'Date VL',
        format: DataTableColumnsFormat.DATE,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.DATE_VL'
        }
      },
      {
        name: 'vl',
        title: 'VL',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.VL'
        }
      },
      {
        name: 'valorisation',
        title: 'Valorisation',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.VALORISATION'
        }
      },
      {
        name: 'pmup',
        title: 'PMUP',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.PMUP'
        }
      },
      {
        name: 'pmvLatentes',
        title: 'PMV Latentes',
        format: DataTableColumnsFormat.NUMBER,
        data: {
          i18nValue:'COLUMNS.POSITION_SR_LIST.PVM_LATENTES'
        }
      }
    ], actions: [DataTableActions.VIEW]
};
