import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {GenericItemModel, PositionMoyFilterModel} from 'src/app/main/models';
import { ReferenceService } from 'src/app/main/services';

@Component({
  selector: 'app-position-filter',
  templateUrl: './position-filter.component.html',
  styleUrls: ['./position-filter.component.scss']
})
export class PositionFilterComponent implements OnInit {

  @Output()
  submited = new EventEmitter<{ filterBody: PositionMoyFilterModel}>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  selectedEntite: string;

  // List

  PortefeuilleList: GenericItemModel[] = [];
  EntiteList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder, private referenceService: ReferenceService) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadPortefeuilles();
    this.loadEntities();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      entite: [null],
      portefeuille: [null],
      dateDebut: [null],
      dateFin: [null],

    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
    }
  }

  /** SUBMIT EVENT */
  onSubmit(): void {
    if (this.formGroup.invalid ) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit({ filterBody: filter});
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): PositionMoyFilterModel {
    let filter: PositionMoyFilterModel = null;

    if (this.formGroup) {

      filter = {
        ...new PositionMoyFilterModel(),
        entite: this.formGroup.get('entite').value || '*',
        portefeuille: this.formGroup.get('portefeuille').value || '*',
        dateFin: this.formGroup.get('dateFin').value,
        dateDebut: this.formGroup.get('dateDebut').value
      };

    }

    return filter;
  }

  /** GET LIST OF PORTEFEUILLE */
  loadPortefeuilles(entite?: string): void {
    this.referenceService.getListPortefeuilles(entite)
      .subscribe(response => {
        this.PortefeuilleList = response;
        if (this.PortefeuilleList.length === 1) {
          this.formGroup.get('portefeuille').setValue(this.PortefeuilleList[0].code);
        }
      });
  }
  

  /** GET LIST OF ACCOUNT ENTITIES */
  loadEntities(): void {
    this.referenceService.getListEntites()
      .subscribe(response => {
        this.EntiteList = response;
      });
    }

/** ON CHANGE ENTITY */
Onchange(item) {
    if(item){
    
      this.formGroup.patchValue({entite:item});
      this.loadPortefeuilles(item);
    }

  }
/** RENITIIZE  */
  Onclear(){
    this.formGroup.patchValue({entite:"*"});
    this.formGroup.patchValue({portefeuille:"*"});
    this.PortefeuilleList=[];
  }


  /** RENITIIZE  */
  OnclearPortf(){
    this.formGroup.patchValue({portefeuille:"*"});
  }




}
