import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionFilterComponent } from './position-filter.component';

describe('PositionFilterComponent', () => {
  let component: PositionFilterComponent;
  let fixture: ComponentFixture<PositionFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
