import {Component, OnInit} from '@angular/core';
import {PositionMoyFilterModel, PositionSousRachatFilterModel} from '../../../../models';

@Component({
  selector: 'app-position-consultation',
  templateUrl: './position-consultation.component.html',
  styleUrls: ['./position-consultation.component.scss']
})
export class PositionConsultationComponent implements OnInit {


  filterMoy: { filterBody: PositionMoyFilterModel } = null;
  filterSousRachat: { filterBody: PositionSousRachatFilterModel } = null;

  constructor() {
  }

  ngOnInit(): void {
  }

  setFilters(event: { filterBody: any }): void {
    this.filterMoy = event;

    let sousRachatBody = {
      filterBody: {
        entite: event.filterBody.entite,
        portefeuille: event.filterBody.portefeuille,
        position: event.filterBody.dateFin
      }
    };
    this.filterSousRachat = sousRachatBody;
  }

}
