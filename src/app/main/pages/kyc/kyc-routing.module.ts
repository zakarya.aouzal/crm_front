import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OperationConsultationComponent } from "./operations/operation-consultation/operation-consultation.component";
import { PositionConsultationComponent } from "./positions/position-consultation/position-consultation.component";
import {NewDocumentComponent} from "../documents-management/new-document/new-document.component";


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'gestion-documents/new-document',
        component: NewDocumentComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.KYC.NEW_DOCUMENT'
          }
        },
      },
      {
        path: 'position-consultation',
        component: PositionConsultationComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.KYC.POSITION_CONSULTATION'
          }
        },
      },
      {
        path: 'operation-consultation',
        component: OperationConsultationComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.KYC.OPERATION_CONSULTATION'
          }
        },
      }
    ]
  }
];

@NgModule({
    declarations:[],
    imports:[
        CommonModule,
        RouterModule.forChild(routes)
    ],
    exports:[RouterModule]
})
export class KycRoutingModule{

}
