import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FileUploadModule } from "@iplab/ngx-file-upload";
import { TranslateModule } from "@ngx-translate/core";
import { AutocompleteLibModule } from "angular-ng-autocomplete";
import { BsModalService, ModalModule } from "ngx-bootstrap/modal";
import { OperationsService, PositionsService, ReferenceService } from "src/app/main/services";
import { SharedModule } from "src/app/shared/shared.module";
import { GedService } from "../../services";

import { KycRoutingModule } from "./kyc-routing.module";
import { OperationConsultationComponent } from "./operations/operation-consultation/operation-consultation.component";
import { OperationFilterComponent } from './operations/operation-filter/operation-filter.component';
import { OperationListComponent } from './operations/operation-list/operation-list.component';
import { PositionConsultationComponent } from "./positions/position-consultation/position-consultation.component";
import { PositionFilterComponent } from './positions/position-filter/position-filter.component';
import { PositionMoyListComponent } from './positions/position-moy-list/position-moy-list.component';
import { PositionSousRachatListComponent } from './positions/position-sous-rachat-list/position-sous-rachat-list.component';

@NgModule({
  declarations: [
    OperationConsultationComponent,
    PositionConsultationComponent,
    OperationFilterComponent,
    PositionFilterComponent,
    OperationListComponent,
    PositionMoyListComponent,
    PositionSousRachatListComponent,
  ],
  imports: [
    CommonModule,
    KycRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule,
    AutocompleteLibModule,
    TranslateModule,
    ModalModule.forRoot()
  ],
  exports:[
    OperationListComponent,
    PositionSousRachatListComponent
  ],
  providers: [ReferenceService, OperationsService, PositionsService, GedService, BsModalService]
})
export class KycModule {

}
