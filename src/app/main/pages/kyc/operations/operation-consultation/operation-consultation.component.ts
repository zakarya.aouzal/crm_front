import { Component, OnInit } from '@angular/core';
import { OperationFilterModel } from 'src/app/main/models/kyc/operation/operation.model';

@Component({
  selector: 'app-operation-consultation',
  templateUrl: './operation-consultation.component.html',
  styleUrls: ['./operation-consultation.component.scss']
})
export class OperationConsultationComponent implements OnInit {

  filter: { filterBody: OperationFilterModel} = null;

  constructor() { }

  ngOnInit(): void {
  }

}
