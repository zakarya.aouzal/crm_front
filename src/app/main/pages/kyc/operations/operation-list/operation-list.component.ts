import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { OperationFilterModel, OperationModel } from 'src/app/main/models/kyc/operation/operation.model';
import { OperationsService } from 'src/app/main/services';
import { DataTableOutputActionsModel, DataTableSettingsModel, ToolbarOption } from 'src/app/shared/models';
import { ExportService } from 'src/app/shared/services';
import {OPERATION_LIST_SETTINGS} from 'src/app/main/pages/kyc/operations/operation-list/operation-list.settings'
@Component({
  selector: 'app-operation-list',
  templateUrl: './operation-list.component.html',
  styleUrls: ['./operation-list.component.scss']
})
export class OperationListComponent implements OnInit {

  @Input()
  set filter(value: { filterBody: OperationFilterModel }) {
    console.log('filter recu: ' + value);
    if (!value) return;
    this._filter = value.filterBody;
    if (this._filter) {
         // this.settings = OPERATION_LIST_SETTINGS;
          this.loadListOperations(this._filter);
    }
  }

  settings: DataTableSettingsModel = OPERATION_LIST_SETTINGS;

  operationList: OperationModel[] = new Array<OperationModel>();

  _filter: OperationFilterModel = null;

  loading: boolean = false;

  constructor(private operationsService: OperationsService,
              private router: Router,
              private exportService: ExportService) { }

  ngOnInit(): void {

  }

  /** LOAD LIST OF OPERATIONS */
  loadListOperations(filter: OperationFilterModel) {
    this.operationList = [];
    if (!filter) return;
    this.loading = true;
    this.operationsService.getListOperations(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.operationList = response;
      });
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<OperationModel>) {
    if (event) {
      switch (event.actionType) {
        case 'VIEW':
          let id = event.item ? event.item.numOperation : null;
          this.router.navigate(['/pages/kyc/operation-details',id]);
          break;
      }
    }
  }

  /** ON ACTION TOOLBAR */
  onActionToolbar(event: ToolbarOption) {
    switch (event) {
      case ToolbarOption.DownloadAsExcel:
              this.exportService.exportAsExcelFile(this.operationList, OPERATION_LIST_SETTINGS, 'Liste des operations', 'operations') ;
        break;
      case ToolbarOption.DownloadAsPDF:
            this.exportService.exportAsPDFFile(this.operationList, OPERATION_LIST_SETTINGS, 'Liste des operations', 'operations');
        break;
    }
  }

}
