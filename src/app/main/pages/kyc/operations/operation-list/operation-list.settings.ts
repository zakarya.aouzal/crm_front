import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const OPERATION_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'numOperation',
      title: 'N° operation',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.OPERATION_LIST.N_OPERATION'
      }
    }, {
      name: 'portefeuille',
      title: 'Portefeuille',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.PORTEFEUILLE'
      }
    }, {
      name: 'descriptionPortf',
      title: 'Description',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.DESCRIPTION'
      }
    },
    {
      name: 'poste',
      title: 'Poste',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.POSTE'
      }
    },
    {
      name: 'descriptionFonds',
      title: 'Fonds',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.DESCRIPTION_FONDS'
      }
    }, {
      name: 'dateValeur',
      title: 'Date Opération',
      format: DataTableColumnsFormat.DATE,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.DATE_OPERATION'
      }
    }, {
      name: 'quantite',
      title: 'Quantite',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.QUANTITE'
      }
    },
    {
      name: 'cours',
      title: 'Cours',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.COURS'
      }
    },
    {
      name: 'montantBrut',
      title: 'Montant Brut',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.MONTANT_BRUT'
      }
    },
    {
      name: 'montantNet',
      title: 'Montant Net',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.OPERATION_LIST.MONTANT_NET'
      }
    },
    // {
    //   name: 'frais',
    //   title: 'Frais',
    //   format: DataTableColumnsFormat.NUMBER,
    //   data: {
    //     i18nValue:'COLUMNS.OPERATION_LIST.FRAIS'
    //   }
    // },
    // {
    //   name: 'taxes',
    //   title: 'Taxes',
    //   format: DataTableColumnsFormat.NUMBER,
    //   data: {
    //     i18nValue:'COLUMNS.OPERATION_LIST.TAXES'
    //   }
    // },
    // {
    //   name: 'pmv',
    //   title: 'PVM',
    //   format: DataTableColumnsFormat.NUMBER,
    //   data: {
    //     i18nValue:'COLUMNS.OPERATION_LIST.PVM'
    //   }
    // }
  ], 
  // actions: [DataTableActions.VIEW]
};
