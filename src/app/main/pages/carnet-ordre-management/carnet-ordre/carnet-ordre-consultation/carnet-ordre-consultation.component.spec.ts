import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarnetOrdreConsultationComponent } from './carnet-ordre-consultation.component';

describe('CarnetOrdreConsultationComponent', () => {
  let component: CarnetOrdreConsultationComponent;
  let fixture: ComponentFixture<CarnetOrdreConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarnetOrdreConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarnetOrdreConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
