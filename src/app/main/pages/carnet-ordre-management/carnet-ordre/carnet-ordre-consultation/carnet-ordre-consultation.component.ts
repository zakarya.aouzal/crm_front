import { Component, OnInit } from '@angular/core';
import {CarnetOrdreFilterModel} from '../../../../models/carnet-ordre/carnet-ordre.model';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {PasserOrdreComponent} from '../passer-ordre/passer-ordre.component';
import {AccountType, GenericItemModel} from '../../../../models';
import {ReferenceService} from '../../../../services';

@Component({
  selector: 'app-carnet-ordre-consultation',
  templateUrl: './carnet-ordre-consultation.component.html',
  styleUrls: ['./carnet-ordre-consultation.component.scss']
})
export class CarnetOrdreConsultationComponent implements OnInit {
  clientList: GenericItemModel[] = [];
  filter: { filterBody: CarnetOrdreFilterModel} = null;
  constructor(private dialog : MatDialog , private referenceService : ReferenceService) { }

  ngOnInit(): void {
  }

  passerOrdre() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'passerOrdre-popup' ;
    dialogConfig.autoFocus = true ;
    this.dialog.open(PasserOrdreComponent, dialogConfig)
  }
}
