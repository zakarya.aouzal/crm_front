import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasserOrdreComponent } from './passer-ordre.component';

describe('PasserOrdreComponent', () => {
  let component: PasserOrdreComponent;
  let fixture: ComponentFixture<PasserOrdreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasserOrdreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasserOrdreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
