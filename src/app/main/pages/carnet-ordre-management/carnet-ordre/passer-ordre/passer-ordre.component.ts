import { Component, OnInit, Inject } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountType, GenericItemModel} from '../../../../models';
import {CarnetOrdreService} from '../../../../services/carnetOrdre/carnet-ordre.service';
import {ReferenceService} from '../../../../services';
import {OrdreModel} from '../../../../models/carnet-ordre/carnet-ordre.model';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-passer-ordre',
  templateUrl: './passer-ordre.component.html',
  styleUrls: ['./passer-ordre.component.scss']
})
export class PasserOrdreComponent implements OnInit {

  formGroup: FormGroup = null;
  clientList: GenericItemModel[] = null;
  client : string = ''
  compteEspeceList : GenericItemModel[] = [];
  sensList : GenericItemModel[] = [];
  fondList: GenericItemModel[] = [];
  optionList : any[] = [ {code:'B' , value : 'Budget' }  , {code : 'Q' , value : 'Quantite' }];
  showValidationsMsg: boolean = false;
  vl_estime: number = 0.00;
  stock : number = 0.00 ;
  currentClient : string = '';
  currentFond : string = '';
  checkedSens: string = 'S';
  checkedOption : string = 'Budget'

  constructor(private formBuilder : FormBuilder ,
              private referenceService : ReferenceService ,
              private carnetOrdreService : CarnetOrdreService ,
              @Inject(MAT_DIALOG_DATA) public data : any ) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.loadFonds();
    this.loadSens();
    if(this.data){                  // càd on est dans un client specifique
      this.loadCurrentClient();
    }
    else
      this.loadClients();           // le cas generale  on est pas dans un client specifique
  }
  createFormGroup(): void {
      this.formGroup = this.formBuilder.group({
      client: ['', ] ,
      compteEspece : ['' , [Validators.required]],
      fond : ['' , [Validators.required]],
      option: ['Budget', [Validators.required]] ,
      budget : [ '' ] ,
      quantite : ['' ] ,
      sens : ['S' , ],
      }
    );
  }
  initFormGroup() {
    this.formGroup.reset();
    this.showValidationsMsg = false
    this.formGroup.get(['option']).setValue('Budget') ;  //conserver la meme option par default ('Budget')
    this.formGroup.get(['sens']).setValue('S') ; //conserver le meme sens par default ('Souscription')
  }

   loadCurrentClient() {
    switch (this.data.type) {
      case AccountType.PP:
        this.client =  this.data.nom + ' '+ this.data.prenom
        this.currentClient = 'M0018' ;         // this.data.code
        this.loadCompteEspece('M0018');  // this.data.code
        break;
      case AccountType.PM:
        this.client = this.data.raisonSociale ;
        this.currentClient = this.data.code ;
        this.loadCompteEspece(this.data.code);
        break;
    }
  }
  loadClients() {
    this.referenceService.getClientsList().subscribe(
      (response: any) => {
        this.clientList = response;
      }
    );
  }

  loadCompteEspece(client : string) {
    this.referenceService.getCompteEspeceList(client).subscribe(
      (response: any) => {
        this.compteEspeceList = response;
      }
    );
  }

  loadFonds(){
    this.referenceService.getFondsPasserOrdreList().subscribe(
      (response : any ) => {
        this.fondList = response ;
      }
    )
  }
  loadInfoSousRachatTitre(client : string , fond : string){
    this.referenceService.getInfoSousRachatTitre(client, fond).subscribe(
      (response: any) => {
          this.vl_estime = response.vl_estime ;
          this.stock = response.stock ;
      }
    );
  }
  loadSens() {
    this.referenceService.getSensList().subscribe(
      (response: any) => {
        this.sensList = response;
      }
    );
  }

  onSelectClient(client: string) {
    this.currentClient = client ;
    this.loadCompteEspece(client);
  }
  onSelectCompteEspece(fond: any) {
    this.currentFond = fond ;
    this.loadInfoSousRachatTitre(this.currentClient , this.currentFond);
  }
  onSubmit() : void{
    console.log(this.formGroup.get('budget').value)
    if (this.formGroup.invalid) {
      console.log('non valid')
      this.showValidationsMsg = true;
      return;
    }
    if (this.formGroup.valid) {
      const payload = this.generatePayload();
      this.carnetOrdreService.passerOrdre(payload).subscribe(
        (next:any)=> { console.log('passer ordre') }
      )
    }
  }

   generatePayload() {
     let ordre : OrdreModel = null;
     let budgetPayload : number = null ;
     let quantitePayload : number = null ;
     switch (this.checkedOption) {
       case 'Budget':
             budgetPayload = this.formGroup.get('budget').value ,
             quantitePayload = (this.formGroup.get('budget').value)/this.vl_estime
             break;
       case 'Quantite':
             budgetPayload =  (this.formGroup.get('quantite').value)*this.vl_estime
             quantitePayload =  this.formGroup.get('quantite').value
             break;
     }
     if (this.formGroup) {
       ordre = {
         ...new OrdreModel(),
         client: this.currentClient ,
         compteEspece: this.formGroup.get('compteEspece').value,
         fond : this.formGroup.get('fond').value ,
         budget:    budgetPayload ,
         quantite:  quantitePayload ,
         vl : this.vl_estime ,
         sens : this.formGroup.get('sens').value
       };
     }
     return ordre ;
  }

  budgetCalcule() : number{
    if(this.formGroup.get('quantite').value>0)
      return (this.formGroup.get('quantite').value) * (this.vl_estime);
    return null;
  }

  quantiteCalcule() : number{
    if(this.formGroup.get('budget').value>0)
      return (this.formGroup.get('budget').value) / (this.vl_estime);
    return null ;
  }


  onOptionRadioChange( option : string)  {
    this.checkedOption = option ;
    switch (option) {
      case 'Budget':
        this.checkedOption = 'Budget';
        this.formGroup.patchValue({'budget': null })  // cas limit : si l user remplir le budget puis il bascule vers qte il faut initialiser les 2 champs
        this.formGroup.patchValue({'quantite': null}) // et vice versa
        break;
      case 'Quantite':
        this.checkedOption = 'Quantite';
        this.formGroup.patchValue({'quantite': null})
        this.formGroup.patchValue({'budget': null})
        break;
    }

}

  onSensRadioChange( sens : string) {
    this.checkedSens = sens ;
  }



}
