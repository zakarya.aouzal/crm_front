import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {TranslateModule} from '@ngx-translate/core';
import {ModalModule} from 'ngx-bootstrap/modal';
import {CarnetOrdreRoutingModule} from './carnet-ordre-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';
import {SharedModule} from '../../../../shared/shared.module';
import {CarnetOrdreConsultationComponent} from './carnet-ordre-consultation/carnet-ordre-consultation.component';
import {CarnetOrdreFilterComponent} from './carnet-ordre-filter/carnet-ordre-filter.component';
import {CarnetOrdreListComponent} from './carnet-ordre-list/carnet-ordre-list.component';
import {MaterialModule} from '../../material.module';
import {PasserOrdreComponent} from './passer-ordre/passer-ordre.component';


@NgModule({
  declarations: [CarnetOrdreConsultationComponent, CarnetOrdreFilterComponent , CarnetOrdreListComponent , PasserOrdreComponent],
  imports: [
    CommonModule ,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CarnetOrdreRoutingModule,
    TranslateModule,
    NgxSelectModule,
    MaterialModule ,
    ModalModule.forRoot()
  ]
})
export class CarnetOrdreModule { }
