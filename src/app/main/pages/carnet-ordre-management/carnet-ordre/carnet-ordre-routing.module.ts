import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PositionConsultationComponent} from '../../kyc/positions/position-consultation/position-consultation.component';
import {CarnetOrdreConsultationComponent} from './carnet-ordre-consultation/carnet-ordre-consultation.component';

const routes: Routes = [
  {
    path: 'carnet-ordre-management-consultation',
    component: CarnetOrdreConsultationComponent
  }];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CarnetOrdreRoutingModule { }
