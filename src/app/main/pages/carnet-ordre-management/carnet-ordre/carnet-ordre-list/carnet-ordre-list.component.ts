import {Component, Input, OnInit} from '@angular/core';
import {DataTableSettingsModel} from '../../../../../shared/models';
import {OPERATION_LIST_SETTINGS} from '../../../kyc/operations/operation-list/operation-list.settings';
import {CARNET_ORDRE_LIST_SETTINGS} from './carnet-ordre-list-settings';
import {OperationFilterModel} from '../../../../models';
import {CarnetOrdreFilterModel, CarnetOrdreModel} from '../../../../models/carnet-ordre/carnet-ordre.model';
import {CarnetOrdreService} from '../../../../services/carnetOrdre/carnet-ordre.service';

@Component({
  selector: 'app-carnet-ordre-list',
  templateUrl: './carnet-ordre-list.component.html',
  styleUrls: ['./carnet-ordre-list.component.scss']
})
export class CarnetOrdreListComponent implements OnInit {
  @Input()
  set filter(value: { filterBody: CarnetOrdreFilterModel }) {
    console.log('filter recu: ' + value);
    if (!value) return;
    this.filterModel = value.filterBody;
    if (this.filterModel) {
      this.loadListCarnetOrdre(this.filterModel);
    }
  }

  filterModel: CarnetOrdreFilterModel = null ;
  settings: DataTableSettingsModel = CARNET_ORDRE_LIST_SETTINGS;
  carnetOrdreList: CarnetOrdreModel[] = new Array<CarnetOrdreModel>();
  constructor(private carnetOrdreService: CarnetOrdreService) { }

  ngOnInit(): void {
  }

   loadListCarnetOrdre(filter: CarnetOrdreFilterModel) {
    if(!filter) return;
    this.carnetOrdreService.getListCarnetOrdre(filter).subscribe(
      (value: any) => { this.carnetOrdreList = value ; }
    );
  }
}
