import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarnetOrdreListComponent } from './carnet-ordre-list.component';

describe('CarnetOrdreListComponent', () => {
  let component: CarnetOrdreListComponent;
  let fixture: ComponentFixture<CarnetOrdreListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarnetOrdreListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarnetOrdreListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
