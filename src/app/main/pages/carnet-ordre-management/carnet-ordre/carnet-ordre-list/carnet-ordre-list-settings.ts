import {DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const CARNET_ORDRE_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'NUM_ORDRE',
      title: 'N° Ordre',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.CARNET_ORDRE_LIST.NUM_ORDRE'
      }
    }, {
      name: 'CODE_CLIENT',
      title: 'Code client',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.CODE_CLIENT'
      }
    }, {
      name: 'DESC_CLIENT',
      title: 'Description client',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.DESC_CLIENT'
      }
    },
    {
      name: 'SENS',
      title: 'Sens',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.SENS'
      }
    },
    {
      name: 'TITRE',
      title: 'Titre',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.TITRE'
      }
    }, {
      name: 'DESC_TITRE',
      title: 'Description titre',
      format: DataTableColumnsFormat.DATE,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.DESC_TITRE'
      }
    }, {
      name: 'QUANTITE',
      title: 'Quantite',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.QUANTITE'
      }
    },
    {
      name: 'COURS',
      title: 'Cours',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.COURS'
      }
    },
    {
      name: 'DATE_ORDRE',
      title: 'Date ordre',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.DATE_ORDRE'
      }
    },
    {
      name: 'DATE_EXPIRATION',
      title: 'Date Expiration',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.CARNET_ORDRE_LIST.DATE_EXPIRATION'
      }
    },

  ],
}
