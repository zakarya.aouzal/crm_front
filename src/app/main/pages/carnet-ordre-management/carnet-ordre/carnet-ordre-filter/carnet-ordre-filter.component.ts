import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {AccountType, GenericItemModel, OperationFilterModel} from '../../../../models';
import {TranslateService} from '@ngx-translate/core';
import {finalize} from 'rxjs/operators';
import {AccountService, ReferenceService} from '../../../../services';
import {Subscription} from 'rxjs';
import {CarnetOrdreService} from '../../../../services/carnetOrdre/carnet-ordre.service';
import {CarnetOrdreFilterModel, CarnetOrdreModel} from '../../../../models/carnet-ordre/carnet-ordre.model';

@Component({
  selector: 'app-carnet-ordre-filter',
  templateUrl: './carnet-ordre-filter.component.html',
  styleUrls: ['./carnet-ordre-filter.component.scss']
})
export class CarnetOrdreFilterComponent implements OnInit {
  @Output()
  submited = new EventEmitter<{ filterBody: CarnetOrdreFilterModel }>();
  formGroup: FormGroup = null;
  fundsTypeList: GenericItemModel[] = [];
  fundsList: GenericItemModel[] = [];
  sensList: GenericItemModel[] = [];
  typeClientList: GenericItemModel[] = [];
  clientList: GenericItemModel[] = [];
  carnetOrdreList: CarnetOrdreModel[] = new Array<CarnetOrdreModel>();
  carnetOrdreFilter: CarnetOrdreFilterModel = new CarnetOrdreFilterModel();

  constructor(private translate: TranslateService,
              private accountService: AccountService,
              private carnetOrdreService: CarnetOrdreService, private refercenceService: ReferenceService) {
  }

  ngOnInit(): void {
    this.formGroup = new FormGroup({
        fundsType: new FormControl(),
        fund: new FormControl(),
        sens: new FormControl(),
        clientType: new FormControl(),
        client: new FormControl(),
        date : new FormControl()
      }
    );
    this.loadFundsTypes();
    this.loadFunds();
    this.loadSens();
    this.loadTypesClient();
    this.loadClients();
  }

  onSubmit() {
    switch (this.formGroup.get('clientType').value) {
      case 'PP' :
        this.formGroup.get('clientType').setValue('O');
        break;
      case 'PM' :
        this.formGroup.get('clientType').setValue('N');
        break;
    }
    if (this.formGroup.valid) {
      const filter = this.generatePayload();
      if (filter) {
        this.submited.emit({filterBody: filter});
      }
    }
  }

  initFormGroup() {
    this.formGroup.reset();
  }

  loadFundsTypes() {
    this.refercenceService.getTypeFundsList().subscribe(
      (value: any) => {
        this.fundsTypeList = value;
      }
    );
  }

  loadFunds() {
    this.refercenceService.getFundsList().subscribe(
      (value: any) => {
        this.fundsList = value;
      }
    );
  }

  loadSens() {
    this.refercenceService.getSensList().subscribe(
      (value: any) => {
        this.sensList = value;
      }
    );
  }

  loadTypesClient() {
    this.refercenceService.getTypesClientList().subscribe(
      (value: any) => {
        this.typeClientList = value;
      }
    );
  }

  loadClients() {
    this.refercenceService.getClientsList().subscribe(
      (value: any) => {
        this.clientList = value;
      }
    );
  }

  private generatePayload(): CarnetOrdreFilterModel {
    let filter: CarnetOrdreFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new CarnetOrdreFilterModel(),
        typeFonds: this.formGroup.get('fundsType').value || '*',
        fond: this.formGroup.get('fund').value || '*',
        sens: this.formGroup.get('sens').value || '*',
        typeClient: this.formGroup.get('clientType').value || '*',
        client: this.formGroup.get('client').value || '*' ,
        date: this.formGroup.get('date').value
      };

    }
    return filter;
  }


}
