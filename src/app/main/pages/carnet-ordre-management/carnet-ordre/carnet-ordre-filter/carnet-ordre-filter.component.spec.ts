import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarnetOrdreFilterComponent } from './carnet-ordre-filter.component';

describe('CarnetOrdreFilterComponent', () => {
  let component: CarnetOrdreFilterComponent;
  let fixture: ComponentFixture<CarnetOrdreFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarnetOrdreFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarnetOrdreFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
