import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../../../shared/components/page-not-found/page-not-found.component';
import { ContactConsultationComponent } from "./contact/contact-consultation/contact-consultation.component";
import { NewContactComponent } from './contact/new-contact/new-contact.component';


const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: {
        label: 'BREADCRUMBS.CONTACT_MANAGEMENT.TITLE'
      }
    },
    children: [
      {
        path: 'new-contact',
        component: NewContactComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.CONTACT_MANAGEMENT.NEW_CONTACT'
          }
        },
      }, {
        path: 'contact-consultation',
        component: ContactConsultationComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.CONTACT_MANAGEMENT.CONTACT_CONSULTATION'
          }
        },
      }, {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ContactstManagementRoutingModule { }
