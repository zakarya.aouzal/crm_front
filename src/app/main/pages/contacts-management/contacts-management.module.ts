import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from '@iplab/ngx-file-upload';



import {AccountContactListComponent} from './contact/account-contact-list/account-contact-list.component';
import {AddContactComponent} from './contact/add-contact/add-contact.component';
import {ContactConsultationComponent} from './contact/contact-consultation/contact-consultation.component';
import {ContactFilterComponent} from './contact/contact-filter/contact-filter.component';
import {ContactFormComponent} from './contact/contact-form/contact-form.component';
import {ContactListComponent} from './contact/contact-list/contact-list.component';
import {NewContactComponent} from './contact/new-contact/new-contact.component';
import {BsModalService, ModalModule} from "ngx-bootstrap/modal";
import { CreateContactComponent } from './contact/create-contact/create-contact.component';
import { EditContactComponent } from './contact/edit-contact/edit-contact.component';
import { ReferenceService,ContactService,CompteContactService, AccountService} from '../../services';
import {SharedModule} from '../../../shared/shared.module'
import { ContactstManagementRoutingModule } from './contacts-management-routing.module';
@NgModule({
  declarations: [
    ContactListComponent,
    NewContactComponent,
    AddContactComponent,
    ContactConsultationComponent,
    ContactFilterComponent,
    AccountContactListComponent,
    ContactFormComponent,
    CreateContactComponent,
    EditContactComponent,

  ],
  imports: [
    CommonModule,
    ContactstManagementRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule,
    ModalModule.forRoot()
  ],
  exports: [
    ContactListComponent,
    NewContactComponent,
    AddContactComponent,
    ContactConsultationComponent,
    ContactFilterComponent,
    AccountContactListComponent,
    ContactFormComponent,
    CreateContactComponent,
    EditContactComponent,
  ],
  providers: [
     ReferenceService,
     ContactService,
     CompteContactService,
     BsModalService,
     AccountService
  ]
})
export class ContactsManagementModule { }
