import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CompteContactService, ContactService} from "../../../../services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {AccountType, CompteContactPayload, ContactModel} from "../../../../models";

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {

  constructor(private contactService: ContactService,
              private fb: FormBuilder,
              private ccService: CompteContactService,
              private toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public accountData: { contact: ContactModel, compteContact: CompteContactPayload, viewMode: boolean, viewOnly?: boolean },
              private dialogRef: MatDialogRef<EditContactComponent>) {
  }

  // form group
  showFormErrors: boolean = false;

  loading: boolean = false;

  compteContactForm: FormGroup = null;

  compteContactPayload: CompteContactPayload = null;

  ngOnInit(): void {
    this.createCompteContactForm();
    this.fillCompteContactForm(this.accountData?.compteContact);

    if (this.accountData?.viewMode) {
      this.compteContactForm?.disable();
    }
  }

  createCompteContactForm(): void {
    this.compteContactForm = this.fb.group({
      descriptionContact: [null, Validators.required],
      role: [null, Validators.required],
      qualite: [null, Validators.required],
      dateDebutQualite: [null],
      dateFinQualite: [null],
      pourcentageCapital: [null],
      pourcentageDroitVote: [null]
    });
  }

  fillCompteContactForm(payload: CompteContactPayload): void {
    this.compteContactForm.patchValue({
      descriptionContact: payload?.descriptionContact,
      role: payload?.role,
      qualite: payload?.qualite,
      dateDebutQualite: payload?.dateDebutQualite,
      dateFinQualite: payload?.dateFinQualite,
      pourcentageCapital: payload?.pourcentageCapital,
      pourcentageDroitVote: payload?.pourcentageDroitVote
    });
  }

  onSubmit(): void {
    if (this.accountData?.viewMode) {
      this.accountData.viewMode = false;
      this.compteContactForm?.enable();
      return;
    }

    if (this.compteContactForm?.invalid) {
      this.showFormErrors = true;
      return;
    }
    this.showFormErrors = false;

    this.compteContactPayload = this.generateCompteContactPayload();
    this.closePopup();
  }

  private generateCompteContactPayload(): CompteContactPayload {
    return {
      ...new CompteContactPayload(),
      codeCompte: this.accountData?.compteContact?.codeCompte,
      codeContact: this.accountData?.compteContact?.codeContact,
      descriptionContact: this.compteContactForm.get('descriptionContact').value,
      role: this.compteContactForm.get('role').value,
      qualite: this.compteContactForm.get('qualite').value,
      dateDebutQualite: this.compteContactForm.get('dateDebutQualite').value,
      dateFinQualite: this.compteContactForm.get('dateFinQualite').value,
      pourcentageCapital: this.compteContactForm.get('pourcentageCapital').value,
      pourcentageDroitVote: this.compteContactForm.get('pourcentageDroitVote').value
    };
  }

  closePopup(): void {
    this.dialogRef.close(true);
  }

}
