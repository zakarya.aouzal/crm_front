import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountContactListComponent } from './account-contact-list.component';

describe('AccountContactListComponent', () => {
  let component: AccountContactListComponent;
  let fixture: ComponentFixture<AccountContactListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountContactListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
