import {Component, Input, OnInit} from '@angular/core';
import {ContactAccountModel} from '../../../../models';
import {ContactService} from '../../../../services';

@Component({
  selector: 'app-account-contact-list',
  templateUrl: './account-contact-list.component.html',
  styleUrls: ['./account-contact-list.component.scss']
})
export class AccountContactListComponent implements OnInit {

  @Input()
  set accountCode(value: string) {
    this.loadContacts(value);
  }

  contactsList: ContactAccountModel[] = [];

  constructor(private contactService: ContactService) { }

  ngOnInit(): void { }

  /** LOAD CONTACTS */
  loadContacts(accountCode: string): void {
    this.contactsList = [];
    this.contactService.getListContactsByAccount(accountCode)
      .subscribe(response => this.contactsList = response);
  }

}
