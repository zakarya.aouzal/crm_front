import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ContactType, GenericItemModel} from "../../../../models";
import {ContactService, ReferenceService} from "../../../../services";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {finalize} from "rxjs/operators";
import {ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContactFormComponent implements OnInit {

  // form group
  contactPPForm: FormGroup = null;
  contactPMForm: FormGroup = null;

  // validation
  showValidationsMsg = false;
  loading: boolean;

  // references
  countryList: GenericItemModel[] = [];

  constructor(private contactService: ContactService,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private referenceService: ReferenceService,
              @Inject(MAT_DIALOG_DATA) public contactData: { code: any, contactType: ContactType, editEnabled: boolean },
              private dialogRef: MatDialogRef<ContactFormComponent>) {
  }

  ngOnInit(): void {
    this.loadContactForms();
    this.loadContact();
    this.checkEditEnabled();
    this.loadCountries();
  }

  /** Load Current Contact */
  loadContact(): void {
    if (this.contactData?.contactType === ContactType.PP) {
      this.contactService.getContactPPByCode(this.contactData?.code)
        .subscribe(contact => {
          this.contactPPForm.patchValue({
            nom: contact.nom,
            prenom: contact.prenom,
            civilite: contact.civilite,
            dateNaissance: contact.dateNaissance,
            email: contact.email,
            nomPrenomPere: contact.nomPrenomPere,
            nomPrenomMere: contact.nomPrenomMere,
            mobile1: contact.mobile1,
            mobile2: contact.mobile2,
            adresse1: contact.adresse1,
            adresse2: contact.adresse2,
            nationalite: contact.nationalite,
            typeResidence: contact.typeResidence,
            paysResidence: contact.paysResidence,
            ville: contact.ville,
            codePostale: contact.codePostale,
            profilRisque: contact.profilRisque,
            statutPPE: contact.statutPPE,
            rcRn: contact.rcRn,
            numTaxePro: contact.numTaxePro,
            tribImmatricule: contact.tribImmatricule,
            pieceIdentite: contact.pieceIdentite,
            numPI: contact.numPI,
            emetteurPI: contact.emetteurPI,
            dateEmiPI: contact.dateEmiPI,
            dateExiPI: contact.dateExiPI
          });
        });
    } else {
      this.contactService.getContactPMByCode(this.contactData?.code)
        .subscribe(contact => {
          this.contactPMForm.patchValue({
            raisonSociale: contact.raisonSociale,
            formeJuridique: contact.formeJuridique,
            agrementDelivrance: contact.agrementDelivrance,
            tribImmatricule: contact.tribImmatricule,
            statutPPE: contact.statutPPE,
            statutFatca: contact.statutFatca,
            mobile1: contact.mobile1,
            mobile2: contact.mobile2,
            fax: contact.fax,
            email: contact.email,
            ville: contact.ville,
            codePostale: contact.codePostale,
            adresse1: contact.adresse1,
            idFiscal: contact.idFiscal,
            rc: contact.rc,
            ice: contact.ice,
            numTaxePro: contact.numTaxePro,
            profilRisque: contact.profilRisque
          });
        });
    }
  }

  /** CREATE FORM GROUPS */
  loadContactForms(): void {
    this.contactPPForm = this.fb.group({
      // infos g�n�rales
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      civilite: [null, [Validators.required]],
      dateNaissance: ['', [Validators.required]],
      email: ['', [Validators.required]],
      nomPrenomPere: [null],
      nomPrenomMere: [null],
      mobile1: ['', [Validators.required]],
      mobile2: [''],
      adresse1: ['', [Validators.required]],
      adresse2: [null],
      nationalite: ['', [Validators.required]],
      typeResidence: ['', [Validators.required]],
      paysResidence: ['', [Validators.required]],
      ville: ['', [Validators.required]],
      codePostale: ['', [Validators.required]],
      profilRisque: ['', [Validators.required]],
      statutPPE: ['', [Validators.required]],
      // infos indentit�s
      rcRn: ['', [Validators.required]],
      numTaxePro: ['', [Validators.required]],
      tribImmatricule: ['', [Validators.required]],
      pieceIdentite: ['', [Validators.required]],
      numPI: ['', [Validators.required]],
      emetteurPI: ['', [Validators.required]],
      dateEmiPI: [null],
      dateExiPI: [null]
    });
    this.contactPMForm = this.fb.group({
      raisonSociale: [null, [Validators.required]],
      formeJuridique: ['', [Validators.required]],
      agrementDelivrance: ['', [Validators.required]],
      tribImmatricule: ['', [Validators.required]],
      statutPPE: ['', [Validators.required]],
      statutFatca: ['', [Validators.required]],
      mobile1: [null, [Validators.required]],
      mobile2: [null],
      fax: ['', [Validators.required]],
      email: ['', [Validators.required]],
      ville: ['', [Validators.required]],
      codePostale: ['', [Validators.required]],
      adresse1: ['', [Validators.required]],
      idFiscal: ['', [Validators.required]],
      rc: ['', [Validators.required]],
      ice: ['', [Validators.required]],
      numTaxePro: ['', [Validators.required]],
      profilRisque: ['', [Validators.required]],
    });
  }

  /** Check Edit/Show Mode */
  private checkEditEnabled(): void {
    if (!this.contactData?.editEnabled) {
      this.contactPPForm.disable();
      this.contactPMForm.disable();
    }
  }

  /** Edit mode switch */
  private switchEditMode(): void {
    this.contactData.editEnabled = true;
    this.contactPPForm.enable();
    this.contactPMForm.enable();
  }

  /** Forms reset methods */
  onReset(): void {
    this.showValidationsMsg = false;
    if (this.contactPPForm) {
      this.contactPPForm.reset();
    }
    if (this.contactPMForm) {
      this.contactPMForm.reset();
    }
  }

  /** ContactPP Form sumbmission */
  onSubmitContactPP(): void {
    if (!this.contactData?.editEnabled) {
      this.switchEditMode();
      return;
    }
    if (this.contactPPForm.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const contactPP = {
      ...this.generateContactSubmitPayload(),
      code: this.contactData?.code
    };
    this.loading = true;
    this.contactService.updateContactPP(contactPP)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(value => {
        if (value.valid) {
          this.toastr.success('Contact modifi�');
          this.closePopup(true);
        } else {
          this.toastr.error('Erreur pendant la modification du contact');
        }
      });
  }

  /** ContactPM Form submission */
  onSubmitContactPM(): void {
    if (!this.contactData?.editEnabled) {
      this.switchEditMode();
      return;
    }
    if (this.contactPMForm.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const contactPM = {
      ...this.generateContactSubmitPayload(),
      code: this.contactData?.code
    };
    this.loading = true;
    this.contactService.updateContactPM(contactPM)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(value => {
        if (value.valid) {
          this.toastr.success('Contact modifi�');
          this.closePopup(true);
        } else {
          this.toastr.error('Erreur pendant la modification du contact');
        }
      });
  }

  private generateContactSubmitPayload(): any {
    return this.contactData?.contactType === ContactType.PP ?
      {
        // infos g�n�rales
        nom: this.contactPPForm.get('nom').value,
        prenom: this.contactPPForm.get('prenom').value,
        civilite: this.contactPPForm.get('civilite').value,
        nationalite: this.contactPPForm.get('nationalite').value,
        typeResidence: this.contactPPForm.get('typeResidence').value,
        paysResidence: this.contactPPForm.get('paysResidence').value,
        adresse1: this.contactPPForm.get('adresse1').value,
        adresse2: this.contactPPForm.get('adresse2').value,
        ville: this.contactPPForm.get('ville').value,
        codePostale: this.contactPPForm.get('codePostale').value,
        profilRisque: this.contactPPForm.get('profilRisque').value,
        dateNaissance: this.contactPPForm.get('dateNaissance').value,
        nomPrenomPere: this.contactPPForm.get('nomPrenomPere').value,
        nomPrenomMere: this.contactPPForm.get('nomPrenomMere').value,
        mobile1: this.contactPPForm.get('mobile1').value,
        mobile2: this.contactPPForm.get('mobile2').value,
        email: this.contactPPForm.get('email').value,
        statutPPE: this.contactPPForm.get('statutPPE').value,
        // infos indentit�s
        pieceIdentite: this.contactPPForm.get('pieceIdentite').value,
        numPI: this.contactPPForm.get('numPI').value,
        emetteurPI: this.contactPPForm.get('emetteurPI').value,
        dateEmiPI: this.contactPPForm.get('dateEmiPI').value,
        dateExiPI: this.contactPPForm.get('dateExiPI').value,
        tribImmatricule: this.contactPPForm.get('tribImmatricule').value,
        rcRn: this.contactPPForm.get('rcRn').value,
        numTaxePro: this.contactPPForm.get('numTaxePro').value
      } : {
        raisonSociale: this.contactPMForm.get('raisonSociale').value,
        formeJuridique: this.contactPMForm.get('formeJuridique').value,
        agrementDelivrance: this.contactPMForm.get('agrementDelivrance').value,
        tribImmatricule: this.contactPMForm.get('tribImmatricule').value,
        rc: this.contactPMForm.get('rc').value,
        idFiscal: this.contactPMForm.get('idFiscal').value,
        ice: this.contactPMForm.get('ice').value,
        numTaxePro: this.contactPMForm.get('numTaxePro').value,
        statutPPE: this.contactPMForm.get('statutPPE').value,
        statutFatca: this.contactPMForm.get('statutFatca').value,
        profilRisque: this.contactPMForm.get('profilRisque').value,
        adresse1: this.contactPMForm.get('adresse1').value,
        ville: this.contactPMForm.get('ville').value,
        codePostale: this.contactPMForm.get('codePostale').value,
        mobile1: this.contactPMForm.get('mobile1').value,
        mobile2: this.contactPMForm.get('mobile2').value,
        fax: this.contactPMForm.get('fax').value,
        email: this.contactPMForm.get('email').value
      };
  }

  /** GET LIST OF COUNTRIES */
  loadCountries(): void {
    this.referenceService.getPaysList()
      .subscribe(response => {
        this.countryList = response;
      });
  }

  closePopup(result?: boolean): void {
    this.dialogRef.close(result);
  }

}
