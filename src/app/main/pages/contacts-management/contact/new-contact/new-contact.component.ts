import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {finalize} from 'rxjs/operators';
import {
  ContactPMModel,
  ContactPPModel,
  GenericItemModel
} from '../../../../models';
import {CompteContactService, ContactService, ReferenceService} from '../../../../services';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.scss']
})
export class NewContactComponent implements OnInit {

  // form group
  formGroup: FormGroup = null;
  contactPPForm: FormGroup = null;
  contactPMForm: FormGroup = null;

  // contact
  contactPP: ContactPPModel;
  contactPM: ContactPMModel;

  // validation
  showValidationMsgs: boolean = false;
  loadingCreation: boolean = false;

  // Listes
  contactTypeList = [{code: 'PP', label: this.translate.instant('NEW_CONTACT.PP')}, {code: 'PM', label: this.translate.instant('NEW_CONTACT.PM')}];
  civilitesList: GenericItemModel[] = [];
  nationalitesList: GenericItemModel[] = [];
  piecesIdentitesList: GenericItemModel[] = [];
  typeResidenceList: GenericItemModel[] = [];
  paysResidenceList: GenericItemModel[] = [];
  formeJuridiqueList: GenericItemModel[] = [];

  @Output()
  submitTrigger: EventEmitter<any> = new EventEmitter<any>();

  private typePP: boolean;

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private referenceService: ReferenceService,
              private contactService: ContactService,
              private ccService: CompteContactService,
              private toastr: ToastrService) {
  }

  /** Component load methods */
  ngOnInit(): void {
    this.createFormGroup();
    this.loadContactForms();
    this.loadLists();
  }

  /** Form group */
  createFormGroup(): void {
    this.formGroup = this.fb.group({
      contactType: ['PP', [Validators.required]]
    });
  }

  /** Load contactPP & contactPM forms */
  loadContactForms(): void {
    this.contactPPForm = this.fb.group({
      // infos générales
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      civilite: [null, [Validators.required]],
      dateNaissance: ['', [Validators.required]],
      email: [''],
      nomPrenomPere: [null],
      nomPrenomMere: [null],
      mobile1: ['', [Validators.required]],
      mobile2: [null],
      adresse1: ['', [Validators.required]],
      adresse2: [null],
      nationalite: ['', [Validators.required]],
      typeResidence: ['', [Validators.required]],
      paysResidence: ['', [Validators.required]],
      ville: ['', [Validators.required]],
      codePostale: ['', [Validators.required]],
      profilRisque: ['', [Validators.required]],
      statutPPE: ['', [Validators.required]],
      // infos indentités
      rcRn: ['', [Validators.required]],
      numTaxePro: ['', [Validators.required]],
      tribImmatricule: ['', [Validators.required]],
      pieceIdentite: ['', [Validators.required]],
      numPI: ['', [Validators.required]],
      emetteurPI: ['', [Validators.required]],
      dateEmiPI: [null],
      dateExiPI: [null]
    });
    this.contactPMForm = this.fb.group({
      raisonSociale: [null, [Validators.required]],
      formeJuridique: ['', [Validators.required]],
      agrementDelivrance: ['', [Validators.required]],
      tribImmatricule: ['', [Validators.required]],
      statutPPE: ['', [Validators.required]],
      statutFatca: ['', [Validators.required]],
      mobile1: ['', [Validators.required]],
      mobile2: [null],
      fax: [''],
      email: [''],
      ville: ['', [Validators.required]],
      codePostale: ['', [Validators.required]],
      adresse1: ['', [Validators.required]],
      idFiscal: ['', [Validators.required]],
      rc: ['', [Validators.required]],
      ice: ['', [Validators.required]],
      numTaxePro: ['', [Validators.required]],
      profilRisque: ['', [Validators.required]],
    });
  }

  /** Forms reset methods */
  onReset(): void {
    this.showValidationMsgs = false;
    if (this.contactPPForm) {
      this.contactPPForm.reset();
    }
    if (this.contactPMForm) {
      this.contactPMForm.reset();
    }
  }

  /** ContactPP Form sumbmission */
  onSubmitContactPP(): void {
    if (this.contactPPForm.invalid) {
      this.showValidationMsgs = true;
      return;
    }
    this.contactPP = this.generateContactSubmitPayload();
    this.loadingCreation = true;
    this.contactService.insertContactPP(this.contactPP)
      .pipe(finalize(() => {
        this.loadingCreation = false;
      }))
      .subscribe(value => {
        if (value.valid) {
          this.toastr.success('Contact inséré');
          this.submitTrigger.emit(value.code);
        } else {
          this.toastr.error('Erreur pendant la création du contact');
        }
      });
  }

  /** ContactPM Form submission */
  onSubmitContactPM(): void {
    if (this.contactPMForm.invalid) {
      this.showValidationMsgs = true;
      return;
    }
    this.contactPM = this.generateContactSubmitPayload();
    this.loadingCreation = true;
    this.contactService.insertContactPM(this.contactPM)
      .pipe(finalize(() => {
        this.loadingCreation = false;
      }))
      .subscribe(value => {
        if (value.valid) {
          this.toastr.success('Contact inséré');
          this.submitTrigger.emit(value.code);
        } else {
          this.toastr.error('Erreur pendant la création du contact');
        }
      });
  }

  private generateContactSubmitPayload(): any {
    return this.formGroup?.get('contactType')?.value?.includes('PP') ?
      {
        // infos générales
        nom: this.contactPPForm.get('nom').value,
        prenom: this.contactPPForm.get('prenom').value,
        civilite: this.contactPPForm.get('civilite').value,
        nationalite: this.contactPPForm.get('nationalite').value,
        typeResidence: this.contactPPForm.get('typeResidence').value,
        paysResidence: this.contactPPForm.get('paysResidence').value,
        adresse1: this.contactPPForm.get('adresse1').value,
        adresse2: this.contactPPForm.get('adresse2').value,
        ville: this.contactPPForm.get('ville').value,
        codePostale: this.contactPPForm.get('codePostale').value,
        profilRisque: this.contactPPForm.get('profilRisque').value,
        dateNaissance: this.contactPPForm.get('dateNaissance').value,
        nomPrenomPere: this.contactPPForm.get('nomPrenomPere').value,
        nomPrenomMere: this.contactPPForm.get('nomPrenomMere').value,
        mobile1: this.contactPPForm.get('mobile1').value,
        mobile2: this.contactPPForm.get('mobile2').value,
        email: this.contactPPForm.get('email').value,
        statutPPE: this.contactPPForm.get('statutPPE').value,
        // infos indentités
        pieceIdentite: this.contactPPForm.get('pieceIdentite').value,
        numPI: this.contactPPForm.get('numPI').value,
        emetteurPI: this.contactPPForm.get('emetteurPI').value,
        dateEmiPI: this.contactPPForm.get('dateEmiPI').value,
        dateExiPI: this.contactPPForm.get('dateExiPI').value,
        tribImmatricule: this.contactPPForm.get('tribImmatricule').value,
        rcRn: this.contactPPForm.get('rcRn').value,
        numTaxePro: this.contactPPForm.get('numTaxePro').value
      } : {
        raisonSociale: this.contactPMForm.get('raisonSociale').value,
        formeJuridique: this.contactPMForm.get('formeJuridique').value,
        agrementDelivrance: this.contactPMForm.get('agrementDelivrance').value,
        tribImmatricule: this.contactPMForm.get('tribImmatricule').value,
        rc: this.contactPMForm.get('rc').value,
        idFiscal: this.contactPMForm.get('idFiscal').value,
        ice: this.contactPMForm.get('ice').value,
        numTaxePro: this.contactPMForm.get('numTaxePro').value,
        statutPPE: this.contactPMForm.get('statutPPE').value,
        statutFatca: this.contactPMForm.get('statutFatca').value,
        profilRisque: this.contactPMForm.get('profilRisque').value,
        adresse1: this.contactPMForm.get('adresse1').value,
        ville: this.contactPMForm.get('ville').value,
        codePostale: this.contactPMForm.get('codePostale').value,
        mobile1: this.contactPMForm.get('mobile1').value,
        mobile2: this.contactPMForm.get('mobile2').value,
        fax: this.contactPMForm.get('fax').value,
        email: this.contactPMForm.get('email').value
      };
  }

  /** GET LIST OF COUNTRIES */
  loadLists(): void {
    this.referenceService.getCivilites().subscribe(value => this.civilitesList = value);
    this.referenceService.getNationalites().subscribe(value => this.nationalitesList = value);
    this.referenceService.getTypesDocs().subscribe(value => this.piecesIdentitesList = value);
    this.referenceService.getTypeResidenceList().subscribe(value => this.typeResidenceList = value);
    this.referenceService.getPaysList().subscribe(value => this.paysResidenceList = value);
    this.referenceService.getFormesJuridiques().subscribe(value => this.formeJuridiqueList = value);
  }

  /** GENERAL SUBMIT */
  onSubmit(): void {
    switch(this.formGroup?.get('contactType')?.value) {
      case 'PP':
        this.onSubmitContactPP(); break;
      case 'PM':
        this.onSubmitContactPM(); break;
    }
  }
}
