import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from "rxjs";
import { finalize } from 'rxjs/operators';
import {
  ContactAccountModel,
  ContactFilterModel, ContactType
} from 'src/app/main/models';
import { ContactService } from 'src/app/main/services';
import { DataTableOutputActionsModel, DataTableSettingsModel } from '../../../../../shared/models';
import { ExportService } from '../../../../../shared/services';
import { ContactFormComponent } from '../contact-form/contact-form.component';
import { CONTACT_LIST_SETTINGS } from './contact-list.settings';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  @Input()
  set filter(value: { filterBody: ContactFilterModel }) {

    this.contactsList = [];
    if (!value) return;
    this._filter = value.filterBody;
    if (this._filter) {
      this.loadListContact();
    }
  }

  @Input()
  set enableActions(value: boolean) {
    this._enableActions = value;
  }

  @Input()
  isRecap:boolean;
  _enableActions:boolean=true;


  settings: DataTableSettingsModel = CONTACT_LIST_SETTINGS;

  _filter: ContactFilterModel = null;

  contactsList: ContactAccountModel[] = [];

  loading: boolean = false;

  matDialogCloseSubscription: Subscription;

  constructor(private contactService: ContactService, private router: Router,
    private exportService: ExportService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  /** LOAD LIST OF CONTACTS PP */
  loadListContact(): void {
    this.contactsList = [];
    if (!this._filter) return;
    this.loading = true;
    this.contactService.getListContactsByAccount(this._filter?.accountCode)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        console.log(response);
        this.contactsList = response;
      });
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<ContactAccountModel>): void {
    if (event) {
      let codeContact = event.item ? event.item.codeContact : null;
      let contactType = this.extractContactType(codeContact);
      switch (event.actionType) {
        case 'VIEW':
          this.onShowEditContact(codeContact, contactType, false);
          break;
        case 'EDIT':
          this.onShowEditContact(codeContact, contactType, true);
          break;
      }
    }
  }

  extractContactType(code: string): ContactType {
    return code.startsWith('IP') ? ContactType.PP : ContactType.PM;
  }

  private onShowEditContact(codeContact: string, contactType: ContactType, editEnabled: boolean): void {
    const dialogRef = this.dialog.open(ContactFormComponent, { data: { code: codeContact, contactType, editEnabled }, autoFocus: false });
    this.matDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      /** REFRESH CONTACTS LIST */
      if (result === true) {
        this.loadListContact();
      }
    });
  }
}
