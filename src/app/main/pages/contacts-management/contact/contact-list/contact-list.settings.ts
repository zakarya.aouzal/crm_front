import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const CONTACT_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'codeContact',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CONTACT_LIST.CODE'
      }
    }, {
      name: 'descriptionContact',
      title: 'Description',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CONTACT_LIST.DESCRIPTION'
      }
    }, {
      name: 'email',
      title: 'Email',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CONTACT_LIST.EMAIL'
      }
    }, {
      name: 'mobile',
      title: 'Mobile',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CONTACT_LIST.MOBILE'
      }
    }, {
      name: 'role',
      title: 'Role',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.CONTACT_LIST.ROLE'
      }
    }
  ], actions: [DataTableActions.VIEW, DataTableActions.EDIT]
};
