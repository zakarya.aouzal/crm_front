import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactConsultationComponent } from './contact-consultation.component';

describe('ContactConsultationComponent', () => {
  let component: ContactConsultationComponent;
  let fixture: ComponentFixture<ContactConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
