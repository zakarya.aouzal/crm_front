import { Component, OnInit } from '@angular/core';
import {ContactFilterModel} from '../../../../models';

@Component({
  selector: 'app-contact-consultation',
  templateUrl: './contact-consultation.component.html',
  styleUrls: ['./contact-consultation.component.scss']
})
export class ContactConsultationComponent implements OnInit {

  filter: { filterBody: ContactFilterModel } = null;

  constructor() { }

  ngOnInit(): void {
  }

}
