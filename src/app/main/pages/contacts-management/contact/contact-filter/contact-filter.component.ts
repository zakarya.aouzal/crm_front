import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AccountFilterModel, AccountPMModel, AccountPPModel, AccountType, ContactFilterModel} from '../../../../models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../../../services';
import {finalize} from "rxjs/operators";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-contact-filter',
  templateUrl: './contact-filter.component.html',
  styleUrls: ['./contact-filter.component.scss']
})
export class ContactFilterComponent implements OnInit {

  @Output()
  submited = new EventEmitter<{ filterBody: ContactFilterModel }>();
  constructor(private fb: FormBuilder, private accountService: AccountService,private translate:TranslateService) {
  }
  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;
  loading: boolean = false;

  // List
  accountTypeList = [{code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP")}, {code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM")}];
  filter: ContactFilterModel = null;

  accountPPList: AccountPPModel[];
  accountPMList: AccountPMModel[];

  

  ngOnInit(): void {
    this.createFormGroup();
    this.loadListAccountPP();
    this.loadListAccountPM();
    this.initFormGroup();
  }

  /** INIT FORM GROUP */
  initFormGroup(): void {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
      let accountType = this.accountTypeList.find(item => item.code === 'PP');
      this.formGroup.get('accountType').setValue(accountType);
    }
  }

  /** CREATE FORM GROUP */
  createFormGroup(): void {
    this.formGroup = this.fb.group({
      accountType: [null, [Validators.required]],
      accountCode: [null, [Validators.required]],
    });
  }

  /** LOAD LIST OF ACCOUNTS PP */
  loadListAccountPP(): void {
    this.accountPPList = [];
    this.loading = true;
    this.accountService.getListAccountPP(null)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.accountPPList = response;
      });
  }

  /** LOAD LIST OF ACCOUNTS PM */
  loadListAccountPM(): void {
    this.accountPMList = [];
    this.loading = true;
    this.accountService.getListAccountPM(null)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.accountPMList = response;
      });
  }

  /** SEND FILTER */
  onSubmit(): void {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit({filterBody: filter});
    }
  }

  private generatePayload(): ContactFilterModel {
    let filter: ContactFilterModel = null;

    filter = {
      ...new ContactFilterModel(),
      accountType: this.formGroup?.get('accountType')?.value,
      accountCode: this.formGroup?.get('accountCode')?.value
    };

    return filter;
  }

}
