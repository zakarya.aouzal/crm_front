import {Component, Inject, OnInit} from '@angular/core';
import {CompteContactService, ContactService} from "../../../../services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AccountType, CompteContactPayload, ContactModel, ContactPMModel, ContactPPModel} from "../../../../models";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit {

  constructor(private contactService: ContactService,
              private fb: FormBuilder,
              private ccService: CompteContactService,
              private toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public accountData: { code: any, type: AccountType },
              private dialogRef: MatDialogRef<CreateContactComponent>) {
  }
  private typeContact: string;
  selectedContacts: ContactModel[] = [];

  // form group
  showFormErrors: boolean = false;

  loading: boolean = false;

  compteContactForm: FormGroup = null;

  readonly steps = [
    {id: 1, label: 'CREATE_CONTACT.STEPS.CREATION', icon: 'ri-contacts-line'},
    {id: 2, label: 'CREATE_CONTACT.STEPS.AFFECTATION', icon: 'ri-lightbulb-flash-line'}
  ];
  currentStep: { id: number, label: string } = null;

  createdContact: ContactPMModel | ContactPPModel = null;
  compteContactPayload: CompteContactPayload = null;

  ngOnInit(): void {
    this.createCompteContactForm();
    this.currentStep = this.steps[0];
  }

  createCompteContactForm(): void {
    this.compteContactForm = this.fb.group({
      descriptionContact: [null, Validators.required],
      role: [null, Validators.required],
      qualite: [null, Validators.required],
      dateDebutQualite: [null],
      dateFinQualite: [null],
      pourcentageCapital: [null],
      pourcentageDroitVote: [null]
    });
  }

  contactToModel(contact: ContactPPModel | ContactPMModel): any {
    return {
      code: contact.code,
      nom: contact['nom'],
      prenom: contact['prenom'],
      raisonSociale: contact['raisonSociale'],
      mobile1: contact.mobile1,
      email: contact.email,
      ville: contact.ville,
      codePostale: contact.codePostale
    };
  }

  onSubmit(): void {
    if (this.compteContactForm?.invalid) {
      this.showFormErrors = true;
      return;
    }
    this.showFormErrors = false;

    this.compteContactPayload = this.generateCompteContactPayload();

    this.loading = true;
    this.ccService.insertCompteContact([this.compteContactPayload])
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => {
        if (response && response.valid) {
          this.toastr.success('Le contact à été bien affecté au compte');
          this.closePopup();
        } else {
          this.toastr.error('Erreur pendant la création du contact');
        }
      });
  }

  private generateCompteContactPayload(): CompteContactPayload {
    return {
      ...new CompteContactPayload(),
      codeCompte: this.accountData?.code,
      codeContact: this.createdContact?.code,
      descriptionContact: this.compteContactForm.get('descriptionContact').value,
      role: this.compteContactForm.get('role').value,
      qualite: this.compteContactForm.get('qualite').value,
      dateDebutQualite: this.compteContactForm.get('dateDebutQualite').value,
      dateFinQualite: this.compteContactForm.get('dateFinQualite').value,
      pourcentageCapital: this.compteContactForm.get('pourcentageCapital').value,
      pourcentageDroitVote: this.compteContactForm.get('pourcentageDroitVote').value
    };
  }

  onReset(): void {
    this.compteContactForm.reset();
  }

  closePopup(): void {
    this.dialogRef.close(true);
  }

  submitTrigger(contactCode: string): void {
    switch (contactCode.substr(0, 2)) {
      case 'IP':
        this.typeContact = 'PP';
        this.contactService.getContactPPByCode(contactCode)
          .subscribe(value =>{ this.createdContact = value ; } );
        break;
      case 'IM':
        this.typeContact = 'PM';
        this.contactService.getContactPMByCode(contactCode)
          .subscribe(value => { this.createdContact = value ; } );
        break;
    }
    this.currentStep = this.steps[1];
  }

  onNotify($event: string) {
    console.log($event);
  }
}
