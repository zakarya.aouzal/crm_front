import {DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const CONTACT_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.CODE'
      }
    }, {
      name: 'nom',
      title: 'Nom',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.NOM'
      }
    }, {
      name: 'prenom',
      title: 'Prénom',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.PRENOM'
      }
    }, {
      name: 'raisonSociale',
      title: 'Raison Sociale',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.RAISON_SOCIALE'
      }
    }, {
      name: 'mobile1',
      title: 'Mobile',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.MOBILE'
      }
    }, {
      name: 'email',
      title: 'Email',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.EMAIL'
      }
    }, {
      name: 'codePostale',
      title: 'Code postale',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.CODE_POSTAL'
      }
    }, {
      name: 'ville',
      title: 'Ville',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.VILLE'
      }
    }
  ], selectable: true
};
