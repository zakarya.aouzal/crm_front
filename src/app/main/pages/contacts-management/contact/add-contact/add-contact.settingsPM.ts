import {DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const CONTACT_PM_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.CODE'
      }
    }  , {
      name: 'raisonSociale',
      title: 'Raison Sociale',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.RAISON_SOCIALE'
      }
    },
    {
      name: 'formeJuridique',
      title: 'Forme Juridique',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'FIELD.COMPTE_PM.FORME_JURI'
      }
    },
    {
      name: 'mobile1',
      title: 'Mobile',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.MOBILE'
      }
    }, {
      name: 'email',
      title: 'Email',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ADD_CONTACT.EMAIL'
      }
    },
    {
      name: 'ville',
      title: 'Ville',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'FIELD.COMPTE_PP.VILLE'
      }
    }
  ], selectable: true
};
