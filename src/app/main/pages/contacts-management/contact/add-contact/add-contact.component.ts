import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {finalize} from 'rxjs/operators';
import {AccountType, CompteContactPayload, ContactModel, ContactPMModel, ContactPPModel} from 'src/app/main/models';
import {CompteContactService, ContactService} from 'src/app/main/services';
import {DataTableSettingsModel} from 'src/app/shared/models';
import {CONTACT_LIST_SETTINGS} from './add-contact.settings';
import {CONTACT_PM_LIST_SETTINGS} from './add-contact.settingsPM';
import {CONTACT_PP_LIST_SETTINGS} from './add-contact.settingsPP';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent implements OnInit {

  constructor(private contactService: ContactService,
              private fb: FormBuilder,
              private ccService: CompteContactService,
              private toastr: ToastrService,
              @Inject(MAT_DIALOG_DATA) public accountData: { code: any, type: AccountType, mode: 'ACCOUNT' | 'AFFECT', selectedList: string[] },
              private dialogRef: MatDialogRef<AddContactComponent>) {
  }

  // table settings

  loading: boolean;
  selectionEnabled: boolean = true;

  // contact data
  payloadList: CompteContactPayload[] = [];

  currentItemIndex: number = 0;

  // form group
  formGroup: FormGroup;
  showFormErrors: boolean = false;
  settings: DataTableSettingsModel = CONTACT_LIST_SETTINGS;
  contactsList: ContactModel[];
  selectedContacts: ContactModel[] = [];
  compteContactForm: FormGroup = null;
  settingsPM: DataTableSettingsModel = CONTACT_PM_LIST_SETTINGS;
  settingsPP: DataTableSettingsModel = CONTACT_PP_LIST_SETTINGS;
  contactsPMList: ContactPMModel[];
  contactsPPList: ContactPPModel[];
  ngOnInit(): void {
    this.loadContactList();
    this.createCompteContactForm();}

  loadContactList(): void {
    this.contactService.getListContactPMByCode(this.accountData.code)
      .subscribe(
        (value :any) => { value = value.map(element => this.contactPMToModel(element));
                    this.contactsPMList = value;
                   }
      )
    /*this.contactService.getContactsPP().subscribe(
      (value:any)=> { this.contactsList = value ; console.log(value) }
    )*/
    this.contactService.getListContactPPByCode(this.accountData.code)
      .subscribe(
        (value :any) => {
          value = value.map(element => this.contactPPToModel(element));
          this.contactsPPList = value;})
  }

  createCompteContactForm(): void {
    this.compteContactForm = this.fb.group({
      descriptionContact: [null, Validators.required],
      role: [null, Validators.required],
      qualite: [null, Validators.required],
      dateDebutQualite: [null],
      dateFinQualite: [null],
      pourcentageCapital: [null],
      pourcentageDroitVote: [null]
    });
  }

  private contactPMToModel(contact: ContactPMModel): any {
    return {
      code: contact.code,
      raisonSociale: contact.raisonSociale,
      mobile1: contact.mobile1,
      email: contact.email,
      formeJuridique : contact.formeJuridique ,
      ville: contact.ville
    };
  }

  private contactPPToModel(contact: ContactPPModel): any {
  return {
    code: contact.code,
    nom: contact.nom ,
    prenom : contact.prenom ,
    mobile1: contact.mobile1,
    email: contact.email,
    ville: contact.ville,
    codePostale: contact.codePostale
  };
}



  generateCompteContactPayload(contactCode: string): CompteContactPayload {
    return {
      ...new CompteContactPayload(),
      codeCompte: this.accountData?.code,
      codeContact: contactCode,
      descriptionContact: this.compteContactForm.get('descriptionContact').value,
      role: this.compteContactForm.get('role').value,
      qualite: this.compteContactForm.get('qualite').value,
      dateDebutQualite: this.compteContactForm.get('dateDebutQualite').value,
      dateFinQualite: this.compteContactForm.get('dateFinQualite').value,
      pourcentageCapital: this.compteContactForm.get('pourcentageCapital').value,
      pourcentageDroitVote: this.compteContactForm.get('pourcentageDroitVote').value
    };
  }

  onReset(): void {
    this.compteContactForm.reset();
  }

  onItemChecked(event: { item: any, isChecked: boolean }): void {
    if (!event) return;
    if (event.isChecked) {
      const index: number = this.selectedContacts.findIndex(item => item.code === event.item.code);
      if (index === -1) {
        this.selectedContacts.push(event.item);
      }
    } else {
      this.selectedContacts = this.selectedContacts.filter(item => item.code !== event.item.code);
    }
  }

  onSubmit(): void {
    if (this.compteContactForm?.invalid) {
      this.showFormErrors = true;
      return;
    }
    this.showFormErrors = false;

    const contactCode: string = this.selectedContacts[this.currentItemIndex].code;
    this.moveNextItem(contactCode);

    if (this.currentItemIndex === this.selectedContacts?.length) {
      if (this.accountData.mode === 'AFFECT') {
        this.loading = true;
        this.ccService.insertCompteContact(this.payloadList)
          .pipe(finalize(() => this.closePopup(true)))
          .subscribe(response => {
            if (response && response.valid) {
              this.toastr.success('Le contact à été bien affecté au compte');
            } else {
              this.toastr.error('Erreur pendant la création du contact');
            }
          });
      } else {
        this.closePopup(true);
      }
    }
  }

  /** EDIT ON CONTACT CLICK */
  onContactClick(code: string, index: number, affected: boolean) {
    if (!affected) {
      this.toastr.warning('Le contact n\'est pas encore rempli')
      return;
    }
    this.currentItemIndex = index;
    this.fillExistingCC(code);
  }

  private moveNextItem(code: string): void {
    if (this.payloadList.filter(value => value.codeContact===code).length > 0) {
      this.payloadList = this.payloadList.map(value => {
        return (value.codeContact === code) ? this.generateCompteContactPayload(code) : value;
      });
    } else {
      this.payloadList.push(this.generateCompteContactPayload(code));
    }

    this.selectedContacts.forEach(value => {
      if (value.code===code) {
        value['affected'] = true;
      }
    });

    this.currentItemIndex++;
    const contact = this.selectedContacts[this.currentItemIndex];
    if (contact?.affected) {
      this.fillExistingCC(contact?.code);
    } else {
      this.compteContactForm.reset();
    }
  }

  private fillExistingCC(code: string): void {
    const payload = this.payloadList.filter(value => value.codeContact===code)[0];

    this.compteContactForm.patchValue({
      descriptionContact: payload?.descriptionContact,
      role: payload?.role,
      qualite: payload?.qualite,
      dateDebutQualite: payload?.dateDebutQualite,
      dateFinQualite: payload?.dateFinQualite,
      pourcentageCapital: payload?.pourcentageCapital,
      pourcentageDroitVote: payload?.pourcentageDroitVote
    });
  }

  closePopup(result: boolean): void {
    this.dialogRef.close(result);
  }

  switchMode() {
    this.selectionEnabled = !this.selectionEnabled;
  }

  returnSelect(): void {
    this.selectionEnabled = !this.selectionEnabled;
    this.selectedContacts = [];
  }
}
