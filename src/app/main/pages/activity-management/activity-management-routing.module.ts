import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/shared/components/page-not-found/page-not-found.component';
import { ActivityGlobalComponent } from './activites-consultation/activity-global/activity-global.component';
import { IncidentDetailsComponent } from './incident/incident-details/incident-details.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: {
        label: 'BREADCRUMBS.ACTIVITY_MANAGEMENT.TITLE'
      }
    },
    children: [
      {
        path: 'activity-consultation',
        component: ActivityGlobalComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.ACTIVITY_MANAGEMENT.ACTIVITY_CONSULTATION'
          }
        },
      },
      // {
      //   path: 'new-activity',
      //   component: IncidentDetailsComponent,
      //   data: {
      //     breadcrumb: {
      //       label: 'BREADCRUMBS.ACTIVITY_MANAGEMENT.INCIDENT_DETAILS'
      //     }
      //   },
      // },
      // INCIDENTS
      {
        path: 'incident-details/:incidentCode/:formType',
        component: IncidentDetailsComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.ACTIVITY_MANAGEMENT.INCIDENT_DETAILS'
          }
        },
      },

       {
        path: '',
        redirectTo: 'activity-consultation',
        pathMatch: 'full'
      }, {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ActivityManagementRoutingModule { }
