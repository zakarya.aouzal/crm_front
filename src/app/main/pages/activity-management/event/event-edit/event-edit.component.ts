import {Component, Inject, OnInit} from '@angular/core';
import {ContactService, EventService} from "../../../../services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ContactAccountModel, EventPayloadModel, GenericItemModel} from "../../../../models";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss']
})
export class EventEditComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private eventService: EventService,
              private dialogRef: MatDialogRef<EventEditComponent>,
              private toastr: ToastrService,
              private contactService: ContactService,
              @Inject(MAT_DIALOG_DATA) public data: { code: any, viewMode: boolean }) {
  }

  loading: boolean = false;

  payloadModel: EventPayloadModel = null;

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  eventTypeList: GenericItemModel[] = [];
  statusList: GenericItemModel[] = [];
  contactsList: ContactAccountModel[] = [];

  ngOnInit(): void {
    this.loadListes();

    this.createForm();
    this.fillForm(this.data?.code);

    if (this.data?.viewMode) {
      this.formGroup?.disable();
    }
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      subject: [null, [Validators.required]],
      type: [null, [Validators.required]],
      description: [null],
      location: [null],
      account: [null, [Validators.required]],
      contact: [null],
      startDate: [null],
      endDate: [null],
      status: [null, [Validators.required]]
    });
  }

  private fillForm(code: any): void {
    this.eventService.getEventByCode(code)
      .subscribe((payload) => {
        this.formGroup.patchValue({
          subject: payload?.subject,
          type: payload?.type,
          description: payload?.description,
          location: payload?.location,
          account: payload?.compte_id,
          contact: payload?.contact_id,
          status: payload?.status,
          startDate: payload?.start_date,
          endDate: payload?.end_date
        });
        this.loadContacts(payload?.compte_id);
      });
  }

  onSubmit(): void {
    if (this.data?.viewMode) {
      this.data.viewMode = false;
      this.formGroup?.enable();
      this.formGroup?.get('account')?.disable();
      return;
    }

    if (this.formGroup?.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    this.showValidationsMsg = false;

    this.payloadModel = this.generatePayload();

    this.eventService.updateEvent(this.payloadModel)
      .subscribe(value => {
        if (value.valid) {
          this.dialogRef.close();
          this.toastr.success("Evénement modifié");
        } else {
          this.toastr.error("Erreur du serveur");
        }
      });
  }

  generatePayload(): EventPayloadModel {
    let filter: EventPayloadModel = null;
    if (this.formGroup) {
      filter = {
        ...new EventPayloadModel(),
        code: this.data?.code,
        subject: this.formGroup.get('subject').value,
        type: this.formGroup.get('type').value,
        description: this.formGroup.get('description').value,
        location: this.formGroup.get('location').value,
        compte_id: this.formGroup.get('account').value ? this.formGroup.get('account').value.toUpperCase() : null,
        contact_id: this.formGroup.get('contact').value ? this.formGroup.get('contact').value : null,
        status: this.formGroup.get('status').value ? this.formGroup.get('status').value : null,
        start_date: this.formGroup.get('startDate').value,
        end_date: this.formGroup.get('endDate').value,
      }
    }
    return filter;
  }

  loadContacts(compteCode: string): void {
    this.contactService.getListContactsByAccount(compteCode).subscribe(response => this.contactsList = response);
  }

  loadListes(): void {
    this.eventService.getListOfEventTypes().subscribe(response => this.eventTypeList = response);
    this.eventService.getListOfEventStatus().subscribe(response => this.statusList = response);
  }
}
