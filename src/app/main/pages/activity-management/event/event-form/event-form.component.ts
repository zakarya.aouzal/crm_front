import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import {
  AccountPMModel,
  AccountPPModel,
  AccountType,
  ContactAccountModel,
  EventPayloadModel,
  GenericItemModel
} from 'src/app/main/models';
import {AccountService, ContactService, EventService} from 'src/app/main/services';
import { endDateLessThanStartDateValidator } from 'src/app/shared/utils/validate';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  @Input()
  set accountInfo(value: { accountType: AccountType, accountPP: AccountPPModel, accountPM: AccountPMModel }) {



    this.accountType = null;
    this.accountPP = null;
    this.accountPM = null;
    if (value) {
      this.accountType = value.accountType;
      this.accountPP = value.accountPP;
      this.accountPM = value.accountPM;
      // load list of contacts by account
      let accountCode: string = null;
      switch (this.accountType) {
        case AccountType.PP:
          accountCode = this.accountPP ? this.accountPP.code : null;
          break;
        case AccountType.PM:
          accountCode = this.accountPM ? this.accountPM.code : null;
      }
      if (accountCode) {
        this.loadContacts(accountCode);
      }
    }
  }

  @Input()
  hideTitle:boolean=true;

  @Output()
  submitted: EventEmitter<void> = new EventEmitter<void>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];
  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];

  // Loading
  loading: boolean = false;

  // Account
  accountType: AccountType = null;
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;

  // List
  statusList: GenericItemModel[] = [];
  contactsList: ContactAccountModel[] = [];
  eventTypeList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder,
              private eventService: EventService,
              private toastrService: ToastrService,
              public dialogRef: MatDialogRef<EventFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { redirect: boolean, accountCode: string },
              private translate: TranslateService,
              private accountService: AccountService,
              private contactService: ContactService) {
  }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadListOfEventTypes();
    this.loadListEventStatus();

    if (this.data?.redirect) {
      this.formGroup?.get('accountType')?.valueChanges.subscribe(value => {
        this.accountPPList = [];
        this.accountPMList = [];
        if (value) {
          this.loadAccounts(value);
        }
      });
      this.formGroup?.get('account')?.valueChanges.subscribe(value => {
        this.loadContacts(value);
      });
    }

    if (this.data?.accountCode) {
      this.formGroup?.get('account').patchValue(this.data?.accountCode);
    }
  }

  /** LOAD LIST OF CONTACTS BY ACCOUNT */
  loadContacts(accountCode: string) {
    this.contactsList = [];
    this.contactService.getListContactsByAccount(accountCode)
      .subscribe(response => this.contactsList = response);
  }

  /** GET LIST OF EVENT TYPES */
  loadListOfEventTypes() {
    this.eventService.getListOfEventTypes()
      .subscribe(response => this.eventTypeList = response);
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      subject: [null, [Validators.required]],
      type: [null, [Validators.required]],
      description: [null],
      location: [null],
      startDate: [null, [Validators.required]],
      accountType: [null],
      account: [null],
      endDate: [null, [Validators.required]],
      contact: [null],
      status: [null, [Validators.required]],
    }, { validators: endDateLessThanStartDateValidator() });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
    }
  }

  /** LOAD EVENT STATUS */
  loadListEventStatus() {
    this.eventService.getListOfEventStatus()
      .subscribe(response => {
        this.statusList = response;
      })
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const payload: EventPayloadModel = this.generatePayload();
    if (!payload) return;
    this.loading = true;
    this.eventService.addEvent(payload)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        if (response && response.valid) {
          this.submitted.emit();
          this.toastrService.success('Evénement ajouté');
          this.dialogRef.close(
            {
              valid:true,
              compte: this.formGroup.get('account').value.toUpperCase(),
              type:this.formGroup?.get('accountType')?.value,
            }
          );
          this.initFormGroup();
        } else {
          this.toastrService.warning("Un problème est survenu lors d'ajout de l'événement");
        }
      })
  }

  /** GENERATE PAYLOAD */
  generatePayload(): EventPayloadModel {

    
    let filter: EventPayloadModel = null;
    if (this.formGroup) {
      filter = {
        ...new EventPayloadModel(), 
        subject: this.formGroup.get('subject').value,
        type: this.formGroup.get('type').value,
        description: this.formGroup.get('description').value,
        location: this.formGroup.get('location').value,
        compte_id: this.formGroup.get('account').value ? this.formGroup.get('account').value.toUpperCase() : null,
        contact_id: this.formGroup.get('contact').value ? this.formGroup.get('contact').value.code : null,
        status: this.formGroup.get('status').value ? this.formGroup.get('status').value : null,
        start_date: this.formGroup.get('startDate').value,
        end_date: this.formGroup.get('endDate').value,
      }
      if (!filter.compte_id) {
        switch (this.accountType) {
          case AccountType.PP:
            filter.compte_id = this.accountPP ? this.accountPP?.code : null;
            break;
          case AccountType.PM:
            filter.compte_id = this.accountPM ? this.accountPM?.code : null;
        }
      }
    }
    return filter;
  }

  /** GET LIST OF ACCOUNTS */
  loadAccounts(accountType: AccountType) {
    switch (accountType) {
      case AccountType.PP: {
        this.accountPPList = [];
        this.accountService.getListAccountPP(null)
          .subscribe(response => {
            this.accountPPList = response;
          })
        break;
      }
      case AccountType.PM: {
        this.accountPMList = [];
        this.accountService.getListAccountPM(null)
          .subscribe(response => {
            this.accountPMList = response;
          })
      }
    }
  }

}
