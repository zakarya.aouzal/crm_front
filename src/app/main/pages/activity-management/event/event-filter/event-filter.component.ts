import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {ActivityGlobalFilterModel, EventFilterModel, GenericItemModel} from 'src/app/main/models';
import { EventService, ReferenceService } from 'src/app/main/services';
import { endDateLessThanStartDateValidator } from 'src/app/shared/utils/validate';

@Component({
  selector: 'app-event-filter',
  templateUrl: './event-filter.component.html',
  styleUrls: ['./event-filter.component.scss']
})
export class EventFilterComponent implements OnInit {

  @Output()
  submited = new EventEmitter<EventFilterModel>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  statusList: GenericItemModel[] = [];
  accountManagerList: GenericItemModel[] = [];
  eventTypeList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder, private referenceService: ReferenceService, private eventService: EventService) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadAccountManagers();
    this.loadListOfEventTypes();
    this.loadListOfEventStatus();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      type: [null],
      startDate: [null],
      endDate: [null],
      status: [null],
      gestionnaire: [null]
    }, { validator: endDateLessThanStartDateValidator() });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    const filter = {...new EventFilterModel()};
    if (this.formGroup) {
      this.formGroup.reset();
      this.submited.emit(filter);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit(filter);
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): EventFilterModel {
    let filter: EventFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new EventFilterModel(),
        type: this.formGroup.get('type').value,
        dateDebut: this.formGroup.get('startDate').value,
        dateFin: this.formGroup.get('endDate').value,
        status: this.formGroup.get('status').value,
        gestionnaire: this.formGroup.get('gestionnaire').value
      }
    }
    return filter;
  }

  /** GET LIST OF ACCOUNT MANAGERS */
  loadAccountManagers() {
    this.referenceService.getGestionnaireFronts()
      .subscribe(response => this.accountManagerList = response);
  }

  /** GET LIST OF EVENT TYPES */
  loadListOfEventTypes() {
    this.eventService.getListOfEventTypes()
      .subscribe(response => this.eventTypeList = response);
  }

  /** GET LIST OF EVENT STATUS */
  loadListOfEventStatus() {
    this.eventService.getListOfEventStatus()
      .subscribe(value => this.statusList = value);

  }

}
