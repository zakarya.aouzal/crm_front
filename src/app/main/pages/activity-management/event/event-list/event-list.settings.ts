import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const EVENT_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.CODE'
      }
    }, {
      name: 'subject',
      title: 'Sujet',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.SUJET'
      },style:{
        orientation:"left"
      }
    }, {
      name: 'description',
      title: 'Description',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.DESCRIPTION'
      },style:{
        orientation:"left"
      }
    }, {
      name: 'location',
      title: 'Location',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.LOCATION'
      }
    }, {
      name: 'start_date',
      title: 'Date début',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.DATE_DEBUT'
      }
    }, {
      name: 'end_date',
      title: 'Date fin',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.DATE_FIN'
      }
    }, {
      name: 'gestionnaire',
      title: 'Gestionnaire',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.GESTIONNAIRE'
      },style:{
        orientation:"left"
      }
    }, {
      name: 'compte_id',
      title: 'Compte',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.COMPTE'
      }
    }, {
      name: 'type',
      title: 'Type',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.TYPE'
      }
    }, {
      name: 'status',
      title: 'Status',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.EVENT_LIST.STATUT'
      },style:{
        orientation:"left"
      }
    }
  ], actions: [DataTableActions.VIEW, DataTableActions.DELETE]
};
