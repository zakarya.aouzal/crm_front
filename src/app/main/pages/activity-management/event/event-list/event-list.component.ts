import {Component, Input, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {
  AccountType,
  ActivityGlobalFilterModel, EventFilterModel,
  EventModel
} from 'src/app/main/models';
import {EventService} from 'src/app/main/services';
import {DataTableOutputActionsModel, DataTableSettingsModel} from 'src/app/shared/models';
import {EVENT_LIST_SETTINGS} from './event-list.settings';
import {finalize} from "rxjs/operators";
import {ToastrService} from "ngx-toastr";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {MatDialog} from "@angular/material/dialog";
import {EditContactComponent} from "../../../contacts-management/contact/edit-contact/edit-contact.component";
import {EventEditComponent} from "../event-edit/event-edit.component";
import {Subscription} from "rxjs";


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit, OnDestroy {

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.eventList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfEvents();
    }
  }

  @Input()
  set filter(value: EventFilterModel) {
    this.eventList = [];
    if (!value) return;
    this._filter = {
      ...this._globalFilter,
      ...value
    };
    if (this._filter) {
      this.loadEventsList();
    }
  }

  @Input()
  set CustomSettings(value: DataTableSettingsModel) {
    if(value){
      this.settings=value
    }
  }

  confirmationModalRef: BsModalRef;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  @Input()
  set enableActions(value: boolean) {
    this._enableActions = value;
  }

  _enableActions: boolean = true;

  settings: DataTableSettingsModel = EVENT_LIST_SETTINGS;

  currentAccountCode: string;
  selectedEventCode: string = null;

  _globalFilter: ActivityGlobalFilterModel = null;

  _accountType: AccountType = null;

  _filter: EventFilterModel = null;

  eventList: EventModel[] = [];

  loading: boolean = false;

  dialogCloseSubscription: Subscription = null;

  constructor(private eventService: EventService,
              private toastr: ToastrService,
              private dialog: MatDialog,
              private modalService: BsModalService) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.dialogCloseSubscription?.unsubscribe();
  }

  /** LOAD GLOBAL LIST OF EVENTS */
  loadGlobalListOfEvents() {
    this.eventList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.eventService.getEventsListByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.eventList = response);
  }

  /** LOAD LIST OF EVENTS */
  loadEventsList(): void {
    this.eventList = [];
    if (!this._filter) return;
    this.loading = true;
    this.eventService.getEventsListByFilter(this._filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.eventList = response);
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<EventModel>) {
    if (event) {
      this.selectedEventCode = event.item?.code;
      switch (event.actionType) {
        case 'VIEW':
          this.dialogCloseSubscription = this.dialog.open(EventEditComponent, {panelClass: 'activity-popup', data: {code: this.selectedEventCode, viewMode: true}})
            .afterClosed().subscribe(() => this.loadGlobalListOfEvents());
          break;
        case 'DELETE':
          this.confirmationModalRef = this.modalService.show(this.templateRef);
          break;
      }
    }
  }

  confirmDelete(event: DataTableOutputActionsModel<EventModel>): void {
    if (event) {
      this.eventService.deleteEvent(this.selectedEventCode)
        .subscribe(value => {
          if (value.valid) {
            this.loadGlobalListOfEvents();
            this.toastr.success('Incident supprimé');
          } else {
            this.toastr.error('Erreur pendant la suppression')
          }
        });
    }
  }
}
