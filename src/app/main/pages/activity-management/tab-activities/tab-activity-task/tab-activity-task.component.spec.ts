import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabActivityTaskComponent } from './tab-activity-task.component';

describe('TabActivityTaskComponent', () => {
  let component: TabActivityTaskComponent;
  let fixture: ComponentFixture<TabActivityTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabActivityTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabActivityTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
