import { Component, Inject, Input, OnInit, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AccountType, ActivityGlobalFilterModel, TaskModel } from 'src/app/main/models';
import { TaskService } from 'src/app/main/services';
import { SearchDto } from '../tab-activities.component';

@Component({
  selector: 'app-tab-activity-task',
  templateUrl: './tab-activity-task.component.html',
  styleUrls: ['./tab-activity-task.component.scss']
})
export class TabActivityTaskComponent implements OnInit {

  taskList: TaskModel[];

  _globalFilter: ActivityGlobalFilterModel;
  
  _accountType: AccountType = null;
  
  currentAccountCode: string;
    loading: boolean = false;

  @Input()
  compte:string

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.taskList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOftasks();
    }
  }


  searchDto:SearchDto={date:new Date(),compte:""};

  constructor(private taskService:TaskService, @Inject("pipeParameters") public pipeParameters: any) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.searchDto.compte=this.compte
    }
  }

  ngOnInit(): void {
    
  }


   /** LOAD GLOBAL task LIST */
   loadGlobalListOftasks() {
    this.taskList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.taskService.getTasksListByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.taskList = response);
  }
}
