import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabActivityIncidentComponent } from './tab-activity-incident.component';

describe('TabActivityIncidentComponent', () => {
  let component: TabActivityIncidentComponent;
  let fixture: ComponentFixture<TabActivityIncidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabActivityIncidentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabActivityIncidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
