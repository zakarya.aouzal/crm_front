import { Component, Inject, Input, OnInit, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AccountType, ActivityGlobalFilterModel, IncidentModel } from 'src/app/main/models';
import { IncidentService } from 'src/app/main/services';
import { SearchDto } from '../tab-activities.component';

@Component({
  selector: 'app-tab-activity-incident',
  templateUrl: './tab-activity-incident.component.html',
  styleUrls: ['./tab-activity-incident.component.scss']
})
export class TabActivityIncidentComponent implements OnInit {

  incidentList: IncidentModel[];

  _globalFilter: ActivityGlobalFilterModel;
  
  _accountType: AccountType = null;
  
  currentAccountCode: string;
    loading: boolean = false;

  @Input()
  compte:string

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.incidentList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfincidents();
    }
  }


  searchDto:SearchDto={date:new Date(),compte:""};

  constructor(private incidentService:IncidentService, @Inject("pipeParameters") public pipeParameters: any) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.searchDto.compte=this.compte
    }
  }

  ngOnInit(): void {
    
  }


   /** LOAD GLOBAL incident LIST */
   loadGlobalListOfincidents() {
    this.incidentList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.incidentService.fetchAllByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.incidentList = response);
  }


}
