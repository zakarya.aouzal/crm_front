import { Component, Inject, Input, OnInit, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AccountType, ActivityGlobalFilterModel, OpportuniteModel } from 'src/app/main/models';
import { OpportuniteService } from 'src/app/main/services';
import { SearchDto } from '../tab-activities.component';

@Component({
  selector: 'app-tab-activity-opportunite',
  templateUrl: './tab-activity-opportunite.component.html',
  styleUrls: ['./tab-activity-opportunite.component.scss']
})
export class TabActivityOpportuniteComponent implements OnInit {

  opportuniteList: OpportuniteModel[];

  _globalFilter: ActivityGlobalFilterModel;
  
  _accountType: AccountType = null;
  
  currentAccountCode: string;
    loading: boolean = false;

  @Input()
  compte:string

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.opportuniteList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfOpportunites();
    }
  }


  searchDto:SearchDto={date:new Date(),compte:""};

  constructor(private opportuniteService:OpportuniteService, @Inject("pipeParameters") public pipeParameters: any) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.searchDto.compte=this.compte
    }
  }

  ngOnInit(): void {
    
  }


   /** LOAD GLOBAL OPPORTUNITE LIST */
   loadGlobalListOfOpportunites() {
    this.opportuniteList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.opportuniteService.fetchAllByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.opportuniteList = response);
  }

}
