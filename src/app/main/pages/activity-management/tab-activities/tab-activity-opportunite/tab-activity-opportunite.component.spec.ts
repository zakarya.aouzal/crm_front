import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabActivityOpportuniteComponent } from './tab-activity-opportunite.component';

describe('TabActivityOpportuniteComponent', () => {
  let component: TabActivityOpportuniteComponent;
  let fixture: ComponentFixture<TabActivityOpportuniteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabActivityOpportuniteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabActivityOpportuniteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
