import { Component, Inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AccountType, ActivityGlobalFilterModel, HistoryActivityModel, OpportuniteModel } from 'src/app/main/models';
import { HistoryActivityService, OpportuniteService } from 'src/app/main/services';
export interface SearchDto{
  date:Date;
  compte:string;
}
@Component({
  selector: 'app-tab-activities',
  templateUrl: './tab-activities.component.html',
  styleUrls: ['./tab-activities.component.scss']
})

export class TabActivitiesComponent implements OnInit, OnChanges  {


  opportuniteList: OpportuniteModel[];

  _globalFilter: ActivityGlobalFilterModel;
  
  _accountType: AccountType = null;
  
  currentAccountCode: string;
    loading: boolean = false;

  @Input()
  compte:string

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.opportuniteList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfOpportunites();
    }
  }


  searchDto:SearchDto={date:new Date(),compte:""};

  constructor(private opportuniteService:OpportuniteService, @Inject("pipeParameters") public pipeParameters: any) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.searchDto.compte=this.compte
    }
  }

  ngOnInit(): void {
    
  }


   /** LOAD GLOBAL OPPORTUNITE LIST */
   loadGlobalListOfOpportunites() {
    this.opportuniteList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.opportuniteService.fetchAllByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.opportuniteList = response);
  }

}
