import { Component, Inject, Input, OnInit, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { AccountType, ActivityGlobalFilterModel, EventModel } from 'src/app/main/models';
import { EventService } from 'src/app/main/services';
import { SearchDto } from '../tab-activities.component';

@Component({
  selector: 'app-tab-activity-event',
  templateUrl: './tab-activity-event.component.html',
  styleUrls: ['./tab-activity-event.component.scss']
})
export class TabActivityEventComponent implements OnInit {

  eventList: EventModel[];

  _globalFilter: ActivityGlobalFilterModel;
  
  _accountType: AccountType = null;
  
  currentAccountCode: string;
    loading: boolean = false;

  @Input()
  compte:string

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.eventList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfevents();
    }
  }


  searchDto:SearchDto={date:new Date(),compte:""};

  constructor(private eventService:EventService, @Inject("pipeParameters") public pipeParameters: any) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.searchDto.compte=this.compte
    }
  }

  ngOnInit(): void {
    
  }


   /** LOAD GLOBAL event LIST */
   loadGlobalListOfevents() {
    this.eventList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.eventService.getEventsListByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.eventList = response);
  }

}
