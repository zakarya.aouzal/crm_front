import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabActivityEventComponent } from './tab-activity-event.component';

describe('TabActivityEventComponent', () => {
  let component: TabActivityEventComponent;
  let fixture: ComponentFixture<TabActivityEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabActivityEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabActivityEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
