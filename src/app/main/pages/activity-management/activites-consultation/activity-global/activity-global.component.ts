import {AfterContentChecked, ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {
  AccountType,
  ActivityGlobalFilterModel,
  EventFilterModel,
  IncidentFilterModel,
  OpportuniteFilterModel,
  TaskFilterModel
} from 'src/app/main/models';
import {EventFormComponent} from '../../event/event-form/event-form.component';
import {EventListComponent} from '../../event/event-list/event-list.component';
import {IncidentFormComponent} from '../../incident/incident-form/incident-form.component';
import {IncidentListComponent} from '../../incident/incident-list/incident-list.component';
import {OpportuniteFormComponent} from '../../opportunite/opportunite-form/opportunite-form.component';
import {OpportuniteListComponent} from '../../opportunite/opportunite-list/opportunite-list.component';
import {TaskFormComponent} from '../../task/task-form/task-form.component';
import {TaskListComponent} from '../../task/task-list/task-list.component';


@Component({
  selector: 'app-activity-global',
  templateUrl: './activity-global.component.html',
  styleUrls: ['./activity-global.component.scss']
})
export class ActivityGlobalComponent implements OnInit, AfterContentChecked {

  // instances
  @ViewChild(TaskListComponent)
  taskListComponent: TaskListComponent;

  @ViewChild(EventListComponent)
  eventListComponent: EventListComponent;

  @ViewChild(IncidentListComponent)
  incidentListComponent: IncidentListComponent;

  @ViewChild(OpportuniteListComponent)
  opportuniteListComponent: OpportuniteListComponent;

  // dialog window
  taskDialogRef: any = null;
  eventDialogRef: any = null;
  incidentDialogRef: any = null;
  opportuniteDialogRef: any = null;

  eventDialogCloseSubscription: Subscription = null;
  taskDialogCloseSubscription: Subscription = null;
  incidentDialogCloseSubscription: Subscription = null;
  opportinuteDialogCloseSubscription: Subscription = null;

  // filters
  globalFilter: ActivityGlobalFilterModel = null;
  taskFilter: TaskFilterModel = null;
  eventFilter: EventFilterModel = null;
  incidentFilter: IncidentFilterModel = null;
  opportuniteFilter: OpportuniteFilterModel = null;

  constructor(private cdref: ChangeDetectorRef,
              private dialog: MatDialog) {  }

  ngOnInit(): void {
  }

  ngAfterContentChecked(): void {
    this.cdref.detectChanges();
  }


  openDialog(type: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {redirect: true};
    dialogConfig.panelClass = 'activity-popup';
    switch (type) {
      case 'EVENT': {
        this.eventDialogRef = this.dialog.open(EventFormComponent, dialogConfig);
        this.eventDialogCloseSubscription = this.eventDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.eventListComponent?.loadGlobalListOfEvents();
            }
          });
        break;
      }
      case 'TASK': {
        this.taskDialogRef = this.dialog.open(TaskFormComponent, dialogConfig);
        this.taskDialogCloseSubscription = this.taskDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.taskListComponent?.loadGlobalListOfTasks();
            }
          });
        break;
      }
      case 'TICKET': {
        this.incidentDialogRef = this.dialog.open(IncidentFormComponent, {...dialogConfig, height: '500px'});
        this.incidentDialogCloseSubscription = this.incidentDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.incidentListComponent.loadGlobalListOfIncidents();
            }
          });
        break;
      }
      case 'OPPORTUNITY': {
        this.opportuniteDialogRef = this.dialog.open(OpportuniteFormComponent, dialogConfig);
        this.opportinuteDialogCloseSubscription = this.opportuniteDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.opportuniteListComponent?.loadGlobalListOfOpportunites();
            }
          });
        break;
      }
    }
  }

}
