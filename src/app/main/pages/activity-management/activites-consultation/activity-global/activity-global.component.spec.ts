import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityGlobalComponent } from './activity-global.component';

describe('ActivityGlobalComponent', () => {
  let component: ActivityGlobalComponent;
  let fixture: ComponentFixture<ActivityGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
