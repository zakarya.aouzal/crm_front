import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { finalize, map } from 'rxjs/operators';
import { AccountPMModel, AccountPPModel, AccountType, ActivityGlobalFilterModel } from 'src/app/main/models';
import { AccountService } from 'src/app/main/services';

@Component({
  selector: 'app-activity-filter',
  templateUrl: './activity-filter.component.html',
  styleUrls: ['./activity-filter.component.scss']
})
export class ActivityFilterComponent implements OnInit, OnDestroy {

  @Output()
  submited = new EventEmitter<ActivityGlobalFilterModel>();
  color: ThemePalette = 'primary';
  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];
  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];

  // Loading
  loadingAccountList: boolean = false;

  // Subscription
  accountTypeValueChangesSubscribe: Subscription = null;

  constructor(private fb: FormBuilder, private accountService: AccountService,private translate:TranslateService) { }

  ngOnInit(): void {
    this.createFormGroup();
    // subscribe on accountType change
    this.accountTypeValueChangesSubscribe = this.formGroup.get('accountType').valueChanges.subscribe(value => {
      this.accountPPList = [];
      this.accountPMList = [];
      if (value) {
        this.loadAccounts(value);
      }
    });
    this.initFormGroup();
  }

  ngOnDestroy(): void {
    if (this.accountTypeValueChangesSubscribe) {
      this.accountTypeValueChangesSubscribe.unsubscribe();
    }
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      accountType: [null],
      account: [null]
    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    const filter = {...new ActivityGlobalFilterModel()};
    if (this.formGroup) {
      this.formGroup.reset();
      this.submited.emit(filter);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit(filter);
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): ActivityGlobalFilterModel {
    let filter: ActivityGlobalFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new ActivityGlobalFilterModel(),
        typeCompte: this.formGroup?.get('accountType')?.value,
        compte: this.formGroup?.get('account')?.value?.toUpperCase(),
      }
    }
    return filter;
  }

  /** GET LIST OF ACCOUNTS */
  loadAccounts(accountType: AccountType) {
    switch (accountType) {
      case AccountType.PP: {
        this.accountPPList = [];
        this.formGroup.get('account').disable();
        this.accountService.getListAccountPP(null)
          .pipe(finalize(() => {
            this.formGroup.get('account').enable();
          }))
          .subscribe(response => {
            this.accountPPList = response;
          })
        break;
      }
      case AccountType.PM: {
        this.accountPMList = [];
        this.loadingAccountList = true;
        this.formGroup.get('account').disable();
        this.accountService.getListAccountPM(null)
          .pipe(finalize(() => {
            this.formGroup.get('account').enable();
          }))
          .subscribe(response => {
            this.accountPMList = response;
          })
      }
    }
  }




}
