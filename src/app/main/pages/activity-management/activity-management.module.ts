import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AccountService, ReferenceService,TaskService,EventService,OpportuniteService,IncidentService, CommentService, ContactService, AttachmentService} from 'src/app/main/services';
import { SharedModule } from 'src/app/shared/shared.module';
import { ActivityFilterComponent } from './activites-consultation/activity-filter/activity-filter.component';
import {TaskFilterComponent} from './task/task-filter/task-filter.component';
import {TaskFormComponent} from './task/task-form/task-form.component';
import {TaskListComponent} from './task/task-list/task-list.component';
import {ActivityGlobalComponent} from './activites-consultation/activity-global/activity-global.component'
import { ActivityManagementRoutingModule } from './activity-management-routing.module';
import {IncidentDetailsComponent} from './incident/incident-details/incident-details.component';
import {IncidentFilterComponent} from './incident/incident-filter/incident-filter.component';
import {IncidentFormComponent} from './incident/incident-form/incident-form.component';
import {IncidentListComponent} from './incident/incident-list/incident-list.component';
import { EventListComponent } from './event/event-list/event-list.component';
import { EventFilterComponent } from './event/event-filter/event-filter.component';
import { EventFormComponent } from './event/event-form/event-form.component';
import { OpportuniteListComponent } from './opportunite/opportunite-list/opportunite-list.component';
import { OpportuniteFormComponent } from './opportunite/opportunite-form/opportunite-form.component';
import { OpportuniteFilterComponent } from './opportunite/opportunite-filter/opportunite-filter.component';
import { HistoryListComponent } from './incident/incident-details/history-list/history-list.component';
import {ActivitiesHistoryComponent} from './activities-history/activities-history.component'
import {CommentListComponent} from './incident/incident-details/comment-list/comment-list.component'
import { DocumentsManagementModule } from '../documents-management/documents-management.module';
import { MaterialModule } from '../material.module';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {EventEditComponent} from "./event/event-edit/event-edit.component";
import {OpportuniteEditComponent} from "./opportunite/opportunite-edit/opportunite-edit.component";
import {TastEditComponent} from "./task/task-edit/tast-edit.component";
import { TabActivitiesComponent } from './tab-activities/tab-activities.component';
import { TabActivityTaskComponent } from './tab-activities/tab-activity-task/tab-activity-task.component';
import { TabActivityEventComponent } from './tab-activities/tab-activity-event/tab-activity-event.component';
import { TabActivityIncidentComponent } from './tab-activities/tab-activity-incident/tab-activity-incident.component';
import { TabActivityOpportuniteComponent } from './tab-activities/tab-activity-opportunite/tab-activity-opportunite.component';

@NgModule({
  declarations: [
    ActivityGlobalComponent,
    ActivityFilterComponent,
    TaskFilterComponent,
    TaskFormComponent,
    TaskListComponent,
    IncidentFormComponent,
    IncidentDetailsComponent,
    IncidentFilterComponent,
    ActivitiesHistoryComponent,
    IncidentListComponent,
    EventListComponent,
    EventFilterComponent,
    EventFormComponent,
    OpportuniteFilterComponent,
    OpportuniteFormComponent,
    OpportuniteListComponent,
    HistoryListComponent,
    CommentListComponent,
    EventEditComponent,
    OpportuniteEditComponent,
    TastEditComponent,
    TabActivitiesComponent,
    TabActivityTaskComponent,
    TabActivityEventComponent,
    TabActivityIncidentComponent,
    TabActivityOpportuniteComponent,
   
  ],
  imports: [
    CommonModule,
    SharedModule,
    ActivityManagementRoutingModule,
    DocumentsManagementModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  exports:[
    ActivityGlobalComponent,
    ActivityFilterComponent,
    TaskFilterComponent,
    TaskFormComponent,
    TaskListComponent,
    IncidentFormComponent,
    IncidentDetailsComponent,
    IncidentFilterComponent,
    ActivitiesHistoryComponent,
    IncidentListComponent,
    EventListComponent,
    EventFilterComponent,
    EventFormComponent,
    OpportuniteFilterComponent,
    OpportuniteFormComponent,
    OpportuniteListComponent,
    HistoryListComponent,
    CommentListComponent,
    TabActivitiesComponent,
    TabActivityTaskComponent,
    TabActivityEventComponent,
    TabActivityIncidentComponent,
    TabActivityOpportuniteComponent,
  ],
  providers: [
    ReferenceService,
    AccountService,
    TaskService,
    EventService,
    OpportuniteService,
    IncidentService,
    CommentService,
    ContactService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    AttachmentService
  ]
})
export class ActivityManagementModule { }
