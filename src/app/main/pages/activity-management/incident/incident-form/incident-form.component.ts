import {Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
  AccountPMModel,
  AccountPPModel,
  AccountType,
  ContactAccountModel,
  GenericItemModel,
  IncidentModel
} from "../../../../models";
import {AccountService, ContactService, GedService, IncidentService, ReferenceService} from "../../../../services";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {finalize} from "rxjs/operators";
import { AttachmentFormComponent } from '../../../documents-management/attachment-form/attachment-form.component';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-incident-form',
  templateUrl: './incident-form.component.html',
  styleUrls: ['./incident-form.component.scss']
})
export class IncidentFormComponent implements OnInit {


  @Input()
  set accountInfo(value: { accountType: AccountType, accountPP: AccountPPModel, accountPM: AccountPMModel }) {
    this.accountType = null;
    this.accountPP = null;
    this.accountPM = null;
    if (value) {
      this.accountType = value.accountType;
      this.accountPP = value.accountPP;
      this.accountPM = value.accountPM;
      // load list of contacts by account
      let accountCode: string = null;
      switch (this.accountType) {
        case AccountType.PP:
          accountCode = this.accountPP ? this.accountPP.code : null;
          break;
        case AccountType.PM:
          accountCode = this.accountPM ? this.accountPM.code : null;
      }
      if (accountCode) {
        this.loadContacts(accountCode);
      }
    }
  }

  @Output()
  submitted: EventEmitter<void> = new EventEmitter<void>();
  
  @Input()
  hideTitle:boolean=true;

  // form group
  incidentForm: FormGroup = null;

  // validation
  showValidationsMsg = false;
  loading: boolean;

  // references & lists
  prioritesList: GenericItemModel[] = [];
  typeTicketsList: GenericItemModel[] = [];
  originesList: GenericItemModel[] = [];
  statusList: GenericItemModel[] = [];

  accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];
  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];
  contactsList: ContactAccountModel[];

   // Account
   accountType: AccountType = null;
   accountCode: string = null;
   accountPP: AccountPPModel = null;
   accountPM: AccountPMModel = null;


  @ViewChild(AttachmentFormComponent)
  attachementFormComponent: AttachmentFormComponent;

  constructor(private incidentService: IncidentService,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private gedService: GedService,
              private translate: TranslateService,
              private referenceService: ReferenceService,
              private accountService: AccountService,
              private contactService: ContactService,
              @Inject(MAT_DIALOG_DATA) public incidentData: { accountCode: any, editEnabled: boolean },
              private dialogRef: MatDialogRef<IncidentFormComponent>) {
  }

  ngOnInit(): void {
    this.initIncidentForm();
    this.loadIncident();
    this.loadLists();

    if (this.incidentData?.accountCode) {
      this.loadContactsByCompte(this.incidentData?.accountCode);
    } else {
      this.incidentForm.controls['accountType'].valueChanges.subscribe(value => {
        this.accountPPList = [];
        this.accountPMList = [];
        if (value) {
          this.loadAccounts(value);
        }
      })
      this.incidentForm.controls['compte'].valueChanges.subscribe(value => {
        this.loadContactsByCompte(value);
      })
    }
  }

  /** CREATE FORM GROUPS */
  initIncidentForm(): void {
    this.incidentForm = this.fb.group({
      code: [null],
      objet: ['', Validators.required],
      description: ['', Validators.required],
      type: ['', Validators.required],
      origine: ['', Validators.required],
      accountType: [null],
      compte: [this.incidentData?.accountCode, Validators.required],
      contact: [''],
      statut: ['', Validators.required],
      priorite: ['', Validators.required],
      dateDeclaration: [null, Validators.required],
      dateMaj: [null],

      affectation: [''], // optional
      gestionnaire: [''],
      operateurSaisie: [''],
      operateurModification: ['']
    });
    if (!this.incidentData?.editEnabled) {
      this.incidentForm.patchValue({dateDeclaration: new Date().toLocaleString()});
    }
  }

  /** Load Current Contact */
  loadIncident(): void {
    if (this.incidentData?.editEnabled) {
      this.incidentService.getIncidentByCode(this.incidentData?.accountCode)
        .subscribe(value => {
          this.incidentForm.patchValue({
            code: value.code,
            type: value.type,
            origine: value.origine,
            compte: value.compte,
            priorite: value.priorite,
            contact: value.contact,
            statut: value.statut,
            dateDeclaration: value.dateDeclaration?.toLocaleString(),
            dateMaj: new Date().toLocaleString(),
            objet: value.objet,
            description: value.description
          });
        });
    }
  }

   /** LOAD LIST OF CONTACTS BY ACCOUNT */
   loadContacts(accountCode: string) {
    if (!accountCode) return;
    this.contactService.getListContactsByAccount(accountCode)
      .subscribe(response => this.contactsList = response);
  } 
  /** Edit mode switch */
  private switchEditMode(): void {
    this.incidentData.editEnabled = true;
    this.incidentForm.enable();
  }

  /** Forms reset methods */
  onReset(): void {
    this.showValidationsMsg = false;
    if (this.incidentForm) {
      this.incidentForm.reset();
    }
  }

  /** ContactPP Form sumbmission */
  onSubmit(): void {
    if (this.incidentForm.invalid) {
      this.showValidationsMsg = true;
      return;
    }

    const incident = this.generateIncidentPayload();
    this.loading = true;
    if (this.incidentData?.editEnabled) {
      incident.code = this.incidentForm.get('code').value;
      this.updateIncident(incident);
    } else {
      this.createIncident(incident);
    }
  }

  createIncident(dto: IncidentModel): void {
    this.incidentService.createIncident(dto)
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {
        if (value.valid) {
          this.uploadDocuments(value.code);
          this.toastr.success('Incident inseré avec succès');

          this.submitted.emit();
        } else {
          this.toastr.error('L\'opération à échoué : ' + value.message);
        }
      });
  }

  updateIncident(dto: IncidentModel): void {
    this.incidentService.updateIncident(dto)
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {
        if (value.valid) {
          this.toastr.success('Incident modifié avec succès');
        } else {
          this.toastr.error('L\'opération à échoué : ' + value.message);
        }
      });
  }

  uploadDocuments(incidentCode: string): void {
    const files = this.attachementFormComponent?.uploadedFiles;

    this.gedService.uploadFilesForIncident(incidentCode, files)
      .pipe(finalize(() => this.closePopup({
        valid:true,
        compte: this.incidentForm.get('compte').value,
        type:this.incidentForm?.get('accountType')?.value,
      })))
      .subscribe(value => {
        if (value.valid) {
          this.attachementFormComponent.reset();
        } else {
          this.toastr.error('Il y avait une erreur pendant l\'upload des fichiers : \n' + value.detail);
        }
      });
  }

  private generateIncidentPayload(): IncidentModel {
    return {
      type: this.incidentForm?.get('type')?.value,
      origine: this.incidentForm?.get('origine')?.value,
      compte: this.incidentForm?.get('compte')?.value,
      priorite: this.incidentForm?.get('priorite')?.value,
      contact: this.incidentForm?.get('contact')?.value,
      statut: this.incidentForm?.get('statut')?.value,
      objet: this.incidentForm?.get('objet')?.value,
      description: this.incidentForm?.get('description')?.value
    };
  }

  /** GET LIST OF COUNTRIES */
  loadLists(): void {
    this.referenceService.getListPriorites().subscribe(value => this.prioritesList = value);
    this.referenceService.getListOrigines().subscribe(value => this.originesList = value);
    this.referenceService.getListTypeTickets().subscribe(value => this.typeTicketsList = value);
    this.incidentService.getListOfTicketsStatus().subscribe(value => this.statusList = value);
  }

  /** GET LIST OF CONTACTS FOR COMPTE */
  loadContactsByCompte(compte: string): void {
    this.contactService.getListContactsByAccount(compte)
      .subscribe(response => {
        this.contactsList = response;
      });
  }

  /** GET LIST OF ACCOUNTS */
  loadAccounts(accountType: AccountType) {
    switch (accountType) {
      case AccountType.PP: {
        this.accountPPList = [];
        this.accountService.getListAccountPP(null)
          .subscribe(response => {
            this.accountPPList = response;
          })
        break;
      }
      case AccountType.PM: {
        this.accountPMList = [];
        this.accountService.getListAccountPM(null)
          .subscribe(response => {
            this.accountPMList = response;
          })
      }
    }
  }

  closePopup(result?: any): void {
    this.dialogRef.close(result);
  }

}
