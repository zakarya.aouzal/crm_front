import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncidentFilterComponent } from './incident-filter.component';

describe('IncidentFilterComponent', () => {
  let component: IncidentFilterComponent;
  let fixture: ComponentFixture<IncidentFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncidentFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncidentFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
