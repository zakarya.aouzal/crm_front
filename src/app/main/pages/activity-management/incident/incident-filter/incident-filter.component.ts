import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {GenericItemModel, IncidentFilterModel, PrioriteIncident, TaskFilterModel} from 'src/app/main/models';
import { IncidentService, ReferenceService } from 'src/app/main/services';

@Component({
  selector: 'app-incident-filter',
  templateUrl: './incident-filter.component.html',
  styleUrls: ['./incident-filter.component.scss']
})
export class IncidentFilterComponent implements OnInit {

  @Output()
  submited = new EventEmitter<IncidentFilterModel>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  typeTicketsList: GenericItemModel[] = [];
  priorityList: PrioriteIncident[] = [PrioriteIncident.HAUTE, PrioriteIncident.MOYENNE, PrioriteIncident.BASSE];
  statusList: GenericItemModel[] = [];
  accountManagerList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder, private incidentService: IncidentService, private referenceService: ReferenceService) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadTypeTickets();
    this.loadAccountManagers();
    this.loadListIncidentStatus();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      type: [null],
      priorite: [null],
      dateDeclaration: [null],
      status: [null],
      gestionnaire: [null]
    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    const filter = {...new IncidentFilterModel()};
    if (this.formGroup) {
      this.formGroup.reset();
      this.submited.emit(filter);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    };
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit(filter);
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): IncidentFilterModel {
    let filter: IncidentFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new IncidentFilterModel(),
        type: this.formGroup.get('type').value,
        priorite: this.formGroup.get('priorite').value,
        dateDeclaration: this.formGroup.get('dateDeclaration').value,
        status: this.formGroup.get('status').value,
        gestionnaire: this.formGroup.get('gestionnaire').value
      }
    }
    return filter;
  }

  /** GET LIST OF TYPE TICKETS */
  loadTypeTickets() {
    this.incidentService.getListOfTypeTickets()
      .subscribe(response => {
        this.typeTicketsList = response;
      })
  }

  /** GET LIST OF ACCOUNT MANAGERS */
  loadAccountManagers() {
    this.referenceService.getGestionnaireFronts()
      .subscribe(response => {
        this.accountManagerList = response;
      })
  }

  /** GET LIST OF INCIDENT STATUS */
  loadListIncidentStatus() {
    this.incidentService.getListOfTicketsStatus()
      .subscribe(response => {
        this.statusList = response;
      })
  }

}
