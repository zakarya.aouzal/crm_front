import {Component, Input, OnInit} from '@angular/core';
import {CommentModel} from "../../../../../models";

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit {

  @Input()
  commentList: CommentModel[];

  @Input()
  loading: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
