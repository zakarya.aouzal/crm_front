import {Component, Input, OnInit} from '@angular/core';
import {HistoIncident} from "../../../../../models";

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss']
})
export class HistoryListComponent implements OnInit {

  @Input()
  set histoListInput(value: HistoIncident[]) {
    if (value) {
      const histoIncidents = value.sort((a, b) => this.compareDate(b.dateModification, a.dateModification));
      const tempMap: Map<string, HistoIncident[]> = new Map<string, HistoIncident[]>();

      histoIncidents.forEach(incident => {
        const date = incident?.dateModification?.toString();
        if (tempMap.has(date)) {
          tempMap.set(date, Array.of(incident, ...tempMap.get(date)));
        } else {
          tempMap.set(date, Array.of(incident));
        }
      });
      this.histMap = tempMap;
      this.keys = [...this.histMap.keys()];
    }
  }

  histMap: Map<string, HistoIncident[]>;
  keys: string[] = [];

  @Input()
  loading: boolean;

  ngOnInit(): void {
  }

  private compareDate(date1: Date, date2: Date): number {
    let d1 = new Date(date1);
    let d2 = new Date(date2);
    let same = d1.getTime() === d2.getTime();
    if (same) return 0;
    else if (d1 > d2) return 1;
    else if (d1 < d2) return -1;
    return 0;
  }

}
