import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Observable, Subscription} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Location} from '@angular/common';
import {
  CommentModel,
  ContactAccountModel,
  GenericItemModel,
  HistoIncident,
  IncidentModel
} from "../../../../models";
import {
  AccountService,
  CommentService,
  ContactService,
  GedService,
  IncidentService,
  ReferenceService
} from "../../../../services";
import {ToastrService} from "ngx-toastr";
import {finalize} from "rxjs/operators";
import { AttachmentFormComponent } from '../../../documents-management/attachment-form/attachment-form.component';

@Component({
  selector: 'app-incident-details',
  templateUrl: './incident-details.component.html',
  styleUrls: ['./incident-details.component.scss']
})
export class IncidentDetailsComponent implements OnInit,OnDestroy {

  incidentCode: number = null;
  formType: string = '';

  routeSubscription: Subscription;

  // form group
  incidentForm: FormGroup = null;
  commentForm: FormGroup = null;

  // validation
  showValidationMsgs = false;
  showValidationMsgsComment = false;
  loading: boolean = false;
  loadingComment: boolean = false;
  loadingHistory: boolean = false;

  // references & lists
  prioritesList: GenericItemModel[] = [];
  typeTicketsList: GenericItemModel[] = [];
  originesList: GenericItemModel[] = [];
  statusList: GenericItemModel[] = [];

  contactsList: ContactAccountModel[];

  commentsList: CommentModel[];

  histoIncidentList: HistoIncident[];

  @ViewChild(AttachmentFormComponent)
  attachementFormComponent: AttachmentFormComponent;

  constructor(private activatedRoute: ActivatedRoute,
              private incidentService: IncidentService,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private gedService: GedService,
              private commentService: CommentService,
              private referenceService: ReferenceService,
              private accountService: AccountService,
              private contactService: ContactService,
              private _location: Location) {

    this.routeSubscription = activatedRoute.paramMap.subscribe(params => {
      this.incidentCode = params.get('incidentCode') ? Number.parseInt(params.get('incidentCode')) : null;
      this.formType = params.get('formType') ? params.get('formType').toUpperCase() : null;
      if (this.incidentCode) {
        this.initIncidentForm();
        this.initCommentForm();
        this.loadIncident(this.incidentCode)
          .subscribe(incident => {
            this.fillIncidentForm(incident);
            this.loadIncidentComments(this.incidentCode);
            this.loadIncidentHistory(this.incidentCode);
            this.loadLists(incident);
          });
      }
    })
  }
  ngOnDestroy(): void {
    this.routeSubscription?.unsubscribe()
  }

  ngOnInit(): void {
  }

  /** LOAD COMMENTS FOR INCIDENT */
  loadIncidentComments(incidentCode: number): void {
    this.loadingComment = true;
    this.commentService.getCommentsByTicket(incidentCode)
      .pipe(finalize(() => this.loadingComment = false))
      .subscribe(value => this.commentsList = value);
  }

  /** LOAD HISTORY FOR INCIDENT */
  loadIncidentHistory(incidentCode: number): void {
    this.loadingHistory = true;
    this.incidentService.getListOfTicketsHistory(incidentCode)
      .pipe(finalize(() => this.loadingHistory = false))
      .subscribe(value => this.histoIncidentList = value);
  }

  /** CREATE FORM GROUPS */
  initIncidentForm(): void {
    this.incidentForm = this.fb.group({
      code: [{value: null, disabled: true}],
      objet: ['', Validators.required],
      description: ['', Validators.required],
      type: [{value: '', disabled: true}, Validators.required],
      origine: ['', Validators.required],
      compte: [{value: '', disabled: true}, Validators.required],
      contact: [''],
      statut: ['', Validators.required],
      priorite: ['', Validators.required],
      dateDeclaration: [{value: null, disabled: true}, Validators.required],
      dateMaj: [{value: null, disabled: true}],

      typeCompte: ['PP'],
      affectation: [''], // optional
      gestionnaire: [''],
      operateurSaisie: [''],
      operateurModification: ['']
    });

    if (this.formType === 'VIEW') {
      this.incidentForm.disable();
    }
  }

  fillIncidentForm(incident: IncidentModel): void {
    this.incidentForm.patchValue({
      code: incident.code,
      type: incident.type,
      origine: incident.origine,
      compte: incident.compte,
      priorite: incident.priorite,
      contact: incident.contact,
      statut: incident.statut,
      dateDeclaration: incident.dateDeclaration?.toLocaleString(),
      dateMaj: new Date().toLocaleString(),
      objet: incident.objet,
      description: incident.description
    });
  }

  initCommentForm(): void {
    this.commentForm = this.fb.group({
      comment: ['', Validators.required]
    });
  }

  /** Load Current Incident */
  loadIncident(incidentCode: number): Observable<IncidentModel> {
    return this.incidentService.getIncidentByCode(incidentCode);
  }

  /** Forms reset methods */
  onReset(): void {
    this.showValidationMsgs = false;
    if (this.incidentForm) {
      this.incidentForm.reset();
    }
  }

  /** Edit mode switch */
  private switchEditMode(): void {
    this.formType = 'EDIT';
    this.incidentForm.enable();
    this.incidentForm.get('dateMaj').disable();
    this.incidentForm.get('dateDeclaration').disable();
    this.incidentForm.get('compte').disable();
    this.incidentForm.get('code').disable();
  }

  /** ContactPP Form sumbmission */
  onSubmit(): void {
    if (this.formType === 'VIEW') {
      this.switchEditMode();
      return;
    }

    if (this.incidentForm.invalid) {
      this.showValidationMsgs = true;
      return;
    }

    const incident = this.generateIncidentPayload();
    this.loading = true;
    this.updateIncident(incident);
  }

  updateIncident(dto: IncidentModel): void {
    this.incidentService.updateIncident(dto)
      .subscribe(value => {
        if (value.valid) {
          this.toastr.success('Incident modifié avec succès');
          window.location.reload();
        } else {
          this.toastr.error('L\'opération à échoué : ' + value.message);
          this.loading = false;
        }
      });
  }

  /** CREATE COMMENT FOR INCIDENT */
  addComment(): void {
    if (this.commentForm.invalid) {
      this.showValidationMsgsComment = true;
      return;
    }

    const comment: CommentModel = {
      commentaire: this.commentForm.get('comment').value,
      ticketCode: this.incidentCode
    };
    this.loadingComment = true;

    this.commentService.insertComment(comment)
      .subscribe(value => {
        if (value.valid) {
          this.loadIncidentComments(this.incidentCode);
          this.commentForm.reset();
        } else {
          this.toastr.error('Commentaire non ajouté : ' + value.message);
        }
      });
  }

  private generateIncidentPayload(): IncidentModel {
    return {
      code: this.incidentForm?.get('code')?.value,
      type: this.incidentForm?.get('type')?.value,
      origine: this.incidentForm?.get('origine')?.value,
      compte: this.incidentForm?.get('compte')?.value,
      priorite: this.incidentForm?.get('priorite')?.value,
      contact: this.incidentForm?.get('contact')?.value,
      statut: this.incidentForm?.get('statut')?.value,
      objet: this.incidentForm?.get('objet')?.value,
      description: this.incidentForm?.get('description')?.value,
      dateMaj: new Date()
    };
  }

  /** GET LISTS */
  loadLists(incident: IncidentModel): void {
    this.referenceService.getListPriorites().subscribe(value => this.prioritesList = value);
    this.referenceService.getListOrigines().subscribe(value => this.originesList = value);
    this.referenceService.getListTypeTickets().subscribe(value => this.typeTicketsList = value);
    this.incidentService.getListOfTicketsStatus().subscribe(value => this.statusList = value);
    this.contactService.getListContactsByAccount(incident?.compte).subscribe(response => this.contactsList = response);
  }


  back() {
    this._location.back();
  }
}
