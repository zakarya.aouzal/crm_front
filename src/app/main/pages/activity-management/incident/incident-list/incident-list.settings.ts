import { DataTableActions, DataTableColumnsFormat, DataTableSettingsModel } from 'src/app/shared/models';

export const INCIDENT_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.INCIDENT_LIST.CODE'
            }
        }, {
            name: 'objet',
            title: 'Incident',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.INCIDENT_LIST.INCIDENT'
            },
            style:{
              orientation:"left"
                }
        }, {
            name: 'description',
            title: 'Description',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.INCIDENT_LIST.DESCRIPTION'
            },style:{
              orientation:"left"
                }
        }, {
            name: 'dateDeclaration',
            title: 'Date Déclaration',
            format: DataTableColumnsFormat.DATE,
            data:{
              i18nValue:'COLUMNS.INCIDENT_LIST.DATE_DECLARATION'
            }
        }, {
            name: 'type',
            title: 'Type de ticket',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.INCIDENT_LIST.TYPE'
            },style:{
              orientation:"left"
                }
        }, {
            name: 'gestionnaire',
            title: 'Responsable',
            format: DataTableColumnsFormat.STRING,
        data:{
          i18nValue:'COLUMNS.INCIDENT_LIST.GESTIONNAIRE'
        }
      }, {
            name: 'compte',
            title: 'Compte',
            format: DataTableColumnsFormat.STRING,
        data:{
          i18nValue:'COLUMNS.INCIDENT_LIST.COMPTE'
        }
        }, {
            name: 'statut',
            title: 'Statut',
            format: DataTableColumnsFormat.STRING,
        data:{
          i18nValue:'COLUMNS.INCIDENT_LIST.STATUT'
        },style:{
          orientation:"left"
            }
        }, {
            name: 'priorite',
            title: 'Priorité',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.INCIDENT_LIST.PRIORITE'
            },
            style: {
                colors: [
                    {
                        colorValue: 'red',
                        conditionValue: 'HAUTE'
                    }, {
                        colorValue: 'orange',
                        conditionValue: 'MOYENNE'
                    }, {
                        colorValue: 'blue',
                        conditionValue: 'BASSE'
                    }
                ],orientation:"left"
            }
        }
    ], actions: [DataTableActions.VIEW, DataTableActions.DELETE]
};
