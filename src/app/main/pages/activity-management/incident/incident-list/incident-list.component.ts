import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {finalize} from 'rxjs/operators';
import {AccountType, ActivityGlobalFilterModel, IncidentFilterModel, IncidentModel} from 'src/app/main/models';
import {IncidentService} from 'src/app/main/services';
import {DataTableOutputActionsModel, DataTableSettingsModel} from 'src/app/shared/models';
import {INCIDENT_LIST_SETTINGS} from "./incident-list.settings";
import {Router} from "@angular/router";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-incident-list',
  templateUrl: './incident-list.component.html',
  styleUrls: ['./incident-list.component.scss']
})
export class IncidentListComponent implements OnInit {

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.incidentList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfIncidents();
    }
  }

  @Input()
  set filter(value: IncidentFilterModel) {
    this.incidentList = [];
    if (!value) return;
    this._filter = {
      ...this._globalFilter,
      ...value
    };
    if (this._filter) {
      this.loadIncidentsList();
    }
  }

  @Input()
  set CustomSettings(value: DataTableSettingsModel) {
    if(value){

      console.log(value);
      this.settings=value
    }
  }
  
  @Input()
  set accountCode(value: string) {
    this.incidentList = [];
    if (!value) return;
    this._globalFilter = {
      typeCompte: this.currentAccountCode.startsWith('PP')?AccountType.PP:AccountType.PM,
      compte: this.currentAccountCode
    };
    this._accountType = this.currentAccountCode.startsWith('PP')?AccountType.PP:AccountType.PM;
    this.currentAccountCode = value.toUpperCase();
    if (this._globalFilter) {
      this.loadGlobalListOfIncidents();
    }
  }

  @Input()
  set enableActions(value: boolean) {
    this._enableActions = value;
  }
  _enableActions:boolean=true;

  _globalFilter: ActivityGlobalFilterModel;

  _accountType: AccountType = null;

  _filter: IncidentFilterModel = null;

  settings : DataTableSettingsModel = INCIDENT_LIST_SETTINGS;

  incidentList: IncidentModel[];

  loading: boolean = false;

  confirmationModalRef: BsModalRef;

  currentAccountCode: string;
  currentIncidentCode: number;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  constructor(private incidentService: IncidentService,
              private modalService: BsModalService,
              private toastr: ToastrService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadIncidents();
  }

  /** LOAD GLOBAL LIST OF INCIDENTS BY FILTER */
  loadGlobalListOfIncidents() {

    this.incidentList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.incidentService.fetchAllByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.incidentList = response);
  }

  /** LOAD LIST OF INCIDENTS BY FILTER */
  loadIncidentsList(): void {
    this.incidentList = [];
    if (!this._filter) return;
    this.loading = true;
    this.incidentService.fetchAllByFilter(this._filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.incidentList = response);
  }

  /** LOAD INCIDENTS LIST */
  loadIncidents(): void {
    if (!this.currentAccountCode) return;
    this.incidentList = [];
    this.loading = true;
    this.incidentService.getListOfIncidents(this.currentAccountCode)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.incidentList = response);
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<IncidentModel>) {
    if (event) {
      this.currentIncidentCode = event.item?.code;
      switch (event.actionType) {
        case 'VIEW':
          this.router.navigate(['/pages/activity-management/incident-details', this.currentIncidentCode, event.actionType]);
          break;
        case 'DELETE':
          this.confirmationModalRef = this.modalService.show(this.templateRef);
          break;
      }
    }
  }

  confirmDelete(event: DataTableOutputActionsModel<IncidentModel>): void {
    if (event) {
      this.incidentService.deleteIncidentByCode(this.currentIncidentCode)
        .subscribe(value => {
          if (value.valid) {
            this.loadGlobalListOfIncidents();
            this.toastr.success('Incident supprimé');
          } else {
            this.toastr.error('Erreur pendant la suppression')
          }
        });
    }
  }
}
