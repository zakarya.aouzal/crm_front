import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { finalize } from 'rxjs/operators';
import { AccountPMModel, AccountPPModel, AccountType, GenericItemModel, TaskPayloadModel } from 'src/app/main/models';
import {AccountService, ContactService, TaskService} from 'src/app/main/services';
import { ContactAccountModel } from '../../../../models';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss']
})
export class TaskFormComponent implements OnInit {

  @Input()
  set accountInfo(value: { accountType: AccountType, accountPP: AccountPPModel, accountPM: AccountPMModel }) {
    this.accountType = null;
    this.accountPP = null;
    this.accountPM = null;
    this.contactsList = [];
    if (value) {
      this.accountType = value.accountType;
      this.accountPP = value.accountPP;
      this.accountPM = value.accountPM;
      // load list of contacts by account
      
      switch (this.accountType) {
        case AccountType.PP:
          this.accountCode = this.accountPP ? this.accountPP.code : null;
          break;
        case AccountType.PM:
          this.accountCode = this.accountPM ? this.accountPM.code : null;
      }
      if (this.accountCode) {
        this.loadContacts(this.accountCode);
      }
    }
  }


@Input()
hideTitle:boolean=true;

  @Output()
  submitted: EventEmitter<void> = new EventEmitter<void>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

 

  // Loading
  loading: boolean = false;

  // List
  accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];
  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];

  // Account
  accountType: AccountType = null;
  accountCode: string = null;
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;

  // List
  statusList: GenericItemModel[] = [];
  contactsList: ContactAccountModel[] = [];
  taskTypeList: GenericItemModel[] = [];

 


  constructor(private fb: FormBuilder,
              private taskService: TaskService,
              public dialogRef: MatDialogRef<TaskFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { redirect: boolean, accountCode: string },
              private toastrService: ToastrService,
              private translate: TranslateService,
              private accountService: AccountService,
              private contactService: ContactService) {
  }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadListOfTaskTypes();
    this.loadListOfTaskStatus();

    if (this.data?.redirect) {
      this.formGroup?.get('accountType')?.valueChanges.subscribe(value => {
        this.accountPPList = [];
        this.accountPMList = [];
        if (value) {
          this.loadAccounts(value);
        }
      });
      this.formGroup?.get('account')?.valueChanges.subscribe(value => {
        this.loadContacts(value);
      });
    }

    if (this.data?.accountCode) {
      this.formGroup?.get('account').patchValue(this.data?.accountCode);
    }
  }

  /** LOAD LIST OF CONTACTS BY ACCOUNT */
  loadContacts(accountCode: string) {
    if (!accountCode) return;
    this.contactService.getListContactsByAccount(accountCode)
      .subscribe(response => this.contactsList = response);
  }

  /** GET LIST OF TASK TYPES */
  loadListOfTaskTypes() {
    this.taskService.getListOfTaskTypes()
      .subscribe(response => this.taskTypeList = response);
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      subject: [null, [Validators.required]],
      type: [null, [Validators.required]],
      description: [null],
      date: [null, [Validators.required]],
      accountType: [null],
      account: [null],
      contact: [null],
      status: [null, [Validators.required]]
    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
    }
  }

  /** LOAD TASK STATUS */
  loadListOfTaskStatus() {
    this.taskService.getListOfTaskStatus()
      .subscribe(response => {
        this.statusList = response;
      })
  }

  /** SUBMIT TASK */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const payload: TaskPayloadModel = this.generatePayload();
    console.log(payload)
    if (!payload) return;
    this.loading = true;
    this.taskService.addTask(payload)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => {
        if (response && response.valid) {
          this.submitted.emit();
          this.toastrService.success('Tâche ajoutée');
          this.dialogRef.close({
            valid:true,
            compte: this.formGroup.get('account').value.toUpperCase(),
            type:this.formGroup?.get('accountType')?.value,
          });
          this.initFormGroup();
        } else {
          this.toastrService.warning("Un problème est survenu lors d'ajout de tâche");
        }
      });
  }

  /** GENERATE PAYLOAD */
  generatePayload(): TaskPayloadModel {
    let filter: TaskPayloadModel = null;
    if (this.formGroup) {
      filter = {
        ...new TaskPayloadModel(),
        subject: this.formGroup.get('subject').value,
        type: this.formGroup.get('type').value,
        description: this.formGroup.get('description').value,
        due_date: this.formGroup.get('date').value,
        contact_id: this.formGroup.get('contact').value ? this.formGroup.get('contact').value.code : null,
        compte_id: this.formGroup.get('account').value ? this.formGroup.get('account').value.toUpperCase() : null,
        status: this.formGroup.get('status').value ? this.formGroup.get('status').value : null
      }
      if (!filter.compte_id) {
        switch (this.accountType) {
          case AccountType.PP:
            filter.compte_id = this.accountPP ? this.accountPP.code : null;
            break;
          case AccountType.PM:
            filter.compte_id = this.accountPM ? this.accountPM.code : null;
        }
      }
    }
    return filter;
  }

  /** GET LIST OF ACCOUNTS */
  loadAccounts(accountType: AccountType) {
    switch (accountType) {
      case AccountType.PP: {
        this.accountPPList = [];
        this.accountService.getListAccountPP(null)
          .subscribe(response => {
            this.accountPPList = response;
          })
        break;
      }
      case AccountType.PM: {
        this.accountPMList = [];
        this.accountService.getListAccountPM(null)
          .subscribe(response => {
            this.accountPMList = response;
          })
      }
    }
  }

}
