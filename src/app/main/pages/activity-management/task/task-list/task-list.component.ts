import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {
  AccountPMModel,
  AccountPPModel,
  AccountType,
  ActivityGlobalFilterModel,
  TaskFilterModel,
  TaskModel
} from 'src/app/main/models';
import { TaskService } from 'src/app/main/services';
import { DataTableOutputActionsModel, DataTableSettingsModel } from 'src/app/shared/models';
import { TASK_LIST_SETTINGS } from './task-list.settings';
import {finalize} from "rxjs/operators";
import {ToastrService} from "ngx-toastr";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {EventEditComponent} from "../../event/event-edit/event-edit.component";
import {MatDialog} from "@angular/material/dialog";
import {TastEditComponent} from "../task-edit/tast-edit.component";

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.taskList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfTasks();
    }
  }

  @Input()
  set filter(value: TaskFilterModel) {
    this.taskList = [];
    if (!value) return;
    this._filter = {
      ...this._globalFilter,
      ...value
    };
    if (this._filter) {
      this.loadTasksList();
    }
  }

  @Input()
  set enableActions(value: boolean) {
    this._enableActions = value;
  }
  
  @Input()
  set CustomSettings(value: DataTableSettingsModel) {
    if(value){
      this.settings=value
    }
  }


  _enableActions:boolean=true;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  selectedTaskCode: string;
  currentAccountCode: string;

  settings: DataTableSettingsModel = TASK_LIST_SETTINGS;

  confirmationModalRef: BsModalRef;

  _globalFilter: ActivityGlobalFilterModel = null;

  _filter: TaskFilterModel;

  _accountType: AccountType = null;

  taskList: TaskModel[] = [];

  loading: boolean = false;

  constructor(private taskService: TaskService,
              private toastr: ToastrService,
              private modalService: BsModalService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  /** LOAD GLOBAL LIST OF TASKS */
  loadGlobalListOfTasks() {
    this.taskList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.taskService.getTasksListByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.taskList = response);
  }

  /** LOAD LIST OF TASKS */
  loadTasksList(): void {
    this.taskList = [];
    if (!this._filter) return;
    this.loading = true;
    this.taskService.getTasksListByFilter(this._filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.taskList = response);
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<TaskModel>) {
    if (event) {
      this.selectedTaskCode = event.item?.code;
      switch (event.actionType) {
        case 'VIEW':
          this.dialog.open(TastEditComponent, {panelClass: 'activity-popup', data: {code: this.selectedTaskCode, viewMode: true}})
            .afterClosed().subscribe((isEdit) => {
              if (isEdit) this.loadGlobalListOfTasks();
          });
          break;
        case 'DELETE':
          this.confirmationModalRef = this.modalService.show(this.templateRef);
          break;
      }
    }
  }

  confirmDelete(event: DataTableOutputActionsModel<TaskModel>): void {
    if (event) {
      this.taskService.deleteTask(this.selectedTaskCode)
        .subscribe(value => {
          if (value.valid) {
            this.loadGlobalListOfTasks();
            this.toastr.success('Incident supprimé');
          } else {
            this.toastr.error('Erreur pendant la suppression')
          }
        });
    }
  }

}
