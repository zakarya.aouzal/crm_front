import { DataTableActions, DataTableColumnsFormat, DataTableSettingsModel } from 'src/app/shared/models';

export const TASK_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.CODE'
          }
        }, {
            name: 'subject',
            title: 'Sujet',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.SUJET'
          },style:{
            orientation:"left"
              }
        }, {
            name: 'description',
            title: 'Description',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.DESCRIPTION'
          },style:{
            orientation:"left"
              }
        }, {
            name: 'type',
            title: 'Type',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.TYPE'
          },style:{
            orientation:"left"
              }
        }, {
            name: 'dateInsert',
            title: 'Date création',
            format: DataTableColumnsFormat.DATE,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.DATE_CREATION'
          }
        }, {
            name: 'gestionnaire',
            title: 'Gestionnaire',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.GESTIONNAIRE'
          },style:{
            orientation:"left"
              }
        }, {
            name: 'compte_id',
            title: 'Compte',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.COMPTE'
          }
        }, {
            name: 'status',
            title: 'Status',
            format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.TASK_LIST.STATUT'
          },style:{
            orientation:"left"
              }
        }
    ], actions: [DataTableActions.VIEW, DataTableActions.DELETE]
};
