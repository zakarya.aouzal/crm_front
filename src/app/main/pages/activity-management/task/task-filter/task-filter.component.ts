import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventFilterModel, GenericItemModel, TaskFilterModel } from 'src/app/main/models';
import { EventService, ReferenceService, TaskService } from 'src/app/main/services';
import { endDateLessThanStartDateValidator } from 'src/app/shared/utils/validate';

@Component({
  selector: 'app-task-filter',
  templateUrl: './task-filter.component.html',
  styleUrls: ['./task-filter.component.scss']
})
export class TaskFilterComponent implements OnInit {

  @Output()
  submited = new EventEmitter<TaskFilterModel>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  taskStatusList: GenericItemModel[] = [];
  accountManagerList: GenericItemModel[] = [];
  taskTypeList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder, private referenceService: ReferenceService, private taskService: TaskService) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadAccountManagers();
    this.loadListOfTaskTypes();
    this.loadListOfTaskStatus();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      type: [null],
      gestionnaire: [null],
      dateCreation: [null],
      status: [null]
    }, { validator: endDateLessThanStartDateValidator() });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    const filter = {...new TaskFilterModel()};
    if (this.formGroup) {
      this.formGroup.reset();
      this.submited.emit(filter);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    };
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit(filter);
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): TaskFilterModel {
    let filter: TaskFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new TaskFilterModel(),
        type: this.formGroup.get('type').value,
        gestionnaire: this.formGroup.get('gestionnaire').value,
        dateCreation: this.formGroup.get('dateCreation').value,
        status: this.formGroup.get('status').value
      }
    }
    return filter;
  }

  /** GET LIST OF ACCOUNT MANAGERS */
  loadAccountManagers() {
    this.referenceService.getGestionnaireFronts()
      .subscribe(response => {
        this.accountManagerList = response;
      })
  }

  /** GET LIST OF TASK TYPES */
  loadListOfTaskTypes() {
    this.taskService.getListOfTaskTypes()
      .subscribe(response => {
        this.taskTypeList = response;
      })
  }

  /** GET LIST OF TASK STATUS */
  loadListOfTaskStatus() {
    this.taskService.getListOfTaskStatus()
      .subscribe(value => {
        this.taskStatusList = value;
      })
  }


}
