import {Component, Inject, OnInit} from '@angular/core';
import {ContactService, TaskService} from "../../../../services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ContactAccountModel, GenericItemModel, TaskPayloadModel} from "../../../../models";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-create-contact',
  templateUrl: './tast-edit.component.html',
  styleUrls: ['./tast-edit.component.scss']
})
export class TastEditComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private taskService: TaskService,
              private dialogRef: MatDialogRef<TastEditComponent>,
              private toastr: ToastrService,
              private contactService: ContactService,
              @Inject(MAT_DIALOG_DATA) public data: { code: any, viewMode: boolean }) {
  }

  loading: boolean = false;

  payloadModel: TaskPayloadModel = null;

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  taskTypeList: GenericItemModel[] = [];
  statusList: GenericItemModel[] = [];
  contactsList: ContactAccountModel[] = [];

  ngOnInit(): void {
    this.loadListes();

    this.createForm();
    this.fillForm(this.data?.code);

    if (this.data?.viewMode) {
      this.formGroup?.disable();
    }
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      subject: [null, [Validators.required]],
      type: [null, [Validators.required]],
      description: [null],
      date: [null, [Validators.required]],
      account: [null, [Validators.required]],
      contact: [null],
      status: [null, [Validators.required]]
    });
  }

  private fillForm(code: any): void {
    this.taskService.getTaskByCode(code)
      .subscribe((payload) => {
        this.formGroup.patchValue({
          subject: payload?.subject,
          type: payload?.type,
          description: payload?.description,
          account: payload?.compte_id,
          date: payload?.due_date,
          contact: payload?.contact_id,
          status: payload?.status
        });
        this.loadContacts(payload?.compte_id);
      });
  }

  onSubmit(): void {
    if (this.data?.viewMode) {
      this.data.viewMode = false;
      this.formGroup?.enable();
      this.formGroup?.get('account')?.disable();
      return;
    }

    if (this.formGroup?.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    this.showValidationsMsg = false;

    this.payloadModel = this.generatePayload();

    this.taskService.updateTask(this.payloadModel)
      .subscribe(value => {
        if (value.valid) {
          this.dialogRef.close(!this.data?.viewMode);
          this.toastr.success("Tâche modifié");
        } else {
          this.toastr.error("Erreur du serveur");
        }
      });
  }

  generatePayload(): TaskPayloadModel {
    let filter: TaskPayloadModel = null;
    if (this.formGroup) {
      filter = {
        ...new TaskPayloadModel(),
        code: this.data?.code,
        subject: this.formGroup.get('subject').value,
        type: this.formGroup.get('type').value,
        description: this.formGroup.get('description').value,
        compte_id: this.formGroup.get('account').value ? this.formGroup.get('account').value.toUpperCase() : null,
        contact_id: this.formGroup.get('contact').value ? this.formGroup.get('contact').value : null,
        due_date: this.formGroup.get('date').value ? this.formGroup.get('date').value : null,
        status: this.formGroup.get('status').value ? this.formGroup.get('status').value : null
      }
    }
    return filter;
  }

  loadContacts(compteCode: string): void {
    this.contactService.getListContactsByAccount(compteCode).subscribe(response => this.contactsList = response);
  }

  loadListes(): void {
    this.taskService.getListOfTaskTypes().subscribe(response => this.taskTypeList = response);
    this.taskService.getListOfTaskStatus().subscribe(response => this.statusList = response);
  }
}
