import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportuniteFormComponent } from './opportunite-form.component';

describe('OpportuniteFormComponent', () => {
  let component: OpportuniteFormComponent;
  let fixture: ComponentFixture<OpportuniteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportuniteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportuniteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
