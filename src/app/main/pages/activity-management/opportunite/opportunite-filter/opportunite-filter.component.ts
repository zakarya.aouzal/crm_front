import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GenericItemModel, OpportuniteFilterModel, TaskFilterModel} from "../../../../models";
import {FormBuilder, FormGroup} from "@angular/forms";
import {OpportuniteService} from "../../../../services";

@Component({
  selector: 'app-opportunite-filter',
  templateUrl: './opportunite-filter.component.html',
  styleUrls: ['./opportunite-filter.component.scss']
})
export class OpportuniteFilterComponent implements OnInit {

  @Output()
  submited = new EventEmitter<OpportuniteFilterModel>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  oppTypesList: GenericItemModel[] = [];
  oppStatusList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder,
              private opportuniteService: OpportuniteService) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadOpportuniteStatus();
    this.loadOpportuniteTypes();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      type: [null],
      dateCreation: [null],
      status: [null],
      gestionnaire: [null]
    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    const filter = {...new OpportuniteFilterModel()};
    if (this.formGroup) {
      this.formGroup.reset();
      this.submited.emit(filter);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit(filter);
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): OpportuniteFilterModel {
    let filter: OpportuniteFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new OpportuniteFilterModel(),
        type: this.formGroup.get('type').value,
        dateCreation: this.formGroup.get('dateCreation').value,
        status: this.formGroup.get('status').value,
        gestionnaire: this.formGroup.get('gestionnaire').value
      }
    }
    return filter;
  }

  /** GET LIST OF TYPES */
  loadOpportuniteTypes() {
    this.opportuniteService.getOpportuniteTypes()
      .subscribe(response => {
        this.oppTypesList = response;
      })
  }

  /** GET LIST OF STATUS */
  loadOpportuniteStatus() {
    this.opportuniteService.getOpportuniteStatus()
      .subscribe(response => {
        this.oppStatusList = response;
      })
  }
}
