import {Component, Inject, OnInit} from '@angular/core';
import {ContactService, OpportuniteService} from "../../../../services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ContactAccountModel, GenericItemModel, OpportuniteModel} from "../../../../models";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-create-contact',
  templateUrl: './opportunite-edit.component.html',
  styleUrls: ['./opportunite-edit.component.scss']
})
export class OpportuniteEditComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private opportuniteService: OpportuniteService,
              private dialogRef: MatDialogRef<OpportuniteEditComponent>,
              private toastr: ToastrService,
              private contactService: ContactService,
              @Inject(MAT_DIALOG_DATA) public data: { code: any, viewMode: boolean }) {
  }

  loading: boolean = false;

  payloadModel: OpportuniteModel = null;

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  opportuniteTypeList: GenericItemModel[] = [];
  statusList: GenericItemModel[] = [];
  contactsList: ContactAccountModel[] = [];

  probabilitesList = [
    {code: 10, value: "10%"},
    {code: 20, value: "20%"},
    {code: 30, value: "30%"},
    {code: 40, value: "40%"},
    {code: 50, value: "50%"},
    {code: 60, value: "60%"},
    {code: 70, value: "70%"},
    {code: 80, value: "80%"},
    {code: 90, value: "90%"},
    {code: 100, value: "100%"},
  ];

  ngOnInit(): void {
    this.loadListes();

    this.createForm();
    this.fillForm(this.data?.code);

    if (this.data?.viewMode) {
      this.formGroup?.disable();
    }
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      subject: [null, [Validators.required]],
      type: [null, [Validators.required]],
      description: [null],
      estimation: [0.00, [Validators.required]],
      probabilite: [null, [Validators.required]],
      account: [null, [Validators.required]],
      date: [null, [Validators.required]],
      contact: [null],
      status: [null, [Validators.required]]
    });
  }

  private fillForm(code: any): void {
    this.opportuniteService.getByCode(code)
      .subscribe((payload) => {
        this.formGroup.patchValue({
          subject: payload?.subject,
          type: payload?.type,
          description: payload?.description,
          estimation: payload?.estimation,
          probabilite: payload?.probabilite,
          account: payload?.compte_id,
          date: payload?.due_date,
          contact: payload?.contact_id,
          status: payload?.status
        });
        this.loadContacts(payload?.compte_id);
      });
  }

  onSubmit(): void {
    if (this.data?.viewMode) {
      this.data.viewMode = false;
      this.formGroup?.enable();
      this.formGroup?.get('account')?.disable();
      return;
    }

    if (this.formGroup?.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    this.showValidationsMsg = false;

    this.payloadModel = this.generatePayload();

    this.opportuniteService.update(this.payloadModel)
      .subscribe(value => {
        if (value.valid) {
          this.dialogRef.close();
          this.toastr.success("Opportunité modifié");
        } else {
          this.toastr.error("Erreur du serveur");
        }
      });
  }

  generatePayload(): OpportuniteModel {
    let filter: OpportuniteModel = null;
    if (this.formGroup) {
      filter = {
        ...new OpportuniteModel(),
        code: this.data?.code,
        subject: this.formGroup.get('subject').value,
        type: this.formGroup.get('type').value,
        description: this.formGroup.get('description').value,
        estimation: this.formGroup.get('estimation').value,
        probabilite: this.formGroup.get('probabilite').value,
        due_date: this.formGroup.get('date').value,
        contact_id: this.formGroup.get('contact').value ? this.formGroup.get('contact').value.code : null,
        compte_id: this.formGroup.get('account').value ? this.formGroup.get('account').value.toUpperCase() : null,
        status: this.formGroup.get('status').value ? this.formGroup.get('status').value : null
      }
    }
    return filter;
  }

  loadContacts(compteCode: string): void {
    this.contactService.getListContactsByAccount(compteCode).subscribe(response => this.contactsList = response);
  }

  loadListes(): void {
    this.opportuniteService.getOpportuniteTypes().subscribe(response => this.opportuniteTypeList = response);
    this.opportuniteService.getOpportuniteStatus().subscribe(response => this.statusList = response);
  }
}
