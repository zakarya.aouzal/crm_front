import { DataTableActions, DataTableColumnsFormat, DataTableSettingsModel } from 'src/app/shared/models';

export const OPPORTUNITE_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.CODE'
            }
        }, {
            name: 'subject',
            title: 'Sujet',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.SUJET'
            }
        }, {
            name: 'probabilite',
            title: 'Probabilité',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.PROBABILITE'
            }
        }, {
            name: 'estimation',
            title: 'Estimation',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.ESTIMATION'
            },style:{
              orientation:"right"
                }
        }, {
            name: 'description',
            title: 'Description',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.DESCRIPTION'
            },style:{
              orientation:"left"
                }
        }, {
            name: 'dateInsert',
            title: 'Date de création',
            format: DataTableColumnsFormat.DATE,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.DATE_CREATION'
            }
        }, {
            name: 'operateur_saisie',
            title: 'Gestionnaire',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.GESTIONNAIRE'
            }
        }, {
          name: 'type',
          title: 'Type',
          format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.OPPORTUNITE_LIST.TYPE'
          },style:{
            orientation:"left"
              }
        }, {
            name: 'compte_id',
            title: 'Compte',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.COMPTE'
            }
        }, {
            name: 'status',
            title: 'Statut',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.OPPORTUNITE_LIST.STATUT'
            },style:{
              orientation:"left"
                }
        }
    ], actions: [DataTableActions.VIEW, DataTableActions.DELETE]
};
