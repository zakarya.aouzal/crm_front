import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportuniteListComponent } from './opportunite-list.component';

describe('OpportuniteListComponent', () => {
  let component: OpportuniteListComponent;
  let fixture: ComponentFixture<OpportuniteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportuniteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportuniteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
