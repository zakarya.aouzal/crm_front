import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AccountType, ActivityGlobalFilterModel, OpportuniteFilterModel, OpportuniteModel} from "../../../../models";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {OpportuniteService} from "../../../../services";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {finalize} from "rxjs/operators";
import {DataTableOutputActionsModel, DataTableSettingsModel} from "../../../../../shared/models";
import {OPPORTUNITE_LIST_SETTINGS} from "./opportunite-list.settings";
import {MatDialog} from "@angular/material/dialog";
import {OpportuniteEditComponent} from "../opportunite-edit/opportunite-edit.component";

@Component({
  selector: 'app-opportunite-list',
  templateUrl: './opportunite-list.component.html',
  styleUrls: ['./opportunite-list.component.scss']
})
export class OpportuniteListComponent implements OnInit {

  @Input()
  set globalFilter(value: ActivityGlobalFilterModel) {
    this.opportuniteList = [];
    if (!value) return;
    this._globalFilter = value;
    this._accountType = value?.typeCompte;
    this.currentAccountCode = value?.compte;
    if (this._globalFilter) {
      this.loadGlobalListOfOpportunites();
    }
  }

  @Input()
  set filter(value: OpportuniteFilterModel) {
    this.opportuniteList = [];
    if (!value) return;
    this._filter = {
      ...this._globalFilter,
      ...value
    };
    if (this._filter) {
      this.loadOpportunitesList();
    }
  }

  @Input()
  set CustomSettings(value: DataTableSettingsModel) {
    if(value){

      console.log(value);
      this.settings=value
    }
  }

  @Input()
  set accountCode(value: string) {
    this.opportuniteList = [];
    if (!value) return;
    this._globalFilter = {
      typeCompte: this.currentAccountCode.startsWith('PP')?AccountType.PP:AccountType.PM,
      compte: this.currentAccountCode
    };
    this._accountType = this.currentAccountCode.startsWith('PP')?AccountType.PP:AccountType.PM;
    this.currentAccountCode = value.toUpperCase();
    if (this._globalFilter) {
      this.loadGlobalListOfOpportunites();
    }
  }

  @Input()
  set enableActions(value: boolean) {
    this._enableActions = value;
  }
  _enableActions:boolean=true;

  _globalFilter: ActivityGlobalFilterModel;

  _accountType: AccountType = null;

  _filter: OpportuniteFilterModel = null;

  settings : DataTableSettingsModel = OPPORTUNITE_LIST_SETTINGS;

  opportuniteList: OpportuniteModel[];

  loading: boolean = false;

  confirmationModalRef: BsModalRef;

  currentAccountCode: string;
  currentOpportuniteCode: number;

  @ViewChild('template')
  templateRef: TemplateRef<any>;

  constructor(private opportuniteService: OpportuniteService,
              private modalService: BsModalService,
              private toastr: ToastrService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    
  }

  /** LOAD GLOBAL OPPORTUNITE LIST */
  loadGlobalListOfOpportunites() {
    this.opportuniteList = [];
    if (!this._globalFilter) return;
    this.loading = true;
    this.opportuniteService.fetchAllByFilter(this._globalFilter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.opportuniteList = response);
  }

  /** LOAD OPPORTUNITE LIST */
  loadOpportunitesList(): void {
    this.opportuniteList = [];
    if (!this._filter) return;
    this.loading = true;
    this.opportuniteService.fetchAllByFilter(this._filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(response => this.opportuniteList = response);
  }

  onAction(event: DataTableOutputActionsModel<OpportuniteModel>) {
    if (event) {
      this.currentOpportuniteCode = event.item?.code;
      switch (event.actionType) {
        case 'VIEW':
          this.dialog.open(OpportuniteEditComponent, {panelClass: 'activity-popup', data: {code: this.currentOpportuniteCode, viewMode: true}})
            .afterClosed().subscribe(() => this.loadGlobalListOfOpportunites());
          break;
        case 'DELETE':
          this.confirmationModalRef = this.modalService.show(this.templateRef);
          break;
      }
    }
  }

  confirmDelete(event: DataTableOutputActionsModel<OpportuniteModel>): void {
    if (event) {
      this.opportuniteService.deleteOpportunite(this.currentOpportuniteCode)
        .subscribe(value => {
          if (value.valid) {
            this.loadGlobalListOfOpportunites();
            this.toastr.success('Opportunité supprimé');
          } else {
            this.toastr.error('Erreur pendant la suppression')
          }
        });
    }
  }
}
