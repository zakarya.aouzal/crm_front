import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivitiesHistoryComponent } from './activities-history.component';

describe('ActivitiesHistoryComponent', () => {
  let component: ActivitiesHistoryComponent;
  let fixture: ComponentFixture<ActivitiesHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivitiesHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivitiesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
