import { Component, Inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HistoryActivityModel } from 'src/app/main/models';
import { HistoryActivityService } from 'src/app/main/services';
export interface SearchDto{
   date:Date;
   compte:string;
}
@Component({
  selector: 'app-activities-history',
  templateUrl: './activities-history.component.html',
  styleUrls: ['./activities-history.component.scss']
})
export class ActivitiesHistoryComponent implements OnInit,OnChanges {


  @Input()
  compte:string

  searchDto:SearchDto={date:new Date(),compte:""};

  // history list
  nextHistoryList: HistoryActivityModel[] = [];
  previousHistoryList: HistoryActivityModel[] = [];

  constructor(private historyActivityService: HistoryActivityService, @Inject("pipeParameters") public pipeParameters: any) { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes){
      this.searchDto.compte=this.compte
    }
  }

  ngOnInit(): void {
    this.loadNextHistoryActivity(this.searchDto);
    this.loadPreviousHistoryActivity(this.searchDto);
  }

  /** LOAD NEXT HISTORY */
  loadNextHistoryActivity(searchDto:SearchDto) {
    this.nextHistoryList = [];
    this.historyActivityService.getNextHistoryList(searchDto)
      .subscribe(response => {
        if (response) {
          this.nextHistoryList = response.slice(0, 10);
        }
      })
  }

  /** LOAD PREVIOUS HISTORY */
  loadPreviousHistoryActivity(searchDto:SearchDto) {
    this.previousHistoryList = [];
    this.historyActivityService.getPreviousHistoryList(searchDto)
      .subscribe(response => {
        if (response) {
          this.previousHistoryList = response.slice(0, 10);
        }
      })
  }

}
