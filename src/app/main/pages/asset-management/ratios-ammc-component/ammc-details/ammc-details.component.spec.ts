import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmmcDetailsComponent } from './ammc-details.component';

describe('AmmcDetailsComponent', () => {
  let component: AmmcDetailsComponent;
  let fixture: ComponentFixture<AmmcDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmmcDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmmcDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
