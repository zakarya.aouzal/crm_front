import {DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const AMMC_DETAILS_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'date_vl',
      title: 'DATE_VL',
      format: DataTableColumnsFormat.DATE,
      data: {
        i18nValue: 'COLUMNS.RATIOS_AMMC_LIST.DATE_VL'
      },
    }, {
      name: 'groupe',
      title: 'Groupe',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.GROUPE'
      }
    }, {
      name: 'indicateur',
      title: 'Indicateur',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.INDICATEUR'
      }
    },
    {
      name: 'reference',
      title: 'Référence',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.REFERENCE'
      }
    },
    {
      name: 'valeur_reference',
      title: 'Valeur Référence',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.VALEUR_REFERENCE'
      }
    }, {
      name: 'minimum',
      title: 'Minimum',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.MINIMUM'
      }
    }, {
      name: 'valeur',
      title: 'valeur',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.VALEUR'
      }
    },
    {
      name: 'maximum',
      title: 'Maximum',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.MAXIMUM'
      }
    },
    {
      name: 'echelle',
      title: 'Echelle',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.RATIOS_AMMC_LIST.ECHELLE'
      }
    }

  ],
}
