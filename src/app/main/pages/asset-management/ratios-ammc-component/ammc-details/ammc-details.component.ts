import {Component, Input, OnInit} from '@angular/core';
import {DataTableSettingsModel} from '../../../../../shared/models';
import {AMMC_DETAILS_LIST_SETTINGS} from './ammc-details-list-settings';
import {RatiosAMMCModel} from '../../../../models/asset/ratios-ammc.model';
import {RatiosAMMCFilterModel} from '../../../../models';
import {RatiosAmmcService} from '../../../../services/asset/ratiosAmmc.service';

@Component({
  selector: 'app-ammc-details',
  templateUrl: './ammc-details.component.html',
  styleUrls: ['./ammc-details.component.scss']
})
export class AmmcDetailsComponent implements OnInit {
  private filterModel: RatiosAMMCFilterModel;
  @Input()
  set filter(value: { filterBody: RatiosAMMCFilterModel }) {
    if (!value) return;
    this.filterModel = value.filterBody;
    if (this.filterModel) {
      this.loadListRatiosAmmcDetails(this.filterModel);
    }
  }
  settings: DataTableSettingsModel = AMMC_DETAILS_LIST_SETTINGS;
  ratiosAMMMCDetails: RatiosAMMCModel[] = new Array<RatiosAMMCModel>();

  constructor(private ratiosAmmcService : RatiosAmmcService) { }

  ngOnInit(): void {
  }

  private loadListRatiosAmmcDetails(filterModel: RatiosAMMCFilterModel) {
     if(!filterModel) return ;
     this.ratiosAmmcService.getRatiosAmmcDetails(filterModel).subscribe(
        (value :any) => { this.ratiosAMMMCDetails = value ; }
      )
  }
}
