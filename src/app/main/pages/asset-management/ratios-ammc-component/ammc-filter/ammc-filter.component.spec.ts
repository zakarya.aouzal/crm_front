import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmmcFilterComponent } from './ammc-filter.component';

describe('AmmcFilterComponent', () => {
  let component: AmmcFilterComponent;
  let fixture: ComponentFixture<AmmcFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmmcFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmmcFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
