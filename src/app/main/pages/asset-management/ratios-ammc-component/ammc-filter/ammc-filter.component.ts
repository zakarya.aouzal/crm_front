import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import { DateVLItemModel, DateVLModel, GenericItemModel, IndicateursFilterModel, RatiosAMMCFilterModel} from '../../../../models';
import {OpcvmService, ReferenceService} from '../../../../services';
import {CarnetOrdreFilterModel} from '../../../../models/carnet-ordre/carnet-ordre.model';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import {ListItem} from 'ng-multiselect-dropdown/multiselect.model';
import {CommonUtil} from '../../../../../shared/utils/common.util';

@Component({
  selector: 'app-ammc-filter',
  templateUrl: './ammc-filter.component.html',
  styleUrls: ['./ammc-filter.component.scss']
})
export class AmmcFilterComponent implements OnInit {
  @Output()
  submited = new EventEmitter<{ filterBody: RatiosAMMCFilterModel }>();
  formGroup: FormGroup = null;
  clientList: GenericItemModel[] = [];
  fondsList: GenericItemModel[] = [];
  indicateursList: GenericItemModel[] = [];
  dateDebutList: DateVLModel[] = [];
  dateFinList: DateVLModel[] = [];
  currentFond: string;
  currentDateDebut: string;
  currentDateFin: string;
  dropdownSettings: IDropdownSettings;
  itemsIndicateurs : string[] = [];
  formIndicateurs : string[] = [];
  showValidationsMsg: boolean = false;
  constructor(private fb: FormBuilder, private translate: TranslateService, private refercenceService: ReferenceService, private opcvmService: OpcvmService) {
  }

  ngOnInit(): void {
    this.createFormGroup();
    this.loadClientList();
    this.dropdownSettings  = {
      singleSelection: false,
      idField: 'code',
      textField: 'value',
      selectAllText: 'Selectionner tout',
      unSelectAllText: 'Deselectionner tout ',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      noDataAvailablePlaceholderText : "Selectionner un fond , date de début , date de fin !",
      maxHeight : 300
    };
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      client: ["", [Validators.required]],
      fond: ["", [Validators.required]],
      dateDebut: ["", [Validators.required]],
      dateFin: ["", [Validators.required]],
      indicateur: ["", [Validators.required]],

    });
  }

  private loadClientList() {
    this.refercenceService.getClientsList()
      .subscribe(response => {
        this.clientList = response;
      });
  }

  private loadFondsList(client?:string) {
    this.refercenceService.getListfonds(client).subscribe(
      (value) => {
        this.fondsList = value;
      }
    )
  }

  private loadDateVLList(fond:string) {
    this.opcvmService.getListDatesVL(fond).subscribe(
      (value: any) => {
        this.dateDebutList = value;
        this.dateFinList = value;
      }
    )
  }
  private loadIndicateursList(fond: string, dateDebut: string, dateFin: string) {
    this.refercenceService.getIndicateursList(fond, dateDebut, dateFin).subscribe(
      (value: any) => {
        this.indicateursList = value;
      }
    )
  }


  initFormGroup() {
    this.formGroup.reset();
    this.currentFond = null ;
    this.currentDateDebut = null ;
    this.currentDateFin = null ;
  }

  OnSelectClient(client) {
    if(client){
      this.loadFondsList(client);
    }
  }
  onselectFond(fond: string) {
    this.currentFond = fond;
    if(fond){
      this.loadDateVLList(fond);
    }
    if (this.currentFond != null && this.currentDateDebut != null && this.currentDateFin != null)
      this.loadIndicateursList(this.currentFond, this.currentDateDebut, this.currentDateFin);
  }

  onselectDateDebut(dateDebut: string) {
    this.currentDateDebut = dateDebut;
    if (this.currentFond != null && this.currentDateDebut != null && this.currentDateFin != null)
      this.loadIndicateursList(this.currentFond, this.currentDateDebut, this.currentDateFin);
  }

  onselectDateFin(dateFin: string) {
    this.currentDateFin = dateFin;
    if (this.currentFond != null && this.currentDateDebut != null && this.currentDateFin != null)
      this.loadIndicateursList(this.currentFond, this.currentDateDebut, this.currentDateFin);
  }

  OnRemoveClient() {
    this.initFondsList();
    this.initDatesList();
  }
  OnRemoveFond() {
    this.initDatesList();
  }

  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    if(this.itemsIndicateurs.length>0)
      this.itemsIndicateurs.length = 0 ;
    if (this.formGroup.valid) {
      const filter = this.generatePayload();

      if (filter) {
        this.submited.emit({filterBody: filter});

      }
    }

  }

  private generatePayload() {
    let filter: RatiosAMMCFilterModel = null;
    const indicateurs = this.formGroup.get('indicateur').value ;
    for (let i = 0; i < indicateurs.length ; i++) {
      this.itemsIndicateurs.push(indicateurs[i].code);
    }
    if (this.formGroup) {
      filter = {
        ...new RatiosAMMCFilterModel(),
        //client : this.formGroup.get('client').value , on specifie pas le client dans la requete
        fond: this.formGroup.get('fond').value,
        dateDebut: this.formGroup.get('dateDebut').value,
        dateFin: this.formGroup.get('dateFin').value,
        indicateur:  this.itemsIndicateurs
      };

    }
    return filter;
  }


  onDisabledDate(dateFin: string) : boolean{
    return CommonUtil.getDate(dateFin) < CommonUtil.getDate(this.currentDateDebut);
  }
  private initFondsList() {
    this.fondsList = [];
  }
  private initDatesList(){
    this.dateDebutList = [] ;
    this.dateFinList = [] ;
  }


}
