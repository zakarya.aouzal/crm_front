import { Component, Input, OnInit } from '@angular/core';
import { OpcvmService } from 'src/app/main/services';
import {RatiosAmmcService} from '../../../../services/asset/ratiosAmmc.service';
import {OpcvmFilterModel, RatiosAMMCFilterModel} from '../../../../models';

@Component({
  selector: 'app-ammc-graphes',
  templateUrl: './ammc-graphes.component.html',
  styleUrls: ['./ammc-graphes.component.scss']
})
export class AmmcGraphesComponent {

  payload: RatiosAMMCFilterModel  = null;
  listOptions: any = []
  constructor(private serviceRatiosAmmc : RatiosAmmcService) {}
  @Input()
  set filter(value: { filterBody: RatiosAMMCFilterModel}) {
    if (!!value) {
      this.payload = value.filterBody ;
      this.serviceRatiosAmmc.getDataValMinMax(this.payload).subscribe(
        (response :any) => { this.listOptions = response ;  }
      );
    }
  }


}
