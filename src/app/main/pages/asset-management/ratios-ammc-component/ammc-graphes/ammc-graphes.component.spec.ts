import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmmcGraphesComponent } from './ammc-graphes.component';

describe('AmmcGraphesComponent', () => {
  let component: AmmcGraphesComponent;
  let fixture: ComponentFixture<AmmcGraphesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmmcGraphesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmmcGraphesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
