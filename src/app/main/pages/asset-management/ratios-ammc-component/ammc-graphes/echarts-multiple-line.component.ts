import {
  Component,
  OnDestroy,
  Input,
} from '@angular/core';
import { OpcvmService } from 'src/app/main/services';
import { CommonUtil } from 'src/app/shared/utils/common.util';


@Component({
  selector: 'ngx-echarts-multiple-line-ammc',
  template: `
    <div class="card">
      <div class="card-body ">
        <div echarts [options]="options" class="echart" style="height: 400px;"></div>
      </div>
    </div>
  `,
})
export class EchartsMultipleLineComponent  {

  options: any = {};
  chartTitle = "";
  isDepassement=  false;
  localitemOptions: any = null;

  constructor(private service: OpcvmService) {}

  @Input()
  set itemOptions(val: any) {
    if (!!val) {
      this.localitemOptions = val;
      this.chartTitle = val.description;
      this.isDepassement = Math.max(...val.data.depassements) > 0;
      this.updateLineChart(val);
    }
  }

  updateLineChart(options: any) {
      const min =  Math.min(...options.data.mins, ...options.data.valeurs);
      const max =  Math.max(...options.data.maxs, ...options.data.valeurs);
      const echarts: any = {
        bg: '#ffffff',
        textColor: '#484848',
        axisLineColor: '#bbbbbb',
        splitLineColor: '#ebeef2',
        itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
        tooltipBackgroundColor: '#6a7985',
        areaOpacity: '0.7',
      };
      this.options =this.getOptions({
        chartTitle: this.chartTitle,
        isDepassement: this.isDepassement,
        values: { min, max },
        options,
        echarts,
      });

  }


  getOptions(params: any): any {
    return {
        title: {
            top: 10,
            backgroundColor: !params.isDepassement ? '#b3f5d7' : '#ffcfd0',
            text: params.chartTitle,
            itemGap: 14,
            textStyle: {
                align: 'center',
                color: !params.isDepassement ? '#006949' : '#ff3c41',
                fontSize: 12,
                // fontWeight: 100
            },
            subtext: `Du ${params.options.data['dates'][0]} Au ${params.options.data['dates'][params.options.data['dates'].length - 1]}`,
            subtextStyle: {
                color: '#888',
                fontSize: 10
            }
        },
        backgroundColor: params.echarts.bg,
        color: ['#2B3A67', '#17a2b8', '#E88D67'],
        legend: {
            data: ['VL', 'Min', 'Max'],
            top: 0,
            right: 0,
            orient: 'vertical'
        },
        tooltip: {
            trigger: 'axis'
        },
        grid: {
            top: 100,
            bottom: 70,
            left: 50
        },
        dataZoom: [{
            type: 'inside',
            throttle: 50
        }],
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisTick: {
                    alignWithLabel: true,
                },
                axisLine: {
                    onZero: false,
                    lineStyle: {
                        color: '#17a2b8',
                    },
                },
                axisLabel: {
                    rotate: 70,
                    textStyle: {
                        fontSize: 10,
                        color: params.echarts.textColor,
                    },
                },
                axisPointer: {
                    label: {
                        formatter: params => {
                            return (
                                'VL au  ' + params.value
                            );
                        },
                    },
                },
                data: params.options.data['dates'],
            }
        ],
        yAxis: [{
            min: params.values.min,
            max: params.values.max,
            type: 'value',
            axisLine: {
                lineStyle: {
                    color: params.echarts.axisLineColor,
                },
            },
            splitLine: {
                lineStyle: {
                    color: params.echarts.splitLineColor,
                },
            },
            axisLabel: {
                textStyle: {
                    color: params.echarts.textColor
                },
                formatter: v => CommonUtil._n(v).toUpperCase()
            }
        }],
        series: [
            {
                name: 'Max',
                type: 'line',
                smooth: true,
                data: params.options.data['maxs'],
            }, {
                name: 'VL',
                type: 'line',
                smooth: true,
                data: params.options.data['valeurs']
            }, {
                name: 'Min',
                type: 'line',
                smooth: true,
                data: params.options.data['mins'],
            },
        ],
    };
}
}
