import { Component, OnInit } from '@angular/core';
import {CarnetOrdreFilterModel} from '../../../models/carnet-ordre/carnet-ordre.model';
import {RatiosAMMCFilterModel} from '../../../models';

@Component({
  selector: 'app-ratios-ammc-component',
  templateUrl: './ammc.component.html',
  styleUrls: ['./ammc.component.scss']
})
export class AmmcComponent implements OnInit {

  filter: { filterBody: RatiosAMMCFilterModel} = null;
  constructor() { }

  ngOnInit(): void {
  }

}
