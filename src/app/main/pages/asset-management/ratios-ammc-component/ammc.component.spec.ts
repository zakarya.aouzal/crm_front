import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmmcComponent } from './ammc.component';

describe('AmmcComponent', () => {
  let component: AmmcComponent;
  let fixture: ComponentFixture<AmmcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmmcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmmcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
