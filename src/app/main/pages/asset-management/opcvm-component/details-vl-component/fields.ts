export const FIELDS: any = {
    left: [
        {
            name: 'actifBrut',
            title: 'Actif brut'
        }, {
            name: 'actifNet',
            title: 'Actif net'
        }, {
            name: 'nbrParts',
            title: 'Nombre de parts'
        }, {
            name: 'vlcalcule',
            title: 'VL calculée	'
        }, {
            name: 'deviseRef',
            title: 'Devise de référence'
        }
    ],
    right: [
        {
            name: 'espActif',
            title: 'Espèces actif'
        }, {
            name: 'valoActif',
            title: 'Valorisation actif'
        }, {
            name: 'opEnCoursVal',
            title: 'Opérations en cours'
        }, {
            name: 'baseCalculFrais',
            title: 'Base de calcul des frais'
        }, {
            name: 'fraisGestion',
            title: 'Frais de gestion'
        }
    ]
}