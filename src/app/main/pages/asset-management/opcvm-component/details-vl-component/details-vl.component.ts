import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FIELDS } from './fields';
import {CommonUtil} from 'src/app/shared/utils/common.util';
import { OpcvmService } from 'src/app/main/services';
import { OpcvmFilterModel } from 'src/app/main/models';

@Component({
  selector: 'ngx-details-vl-opcvm',
  templateUrl: './details-vl.component.html',
  styleUrls: ['./details-vl.component.scss']
})
export class DetailsVLComponent implements OnInit,OnChanges {

   fields: any = FIELDS;
   date: string;
   data: any = {
  }
  constructor(private service: OpcvmService) { }


  ngOnChanges(changes: SimpleChanges): void {
    console.log("changes",changes)
  }

  ngOnInit() {}

  @Input()
  set payload(val: OpcvmFilterModel) {
    console.log ("payload",val)
    if (!!val) {
      this.service.getVLDetails(val).subscribe(data => this.data = data);
      this.date = val.date_vl;
    }
  }

  @Input() set resetCounter(val: any) {}

  emptyData(): Boolean {
    let empty = true;
    Object.keys(this.data).forEach(key => {
      if (!!this.data[key]) {
        empty = false;
        return;
      }
    });
    return empty;
  }

  displayEntry(val: any): any {
    return !isNaN(val) ? CommonUtil.format(val) : val;
  }

}