import { Component, Input, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { DataPortfValoriseModel, OpcvmFilterModel } from 'src/app/main/models';
import { OpcvmService } from 'src/app/main/services';
import { DataTableSettingsModel } from 'src/app/shared/models';
import {OPE_EN_COURS_SETTINGS} from './data.enCours.settings'
@Component({
  selector: 'app-operations-en-cours-component',
  templateUrl: './operations-en-cours.component.html',
  styleUrls: ['./operations-en-cours.component.scss']
})
export class OperationsEnCoursComponent implements OnInit {

  settings: DataTableSettingsModel = OPE_EN_COURS_SETTINGS;

  _filter: OpcvmFilterModel = null;

  loading: boolean = false;

  data:any[]=[];
  



 

  @Input()
  set filter(value: { filterBody: OpcvmFilterModel}) {

    

    if (!value) return;
    this._filter = value.filterBody;
    if (this._filter) {
    
          this.loadOperations(this._filter)
         
      }
    }
  



  constructor(private opcvmService: OpcvmService) { }

  ngOnInit(): void {
  }

  /** LOAD DATA */
  loadOperations(filter: OpcvmFilterModel) {
    this.data = [];
    if (!filter) return;
    this.loading = true;
    this.opcvmService.getListOperations(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.data = response;
      });
  }



}
