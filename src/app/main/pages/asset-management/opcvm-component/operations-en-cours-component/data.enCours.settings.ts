import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const OPE_EN_COURS_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'descPortef',
            title: 'Portefeuille',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.PORTF'
            }
        }, {
            name: 'depositaire',
            title: 'Depositaire',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.DEPOSITAIRE'
            }
        }, {
            name:  'contrepartie',
            title: 'Contrepartie',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.CONTREPARTIE'
            }
        }, {
            name: 'depositaire',
            title: 'Dépositaire',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.DEPOSITAIRE'
            }
        },{
            name: 'poste',
            title: 'Poste',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.POSTE',
                
            }
        }, {
            name: 'typeActif',
            title: 'Type actif',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.TYPE'
            }
        }, {
            name: 'statut',
            title: 'statut',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.STATUT'
            }
        }, {
            name: 'dateOp',
            title: 'Date opération',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.DATE_OPE'
            }
        },{
            name: 'dateDenou',
            title: 'Date dénouement',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.DATE_DEN'
            }
        },{
            name: 'codeISIN',
            title: 'Code isin',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.ISIN'
            }
        }
        ,{
            name: 'qt',
            title: 'Quantité',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.QUANTITE'
            }
        }
        ,{
            name: 'cours',
            title: 'Cours',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.COURS'
            }
        },
        ,{
            name: 'net',
            title: 'Montant',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.OPE_EN_COURS.NET'
            }
        }
    ], actions: []
};

