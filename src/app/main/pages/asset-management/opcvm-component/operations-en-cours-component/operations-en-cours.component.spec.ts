import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationsEnCoursComponent } from './operations-en-cours.component';

describe('OperationsEnCoursComponent', () => {
  let component: OperationsEnCoursComponent;
  let fixture: ComponentFixture<OperationsEnCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationsEnCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationsEnCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
