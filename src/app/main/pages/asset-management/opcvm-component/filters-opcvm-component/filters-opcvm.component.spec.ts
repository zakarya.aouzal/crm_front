import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersOpcvmComponent } from './filters-opcvm.component';

describe('FiltersOpcvmComponent', () => {
  let component: FiltersOpcvmComponent;
  let fixture: ComponentFixture<FiltersOpcvmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltersOpcvmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersOpcvmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
