import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericItemModel, DateVLItemModel, OpcvmFilterModel } from 'src/app/main/models';
import { OpcvmService, ReferenceService } from 'src/app/main/services';

@Component({
  selector: 'app-filters-opcvm-component',
  templateUrl: './filters-opcvm.component.html',
  styleUrls: ['./filters-opcvm.component.scss']
})
export class FiltersOpcvmComponent implements OnInit {




  @Output()
  submited = new EventEmitter<{ filterBody: OpcvmFilterModel }>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;
  selectedEntite: string;


// LISTS
  FondList: GenericItemModel[] = [];
  clientList: GenericItemModel[] = [];
  datesList: DateVLItemModel[] = [];



  constructor(private fb: FormBuilder, private referenceService: ReferenceService,private opcvmService:OpcvmService) {}

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadClients();

  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      client: [null, [Validators.required]],
      fond: [null, [Validators.required]],
      date_vl: [null, [Validators.required]],

    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
    }
  }

  /** SUBMIT EVENT */
  onSubmit(): void {
    if (this.formGroup.invalid)
           {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    if (filter) {
      this.submited.emit({filterBody: filter});
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): OpcvmFilterModel {
    let filter: OpcvmFilterModel = null;

    if (this.formGroup) {

      filter = {
        ...new OpcvmFilterModel(),
        client: this.formGroup.get('client').value || '*',
        fond: this.formGroup.get('fond').value || '*',
        date_vl: this.formGroup.get('date_vl').value || '*',
      };

    }
    return filter;
  }


    /** GET LIST OF DEPOSITAIRES */
    loadClients(): void {
      this.referenceService.getListclient()
        .subscribe(response => {
          this.clientList = response;
        });
    }

  /** GET LIST OF PORTEFEUILLE */
  loadFond(client?: string): void {
    this.referenceService.getListfonds(client)
      .subscribe(response => {
        this.FondList = response;
        // if (this.FondList.length === 1) {
        //   this.formGroup.get('fond').setValue(this.FondList[0].code);
        // }
      });
  }




  /** GET LIST OF DEPOSITAIRES */
  loadDateVL(fond?: string): void {
    this.opcvmService.getListDatesVL(fond)
      .subscribe(response => {
        this.datesList = response;
        // if (this.datesList.length === 1) {
        //   this.formGroup.get('date_vl').setValue(this.datesList[0].DATE_VL);
        // }
      });
  }



  /** ON CHANGE ENTITY */
OnchangeClient(item) {
  if(item){
    this.loadFond(item);
  }
}


  /** ON CHANGE PORTEFEUILLE */
  OnchangeFond(item) {
    if(item){
      this.loadDateVL(item);
    }
  }


/** RENITIIZE  */
Onclear(){
  this.formGroup.patchValue({client:"*"});
  this.formGroup.patchValue({fond:"*"});
  this.formGroup.patchValue({date_vl:"*"});
  this.FondList=[];
}

 /** RENITIIZE  */
 OnclearPortf(){
  this.formGroup.patchValue({fond:"*"});
  this.formGroup.patchValue({date_vl:"*"});
}


}
