import { Component, Input, OnInit } from '@angular/core';
import {OpcvmFilterModel} from 'src/app/main/models'
@Component({
  selector: 'app-graphes-component',
  templateUrl: './graphes.component.html',
  styleUrls: ['./graphes.component.scss']
})
export class GraphesComponent implements OnInit {


  ngOnInit(): void {
   // throw new Error('Method not implemented.');
  }

  payload:OpcvmFilterModel =null;
  pieCounter = 0;

  @Input()
  set filter(value: { filterBody: OpcvmFilterModel}) {
    if (!!value) {
      this.payload = value.filterBody;
    }

  }



}
