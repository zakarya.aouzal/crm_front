import { AfterViewInit, Component, OnDestroy, Input } from '@angular/core';
import { OpcvmFilterModel } from 'src/app/main/models';
//import { NbThemeService } from '@nebular/theme';
import { OpcvmService } from 'src/app/main/services';
import { CommonUtil } from 'src/app/shared/utils/common.util';

const echarts: any  = {
  bg: '#ffffff',
  textColor: '#484848',
  axisLineColor: '#bbbbbb',
  splitLineColor: '#ebeef2',
  itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
  tooltipBackgroundColor: '#6a7985',
  areaOpacity: '0.7',
};


@Component({
  selector: 'ngx-echarts-pie-opcvm',
  template: `
  <div class="border border-secondary rounded-top">
  <h6 class="alert alert-orange2">Répartition par catégorie d'actif</h6>

    <div echarts [options]="options" class="echart"></div>
    
    </div>
  `,
})
export class EchartsPieComponent implements AfterViewInit, OnDestroy {

   Piedata: any = {
    "BDT": {
      "montant": 975745857.79,
      "devise": "MAD"
    },
    "ORD": {
      "montant": 59611849.3,
      "devise": "MAD"
    },
    "CD": {
      "montant": 269696763.58,
      "devise": "MAD"
    },
    "BSF": {
      "montant": 201629900.16,
      "devise": "MAD"
    },
    "OBL": {
      "montant": 458551213.49,
      "devise": "MAD"
    },
    "AUTRES": {
      "montant": 39999700,
      "devise": "MAD"
    }
  };
  options: any = {
    backgroundColor: echarts.bg,
    color: [
      '#FFDCB6','#FF926B','#F3825A','#E3724A','#D3623A','#C96619','#F9B042','#F49F35','#F19325','#EB8A12'
    ],
    tooltip: {
      trigger: 'item',
      formatter: params => {
        return (
          params.seriesName + ' ' + params.name + ' : <br/> ' + CommonUtil.format(params.value) + ' ' + params.data.devise + ' (' + params.percent + '%)'
        );
    },
    },
  //   legend: {
  //     enabled: false
  // },
    series: [
      {
        name: 'Classe d\'actif',
        type: 'pie',
        radius: '80%',
        center: ['50%', '50%'],
        data: CommonUtil.getChartFormedDataDevise(this.Piedata, 'montant'),
        showInLegend: false,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: echarts.itemHoverShadowColor,
          },
        },
        label: {
          normal: {
            textStyle: {
              color: echarts.textColor,
            },
          },
        },
        labelLine: {
          normal: {
            lineStyle: {
              color: echarts.axisLineColor,
            },
          },
        },
      },
      
    ],
   
  };
  themeSubscription: any;
 
  constructor(private service: OpcvmService) {

    this.Piedata = 
    this.updatePieChart()
  }

  @Input()
  set payload(val:OpcvmFilterModel) {
    if (!!val) {


      console.log("hhhhhhhhhhhhhhhhhhhhhhhh")

      // this.service.getPieChartClasseActif(val).subscribe(
      //   data => {
        this.Piedata = {
          "BDT": {
            "montant": 975745857.79,
            "devise": "MAD"
          },
          "ORD": {
            "montant": 59611849.3,
            "devise": "MAD"
          },
          "CD": {
            "montant": 269696763.58,
            "devise": "MAD"
          },
          "BSF": {
            "montant": 201629900.16,
            "devise": "MAD"
          },
          "OBL": {
            "montant": 458551213.49,
            "devise": "MAD"
          },
          "AUTRES": {
            "montant": 39999700,
            "devise": "MAD"
          }
        };
        this.updatePieChart();
      //   },
      // );
    }
  }

  @Input()
  set ChartCounter(val: any) {
    this.updatePieChart();
  }

  updatePieChart() {
    // this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
     // const colors = config.variables;
      const echarts: any  = {
        bg: '#ffffff',
        textColor: '#484848',
        axisLineColor: '#bbbbbb',
        splitLineColor: '#ebeef2',
        itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
        tooltipBackgroundColor: '#6a7985',
        areaOpacity: '0.7',
      };
      this.options = {
        backgroundColor: echarts.bg,
        color: [
          '#FFDCB6','#FF926B','#F3825A','#E3724A','#D3623A','#C96619','#F9B042','#F49F35','#F19325','#EB8A12'
        ],
        tooltip: {
          trigger: 'item',
          formatter: params => {
            return (
              params.seriesName + ' ' + params.name + ' : <br/> ' + CommonUtil.format(params.value) + ' ' + params.data.devise + ' (' + params.percent + '%)'
            );
        },
        },
      //   legend: {
      //     enabled: false
      // },
        series: [
          {
            name: 'Classe d\'actif',
            type: 'pie',
            radius: '80%',
            center: ['50%', '50%'],
            data: CommonUtil.getChartFormedDataDevise(this.Piedata, 'montant'),
            showInLegend: false,
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },
          
        ],
       
      };
    };
    // );
  //}

  ngAfterViewInit() {

    
    
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
