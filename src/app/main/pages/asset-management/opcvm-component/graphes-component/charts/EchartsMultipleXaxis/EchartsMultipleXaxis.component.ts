import { Component, AfterViewInit, OnDestroy, Input, OnInit } from '@angular/core';
import { OpcvmFilterModel } from 'src/app/main/models';
//import { NbThemeService } from '@nebular/theme';
import { OpcvmService } from 'src/app/main/services';
import { CommonUtil } from 'src/app/shared/utils/common.util';


@Component({
  selector: 'ngx-echarts-multiple-xaxis-opcvm',
  templateUrl: './EchartsMultipleXaxis.component.html',
  styleUrls: ['./EchartsMultipleXaxis.component.scss']
})
export class EchartsMultipleXaxisComponent implements OnInit{

  options: any = {};
  themeSubscription: any;
  Axisdata : any = {};
   arrayFormed= [];
   min = 0;
   max = 0;

  constructor( private service: OpcvmService) {
  }
  ngOnInit(): void {
   }

  @Input()
  set payload(val:OpcvmFilterModel) {
    if (!!val) {
        this.service.getAxisChartEvolutionVL(val).subscribe(
        data => { this.Axisdata = data ;
           this.arrayFormed = CommonUtil.getChartFormedDataDevise(this.Axisdata, 'vl');
           this.min = CommonUtil.getMinMax(this.arrayFormed).min;
           this.max = CommonUtil.getMinMax(this.arrayFormed).max;
           this.updateAxisChart();
        });
    }
  }

  updateAxisChart() {
      //this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      //const colors: any = config.variables;
      const echarts: any  = {
        bg: '#ffffff',
        textColor: '#484848',
        axisLineColor: '#bbbbbb',
        splitLineColor: '#ebeef2',
        itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
        tooltipBackgroundColor: '#6a7985',
        success:"#4d8ea9",
        areaOpacity: '0.7',
      };

      this.options = {
        backgroundColor: echarts.bg,
        color: [echarts.success, '#4d8ea9'],
        tooltip: {
          trigger: 'axis',
          formatter: params => {
            return (
              params[0].seriesId + '  ' + params[0].name + ' : <br/>' + CommonUtil.format(params[0].value) + ' ' + params[0].data.devise
            );
        },
        },
        grid: {
          top: 70,
          bottom: 50,
          left : 70,
        },
        xAxis: [
          {
            type: 'category',
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              onZero: false,
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
            },
            axisPointer: {
              label: {
                formatter: params => {
                  return (
                    'VL au  ' + params.value
                  );
                },
              },
            },
            data: Object.keys(this.Axisdata),
          },
        ],
        yAxis: [
          {
            min: this.min - ((this.max - this.min) * 30 / 100),
            max: CommonUtil.getMinMax(this.arrayFormed).max,
            type: 'value',
            axisLine: {
              lineStyle: {
                color: echarts.axisLineColor,
              },
            },
            splitLine: {
              lineStyle: {
                color: echarts.splitLineColor,
              },
            },
            axisLabel: {
              textStyle: {
                color: echarts.textColor,
              },
               formatter: v => v.toFixed(2),
               // _n(v).toUpperCase()
            },
          },
        ],
        series: [
          {
            name: 'VL au ',
            type: 'line',
            smooth: true,
            data: this.arrayFormed,
          },
        ],
      };
    //});
  }

  ngAfterViewInit() {}

  // ngOnDestroy(): void {
  //   this.themeSubscription.unsubscribe();
  // }

}
