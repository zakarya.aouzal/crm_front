import { AfterViewInit, Component, OnDestroy, Input, OnInit } from '@angular/core';
import { OpcvmFilterModel } from 'src/app/main/models';
//import { NbThemeService } from '@nebular/theme';
import { OpcvmService } from 'src/app/main/services';
import { CommonUtil } from 'src/app/shared/utils/common.util';



@Component({
  selector: 'ngx-echarts-pie-opcvm',
  templateUrl: './EchartsPie.component.html',
  styleUrls: ['./EchartsPie.component.scss']
})
export class EchartsPieComponent implements OnInit {

  Piedata: any =  {}
  options: any = {}
  themeSubscription: any;

  constructor(private service: OpcvmService) {

  }

  ngOnInit(): void {

  }

  @Input()
  set payload(val:OpcvmFilterModel) {
    if (!!val) {
         this.service.getPieChartClasseActif(val).subscribe(
         data => { this.Piedata = data ;
         this.updatePieChart();
         });
    }
  }


  updatePieChart() {
    // this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
     // const colors = config.variables;
      const echarts: any  = {
        bg: '#ffffff',
        textColor: '#484848',
        axisLineColor: '#bbbbbb',
        splitLineColor: '#ebeef2',
        itemHoverShadowColor: 'rgba(0, 0, 0, 0.5)',
        tooltipBackgroundColor: '#6a7985',
        areaOpacity: '0.7',
      };
      this.options = {
        backgroundColor: echarts.bg,
        color: [
          '#64ddf0','#6cebfb','#1c4b6c','#4d8ea9','#048494','#3c5c85','#F9B042','#F49F35','#F19325','#EB8A12'
        ],
        tooltip: {
          trigger: 'item',
          formatter: params => {
            return (
              params.seriesName + ' ' + params.name + ' : <br/> ' + CommonUtil.format(params.value) + ' ' + params.data.devise + ' (' + params.percent + '%)'
            );
        },
        },
      //   legend: {
      //     enabled: false
      // },
        series: [
          {
            name: 'Classe d\'actif',
            type: 'pie',
            radius: '80%',
            center: ['50%', '50%'],
            //selectedMode: 'single',
            data: CommonUtil.getChartFormedDataDevise(this.Piedata, 'montant'),
            showInLegend: false,
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },

        ],

      };
    };
    // );
  //}

  ngAfterViewInit() {



  }

  // ngOnDestroy(): void {
  //   this.themeSubscription.unsubscribe();
  // }
}
