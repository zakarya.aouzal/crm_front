import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const PORTF_VL_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'classe',
            title: 'Classe',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.CLASSE'
            }
        }, {
            name: 'description_titre',
            title: 'Description titre',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.DESC'
            }
        }, {
            name:  'codeISIN',
            title: 'Code ISIN',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.ISIN'
            }
        }, {
            name: 'depositaire',
            title: 'Dépositaire',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.DEPOSITAIRE'
            }
        },{
            name: 'quantite',
            title: 'quantite',
            format: DataTableColumnsFormat.NUMBER,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.QUANTITE',
                format:{
                    minimumFractionDigits:3,
                    maximumFractionDigits:3

                }
            }
        }, {
            name: 'cours',
            title: 'Cours',
            format: DataTableColumnsFormat.NUMBER,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.COURS'
            }
        }, {
            name: 'valo',
            title: 'Valorisation',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.VALO'
            }
        }, {
            name: 'pmup',
            title: 'PMUP',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.PMUP'
            }
        },{
            name: 'cmp',
            title: 'PDR',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.PDR'
            }
        },{
            name: 'pmv_lat',
            title: 'PMV',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.PORTF_VL.PMV'
            }
        }
    ], actions: [DataTableActions.VIEW]
};

