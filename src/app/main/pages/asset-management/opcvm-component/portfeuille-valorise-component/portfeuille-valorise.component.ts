import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { DataPortfValoriseModel, OpcvmFilterModel } from 'src/app/main/models';
import { OpcvmService } from 'src/app/main/services';
import { DataTableSettingsModel } from 'src/app/shared/models';
import {PORTF_VL_SETTINGS} from './data.Valo.settings'
@Component({
  selector: 'app-portfeuille-valorise-component',
  templateUrl: './portfeuille-valorise.component.html',
  styleUrls: ['./portfeuille-valorise.component.scss']
})
export class PortfeuilleValoriseComponent implements OnInit {


  settings: DataTableSettingsModel = PORTF_VL_SETTINGS;

  _filter: OpcvmFilterModel = null;

  loading: boolean = false;

  data:DataPortfValoriseModel[]=[];
  



 

  @Input()
  set filter(value: { filterBody: OpcvmFilterModel}) {

    console.log("PVLO",value)

    if (!value) return;
    this._filter = value.filterBody;
    if (this._filter) {
    
          this.loadData(this._filter)
         
      }
    }
  



  constructor(private opcvmService: OpcvmService, private router: Router) { }

  ngOnInit(): void {
  }

  /** LOAD DATA */
  loadData(filter: OpcvmFilterModel) {
    this.data = [];
    if (!filter) return;
    this.loading = true;
    this.opcvmService.getListDataPortfVal(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.data = response;
      });
  }


}
