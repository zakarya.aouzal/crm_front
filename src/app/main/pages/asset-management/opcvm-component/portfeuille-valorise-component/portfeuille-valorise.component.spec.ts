import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfeuilleValoriseComponent } from './portfeuille-valorise.component';

describe('PortfeuilleValoriseComponent', () => {
  let component: PortfeuilleValoriseComponent;
  let fixture: ComponentFixture<PortfeuilleValoriseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfeuilleValoriseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfeuilleValoriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
