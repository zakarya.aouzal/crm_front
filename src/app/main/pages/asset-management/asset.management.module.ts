import {NgModule} from '@angular/core'
import { AssetManagementRoutingModule } from './asset.management.routing';
import{AmmcComponent} from './ratios-ammc-component/ammc.component'
import {DetailsFondsComponent} from './details-fonds-component/details-fonds.component'
import {OpcvmComponent} from './opcvm-component/opcvm.component';
import { GraphesComponent } from './opcvm-component/graphes-component/graphes.component';
import { DetailsVLComponent } from './opcvm-component/details-vl-component/details-vl.component';
import { FiltersOpcvmComponent } from './opcvm-component/filters-opcvm-component/filters-opcvm.component';
import { OperationsEnCoursComponent } from './opcvm-component/operations-en-cours-component/operations-en-cours.component';
import { PortfeuilleValoriseComponent } from './opcvm-component/portfeuille-valorise-component/portfeuille-valorise.component'
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OpcvmService } from '../../services';
import {EchartsMultipleXaxisComponent} from './opcvm-component/graphes-component/charts/EchartsMultipleXaxis/EchartsMultipleXaxis.component'
import {EchartsPieComponent} from './opcvm-component/graphes-component/charts/EchartsPie/EchartsPie.component'
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts'

import { ListDetailsFondsComponent } from './details-fonds-component/list-details-fonds-component/list-details-fonds-component.component';
import { FiltersDetailsFondsComponent } from './details-fonds-component/filters-details-fonds-component/filters-details-fonds-component.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AmmcFilterComponent } from './ratios-ammc-component/ammc-filter/ammc-filter.component';
import { AmmcDetailsComponent } from './ratios-ammc-component/ammc-details/ammc-details.component';
import { AmmcGraphesComponent } from './ratios-ammc-component/ammc-graphes/ammc-graphes.component';
import {EchartsMultipleLineComponent} from './ratios-ammc-component/ammc-graphes/echarts-multiple-line.component'
import {RatiosAmmcService} from '../../services/asset/ratiosAmmc.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

@NgModule({
    declarations:[
        AmmcComponent,
        DetailsFondsComponent,
        OpcvmComponent,
        GraphesComponent,
        DetailsVLComponent,
        FiltersOpcvmComponent,
        OperationsEnCoursComponent,
        PortfeuilleValoriseComponent,
        EchartsMultipleXaxisComponent,
        EchartsPieComponent,
        ListDetailsFondsComponent,
        FiltersDetailsFondsComponent,
        AmmcFilterComponent,
        AmmcDetailsComponent,
        AmmcGraphesComponent,
        EchartsMultipleLineComponent,
    ],
    imports:[
        AssetManagementRoutingModule,
        CommonModule,
        NgxEchartsModule.forRoot({
            echarts,
          }),
        SharedModule,
        ReactiveFormsModule,
        TranslateModule,
        FormsModule,
        NgxSelectModule,
        NgMultiSelectDropDownModule.forRoot()
    ],
    exports:[],
    providers:[
        OpcvmService , RatiosAmmcService
    ]
})
export class AssetManagementModule{

}
