import { Component, Input, OnInit } from '@angular/core';
import { DetailFondsFilterModel, OpcvmFilterModel } from 'src/app/main/models';

@Component({
  selector: 'app-details-fonds-component',
  templateUrl: './details-fonds.component.html',
  styleUrls: ['./details-fonds.component.scss']
})
export class DetailsFondsComponent implements OnInit {


  filter: { filterBody: DetailFondsFilterModel} = null;

  constructor() { }

  

  ngOnInit(): void {
  }

}
