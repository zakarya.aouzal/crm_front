import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {  FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateVLItemModel, GenericItemModel} from 'src/app/main/models';
import { DetailFondsService, OpcvmService } from 'src/app/main/services';
import {DetailFondsFilterModel, DetailFondsModel} from '../../../../models/asset/detail-fonds.model';
import { ReferenceService} from '../../../../services';

@Component({
  selector: 'app-filters-details-fonds-component',
  templateUrl: './filters-details-fonds-component.component.html',
  styleUrls: ['./filters-details-fonds-component.component.scss']
})
export class FiltersDetailsFondsComponent implements OnInit {

  
  @Output()
  submited = new EventEmitter<{ filterBody: DetailFondsFilterModel }>();

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;
  selectedEntite: string;

// LISTS
  FondList: GenericItemModel[] = [];
  clientList: GenericItemModel[] = [];
  datesList: DateVLItemModel[] = [];
  detailFondsList: DetailFondsModel[]=new Array<DetailFondsModel>();
  detailFondsFilter: DetailFondsFilterModel=new DetailFondsFilterModel();



  constructor( private fb: FormBuilder,  private referenceService: ReferenceService,private detailFondsService:DetailFondsService, private translate: TranslateService) {}

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.loadClientList();
    this.loadFondList();
    this.loadDatesList();

  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      client: [null, [Validators.required]],
      fond: [null, [Validators.required]],
      date_vl: [null, [Validators.required]],

    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    if (this.formGroup.valid) {
      const filter = this.generatePayload();
      if (filter) {
        this.submited.emit({filterBody: filter});
        console.log('filter ');
      }
      /*
      console.log(this.formGroup.get('fundsType').value);
      console.log(this.formGroup.get('fund').value);
      console.log(this.formGroup.get('sens').value);
      console.log(this.formGroup.get('clientType').value);
      console.log(this.formGroup.get('client').value);
  */
    }
  }


  loadFondList(client?: string) {
    this.referenceService.getListfonds(client).subscribe(
      (value: any) => {
        this.FondList = value;
      }
    );
  }

  loadClientList() {
    this.referenceService.getListclient().subscribe(
      (value: any) => {
        this.clientList = value;
      }
    );
  }

  loadDatesList(fond?: string) : void {
    this.detailFondsService.getListDatesVL(fond)
      .subscribe(response => {
        this.datesList = response;
        // if (this.datesList.length === 1) {
        //   this.formGroup.get('date_vl').setValue(this.datesList[0].DATE_VL);
        // }
      });
  }
  private generatePayload(): DetailFondsFilterModel{
    let filter: DetailFondsFilterModel = null;
    if (this.formGroup) {
      filter = {
        ...new DetailFondsFilterModel(),
        fond: this.formGroup.get('fond').value || '',
        client: this.formGroup.get('client').value || '',
        date_vl: this.formGroup.get('date_vl').value || ''
      };

    }
    return filter;
  }

  
  /** ON CHANGE ENTITY */
OnchangeClient(item) {
  if(item){
    this.loadFondList(item);
  }
}


  /** ON CHANGE PORTEFEUILLE */
  OnchangeFond(item) {
    if(item){
      this.loadDatesList(item);
    }
  }


/** RENITIIZE  */
Onclear(){
  this.formGroup.patchValue({client:"*"});
  this.formGroup.patchValue({fond:"*"});
  this.formGroup.patchValue({date_vl:"*"});
  this.FondList=[];
}

 /** RENITIIZE  */
 OnclearPortf(){
  this.formGroup.patchValue({fond:"*"});
  this.formGroup.patchValue({date_vl:"*"});
}



}
