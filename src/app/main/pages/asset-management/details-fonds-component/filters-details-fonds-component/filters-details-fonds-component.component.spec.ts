import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltersDetailsFondsComponentComponent } from './filters-details-fonds-component.component';

describe('FiltersDetailsFondsComponentComponent', () => {
  let component: FiltersDetailsFondsComponentComponent;
  let fixture: ComponentFixture<FiltersDetailsFondsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltersDetailsFondsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltersDetailsFondsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
