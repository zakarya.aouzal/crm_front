import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsFondsComponent } from './details-fonds.component';

describe('DetailsFondsComponent', () => {
  let component: DetailsFondsComponent;
  let fixture: ComponentFixture<DetailsFondsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsFondsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsFondsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
