import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDetailsFondsComponentComponent } from './list-details-fonds-component.component';

describe('ListDetailsFondsComponentComponent', () => {
  let component: ListDetailsFondsComponentComponent;
  let fixture: ComponentFixture<ListDetailsFondsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDetailsFondsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDetailsFondsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
