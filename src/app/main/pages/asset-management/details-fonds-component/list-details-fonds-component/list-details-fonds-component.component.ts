import { Component, Input, OnInit } from '@angular/core';
import { DetailFondsFilterModel, DetailFondsModel } from 'src/app/main/models';
import { DetailFondsService } from 'src/app/main/services';
import {DETAILS_FONDS_LIST_SETTINGS} from './list-details-fonds-settings'
import { DataTableSettingsModel } from 'src/app/shared/models';

@Component({
  selector: 'app-list-details-fonds-component',
  templateUrl: './list-details-fonds-component.component.html',
  styleUrls: ['./list-details-fonds-component.component.scss']
})
export class ListDetailsFondsComponent implements OnInit {

  @Input()
  set filter(value: { filterBody: DetailFondsFilterModel }) {
    console.log('filter rec: ' ,value);
    if (!value) return;
    this.filterModel = value.filterBody;
    if (this.filterModel) {
      this.loadListDetailFonds(this.filterModel);
    }
  } 
 

  filterModel: DetailFondsFilterModel = null ;
  settings: DataTableSettingsModel = DETAILS_FONDS_LIST_SETTINGS;
  detailFondsList: DetailFondsModel[] = new Array<DetailFondsModel>();

  constructor(private detailFondsService: DetailFondsService) { }

  ngOnInit(): void {
  }

  loadListDetailFonds(filter: DetailFondsFilterModel) { 
    if(!filter) return;
    this.detailFondsService.getListDetailFonds(filter).subscribe(
      (value: any) => { this.detailFondsList = value ; }
      
    );
  }

  

}
enum Language { 
  Csharp, Typescript, Javascript 
}
let choosedLanguage: Language = Language.Typescript; 
console.log('la valeur b :',choosedLanguage);

let stringValue: string = 'yes';  
let booleanValue: boolean = !!stringValue; 

