import {DataTableColumnsFormat, DataTableSettingsModel} from '../../../../../shared/models';

export const DETAILS_FONDS_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'classe',
      title: 'Classe',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.DETAILS_FONDS_LIST.CLASSE'
      }
    }, {
      name: 'descriptionTitre',
      title: 'Description titre',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.DESCRIPTION'
      }
    }, {
      name: 'codeIsin',
      title: 'Code isin',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.CODE_ISIN'
      }
    },
    {
      name: 'depositaire',
      title: 'Depositaire',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.DEPOSITAIRE'
      }
    },
    {
      name: 'depot',
      title: 'Depot',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.QTE_DEPOT'
      }
    }, {
      name: 'actif',
      title: 'Actif',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.QTE_ACTIF'
      }
    }, {
      name: 'repo',
      title: 'Repo',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.QTE_REPO'
      }
    },
    {
      name: 'pret',
      title: 'Pret',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.QTE_PRET'
      }
    },
    {
      name: 'emprunt',
      title: 'Emprunt',
      format: DataTableColumnsFormat.NUMBER,
      data: {
        i18nValue:'COLUMNS.DETAILS_FONDS_LIST.QTE_EMPRUNT'
      }
    },
   

  ],
}
