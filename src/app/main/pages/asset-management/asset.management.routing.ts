import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import {RouterModule,Routes} from "@angular/router"
import { PageNotFoundComponent } from '../../../shared/components/page-not-found/page-not-found.component';
import { AmmcComponent } from "./ratios-ammc-component/ammc.component";
import { DetailsFondsComponent } from "./details-fonds-component/details-fonds.component";
import { OpcvmComponent } from "./opcvm-component/opcvm.component";



const routes: Routes = [
    {
      path: '',
      data: {
        breadcrumb: {
          label: 'BREADCRUMBS.CLIENT_MANAGEMENT.TITLE'
        }
      },
      children: [
        {
          path: 'opcvm',
          component: OpcvmComponent,
          data: {
            breadcrumb: {
              label: 'BREADCRUMBS.ASSET.OPCVM'
            }
          },
        }, {
          path: 'details-fond',
          component: DetailsFondsComponent,
          data: {
            breadcrumb: {
              label: 'BREADCRUMBS.ASSET.DETAILS_FONDS'
            }
          },
        },
        {
            path: 'ammc',
            component: AmmcComponent,
            data: {
              breadcrumb: {
                label: 'BREADCRUMBS.ASSET.AMMC'
              }
            },
          },

        {
          path: '',
          redirectTo: 'opcvm',
          pathMatch: 'full'
        }, {
          path: '**',
          component: PageNotFoundComponent
        }
      ]
    }
  ];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    declarations:[],
    exports:[RouterModule]
})
export class AssetManagementRoutingModule{

}
