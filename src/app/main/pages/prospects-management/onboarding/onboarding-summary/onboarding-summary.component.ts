import {Component, Inject, Input, OnDestroy, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {
  AccountPMModel,
  AccountPMPayloadModel,
  AccountPPModel,
  AccountPPPayloadModel,
  AccountType, CompteContactPayload, ContactModel,
  GenericItemModel,
  RegisterAccountPayloadModel
} from 'src/app/main/models';
import {AccountService, GedService, ReferenceService} from 'src/app/main/services';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EditContactComponent} from "../../../contacts-management/contact/edit-contact/edit-contact.component";
import {AddContactComponent} from "../../../contacts-management/contact/add-contact/add-contact.component";


@Component({
  selector: 'app-onboarding-summary',
  templateUrl: './onboarding-summary.component.html',
  styleUrls: ['./onboarding-summary.component.scss']
})
export class OnboardingSummaryComponent implements OnInit {

  @Input()
  set onboardingPayload(value: RegisterAccountPayloadModel) {
    if (value) {
      this.accountType = value.accountType;
      this.selectedContacts = value.selectedContacts;
      this.contactAccounts = value.contactAccounts;
      switch (value.accountType) {
        case AccountType.PP:
          this.accountPP = value.accountPP;
          this.createAccountPPFormGroup();
          break;
        case AccountType.PM:
          this.accountPM = value.accountPM;
          this.createaccountPMFormGroup();
      }
      this.initFormGroup();
    }
  }

  // Form group
  accountPPFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;
  // account
  accountPP: AccountPPPayloadModel = null;
  accountPM: AccountPMPayloadModel = null;
  accountType: AccountType = AccountType.PP;
  // contacts
  selectedContacts: ContactModel[] = [];
  contactAccounts: CompteContactPayload[] = [];

  constructor(private fb: FormBuilder,
              private dialog: MatDialog) {  }

  ngOnInit() {
  }

  createAccountPPFormGroup() {
    this.accountPPFormGroup = this.fb.group({
      // general infos
      civilite: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      pieceIdentite: [null, [Validators.required]],
      numPi: [null, [Validators.required]],
      secteur: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      dateExpPI: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      address: [null, [Validators.required]],
      nationality: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      email: [null, [Validators.required]],
      dateOfBirth: [null],
      fatherFullName: [null],
      motherFullName: [null],
      residenceCountry: [null],
      country: [null],
      city: [null],
      address2: [null],
      mobile2: [null],

      // activities
      capaciteFinanciere: [null],
      origineFonds: [null],
      niveauInv: [null],
      typeProduit:[null],
      produitSouhaites: [null],
      horizonPlacement: [null],
      secteurSouhaites: [null],
      objectifInvestissement: [null],
      profilInvestissement: [null],
      nationaliteUSA: [null],
      compteBancaireUSA: [null],
      adresseUSA: [null],
      greenCardUSA: [null],
      idFiscalUSA: [null],
      ligneTelephoneUSA: [null],

      // contacts

    });
    this.accountPPFormGroup.disable();
  }

  createaccountPMFormGroup() {
    this.accountPMFormGroup = this.fb.group({
      // general infos
      socialReason: [null, [Validators.required]],
      legalForm: [null, [Validators.required]],
      ice: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      fixe: [null, [Validators.required]],
      email: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      webSite: [null, [Validators.required]],
      gestionnaire: [null],

      // activities
      categorie: [null],
      secActivite: [null],
      capaciteFinanciere: [null],
      produitSouhaites: [null],
      niveauInv: [null],
      typeProduit:[null],
      secteurSouhaites: [null],
      horizonPlacement: [null],
      profilInvestissement: [null],
      objectifInvestissement: [null],
      paysResidenceFiscale: [null],
      compteBancaireUSA: [null],
      adresseUSA: [null],
      villeFiscaleUSA: [null],
      idFiscalUSA: [null],
      ligneTelephoneUSA: [null],
      giin: [null]
    });
    this.accountPMFormGroup.disable();
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.accountPPFormGroup.patchValue({
            civilite: this.accountPP.civilite,
            codePostale: this.accountPP.codePostale,
            pays: this.accountPP.paysResidence,
            pieceIdentite: this.accountPP.pieceIdentite,
            numPi: this.accountPP.numPI,
            secteur: this.accountPP.secActivite,
            lastName: this.accountPP.prenom,
            firstName: this.accountPP.nom,
            dateExpPI: this.accountPP.dateExpPI,
            profession: this.accountPP.profession,
            address: this.accountPP.adresse1,
            nationality: this.accountPP.nationalite,
            mobile: this.accountPP.mobile1,
            email: this.accountPP.email,
            dateOfBirth: this.accountPP.dateNaissance,
            fatherFullName: this.accountPP.nomPrenomPere,
            motherFullName: this.accountPP.nomPrenomMere,
            residenceCountry: this.accountPP.paysResidence,
            country: this.accountPP.paysResidence,
            city: this.accountPP.ville,
            address2: this.accountPP.adresse2,
            mobile2: this.accountPP.mobile2,

            capaciteFinanciere: this.accountPP.capaciteFinanciere,
            origineFonds: this.accountPP.origineFonds,
            niveauInv: this.accountPP.niveauInv,
            typeProduit:this.accountPP.typeProduit,
            produitSouhaites: this.accountPP.produitSouhaites,
            horizonPlacement: this.accountPP.horizonPlacement,
            secteurSouhaites: this.accountPP.secteurSouhaites,
            objectifInvestissement: this.accountPP.objectifInvestissement,
            profilInvestissement: this.accountPP.profilInvestissement,
            nationaliteUSA: this.accountPP.nationaliteUSA,
            compteBancaireUSA: this.accountPP.compteBancaireUSA,
            adresseUSA: this.accountPP.adresseUSA,
            greenCardUSA: this.accountPP.greenCardUSA,
            idFiscalUSA: this.accountPP.idFiscalUSA,
            ligneTelephoneUSA: this.accountPP.ligneTelephoneUSA
          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.accountPMFormGroup.patchValue({
            socialReason: this.accountPM.raisonSociale,
            legalForm: this.accountPM.formeJuridique,
            ice: this.accountPM.ice,
            pays: this.accountPM.pays,
            address: this.accountPM.adresse1,
            phone: this.accountPM.mobile1,
            fixe: this.accountPM.fixe,
            email: this.accountPM.email,
            codePostale: this.accountPM.codePostale,
            webSite: this.accountPM.siteWeb,

            categorie: this.accountPM.categorie,
            secActivite: this.accountPM.secActivite,
            capaciteFinanciere: this.accountPM.capaciteFinanciere,
            typeProduit:this.accountPM.typeProduit,
            produitSouhaites: this.accountPM.produitSouhaites,
            niveauInv: this.accountPM.niveauInv,
            secteurSouhaites: this.accountPM.secteurSouhaites,
            horizonPlacement: this.accountPM.horizonPlacement,
            profilInvestissement: this.accountPM.profilInvestissement,
            objectifInvestissement: this.accountPM.objectifInvestissement,
            paysResidenceFiscale: this.accountPM.paysResidenceFiscale,
            compteBancaireUSA: this.accountPM.compteBancaireUSA,
            adresseUSA: this.accountPM.adresseUSA,
            villeFiscaleUSA: this.accountPM.villeFiscaleUSA,
            idFiscalUSA: this.accountPM.idFiscalUSA,
            ligneTelephoneUSA: this.accountPM.ligneTelephoneUSA,
            giin: this.accountPM.giin
          });
        }
    }
  }

  /** ON EDIT CONTACT */
  onViewContact(contact: ContactModel) {
    const compteContact: CompteContactPayload = this.contactAccounts.filter(value => value.codeContact === contact.code)[0];
    this.dialog.open(EditContactComponent, {
      panelClass: 'activity-popup',
      data: {
        contact,
        compteContact,
        viewMode: true,
        viewOnly: true
      }
    });
  }

}
