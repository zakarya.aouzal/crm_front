import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {AccountType, CompteContactPayload, ContactModel, ContactPMModel, ContactPPModel} from 'src/app/main/models';
import {ContactService} from 'src/app/main/services';
import {AddContactComponent} from '../../../contacts-management/contact/add-contact/add-contact.component';
import {CreateContactComponent} from '../../../contacts-management/contact/create-contact/create-contact.component';
import {EditContactComponent} from '../../../contacts-management/contact/edit-contact/edit-contact.component';


@Component({
  selector: 'app-contacts-form',
  templateUrl: './contacts-form.component.html',
  styleUrls: ['./contacts-form.component.scss'],
  
})
export class ContactsFormComponent implements OnInit {

  // subscription
  addcontactMatDialogCloseSubscription: Subscription = null;
  newcontactMatDialogCloseSubscription: Subscription = null;
  editContactMatDialogCloseSubscription: Subscription = null;

  // list of selected contacts
  selectedContacts: ContactModel[] = [];
  contactAccountPayloadList: CompteContactPayload[] = [];

  @Input()
  accountType: AccountType;

  @Input()
  set accountCode(value: string) {
    if (value && this.accountType) {
      this._accountCode = value;

      this.contactService.getListContactsByAccount(value)
        .subscribe((response: CompteContactPayload[]) => {
          if (response) {
            const contacts: string[] = response.map(value => value?.codeContact);
            const tempList: ContactModel[] = [];

            this.contactService.getContactsPP()
              .subscribe(value => {
                value = value.map(element => this.contactToModel(element));
                tempList.push(...value);

                this.contactService.getContactsPM()
                  .subscribe(data => {
                    data = data.map(element => this.contactToModel(element));
                    tempList.push(...data);

                    this.selectedContacts = tempList.filter(contact => contacts?.indexOf(contact?.code) !== -1);
                    const selected: string[] = this.selectedContacts.map(value => value?.code);
                    this.contactAccountPayloadList = response.filter(cc => selected?.indexOf(cc?.codeContact) !== -1);
                  });
              });
          } else {
            console.error("error response", response);
          }
        })

      // switch (this.accountType) {
      //   case "PP":{
      //     this.contactService.getListContactsByAccount(value).subscribe((response:any)=>{
      //       if(response){
      //         this.selectedContacts=response;
      //       }else{
      //         console.error("error response",response);
      //       }
      //     })
      //     break;
      //   }
      //   case "PM":{
      //     this.contactService.getContactPMByCode(value).subscribe((response:any)=>{
      //       if(response){
      //         this.selectedContacts=response;
      //       }else{
      //         console.error("error response",response);
      //       }
      //     })
      //     break;
      //   }


      //   default:
      //     break;
      // }

    }


  }

  _accountCode: string;

  // subscription
  routeSubscription: Subscription = null;

  constructor(private dialog: MatDialog, private contactService: ContactService) {

    // this.routeSubscription = this.activatedRoute.paramMap.subscribe(params => {
    //   this.accountType = params.get('accountType') ? AccountType[params.get('accountType').toUpperCase()] : null;
    //   this.accountCode = params.get('accountCode') ? params.get('accountCode') : null;})

  }

  ngOnInit() {
  }

  /** ON ADD CONTACT */
  onAddContact() {
    const selectedList = this.selectedContacts.map(value => value?.code);
    const dialogRef = this.dialog.open(AddContactComponent, {
      panelClass: 'activity-popup',
      data: {
        code: this.accountCode,
        type: this.accountType,
        mode: 'ACCOUNT',
        selectedList
      }
    });
    this.addcontactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.selectedContacts.push(...dialogRef.componentInstance.selectedContacts);
        this.contactAccountPayloadList.push(...dialogRef.componentInstance.payloadList);
      }
    });
  }

  /** ON NEW CONTACT */
  onNewContact() {
    const dialogRef = this.dialog.open(CreateContactComponent, {
      panelClass: 'activity-popup',
      maxHeight: '500px',
      data: {
        code: this.accountCode,
        type: this.accountType
      }
    });
    this.newcontactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.selectedContacts.push(dialogRef.componentInstance.createdContact);
        this.contactAccountPayloadList.push(dialogRef.componentInstance.compteContactPayload);
      }
    });
  }

  /** ON EDIT CONTACT */
  onViewEditContact(contact: ContactModel, viewMode: boolean) {
    const compteContact: CompteContactPayload = this.contactAccountPayloadList.filter(value => value.codeContact === contact.code)[0];
    const dialogRef: MatDialogRef<EditContactComponent> = this.dialog.open(EditContactComponent, {
      panelClass: 'activity-popup',
      data: {
        contact,
        compteContact,
        viewMode
      }
    });
    this.editContactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        // replace old values using filters!!!
        this.contactAccountPayloadList = this.contactAccountPayloadList.map(value => {
          return (value?.codeContact === dialogRef.componentInstance.compteContactPayload?.codeContact) ?
            dialogRef.componentInstance.compteContactPayload : value;
        });
      }
    });
  }

  private contactToModel(contact: ContactPPModel | ContactPMModel): any {
    return {
      code: contact.code,
      nom: contact['nom'],
      prenom: contact['prenom'],
      raisonSociale: contact['raisonSociale'],
      mobile1: contact.mobile1,
      email: contact.email,
      ville: contact.ville,
      codePostale: contact.codePostale
    };
  }

}
