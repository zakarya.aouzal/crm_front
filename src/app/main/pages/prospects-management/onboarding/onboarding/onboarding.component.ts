import { AfterViewInit, Component, Inject, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AccountPMModel, AccountPMPayloadModel,AccountPPModel,AccountPPPayloadModel, AccountType, CompteContactPayload, ContactModel,RegisterAccountPayloadModel } from 'src/app/main/models';
import { AccountService, GedService, ReferenceService } from 'src/app/main/services';
import { TranslateService } from '@ngx-translate/core';
import { DocumentComponent } from '../../../documents-management/document/document.component';

import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder,} from '@angular/forms';
import { InfosGeneralComponent } from '../infos-general/infos-general.component';
import { ActivitiesFormComponent } from '../activities-form/activities-form.component';
import { trigger, style, animate, transition } from '@angular/animations';
import {ContactsFormComponent} from "../contacts-form/contacts-form.component";
import { WatchListComponent } from '../watch-list/watch-list.component';


@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({opacity: 0, transform: 'translateX(10px)' }),
            animate('1s ease',
                    style({ opacity: 1 ,transform: 'translateX(0)'}))
          ]
        ),
        transition(
          ':leave',
          [
            animate('0s ease-in',
                    style({opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class OnboardingComponent implements OnInit,AfterViewInit,OnDestroy {

  routeSubscription:Subscription=null;
 // account
 accountCode: string;
 accountType: AccountType = AccountType.PP;
 accountPP: AccountPPModel = null;
 accountPM: AccountPMModel = null;

 clientFullName:String="";

  @ViewChild(InfosGeneralComponent)
  infosGeneralComponent: InfosGeneralComponent;

  @ViewChild(ActivitiesFormComponent)
  activitiesFormComponent: ActivitiesFormComponent;

  @ViewChild(ContactsFormComponent)
  contactsFormComponent: ContactsFormComponent;

  @ViewChild(DocumentComponent)
  attachmentFormComponent: DocumentComponent;

  @ViewChild(WatchListComponent)
  watchListComponent: WatchListComponent;

  ngAfterViewInit() {
  }

  // list
  readonly steps = [
    { id: 1, label: 'NOUVEAU_COMPTE.STEPS.INFO_COMPTE' },
    { id: 2, label: 'NOUVEAU_COMPTE.STEPS.ACTIVITY' },
    { id: 3, label: 'NOUVEAU_COMPTE.STEPS.CONTACTS' },
    { id: 4, label: 'NOUVEAU_COMPTE.STEPS.DOCUMENTS' },
    { id: 5, label: 'NOUVEAU_COMPTE.STEPS.WATCHLIST' },
    { id: 6, label: 'NOUVEAU_COMPTE.STEPS.VALIDATION' }
  ];
  readonly accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];

  // current step
  currentStep: { id: number, label: string } = null;

  // selected account type
  //selectedAccountType: AccountType = AccountType.PP;

   // loading
   loading: boolean = false;

  // payload
  generalInformationsPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  //identityInformationPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  activitiesPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;


  // subscription
  addcontactMatDialogCloseSubscription: Subscription = null;
  newcontactMatDialogCloseSubscription: Subscription = null;
  editContactMatDialogCloseSubscription: Subscription = null;

  // list of selected contacts
  selectedContacts: ContactModel[] = [];
  contactAccountPayloadList: CompteContactPayload[] = [];

  // onBoardingPayload
  registerAccountPayload: RegisterAccountPayloadModel;

  constructor(private accountService: AccountService,
              private router: Router,
              private toastrService: ToastrService,
              private translate: TranslateService,
              @Inject('pipeParameters') public pipeParameters: any,
              private gedService: GedService,
              private fb: FormBuilder,
              private referenceService: ReferenceService,
              private activatedRoute: ActivatedRoute) {
    this.routeSubscription = this.activatedRoute.paramMap.subscribe(params => {
      this.accountType = params.get('accountType') ? AccountType[params.get('accountType').toUpperCase()] : null;
      this.accountCode = params.get('accountCode') ? params.get('accountCode') : null;
    });
  }

  ngOnInit(): void {
    this.currentStep = this.steps[0];
    this.registerAccountPayload = new RegisterAccountPayloadModel();
  }

  ngOnDestroy(): void {
    if (this.addcontactMatDialogCloseSubscription) {
      this.addcontactMatDialogCloseSubscription?.unsubscribe();
    }
    if (this.newcontactMatDialogCloseSubscription) {
      this.newcontactMatDialogCloseSubscription?.unsubscribe();
    }
    if (this.editContactMatDialogCloseSubscription) {
      this.editContactMatDialogCloseSubscription?.unsubscribe();
    }
  }

  /** CHANGE STEP WIZARD */
  onChangeStep(stepId: number) {

    if (!this.currentStep || !stepId) {
      return;
    }
    if (this.currentStep.id < stepId) {
      for (let i = this.currentStep.id; i < stepId; i++) {

        switch (i) {
          case 1: {
            // const generalInfoFormIsValid: boolean = this.generalInformationsFormComponent.isValidForm();
            const prospectFormIsValid: boolean = this.infosGeneralComponent.isValidForm();
            if ( prospectFormIsValid) {
              this.generalInformationsPayload = this.infosGeneralComponent.generatePayload();
            } else {
              this.currentStep = this.steps.find(item => item.id === i);
              return;
            }
            break;
          }
          case 2: {
            const activitiesFormIsValid: boolean = this.activitiesFormComponent.isValidForm();
            if (activitiesFormIsValid) {
              this.activitiesPayload = this.activitiesFormComponent.generatePayload();
              // show popup contacts
            } else {
              this.currentStep = this.steps.find(item => item.id === i);
              return;
            }
            break;
          }
          case 5: {
            const watchListActionValid: boolean = this.watchListComponent.isValid();
            if (!watchListActionValid) {
              this.toastrService.warning("Merci de finir le traitement Watchlist");
              this.currentStep = this.steps.find(item => item.id === i);
              return;
            }
            break;
          }
        }
      }
    }
    this.currentStep = this.steps.find(item => item.id === stepId);

    if (this.currentStep.id === 6) {
      this.registerAccountPayload = this.generateRegisterPayload();
    }
  }

  /** GENERATE SAVING PAYLOAD */
  generateRegisterPayload(): RegisterAccountPayloadModel {
    let registerAccountPayload = new RegisterAccountPayloadModel();
    // account type
    registerAccountPayload.accountType = this.accountType;
    // account payload
    if (this.generalInformationsPayload && this.activitiesPayload) {
      let accountPayload: AccountPPPayloadModel | AccountPMPayloadModel = {
        ...this.activitiesPayload,
        //...this.identityInformationPayload,
        ...this.generalInformationsPayload
      }
      switch (this.accountType) {
        case AccountType.PP: {
          registerAccountPayload.accountPP = accountPayload as AccountPPPayloadModel;
          //Watch list
          registerAccountPayload.accountPP.statutWL=this.watchListComponent._statutWL;
          break;
        }
        case AccountType.PM: {
          registerAccountPayload.accountPM = accountPayload as AccountPMPayloadModel;
          registerAccountPayload.accountPM.statutWL=this.watchListComponent._statutWL;
        }
      }
    } else {
      return;
    }


    // account contact payload
    this.contactAccountPayloadList = this.contactsFormComponent?.contactAccountPayloadList;
    this.selectedContacts = this.contactsFormComponent?.selectedContacts;
    registerAccountPayload.selectedContacts = this.selectedContacts;
    registerAccountPayload.contactAccounts = this.contactAccountPayloadList;
    return registerAccountPayload;
  }

  /** ON SUBMIT */
  onSubmit() {
    // save
    this.loading = true;
    this.accountService.onboarding(this.registerAccountPayload)
      .pipe(finalize(() => {
        this.onResetForm();
        this.currentStep = this.steps[0];
        this.loading = false;
      }))
      .subscribe(response => {
        if (response && response.valid) {
          this.toastrService.success("Compte ajouté avec succés");
          // upload attachments
          this.uploadAttachments(response.code);
          // upload compte photo
          // if (this.identityInformationFormComponent.photoFile)
          //   this.uploadPhoto(response.code);
        } else {
          this.toastrService.warning("Un problème est survenu lors d'ajout de compte");
        }
      })

  }

  /** ON RESET FORM */
  onResetForm() {
    // if (this.generalInformationsFormComponent) {
    //   this.generalInformationsFormComponent.onResetForm();
    // }
    // if (this.identityInformationFormComponent) {
    //   this.identityInformationFormComponent.onResetForm();
    // }
    if (this.activitiesFormComponent) {
      this.activitiesFormComponent.onResetForm();
    }
  }

  /** UPLOAD ATTACHMENTS */
  uploadAttachments(accountCode: string) {

    if (!accountCode || this.attachmentFormComponent.files.size === 0) {
      return;
    }
    let formData: FormData = new FormData();

    if (this.attachmentFormComponent.files) {
      for (let file of this.attachmentFormComponent.files) {
        formData.append('files', file[1], file[1].name);
      }
    }
    // upload
    this.gedService.uploadFilesForNewAccount(formData, accountCode)
      .subscribe(response => {
        if (response && response.valid) {
          //this.router.navigate(['/pages/prospects-management/prospect-validation']);
        }else{
          this.toastrService.warning("Certains fichiers ne sont pas téléchargés correctement");
        }
      });
  }

  /** UPLOAD COMPTE PHOTO BY UPDATE */
  // uploadPhoto(accountCode: string): void {
  //   const file = this.identityInformationFormComponent.photoFile;
  //   this.accountService.uploadPhoto(accountCode, file)
  //     .subscribe(response => {
  //       if (!response || !response.valid) {
  //         this.toastrService.warning("La photo n'a pas été chargé correctement");
  //       }
  //     })
  // }

  // onRemoveContact(code: string) {
  //   this.selectedContacts = this.selectedContacts.filter(value => value?.code !== code);
  //   this.contactAccountPayloadList = this.contactAccountPayloadList.filter(value => value?.codeContact !== code);
  // }

  redirect(index: number) {
    switch (index) {
      case 1:
        this.router.navigate(['/pages/prospects-management/prospect-consultation']);
        break;
      case 2:
          this.router.navigate(['/pages/prospects-management/onboarding']);
          break;
      default:
        break;
    }
  }

  accountInfos(value:{accountPP:AccountPPModel,accountPM:AccountPMModel}){
    if(value.accountPP){
      this.clientFullName=value.accountPP.civilite+" "+value.accountPP.nom+" "+value.accountPP.prenom
    }else{
      this.clientFullName=value.accountPM.raisonSociale

    }


    this.accountPP=value.accountPP;
    this.accountPM=value.accountPM;
  }







}
