import { Component, Inject, Input, OnDestroy, OnInit, Output,EventEmitter, ViewChild } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AccountPMModel, AccountPMPayloadModel, AccountPPModel, AccountPPPayloadModel, AccountType, GenericItemModel, RegisterAccountPayloadModel } from 'src/app/main/models';
import { AccountService, GedService, ReferenceService } from 'src/app/main/services';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-infos-general',
  templateUrl: './infos-general.component.html',
  styleUrls: ['./infos-general.component.scss']
})
export class InfosGeneralComponent implements OnInit,OnDestroy {


  routeSubscription: Subscription = null;


  @Input()
  set _accountCode(value: string) {
    this.accountCode = value;
  }

  @Input()
  set _accountType(value: AccountType) {
    this.accountType = value;
  }

  @Input()
  mode: 'ADD' | 'EDIT' = 'ADD';


  @Output()
  submited = new EventEmitter<{ accountPP: AccountPPModel, accountPM: AccountPMModel }>();


  // Form group
  accountPPFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;
  showValidationsMsg: boolean = false;
  //// Listes
  secteurList: GenericItemModel[] = []; /// ---
  // compte pp list
  civiliteList: GenericItemModel[] = []; //
  nationaliteList: GenericItemModel[] = [];
  paysList: GenericItemModel[] = [];
  typeResidenceList: GenericItemModel[] = [];
  gestionnaireFrontsList:GenericItemModel[] = [];
  // compte pm list
  formeJuridiqueList: GenericItemModel[] = [];
  // account
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;
  accountCode: string;
  accountType: AccountType = AccountType.PP;
  // loading
  loading: boolean = false;
  loadingAccount: boolean = false;
  loadingForm: boolean = false;

  isEdit: boolean = false

  onEdit() {
    this.isEdit = !this.isEdit
  }
/**------------------------------------------ */
/**---------- end variables  prospect form---- */
/**------------------------------------------ */

constructor(private accountService: AccountService,
  private router:Router,
  private toastrService: ToastrService,
  private dialog: MatDialog,
  private translate:TranslateService,
  @Inject('pipeParameters') public pipeParameters: any,
  private gedService: GedService,
  private fb: FormBuilder,
  private referenceService: ReferenceService,
  private activatedRoute: ActivatedRoute,


  ) {




  }

  ngOnInit() {
    this.createFormGroup();

      if (this.accountType && this.accountCode) {
        switch (this.accountType) {
          case AccountType.PM: {
            this.loadAccountPMByCode(this.accountCode);

          }
            break;
          case AccountType.PP: {
            this.loadAccountPPByCode(this.accountCode);
            //this.mapCodeToValue()
          }
            break;
        }

        //this.photoImg = `${this.apiUrl}/comptes/${this.accountCode}/photo`;
      }




    this.referenceService.getSectActivitesList().subscribe(value => this.secteurList = value);
    this.referenceService.getCivilites().subscribe(value => this.civiliteList = value);
    this.referenceService.getNationalites().subscribe(value => this.nationaliteList = value);
    this.referenceService.getPaysList().subscribe(value => this.paysList = value);
    this.referenceService.getFormesJuridiques().subscribe(value => this.formeJuridiqueList = value);
    this.referenceService.getTypeResidenceList().subscribe(value => this.typeResidenceList = value);
    this.referenceService.getGestionnaireFronts().subscribe(response => {this.gestionnaireFrontsList = response;})
  }
  ngOnDestroy(): void {

    if (this.routeSubscription) {
      this.routeSubscription?.unsubscribe();
    }
  }




  /**--------------------------------------------------------------------------------------------------------- */
  /**--------------------------------------------------------------------------------------------------------- */
  /**------------------------------------------ PROSPECT  FORM ----------------------------------------------- */
  /**--------------------------------------------------------------------------------------------------------- */
  /**--------------------------------------------------------------------------------------------------------- */

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.accountPPFormGroup = null;
    this.accountPMFormGroup = null;
    switch (this.accountType) {
      case AccountType.PP:
        this.createaccountPPFormGroup()
        break;
      case AccountType.PM:
        this.createaccountPMFormGroup()
    }
  }


  createaccountPPFormGroup() {
    this.accountPPFormGroup = this.fb.group({
      civilite: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      pieceIdentite: [null, [Validators.required]],
      numPi: [null, [Validators.required]],
      secteur: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      dateExpPI: [null, [Validators.required]],
      profission: [null, [Validators.required]],
      address: [null, [Validators.required]],
      nationality: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      email: [null, [Validators.required]],
      dateOfBirth: [null],
      typeResidence:[null],
      fatherFullName: [null],
      motherFullName: [null],
      country: [null],
      city: [null],
      address2: [null],
      mobile2: [null],

    });
    if (this.mode === 'ADD') {
      this.accountPPFormGroup.enable();
    } else {
      this.accountPPFormGroup.disable();
    }
  }
  createaccountPMFormGroup() {
    this.accountPMFormGroup = this.fb.group({
      socialReason: [null, [Validators.required]],
      legalForm: [null, [Validators.required]],
      ice: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      fixe: [null, [Validators.required]],
      email: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      webSite: [null, [Validators.required]],
      gestionnaire: [null],
    });
    if (this.mode === 'ADD') {
      this.accountPMFormGroup.enable();
    } else {
      this.accountPMFormGroup.disable();
    }
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.accountPPFormGroup.patchValue({
            civilite: this.accountPP.civilite,
            codePostale: this.accountPP.codePostale,
            pays: this.accountPP.paysResidence,
            pieceIdentite: this.accountPP.pieceIdentite,
            numPi: this.accountPP.numPI,
            secteur: this.accountPP.secActivite,
            lastName: this.accountPP.prenom,
            firstName: this.accountPP.nom,
            dateExpPI: this.accountPP.dateExpPI,
            profission: this.accountPP.profession,
            address: this.accountPP.adresse1,
            nationality: this.accountPP.nationalite,
            mobile: this.accountPP.mobile1,
            email: this.accountPP.email,
            dateOfBirth: this.accountPP.dateNaissance,
            fatherFullName: this.accountPP.nomPrenomPere,
            motherFullName: this.accountPP.nomPrenomMere,
            residenceCountry: this.accountPP.paysResidence,
            typeResidence:this.accountPP.typeResidence,
            country: this.accountPP.paysResidence,
            city: this.accountPP.ville,
            address2: this.accountPP.adresse2,
            mobile2: this.accountPP.mobile2,

          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.accountPMFormGroup.patchValue({


            socialReason: this.accountPM.raisonSociale,
            legalForm: this.accountPM.formeJuridique,
            ice: this.accountPM.ice,
            pays: this.accountPM.pays,
            address: this.accountPM.adresse1,
            phone: this.accountPM.mobile1,
            fixe: this.accountPM.fixe,
            email: this.accountPM.email,
            codePostale: this.accountPM.codePostale,
            webSite: this.accountPM.siteWeb,
            gestionnaire:this.accountPM.gestionnaire,
          });
        }
    }

    // in case of update

  }

  /** ON ENABLE FORM */
  onEnableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.enable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.enable();
        }
    }
  }

  /** ON DISABLE FORM */
  onDisableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.disable();
        }
    }
  }
  /** CHECK FORM GROUP IS VALID OR NOT */
  isValidForm(): boolean {
    let isValid: boolean = false;
    switch (this.accountType) {
      case AccountType.PP:
        isValid = this.accountPPFormGroup && this.accountPPFormGroup.valid;
        break;
      case AccountType.PM:
        isValid = this.accountPMFormGroup && this.accountPMFormGroup.valid;
    }
    if (!isValid) {
      this.showValidationsMsg = true;
    }
    return isValid;
  }


  /** ON SUBMIT */
  onSubmit1() {

    // generate payload
    let registerAccountPayload: RegisterAccountPayloadModel = new RegisterAccountPayloadModel();

    // account type
    registerAccountPayload.accountType = this.accountType;

    // account payload
    if (this.isValidForm()) {
      let accountPayload: AccountPPPayloadModel | AccountPMPayloadModel = this.generatePayload();

      switch (this.accountType) {
        case AccountType.PP: {

          registerAccountPayload.accountPP = accountPayload as AccountPPPayloadModel;
          break;
        }
        case AccountType.PM: {
          registerAccountPayload.accountPM = accountPayload as AccountPMPayloadModel;
        }
      }
      // save

      switch (this.accountType) {
        case AccountType.PP: {
          // update
          this.loadingForm = true;
          this.accountService.updateAccountPP(registerAccountPayload.accountPP)
            .pipe(finalize(() => {
              this.loadingForm = false;
            }))
            .subscribe(response => {
              if (response && response.valid) {
                this.toastrService.success("Fiche compte modifié avec succés");
                this.onDisableForm();
                this.loadAccountPPByCode(this.accountCode);
              } else {
                this.toastrService.warning("Un problème est survenu lors de modification de fiche compte");
              }
            })
          break;
        }
        case AccountType.PM: {
          // update
          this.loadingForm = true;
          this.accountService.updateAccountPM(registerAccountPayload.accountPM)
            .pipe(finalize(() => {
              this.loadingForm = false;
            }))
            .subscribe(response => {
              if (response && response.valid) {
                this.toastrService.success("Fiche compte modifié avec succés");
                this.onDisableForm();
                this.loadAccountPMByCode(this.accountCode);
              } else {
                this.toastrService.warning("Un problème est survenu lors de modification de fiche compte");
              }
            })
        }
      }
    }



  }


  /** ON RESET FORM */
  onResetForm1() {
    this.showValidationsMsg = false;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.reset();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.reset();
        }
    }
  }


  /** GENERATE PAYLOAD */
  generatePayload(): AccountPPPayloadModel | AccountPMPayloadModel {
    let payload: AccountPPPayloadModel | AccountPMPayloadModel = null;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          payload = {
            ...new AccountPPPayloadModel(),
            code: this.accountCode?.toUpperCase(),
            nom: this.accountPPFormGroup.get("lastName").value,
            prenom: this.accountPPFormGroup.get("firstName").value,
            mobile1: this.accountPPFormGroup.get("mobile").value,
            civilite: this.accountPPFormGroup.get("civilite").value,
            paysResidence: this.accountPPFormGroup.get("pays").value,
            nationalite: this.accountPPFormGroup.get("nationality").value,
            adresse1: this.accountPPFormGroup.get("address").value,
            email: this.accountPPFormGroup.get("email").value,
            codePostale: this.accountPPFormGroup.get("codePostale").value,
            profession: this.accountPPFormGroup.get("profission").value,
            pieceIdentite: this.accountPPFormGroup.get("pieceIdentite").value,
            numPI: this.accountPPFormGroup.get("numPi").value,
            dateExpPI: this.accountPPFormGroup.get("dateExpPI").value,
            secActivite: this.accountPPFormGroup.get("secteur").value,
            typeCompte: this.accountType,
            dateNaissance: this.accountPPFormGroup.get("dateOfBirth").value,
            nomPrenomPere: this.accountPPFormGroup.get("fatherFullName").value,
            nomPrenomMere: this.accountPPFormGroup.get("motherFullName").value,
            typeResidence: this.accountPPFormGroup.get("typeResidence").value,
            country: this.accountPPFormGroup.get("country").value,
            ville: this.accountPPFormGroup.get("city").value,
            adresse2: this.accountPPFormGroup.get("address2").value,
            mobile2: this.accountPPFormGroup.get("mobile2").value,
          }

        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          payload = {
            ... new AccountPMPayloadModel(),
            code: this.accountCode.toUpperCase(),
            raisonSociale: this.accountPMFormGroup.get("socialReason").value,
            formeJuridique: this.accountPMFormGroup.get("legalForm").value,
            ice: this.accountPMFormGroup.get("ice").value,
            pays: this.accountPMFormGroup.get("pays").value,
            adresse1: this.accountPMFormGroup.get("address").value,
            mobile1: this.accountPMFormGroup.get("phone").value,
            fixe: this.accountPMFormGroup.get("fixe").value,
            email: this.accountPMFormGroup.get("email").value,
            codePostale: this.accountPMFormGroup.get("codePostale").value,
            siteWeb: this.accountPMFormGroup.get("webSite").value,
            typeCompte: this.accountType,
            gestionnaire:this.accountPMFormGroup.get("gestionnaire").value,
          }
        }

    }
    return payload;
  }


 /** LOAD ACCOUNT PP BY CODE */
 loadAccountPPByCode(accountCode: string): void {

   this.accountPP = null;
   if (!accountCode) return;
   this.loadingAccount = true;
   this.accountService.getAccountPPByCode(accountCode.toUpperCase())
     .pipe(finalize(() => {
       this.loadingAccount = false;
       this.submited.emit({accountPP: this.accountPP, accountPM: this.accountPM})
     }))
     .subscribe(response => {
       this.accountPP = response;
       this.initFormGroup();
     }, error => this.router.navigate(['/pages/prospects-management/prospects-consultation']))
 }

  /** LOAD ACCOUNT PM BY CODE */
  loadAccountPMByCode(accountCode: string): void {


    this.accountPM = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPMByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;
        this.submited.emit({accountPP: this.accountPP, accountPM: this.accountPM})
      }))
      .subscribe(response => {
        this.accountPM = response;
        this.initFormGroup();
      }, error => this.router.navigate(['/pages/prospects-management/prospects-consultation']))
  }



}
