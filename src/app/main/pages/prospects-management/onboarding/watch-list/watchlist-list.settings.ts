import { DataTableColumnsFormat, DataTableSettingsModel, } from 'src/app/shared/models';


export const WATCHLIST_PP_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code_wl',
      title: 'Code WL',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.CODE_WL'
      }
    },
    {
      name: 'nom',
      title: 'Nom',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.NOM'
      }
    },
    {
      name: 'prenom',
      title: 'Prénom',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.PRENOM'
      }
    },
    {
      name: 'date_naissance',
      title: 'Date de naissance',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.DATE_NAISSANCE'
      }
    },
    {
      name: 'num_pi',
      title: 'Num_PI',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.NUM_PI'
      }
    },
    {
      name: 'nationalite',
      title: 'Nationalite',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.NATIONALITE'
      }
    },
    {
      name: 'similtitude',
      title: 'Similtitude',
      format: DataTableColumnsFormat.PROGRESS_BAR,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.SIMILTITUDE'
      }
    },
    {
      name: 'statut_wl',
      title: 'Statut wl',
      format: DataTableColumnsFormat.ENUM,
      enum: [
        {value: 'R', text: 'Risqué'},
        {value: 'NR', text: 'Non risqué'}
      ],
      style: {
        colors: [{
          colorValue: 'red',
          conditionValue: 'R'
        }, {
          colorValue: 'green',
          conditionValue: 'NR'
        }]
      },
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PP_LIST.STATUT_WL'
      }
    }
  ]
};

export const WATCHLIST_PM_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code_wl',
      title: 'Code WL',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PM_LIST.CODE_WL'
      }
    },
    {
      name: 'raison_sociale',
      title: 'Raison sociale',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PM_LIST.RAISON_SOCIALE'
      }
    },
    {
      name: 'pays',
      title: 'Pays',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PM_LIST.PAYS'
      }
    },
    {
      name: 'similtitude',
      title: 'Similtitude',
      format: DataTableColumnsFormat.PROGRESS_BAR,
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PM_LIST.SIMILTITUDE'
      }
    },
    {
      name: 'statut_wl',
      title: 'Statut wl',
      format: DataTableColumnsFormat.ENUM,
      enum: [
        {value: 'R', text: 'Risqué'},
        {value: 'NR', text: 'Non risqué'}
      ],
      style: {
        colors: [{
          colorValue: 'red',
          conditionValue: 'R'
        }, {
          colorValue: 'green',
          conditionValue: 'NR'
        }]
      },
      data: {
        i18nValue: 'COLUMNS.WATCHLIST_PM_LIST.STATUT_WL'
      }
    }
  ]
};
