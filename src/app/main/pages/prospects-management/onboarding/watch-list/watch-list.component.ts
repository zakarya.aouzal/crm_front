import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AccountPMModel, AccountPPModel, AccountType, IStatusWatchlist, StatusWatchlistPM, StatusWatchlistPP, WatchlistProcessDTO } from 'src/app/main/models';
import { AccountService } from 'src/app/main/services';
import { WatchlistService } from 'src/app/main/services/kyc/account-management/watchlist/watchlist.service';
import { DataTableSettingsModel, ToolbarOption } from 'src/app/shared/models';



import { ExportService } from 'src/app/shared/services';
import {pulseAnimation} from 'src/app/shared/utils/animations'
import { WATCHLIST_PM_LIST_SETTINGS, WATCHLIST_PP_LIST_SETTINGS } from './watchlist-list.settings';

@Component({
  selector: 'app-watch-list',
  templateUrl: './watch-list.component.html',
  styleUrls: ['./watch-list.component.scss'],
  animations: [
    trigger('button_animate', [
      transition('* => *', useAnimation(pulseAnimation,{ params: { scale: 1.1} } )),
    ])
  ]
})
export class WatchListComponent implements OnInit {


  _statutWL:string="";
  finishedWatchlist: boolean = false;


  @Input()
  set _data(value:{ accountPP: AccountPPModel, accountPM: AccountPMModel,accountType: AccountType}){

    this.accountType=value?.accountType;
    switch (this?.accountType) {
      case AccountType.PP:{
        this.accountPP=value?.accountPP;
        this.accountCode=value.accountPP?.code;
        this.settings = WATCHLIST_PP_LIST_SETTINGS;
        break;

      }

      case AccountType.PM:{
        this.accountPM=value?.accountPM;
        this.accountCode=value.accountPM?.code;
        this.settings = WATCHLIST_PM_LIST_SETTINGS;
        break;
      }

    }
  }

  settings: DataTableSettingsModel = null;

  loading:boolean=false;
  disply:boolean=false;

  // account
  accountType: AccountType = null;
  accountCode: string = null;
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;
//list
watchlistStatusList:IStatusWatchlist[];

  constructor(
    private watchlistService: WatchlistService,
    private toastrService: ToastrService,
    private toastr: ToastrService,
              private accountService: AccountService,
              private exportService: ExportService,) { }

  ngOnInit() {

  }

 /** RUN WATCH LIST PROCESS FOR CURRENT ACCOUNT **/
 onWatchListProcess(): void {
   this.loading=true
  const dto: WatchlistProcessDTO = {
    codeCompte: this.accountCode,
    critere: {
      codeListe: "", // --- todo
      flag: true,
      flagTrait: ''
    }
  }

  switch (this.accountType) {
    case AccountType.PP:
      this.watchlistService.traiterComptePPSimiltitudeByCode(dto).pipe(finalize(() => {
        this.loading = false;
        this.disply=true;
      }))
        .subscribe(value => {this.watchlistStatusList=value;},
          error => this.toastrService.error("Erreur pendant le traitement de similtitude Watchlist"));
      break;
    case AccountType.PM:
      this.watchlistService.traiterComptePMSimiltitudeByCode(dto).pipe(finalize(() => {
        this.loading = false;
      }))
        .subscribe(value => this.watchlistStatusList=value,
          error => this.toastrService.error("Erreur pendant le traitement de similtitude Watchlist"));
      break;
  }
}





  /** UPDATE PROFIL RISK RESULT **/
  onSubmit(isRisque:boolean): void {
    this.loading = true;
    this._statutWL=isRisque?'R':'NR'

    switch (this.accountType) {
      case AccountType.PP:{
        const cpp: any = {
          ...this.accountPP,
          statutWL:this._statutWL
        }

        this.accountService.updateAccountPP(cpp)
          .pipe(finalize(() =>  this.loading = false))
          .subscribe(value => {
              this.toastr.success('Profil mis à jour');
              this.finishedWatchlist = true;
            },
            error => this.toastr.error('Erreur pendant la màj du profil'));
        break

      }
        ;
      case AccountType.PM:{
        const cpm: any = {
          ...this.accountPM,
          statutWL: this._statutWL
        }
        //this.accountPM.profilRisque = this.isRisque()?'R':'NR';
        this.accountService.updateAccountPM(cpm)
        .pipe(finalize(() =>  this.loading = false))
          .subscribe(value => {
              this.toastr.success('Profil mis à jour');
              this.finishedWatchlist = true;
            },
            error => this.toastr.error('Erreur pendant la màj du profil'));
        break;
      }

    }
  }


  isValid(): boolean {
    return this.finishedWatchlist;
  }
}
