import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {finalize} from 'rxjs/operators';
import {AccountFilterModel, AccountPMModel, AccountPPModel, AccountType, Flag} from 'src/app/main/models';
import {AccountService} from 'src/app/main/services';
import {DataTableOutputActionsModel, DataTableSettingsModel, ToolbarOption} from 'src/app/shared/models';
import {ExportService} from 'src/app/shared/services';
import {ACCOUNT_PM_LIST_SETTINGS, ACCOUNT_PP_LIST_SETTINGS} from './account-list.settings';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {


  @Input()
  set flag(value:Flag){
    if(value==Flag.VALIDATION){

      this.redirect_URL='/pages/prospects-management/prospect-control-validation';
      switch (this._accountType) {
        case AccountType.PP:
          this.settings = ACCOUNT_PP_LIST_SETTINGS;
          break;
        case AccountType.PM:
          this.settings = ACCOUNT_PM_LIST_SETTINGS;
          break;
      }


  };
}



  @Input()
  set filter(value: { filterBody: AccountFilterModel, accountType: AccountType }) {
    this.accountPPList = [];
    this.accountPMList = [];
    if (!value) return;
    this._filter = value.filterBody;
    this._accountType = value.accountType;
    if (this._filter && this._accountType) {
      switch (this._accountType) {
        case AccountType.PP:
          this.settings = ACCOUNT_PP_LIST_SETTINGS;
          this.loadListAccountPP(this._filter)
          break;
        case AccountType.PM:
          this.settings = ACCOUNT_PM_LIST_SETTINGS;
          this.loadListAccountPM(this._filter);
          break;
      }
    }
  }




  redirect_URL:string='/pages/prospects-management/prospect-details';

  settings: DataTableSettingsModel = null;

  _filter: AccountFilterModel = null;

  _accountType: AccountType = null;

  accountPPList: AccountPPModel[] = [];

  accountPMList: AccountPMModel[] = [];

  loading: boolean = false;

  constructor(private accountService: AccountService,
              private router: Router,
              private exportService: ExportService) { }

  ngOnInit(): void {
  }

  /** LOAD LIST OF ACCOUNTS PP */
  loadListAccountPP(filter: AccountFilterModel) {
    this.accountPPList = [];
    if (!filter) return;
    console.log('filter 2222 ', filter)
    this.loading = true;
    filter.flag = this._filter.flag;
    this.accountService.getListAccountPP(filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {this.accountPPList = value; console.log('accountPPlist: ', this.accountPPList)});
  }

  /** LOAD LIST OF ACCOUNTS PM */
  loadListAccountPM(filter: AccountFilterModel) {
    this.accountPMList = [];
    if (!filter) return;
    this.loading = true;
    filter.flag =  this._filter.flag;
    this.accountService.getListAccountPM(filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => this.accountPMList = value);
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<AccountPMModel | AccountPPModel>) {
    if (event) {
      switch (event.actionType) {
       
        case 'VIEW':
          let id = event.item ? event.item.code : null;
          this.router.navigate([this.redirect_URL, this._accountType, id]);
          break;
      }
      console.log('action', event)
    }
  }

  /** ON ACTION TOOLBAR */
  onActionToolbar(event: ToolbarOption) {
    switch (event) {
      case ToolbarOption.DownloadAsExcel:
        switch (this._accountType) {
          case AccountType.PP:
            if (this.accountPPList) {
              this.exportService.exportAsExcelFile(this.accountPPList, ACCOUNT_PP_LIST_SETTINGS, 'Liste des compte PP', 'comptes-pp');
            }
            break;
          case AccountType.PM:
            if (this.accountPMList) {
              this.exportService.exportAsExcelFile(this.accountPMList, ACCOUNT_PM_LIST_SETTINGS, 'Liste_des_compte_PM', 'comptes-pm');
            }
            break;
        }
        break;
      case ToolbarOption.DownloadAsPDF:
        switch (this._accountType) {
          case AccountType.PP:
            this.exportService.exportAsPDFFile(this.accountPPList, ACCOUNT_PP_LIST_SETTINGS, 'Liste des compte PP', 'comptes-pp');
            break;
          case AccountType.PM:
            this.exportService.exportAsPDFFile(this.accountPMList, ACCOUNT_PM_LIST_SETTINGS, 'Liste des compte PM', 'comptes-pm');
            break;
        }
        break;
    }
  }

}
