import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const ACCOUNT_PP_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
            }
        }, {
            name: 'nom',
            title: 'Nom',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.NOM'
            },style:{
                orientation:"left"
                  }
        }, {
            name: 'prenom',
            title: 'Prénom',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PRENOM'
            },style:{
                orientation:"left"
                  }
        }, {
            name: 'profession',
            title: 'profession',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PROFESSION'
            }
        }, {
            name: 'secActivite',
            title: 'secActivite',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'ACCOUNT_DETAILS.INFOS_PP.SECT_ACTIVITE'
            }
        }, {
            name: 'paysResidence',
            title: 'Pays de résidence',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PAYS_RESIDENCE'
            }
        }, {
            name: 'mobile1',
            title: 'Téléphone',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.ACCOUNT_LIST.MOBILE'
            }
        }, {
            name: 'adresse1',
            title: 'adresse1',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'ACCOUNT_DETAILS.INFOS_PM.ADRESSE'
            },
            
        },{
            name: 'operateur_saisie',
            title: 'Gestionnaire',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'ACCOUNT_DETAILS.INFOS_PM.GESTIONNAIRE'
            },
            
        }, {
            name: 'numPI',
            title: 'Num PI',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.NUMPI'
            },style:{
                //orientation:"right"
                  }
        }
    ], actions: [DataTableActions.VIEW]
};

export const ACCOUNT_PM_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
            }
        }, {
            name: 'raisonSociale',
            title: 'Raison Sociale',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.RAISON_SOCIALE'
            },style:{
                orientation:"left"
                  }
        }, {
            name: 'pays',
            title: 'Pays de résidence',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.PAYS_RESIDENCE'
            }
        }, {
            name: 'formeJuridique',
            title: 'formeJuridique',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'ACCOUNT_DETAILS.INFOS_PM.FORME_JURIDIQUE'
            }
        }, {
            name: 'codePostale',
            title: 'codePostale',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'FIELD.COMPTE_PM.CODE_POSTAL'
            }
        }, {
            name: 'ice',
            title: 'ICE',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.ICE'
            }
        }, {
            name: 'mobile1',
            title: 'Téléphone',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.ACCOUNT_LIST.MOBILE'
            }
        }, {
            name: 'fixe',
            title: 'fixe',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'FIELD.COMPTE_PM.FIXE'
            }
        }, {
            name: 'adresse1',
            title: 'adresse1',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'ACCOUNT_DETAILS.INFOS_PM.ADRESSE'
            }
        }
    ], actions: [DataTableActions.VIEW]
};


