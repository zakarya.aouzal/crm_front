import {ContactListComponent} from "../../../contacts-management/contact/contact-list/contact-list.component";

import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {
  AccountPMModel,
  AccountPMPayloadModel,
  AccountPPModel,
  AccountPPPayloadModel,
  AccountType,
  ActivityGlobalFilterModel,
  ContactFilterModel,
  EventFilterModel,
  GenericItemModel,
  IncidentFilterModel,
  OpportuniteFilterModel,
  RegisterAccountPayloadModel,
  TaskFilterModel
} from 'src/app/main/models';
import {AccountService, ReferenceService} from 'src/app/main/services';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {AddContactComponent} from '../../../contacts-management/contact/add-contact/add-contact.component';
import {TaskListComponent} from '../../../activity-management/task/task-list/task-list.component';
import {EventListComponent} from '../../../activity-management/event/event-list/event-list.component';
import {OpportuniteListComponent} from '../../../activity-management/opportunite/opportunite-list/opportunite-list.component';
import {OpportuniteFormComponent} from "../../../activity-management/opportunite/opportunite-form/opportunite-form.component";
import {IncidentFormComponent} from "../../../activity-management/incident/incident-form/incident-form.component";
import {EventFormComponent} from "../../../activity-management/event/event-form/event-form.component";
import {TaskFormComponent} from "../../../activity-management/task/task-form/task-form.component";
import { IncidentListComponent } from "../../../activity-management/incident/incident-list/incident-list.component";
import {CreateContactComponent} from "../../../contacts-management/contact/create-contact/create-contact.component";


@Component({
  selector: 'app-prospect-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements OnInit {

  isEdit: boolean = true

  onEdit() {
    this.isEdit = !this.isEdit
    this.mode = 'ADD'
    this.onEnableForm();
  }

  // selected account type
  selectedAccountType: AccountType = AccountType.PP;


  // dialog window
  accountDialogRef: any = null;
  taskDialogRef: any = null;
  eventDialogRef: any = null;
  incidentDialogRef: any = null;
  opportuniteDialogRef: any = null;
  //accountDialogRef: any = null;
  accountDialogCloseSubscription: Subscription = null;
  taskDialogCloseSubscription: Subscription = null;
  eventDialogCloseSubscription: Subscription = null;
  incidentDialogCloseSubscription: Subscription = null;
  opportinuteDialogCloseSubscription: Subscription = null;
  newcontactMatDialogCloseSubscription: Subscription = null;
  // loading
  loading: boolean = false;
  loadingAccount: boolean = false;
  loadingForm: boolean = false;

  enableForm: boolean = false;

  // subscription
  routeSubscription: Subscription = null;


  mode: string = 'EDIT';

  readonly accountTypeList = [{
    code: AccountType.PP,
    label: this.translate.instant("NOUVEAU_COMPTE.PP")
  }, {code: AccountType.PM, label: this.translate.instant("NOUVEAU_COMPTE.PM")}];
  // Form group
  accountPPFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // instances
  @ViewChild(TaskListComponent)
  taskListComponent: TaskListComponent;

  @ViewChild(EventListComponent)
  eventListComponent: EventListComponent;

  @ViewChild(OpportuniteListComponent)
  opportuniteListComponent: OpportuniteListComponent;

  @ViewChild(ContactListComponent)
  contactListComponent: ContactListComponent;


  @ViewChild(IncidentListComponent)
  IncidentListComponent: IncidentListComponent;

  // filters
  globalFilter: ActivityGlobalFilterModel = null;
  contactFilter: ContactFilterModel = null;

  //// Listes
  secteurList: GenericItemModel[] = []; /// ---
  // compte pp list
  civiliteList: GenericItemModel[] = []; //
  nationaliteList: GenericItemModel[] = [];
  paysList: GenericItemModel[] = [];
  // compte pm list
  formeJuridiqueList: GenericItemModel[] = [];

  // Avatar image
  photoImg: string = './assets/img/avatar.png';
  photoFile: File = null;

  // account

  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;
  accountCode: string;
  accountType: AccountType = AccountType.PP;

  constructor(private accountService: AccountService,
              private toastrService: ToastrService,
              //private el: ElementRef,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private referenceService: ReferenceService,
              private translate: TranslateService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {


    this.routeSubscription = this.activatedRoute.paramMap.subscribe(params => {
      this.accountType = params.get('accountType') ? AccountType[params.get('accountType')?.toUpperCase()] : null;
      this.accountCode = params.get('accountCode') ? params.get('accountCode')?.toUpperCase() : null;

      //initialise tabs
      this.globalFilter = {...new ActivityGlobalFilterModel(), compte: this.accountCode, typeCompte: this.accountType};
      this.contactFilter = {...new ContactFilterModel(), accountCode: this.accountCode, accountType: this.accountType};

      if (this.accountType && this.accountCode) {
        switch (this.accountType) {
          case AccountType.PM: {
            this.loadAccountPMByCode(this.accountCode);
          }
            break;
          case AccountType.PP: {
            this.loadAccountPPByCode(this.accountCode);
          }
            break;
        }

      }

    });
  }


  ngOnInit(): void {
    this.createFormGroup();
    this.referenceService.getSectActivitesList().subscribe(value => this.secteurList = value);
    this.referenceService.getCivilites().subscribe(value => this.civiliteList = value);
    this.referenceService.getNationalites().subscribe(value => this.nationaliteList = value);
    this.referenceService.getPaysList().subscribe(value => this.paysList = value);
    this.referenceService.getFormesJuridiques().subscribe(value => this.formeJuridiqueList = value);
  }


  /** change type */

  onchangeType() {
    switch (this.accountType) {
      case AccountType.PP:
        this.createaccountPPFormGroup()
        break;
      case AccountType.PM:
        this.createaccountPMFormGroup()
    }
  }


  /** CREATE FORM GROUP */
  createFormGroup() {
    this.accountPPFormGroup = null;
    this.accountPMFormGroup = null;
    switch (this.accountType) {
      case AccountType.PP:
        this.createaccountPPFormGroup()
        break;
      case AccountType.PM:
        this.createaccountPMFormGroup()
    }
  }


  createaccountPPFormGroup() {
    this.accountPPFormGroup = this.fb.group({
      civilite: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      pieceIdentite: [null, [Validators.required]],
      numPi: [null, [Validators.required]],
      secteur: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      dateExpPI: [null, [Validators.required]],
      profission: [null, [Validators.required]],
      address: [null, [Validators.required]],
      nationality: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      email: [null, [Validators.required]],
    });
    if (this.mode === 'ADD') {
      this.accountPPFormGroup.enable();
    } else {
      this.accountPPFormGroup.disable();
    }
  }

  createaccountPMFormGroup() {
    this.accountPMFormGroup = this.fb.group({
      socialReason: [null, [Validators.required]],
      legalForm: [null, [Validators.required]],
      ice: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      fixe: [null, [Validators.required]],
      email: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      webSite: [null, [Validators.required]],
    });
    if (this.mode === 'ADD') {
      this.accountPMFormGroup.enable();
    } else {
      this.accountPMFormGroup.disable();
    }
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.accountPPFormGroup.patchValue({
            civilite: this.accountPP.civilite,
            codePostale: this.accountPP.codePostale,
            pays: this.accountPP.paysResidence,
            pieceIdentite: this.accountPP.pieceIdentite,
            numPi: this.accountPP.numPI,
            secteur: this.accountPP.secActivite,
            lastName: this.accountPP.prenom,
            firstName: this.accountPP.nom,
            dateExpPI: this.accountPP.dateExpPI,
            profission: this.accountPP.profession,
            address: this.accountPP.adresse1,
            nationality: this.accountPP.nationalite,
            mobile: this.accountPP.mobile1,
            email: this.accountPP.email,
          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.accountPMFormGroup.patchValue({
            socialReason: this.accountPM.raisonSociale,
            legalForm: this.accountPM.formeJuridique,
            ice: this.accountPM.ice,
            pays: this.accountPM.pays,
            address: this.accountPM.adresse1,
            phone: this.accountPM.mobile1,
            fixe: this.accountPM.fixe,
            email: this.accountPM.email,
            codePostale: this.accountPM.codePostale,
            webSite: this.accountPM.siteWeb,
          });
        }
    }

    // in case of update

  }

  /** ON ENABLE FORM */
  onEnableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.enable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.enable();
        }
    }
  }

  /** ON DISABLE FORM */
  onDisableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.disable();
        }
    }
  }

  /** CHECK FORM GROUP IS VALID OR NOT */
  isValidForm(): boolean {
    let isValid: boolean = false;
    switch (this.accountType) {
      case AccountType.PP:
        isValid = this.accountPPFormGroup && this.accountPPFormGroup.valid;
        break;
      case AccountType.PM:
        isValid = this.accountPMFormGroup && this.accountPMFormGroup.valid;
    }
    if (!isValid) {
      this.showValidationsMsg = true;
    }
    return isValid;
  }


  /** ON SUBMIT */
  onSubmit() {
    // generate payload
    let registerAccountPayload: RegisterAccountPayloadModel = new RegisterAccountPayloadModel();
    // account type
    registerAccountPayload.accountType = this.accountType;
    // account payload
    if (this.isValidForm()) {
      let accountPayload: AccountPPPayloadModel | AccountPMPayloadModel = this.generatePayload();
      switch (this.accountType) {
        case AccountType.PP: {
          registerAccountPayload.accountPP = accountPayload as AccountPPPayloadModel;
          break;
        }
        case AccountType.PM: {
          registerAccountPayload.accountPM = accountPayload as AccountPMPayloadModel;
        }
      }
      // save
      switch (this.accountType) {
        case AccountType.PP: {
          // update
          this.loadingForm = true;
          this.accountService.updateAccountPP(registerAccountPayload.accountPP)
            .pipe(finalize(() => this.loadingForm = false))
            .subscribe(response => {
              if (response && response.valid) {
                this.toastrService.success("Fiche compte modifié avec succés");
                this.onEdit();
                this.onDisableForm();
                this.loadAccountPPByCode(this.accountCode);
              } else {
                this.toastrService.warning("Un problème est survenu lors de modification de fiche compte");
              }
            })
          break;
        }
        case AccountType.PM: {
          // update
          this.loadingForm = true;
          this.accountService.updateAccountPM(registerAccountPayload.accountPM)
            .pipe(finalize(() => this.loadingForm = false))
            .subscribe(response => {
              if (response && response.valid) {
                this.toastrService.success("Fiche compte modifié avec succés");
                this.onEdit();
                this.onDisableForm();
                this.loadAccountPMByCode(this.accountCode);
              } else {
                this.toastrService.warning("Un problème est survenu lors de modification de fiche compte");
              }
            })
        }
      }
    }
  }


  /** ON RESET FORM */
  onResetForm() {
    this.showValidationsMsg = false;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.reset();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.reset();
        }
    }
  }


  /** GENERATE PAYLOAD */
  generatePayload(): AccountPPPayloadModel | AccountPMPayloadModel {
    let payload: AccountPPPayloadModel | AccountPMPayloadModel = null;

    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          payload = {
            ...new AccountPPPayloadModel(),
            code: this.accountCode.toUpperCase(),
            nom: this.accountPPFormGroup.get("lastName").value,
            prenom: this.accountPPFormGroup.get("firstName").value,
            mobile1: this.accountPPFormGroup.get("mobile").value,
            civilite: this.accountPPFormGroup.get("civilite").value,
            paysResidence: this.accountPPFormGroup.get("pays").value,
            nationalite: this.accountPPFormGroup.get("nationality").value,
            adresse1: this.accountPPFormGroup.get("address").value,
            email: this.accountPPFormGroup.get("email").value,
            codePostale: this.accountPPFormGroup.get("codePostale").value,
            profession: this.accountPPFormGroup.get("profission").value,
            pieceIdentite: this.accountPPFormGroup.get("pieceIdentite").value,
            numPI: this.accountPPFormGroup.get("numPi").value,
            dateExpPI: this.accountPPFormGroup.get("dateExpPI").value,
            secActivite: this.accountPPFormGroup.get("secteur").value,
            typeCompte: this.accountType,

          }

        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          payload = {
            ...new AccountPMPayloadModel(),
            code: this.accountCode.toUpperCase(),
            raisonSociale: this.accountPMFormGroup.get("socialReason").value,
            formeJuridique: this.accountPMFormGroup.get("legalForm").value,
            ice: this.accountPMFormGroup.get("ice").value,
            pays: this.accountPMFormGroup.get("pays").value,
            adresse1: this.accountPMFormGroup.get("address").value,
            mobile1: this.accountPMFormGroup.get("phone").value,
            fixe: this.accountPMFormGroup.get("fixe").value,
            email: this.accountPMFormGroup.get("email").value,
            codePostale: this.accountPMFormGroup.get("codePostale").value,
            siteWeb: this.accountPMFormGroup.get("webSite").value,
            typeCompte: this.accountType,
          }
        }

    }
    return payload;
  }


  /** LOAD ACCOUNT PP BY CODE */
  loadAccountPPByCode(accountCode: string): void {

    this.accountPP = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPPByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;
      }))
      .subscribe(response => {
        this.accountPP = response;
        this.initFormGroup();
      }, error => this.router.navigate(['/pages/prospects-management/prospects-consultation']))
  }

  /** LOAD ACCOUNT PM BY CODE */
  loadAccountPMByCode(accountCode: string): void {


    this.accountPM = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPMByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;

      }))
      .subscribe(response => {
        this.accountPM = response;
        this.initFormGroup();
      }, error => this.router.navigate(['/pages/prospects-management/prospects-consultation']))
  }

  redirect(index: number) {
    switch (index) {
      case 1:
        this.router.navigate(['/pages/prospects-management/prospect-consultation']);
        break;
      case 2:
        this.router.navigate(['/pages/prospects-management/onboarding', this.accountType, this.accountCode]);
        break;
      default:
        break;
    }
  }


  mapCodeToValue(key) {

    switch (key) {
      //PP
      case "civilite":
        return this.civiliteList.filter(elm => elm.code == (this.accountPP?.civilite))[0]?.value;
      case "pays": {
        if (this.accountType == AccountType.PP) {
          return this.paysList.filter(elm => elm.code == (this.accountPP?.paysResidence))[0]?.value;
        } else {
          return this.paysList.filter(elm => elm.code == (this.accountPM?.pays))[0]?.value;
        }
      }

      case "nationalite":
        return this.nationaliteList.filter(elm => elm.code == (this.accountPP?.nationalite))[0]?.value;
      case "secteur":
        return this.secteurList.filter(elm => elm.code == (this.accountPP?.secActivite))[0]?.value;
      case "typeAccount": {
        if (this.accountType == AccountType.PP) {
          return this.accountTypeList.filter(elm => elm.code == AccountType.PP)[0]?.label;
        } else {
          return this.accountTypeList.filter(elm => elm.code == AccountType.PM)[0]?.label;
        }
      }
      //PM
      case "pays":
        return this.paysList.filter(elm => elm.code == (this.accountPM?.pays))[0]?.value;
      case "legalForm":
        return this.formeJuridiqueList.filter(elm => elm.code == (this.accountPM?.formeJuridique))[0]?.value;
      default:
        break;
    }

  }

  /** ON ADD EXISTING CONTACT */
  onAddContact(): void {
    this.accountDialogRef = this.dialog.open(AddContactComponent, {
      panelClass: 'activity-popup',
      data: {
        code: this.accountCode,
        type: this.accountType,
        mode: 'AFFECT'
      }
    });
    this.accountDialogCloseSubscription = this.accountDialogRef.afterClosed()
      .subscribe(reload => {
        if (reload === true) {
          this.contactListComponent?.loadListContact();
        }
      });
  }

  /** ON ADD NEW CONTACT */
  onNewContact() {
    console.log('hello contatc');
    const dialogRef = this.dialog.open(CreateContactComponent, {
      panelClass: 'activity-popup',
      //width : '2000px',
      //maxHeight: '500px',
      data: {
        code: this.accountCode,
        type: this.accountType
      }
    });
    this.newcontactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.contactListComponent?.loadListContact();
      }
    });
  }

  openDialog(type: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {redirect: true, accountCode: this.accountCode,accountType:this.accountType};
    dialogConfig.panelClass = 'activity-popup';
    switch (type) {
      case 'EVENT': {
        this.eventDialogRef = this.dialog.open(EventFormComponent, dialogConfig);
        this.eventDialogCloseSubscription = this.eventDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.eventListComponent?.loadGlobalListOfEvents();
            }
          });
        break;
      }
      case 'TASK': {
        this.taskDialogRef = this.dialog.open(TaskFormComponent, dialogConfig);
        this.taskDialogCloseSubscription = this.taskDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.taskListComponent?.loadGlobalListOfTasks();
            }
          });
        break;
      }
      case 'TICKET': {
        this.incidentDialogRef = this.dialog.open(IncidentFormComponent, {...dialogConfig, height: '500px'});
        this.incidentDialogCloseSubscription = this.incidentDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.IncidentListComponent?.loadGlobalListOfIncidents();
            }
          });
        break;
      }
      case 'OPPORTUNITY': {
        this.opportuniteDialogRef = this.dialog.open(OpportuniteFormComponent, dialogConfig);
        this.opportinuteDialogCloseSubscription = this.opportuniteDialogRef.afterClosed()
          .subscribe((reload: any) => {
            if (reload.valid === true) {
              this.opportuniteListComponent?.loadGlobalListOfOpportunites();
            }
          });
        break;
      }
    }
  }
}
