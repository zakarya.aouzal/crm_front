import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'highcharts';
import { of } from 'rxjs';
import { distinct } from 'rxjs/operators';
import { AccountFilterModel, AccountType, Flag, GenericItemModel } from 'src/app/main/models';
import { ReferenceService } from 'src/app/main/services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-account-filter',
  templateUrl: './account-filter.component.html',
  styleUrls: ['./account-filter.component.scss']
})
export class AccountFilterComponent implements OnInit {


  _flag:Flag;

  @Input()
  set flag(value:Flag){
    this._flag=value;
  }

  @Output()
  submitted = new EventEmitter<{ filterBody: AccountFilterModel, accountType: AccountType }>();
  constructor(private fb: FormBuilder, private referenceService: ReferenceService,private translate:TranslateService) { }
  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  accountTypeList = [{code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP")}, {code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM")}];
  gestionnaireList: GenericItemModel[] = [];
  countryList: GenericItemModel[] = [];



  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.onSubmit();
    this.loadCountries();
    this.loadGestionnaires();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      accountType: [null, [Validators.required]],
      firstName: [null],
      lastName: [null],
      socialReason: [null],
      operateur_saisie: [null],
      phoneNumber: [null],
      creationDate: [null],
      country: [null],
      city: [null],
    });
  }

  translateLabel(elm:string):string{
    return this.translate.instant(elm);

  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
      let accountType: AccountType = this.accountTypeList.find(item => item.code === AccountType.PP).code;
      this.formGroup.get('accountType').setValue(accountType);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    let accountType: AccountType = this.formGroup.get('accountType').value ? this.formGroup.get('accountType').value : null;
    if (filter && accountType) {
      this.submitted.emit({ filterBody: filter, accountType: accountType });
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): AccountFilterModel {
    let filter: AccountFilterModel = null;

    if (this.formGroup) {

      filter = {
        ...new AccountFilterModel(),
        operateur_saisie: this.formGroup.get('operateur_saisie').value,
        mobile1: this.formGroup.get('phoneNumber').value,
        dateInsert: this.formGroup.get('creationDate').value,
        paysResidence: this.formGroup.get('country').value,
        ville: this.formGroup.get('city').value,
        flag:this._flag
      }

      let accountType = this.formGroup.get('accountType').value ? this.formGroup.get('accountType').value : null;

      switch (accountType) {
        case 'PP':
          filter.nom = this.formGroup.get('lastName').value;
          filter.prenom = this.formGroup.get('firstName').value;
          break;
        case 'PM':
          filter.raisonSociale = this.formGroup.get('socialReason').value;
          break;
      }

    }
    console.log('filteeeeeeeer',filter)
    return filter;

  }



  /** GET LIST OF COUNTRIES */
  loadCountries() {
    this.referenceService.getPaysList()
      .subscribe(response => {
        this.countryList = response;
      })
  }

  /** GET LIST OF ACCOUNT MANAGERS */
  loadGestionnaires() {
    this.referenceService.getGestionnaireFronts()
      .subscribe(response => {
        this.gestionnaireList = response;
      });
  }


  

  


}
