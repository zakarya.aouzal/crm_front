import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';



export const ACCOUNT_PP_LIST_SETTINGS_VALIDATION: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
            }
        }, {
            name: 'nom',
            title: 'Nom',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.NOM'
            },style:{
                orientation:"left"
                  }
        }, {
            name: 'prenom',
            title: 'Prénom',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PRENOM'
            },style:{
                orientation:"left"
                  }
        }, {
            name: 'dateNaissance',
            title: 'Date Naissance',
            format: DataTableColumnsFormat.DATE,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.DATE_NAISSANCE'
            }
        }, {
            name: 'gestionnaire',
            title: 'Gestionnaire',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.GESTIONNAIRE'
            }
        }, {
            name: 'paysResidence',
            title: 'Pays de résidence',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PAYS_RESIDENCE'
            }
        }, {
            name: 'mobile1',
            title: 'Téléphone',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.ACCOUNT_LIST.MOBILE'
            }
        }, {
            name: 'ville',
            title: 'Ville',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.VILLE'
            }
        }, {
            name: 'numPI',
            title: 'Num PI',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.NUMPI'
            },style:{
                orientation:"right"
                  }
        },{
            name: 'statutWL',
            title: 'Statut WL',
            format: DataTableColumnsFormat.ENUM,
            enum: [
              {value: 'R', text: 'Risqué'},
              {value: 'NR', text: 'Non risqué'}
            ],
            style: {
              colors: [{
                colorValue: 'red',
                conditionValue: 'R'
              }, {
                colorValue: 'green',
                conditionValue: 'NR'
              }]
            },
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.STATUT_WL'
            }
        }, {
            name: 'flagExport',
            title: 'flagExport',
            format: DataTableColumnsFormat.ENUM,
            enum: [
              {value: 'O', text: 'Exporté'},
              {value: 'N', text: 'Non exporté'}
            ],
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.FLAG_EXPORT'
            }
        }, {
            name: 'motifRejet',
            title: 'motifRejet',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.MOTIF'
            }
        }

    ], actions: [DataTableActions.VIEW]
};

export const ACCOUNT_PM_LIST_SETTINGS_VALIDATION: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
            }
        }, {
            name: 'raisonSociale',
            title: 'Raison Sociale',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.RAISON_SOCIALE'
            },style:{
                orientation:"left"
                  }
        }, {
            name: 'paysResidence',
            title: 'Pays de résidence',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.PAYS_RESIDENCE'
            }
        }, {
            name: 'gestionnaire',
            title: 'Gestionnaire',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.ACCOUNT_LIST.GESTIONNAIRE'
            }
        }, {
            name: 'rc',
            title: 'RC',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.RC'
            }
        }, {
            name: 'idFiscal',
            title: 'Id Fiscal',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.ID_FISCAL'
            }
        }, {
            name: 'ice',
            title: 'ICE',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.ICE'
            }
        }, {
            name: 'mobile1',
            title: 'Téléphone',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.ACCOUNT_LIST.MOBILE'
            }
        }, {
            name: 'ville',
            title: 'Ville',
            format: DataTableColumnsFormat.STRING,
            data:{
              i18nValue:'COLUMNS.ACCOUNT_LIST.VILLE'
            }
        }, {
            name: 'statutWL',
            title: 'Statut WL',
            format: DataTableColumnsFormat.ENUM,
            enum: [
              {value: 'R', text: 'Risqué'},
              {value: 'NR', text: 'Non risqué'}
            ],
            style: {
              colors: [{
                colorValue: 'red',
                conditionValue: 'R'
              }, {
                colorValue: 'green',
                conditionValue: 'NR'
              }]
            },
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.STATUT_WL'
            }
        }, {
            name: 'flagExport',
            title: 'flagExport',
            format: DataTableColumnsFormat.ENUM,
            enum: [
              {value: 'O', text: 'Exporté'},
              {value: 'N', text: 'Non exporté'}
            ],
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.FLAG_EXPORT'
            }
        }, {
            name: 'motifRejet',
            title: 'motifRejet',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.MOTIF'
            }
        }
    ], actions: [DataTableActions.VIEW]
};
