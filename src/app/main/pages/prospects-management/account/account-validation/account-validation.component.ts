import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountFilterModel, AccountType, Flag } from 'src/app/main/models';


@Component({
  selector: 'app-account-validation',
  templateUrl: './account-validation.component.html',
  styleUrls: ['./account-validation.component.scss']
})
export class AccountValidationComponent implements OnInit {

  filter: { filterBody: AccountFilterModel, accountType: AccountType } = null;
  visible:boolean=true;
  


  constructor(private router: Router) { }

  flag:Flag=Flag.VALIDATION;


  // onVisible(){
  //   this.visible=!this.visible;
  // }

  redirect(index:number){
    switch (index) {
      case 1:
        this.router.navigate(['/pages/prospects-management/new-prospect']);
        break;
      case 2:
        this.router.navigate(['/pages/prospects-management/prospect-consultation']);
        break;
    
      default:
        break;
    }
  }

  ngOnInit(): void {
  }

}
