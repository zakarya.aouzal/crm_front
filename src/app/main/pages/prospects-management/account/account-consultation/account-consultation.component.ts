import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountFilterModel, AccountType, Flag } from 'src/app/main/models';

@Component({
  selector: 'app-account-consultation',
  templateUrl: './account-consultation.component.html',
  styleUrls: ['./account-consultation.component.scss']
})
export class AccountConsultationComponent implements OnInit {

  filter: { filterBody: AccountFilterModel, accountType: AccountType } = null;

 visible:boolean=false;
 flag:string=Flag.CONSULTATION;
  constructor(private router: Router) { }


  onVisible(){
    this.visible=!this.visible;
  }

  // onChangeFilter(value: { filterBody: AccountFilterModel, accountType: AccountType }){

  //   this.filter.filterBody=value.filterBody;
  //   this.filter.accountType=value.accountType;
  // }

  redirect(index:number){
    switch (index) {
      case 1:
        this.router.navigate(['/pages/prospects-management/new-prospect']);
        break;
      case 2:
        this.router.navigate(['/pages/prospects-management/prospect-consultation']);
        break;
    
      default:
        break;
    }
  }

  ngOnInit(): void {
  }

}
