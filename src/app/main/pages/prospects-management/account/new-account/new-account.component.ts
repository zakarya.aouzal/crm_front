import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {finalize} from 'rxjs/operators';
import {
  AccountPMModel,
  AccountPMPayloadModel,
  AccountPPModel,
  AccountPPPayloadModel,
  AccountType,
  GenericItemModel,
  RegisterAccountPayloadModel
} from 'src/app/main/models';
import {AccountService, ReferenceService} from 'src/app/main/services';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-prospect',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements OnInit {


  // selected account type
  selectedAccountType: AccountType = AccountType.PP;

  // loading
  loading: boolean = false;

  @Input()
  mode: 'ADD' | 'EDIT' = 'ADD';

  readonly accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];
  // Form group
  accountPPFormGroup: FormGroup = null;
  TypeFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;
  showValidationsMsg: boolean = false;


  //// Listes
  secteurList: GenericItemModel[] = [];
  // compte pp list
  civiliteList: GenericItemModel[] = [];
  nationaliteList: GenericItemModel[] = [];
  paysList: GenericItemModel[] = [];
  // compte pm list
  formeJuridiqueList: GenericItemModel[] = [];

   // Avatar image
   photoImg: string = './assets/img/avatar.png';
   photoFile: File = null;


  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;

  constructor(
    private accountService: AccountService,
    private toastrService: ToastrService,
    private el: ElementRef,
    private fb: FormBuilder,
    private referenceService: ReferenceService,
    private translate: TranslateService,
    private router: Router) {
  }

  onDownload() {
    this.el.nativeElement.querySelector('#imageUpload').click();
  }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.referenceService.getSectActivitesList().subscribe(value => this.secteurList = value);
    this.referenceService.getCivilites().subscribe(value => this.civiliteList = value);
    this.referenceService.getNationalites().subscribe(value => this.nationaliteList = value);
    this.referenceService.getPaysList().subscribe(value => this.paysList = value);
    this.referenceService.getFormesJuridiques().subscribe(value => this.formeJuridiqueList = value);
  }

  get accountType():AccountType{
    return this.TypeFormGroup.get("selectedAccountType").value;
  }


  /** change type */
  onchangeType(){
    switch (this.accountType) {
      case AccountType.PP:
        this.createaccountPPFormGroup();
        break;
      case AccountType.PM:
        this.createaccountPMFormGroup();
    }
  }

   /** CREATE FORM GROUP */
   createFormGroup() {
    this.accountPPFormGroup = null;
    this.accountPMFormGroup = null;
    this.createTypeFormGroup()
    switch (this.accountType) {
      case AccountType.PP:
        this.createaccountPPFormGroup()
        break;
      case AccountType.PM:
        this.createaccountPMFormGroup()
    }
  }

      createTypeFormGroup(){
        this.TypeFormGroup = this.fb.group({
          selectedAccountType: [AccountType.PP, [Validators.required]],
        })
      }
      createaccountPPFormGroup(){
        this.accountPPFormGroup = this.fb.group({
          civilite: [null, [Validators.required]],
          codePostale: [null, [Validators.required]],
          pays: [null, [Validators.required]],
          pieceIdentite: [null, [Validators.required]],
          numPi: [null, [Validators.required]],
          secteur: [null, [Validators.required]],
          lastName: [null, [Validators.required]],
          firstName: [null, [Validators.required]],
          dateExpPI: [null, [Validators.required]],
          profission:[null, [Validators.required]],
          address: [null, [Validators.required]],
          nationality: [null, [Validators.required]],
          mobile: [null, [Validators.required]],
          email: [null, [Validators.required]],
        });
        if (this.mode === 'ADD') {
          this.accountPPFormGroup.enable();
        } else {
          this.accountPPFormGroup.disable();
        }
      }
      createaccountPMFormGroup(){
        this.accountPMFormGroup = this.fb.group({
          socialReason: [null, [Validators.required]],
          legalForm: [null, [Validators.required]],
          ice: [null, [Validators.required]],
          pays: [null, [Validators.required]],
          address: [null, [Validators.required]],
          phone: [null, [Validators.required]],
          fixe: [null, [Validators.required]],
          email: [null, [Validators.required]],
          codePostale: [null, [Validators.required]],
          webSite: [null, [Validators.required]],
        });
        if (this.mode === 'ADD') {
          this.accountPMFormGroup.enable();
        } else {
          this.accountPMFormGroup.disable();
        }
      }

  /** INIT FORM GROUP */
  initFormGroup() {

    this.TypeFormGroup.patchValue({
      selectedAccountType:AccountType.PP
    })

    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.accountPPFormGroup.patchValue({
          civilite: this.accountPP.civilite,
          codePostale: this.accountPP.codePostale,
          pays: this.accountPP.paysResidence,
          pieceIdentite: this.accountPP.pieceIdentite,
          numPi: this.accountPP.numPI,
          secteur: this.accountPP.secActivite,
          lastName: this.accountPP.prenom,
          firstName: this.accountPP.nom,
          dateExpPI: this.accountPP.dateExpPI,
          profission:this.accountPP.profession,
          address: this.accountPP.adresse1,
          nationality: this.accountPP.nationalite,
          mobile: this.accountPP.mobile1,
          email: this.accountPP.email,
          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.accountPMFormGroup.patchValue({
            socialReason: this.accountPM.raisonSociale,
            legalForm: this.accountPM.formeJuridique,
            ice: this.accountPM.ice,
            pays: this.accountPM.paysResidenceFiscale,
            address: this.accountPM.adresse1,
            phone: this.accountPM.mobile1,
            fixe: this.accountPM.fixe,
            email: this.accountPM.email,
            codePostale: this.accountPM.codePostale,
            webSite: this.accountPM.siteWeb,
          });
        }
    }
  }

  /** ON ENABLE FORM */
  onEnableForm() {
    switch (this.TypeFormGroup.get("selectedAccountType").value) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.enable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.enable();
        }
    }
  }

  /** ON DISABLE FORM */
  onDisableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.disable();
        }
    }
  }
 /** CHECK FORM GROUP IS VALID OR NOT */
 isValidForm(): boolean {
  let isValid: boolean = false;
  switch (this.accountType) {
    case AccountType.PP:
      isValid = this.accountPPFormGroup && this.accountPPFormGroup.valid;
      break;
    case AccountType.PM:
      isValid = this.accountPMFormGroup && this.accountPMFormGroup.valid;
  }
  if (!isValid) {
    this.showValidationsMsg = true;
  }
  return isValid;
}


  /** ON SUBMIT */
  onSubmit() {
    // generate payload
    let registerAccountPayload: RegisterAccountPayloadModel = new RegisterAccountPayloadModel();
    // account type
    registerAccountPayload.accountType = this.accountType;
    console.log('type   :  '+this.accountType);
    // account payload
    if (this.isValidForm()) {
      this.loading = true;
      let accountPayload: AccountPPPayloadModel | AccountPMPayloadModel = this.generatePayload();
      switch (this.accountType) {
        case AccountType.PP: {
          registerAccountPayload.accountPP = accountPayload as AccountPPPayloadModel;
          break;
        }
        case AccountType.PM: {
          registerAccountPayload.accountPM = accountPayload as AccountPMPayloadModel;
        }
      }
      // save
      this.accountService.registerAccount(registerAccountPayload)
        .pipe(finalize(() => {
          this.onResetForm();
          this.loading = false;
        }))
        .subscribe(response => {
          if (response && response.valid) {
            this.toastrService.success("Prespect ajouté avec succés");
            this.router.navigate(['/pages/prospects-management/prospect-consultation']);

          } else {
            this.toastrService.warning("Un problème est survenu lors d'ajout de compte");
          }
        });
    }
  }


  /** ON RESET FORM */
  onResetForm() {
    this.showValidationsMsg = false;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.reset();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.reset();
        }
    }
  }


  /** GENERATE PAYLOAD */
  generatePayload(): AccountPPPayloadModel | AccountPMPayloadModel {
    let payload: AccountPPPayloadModel | AccountPMPayloadModel = null;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          payload = {
            ...new AccountPPPayloadModel(),
            nom: this.accountPPFormGroup.get("lastName").value,
            prenom: this.accountPPFormGroup.get("firstName").value,
            mobile1: this.accountPPFormGroup.get("mobile").value,
            civilite: this.accountPPFormGroup.get("civilite").value,
            paysResidence: this.accountPPFormGroup.get("pays").value,
            nationalite: this.accountPPFormGroup.get("nationality").value,
            adresse1: this.accountPPFormGroup.get("address").value,
            email: this.accountPPFormGroup.get("email").value,
            codePostale: this.accountPPFormGroup.get("codePostale").value,
            profession:this.accountPPFormGroup.get("profission").value,
            pieceIdentite:this.accountPPFormGroup.get("pieceIdentite").value,
            numPI: this.accountPPFormGroup.get("numPi").value,
            dateExpPI: this.accountPPFormGroup.get("dateExpPI").value,
            secActivite: this.accountPPFormGroup.get("secteur").value,

          }
          if (this.mode === 'EDIT' && this.accountPP) {
            payload.code = this.accountPP.code;
          }
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          payload = {
            ... new AccountPMPayloadModel(),
            raisonSociale: this.accountPMFormGroup.get("socialReason").value,
            formeJuridique: this.accountPMFormGroup.get("legalForm").value,
            ice: this.accountPMFormGroup.get("ice").value,
            pays: this.accountPMFormGroup.get("pays").value,
            adresse1: this.accountPMFormGroup.get("address").value,
            mobile1: this.accountPMFormGroup.get("phone").value,
            fixe: this.accountPMFormGroup.get("fixe").value,
            email: this.accountPMFormGroup.get("email").value,
            codePostale: this.accountPMFormGroup.get("codePostale").value,
            siteWeb: this.accountPMFormGroup.get("webSite").value,
          }
        }
        if (this.mode === 'EDIT' && this.accountPM) {
          payload.code = this.accountPM.code;
        }
    }
    return payload;
  }


  redirect(index:number){
    switch (index) {

      case 1:
        this.router.navigate(['/pages/prospects-management/prospect-consultation']);
        break;

      default:
        break;
    }
  }

    /** PHOTO SELECTION */
    // onFileSelected(event) {
    //   this.photoFile = event.target.files[0];
    //   if (!this.photoFile)
    //     return;

    //   let imgWidth, imgHeight;
    //   let img = new Image();
    //   img.src = URL.createObjectURL(this.photoFile);
    //   return new Promise<any>((resolve, reject) => {
    //     img.onload = (e: any) => {
    //       imgHeight = e.path[0].height;
    //       imgWidth = e.path[0].width;
    //       // run control on photo
    //       if ((imgHeight === this.photoControl?.defaultHeight) && (imgWidth === this.photoControl?.defaultWidth)) {
    //         this.photoSubmit(this.photoFile);
    //       } else {
    //         this.toastr.warning('l\'image ne respecte pas les dimensions recommendés');
    //       }
    //     }
    //   });
    // }

    // photoSubmit(photo: File): void {
    //   const reader = new FileReader();
    //   reader.onload = () => this.photoImg = reader.result as any;
    //   reader.readAsDataURL(photo);
    // }

}
