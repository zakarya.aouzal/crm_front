import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { PageNotFoundComponent } from 'src/app/shared/components/page-not-found/page-not-found.component';
import { RoleEnum,PermissionEnum } from '../../models';
import { AccountConsultationComponent } from './account/account-consultation/account-consultation.component';
import { AccountDetailsComponent } from './account/account-details/account-details.component';
import { ControlSummaryComponent } from './account/account-validation-controle/control-summary.component';
import { AccountValidationComponent } from './account/account-validation/account-validation.component';
import { NewAccountComponent } from "./account/new-account/new-account.component";
import { OnboardingComponent } from './onboarding/onboarding/onboarding.component';

const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: {
        label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.TITLE'
      }
    },
    children: [
      {
        path: 'prospect-consultation',
        component: AccountConsultationComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.PROSPECT_CONSULTATION'
          }
        },
      },
      {
        path: 'prospect-validation',
        component: AccountValidationComponent,
        canActivate: [NgxPermissionsGuard],
        data: {
            permissions: {
              only: RoleEnum.CONTROLEUR,
              redirectTo: '/pages/not-authorized'
              
      },
          breadcrumb: {
            label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.PROSPECT_VALIDATION'
          },
          
        },
      },
      {
        path: 'prospect-control-validation/:accountType/:accountCode',
        component: ControlSummaryComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.PROSPECT_VALIDATION'
          }
        },
      },{
        path: 'prospect-details/:accountType/:accountCode',
        component: AccountDetailsComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.PROSPECT_DETAILS'
          }
        },
      }, {
        path: 'new-prospect',
        component: NewAccountComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.NEW_PROSPECT'
          }
        },
      },
      {
        path: 'onboarding/:accountType/:accountCode',
        component: OnboardingComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.PROSPECT_MANAGEMENT.NEW_PROSPECT'
          }
        },
      }, 
       {
        path: '',
        redirectTo: 'prospect-consultation',
        pathMatch: 'full'
      }, {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProspectsManagementRoutingModule { }
