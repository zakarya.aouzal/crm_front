import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from '@iplab/ngx-file-upload';
import {SharedModule} from 'src/app/shared/shared.module';
import {ProspectsManagementRoutingModule} from './prospects-management-routing.module';
import {AccountConsultationComponent} from './account/account-consultation/account-consultation.component';
import {AccountDetailsComponent} from './account/account-details/account-details.component';
import {AccountFilterComponent} from './account/account-filter/account-filter.component';
import {AccountListComponent} from './account/account-list/account-list.component';
import {NewAccountComponent} from './account/new-account/new-account.component';
import {BsModalService, ModalModule} from "ngx-bootstrap/modal";
import { ContactsManagementModule } from '../contacts-management/contacts-management.module';
import { DocumentsManagementModule } from '../documents-management/documents-management.module';
import { ActivityManagementModule } from '../activity-management/activity-management.module';
import { AttachmentService, ContactService, GedService, HistoryActivityService } from '../../services';
import { MaterialModule } from '../material.module';
import { ChartModule } from '../chart/chart.module';
import {OnboardingComponent} from './onboarding/onboarding/onboarding.component';
import {InfosGeneralComponent} from './onboarding/infos-general/infos-general.component'
import {ActivitiesFormComponent} from './onboarding/activities-form/activities-form.component'
import {ContactsFormComponent} from './onboarding/contacts-form/contacts-form.component'
import {AccountValidationComponent} from './account/account-validation/account-validation.component'
import {WatchListComponent} from './onboarding/watch-list/watch-list.component'
import { WatchlistService } from '../../services/kyc/account-management/watchlist/watchlist.service';
import {OnboardingSummaryComponent} from "./onboarding/onboarding-summary/onboarding-summary.component";
import {ControlSummaryComponent} from "src/app/main/pages/prospects-management/account/account-validation-controle/control-summary.component"
import {AccountListValidationComponent} from "./account/account-list-validation/account-list.component"
@NgModule({
  declarations: [
    AccountConsultationComponent,
    AccountFilterComponent,
    AccountListComponent,
    AccountDetailsComponent,
    NewAccountComponent,
    OnboardingComponent,
    OnboardingSummaryComponent,
    InfosGeneralComponent,
    ActivitiesFormComponent,
    ContactsFormComponent,
    WatchListComponent,
    AccountValidationComponent,
    ControlSummaryComponent,
    AccountListValidationComponent
  ],
  imports: [
    ProspectsManagementRoutingModule,
    ContactsManagementModule,
    ActivityManagementModule,
    DocumentsManagementModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule,
    MaterialModule,
    ChartModule,
    ModalModule.forRoot()
  ],
  exports: [
    ControlSummaryComponent
  ],
  providers: [
    GedService,
    AttachmentService,
    HistoryActivityService,
    WatchlistService,
    ContactService
  ]
})
export class ProspectsManagementModule { }
