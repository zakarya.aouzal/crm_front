import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutsModule } from './../../layouts/layouts.module';
import { SharedModule } from './../../shared/shared.module';
import { ChartModule } from './chart/chart.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import {TranslatePipe} from "@ngx-translate/core";
import {MaterialModule} from './material.module'
import { NgxPermissionsModule } from 'ngx-permissions';
import { CarnetOrdreConsultationComponent } from './carnet-ordre-management/carnet-ordre/carnet-ordre-consultation/carnet-ordre-consultation.component';
import { CarnetOrdreFilterComponent } from './carnet-ordre-management/carnet-ordre/carnet-ordre-filter/carnet-ordre-filter.component';
import { CarnetOrdreListComponent } from './carnet-ordre-management/carnet-ordre/carnet-ordre-list/carnet-ordre-list.component';


@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    PagesRoutingModule,
    LayoutsModule,
    SharedModule,
    ChartModule,
   NgxPermissionsModule.forChild()
  ],
  providers: [
    TranslatePipe
  ]
})
export class PagesModule { }
