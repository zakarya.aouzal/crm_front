import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from '@iplab/ngx-file-upload';
import { ClientsManagementRoutingModule} from './clients-management-routing.module';
import {AccountConsultationComponent} from './account/account-consultation/account-consultation.component';
import {AccountDetailsComponent} from './account/account-details/account-details.component';
import {AccountFilterComponent} from './account/account-filter/account-filter.component';
import {AccountListComponent} from './account/account-list/account-list.component';
import {NewAccountComponent} from './account/new-account/new-account.component';
import {EspecesAccountComponent} from './especes-account/especes-account.component';
import { ModalModule} from "ngx-bootstrap/modal";
import {SharedModule} from '../../../shared/shared.module'
import {  AttachmentService, CompteEspService, GedService, HistoryActivityService } from '../../services';
import { ContactsManagementModule } from '../contacts-management/contacts-management.module';
import { DocumentsManagementModule } from '../documents-management/documents-management.module';
import { ActivityManagementModule } from '../activity-management/activity-management.module';
import { MaterialModule } from '../material.module';
import { ChartModule } from '../chart/chart.module';
import {WatchlistService} from "../../services/kyc/account-management/watchlist/watchlist.service";
import {ExpiredDocumentComponent} from "./account/expired-document/expired-document.component";
import { ClientVerificationComponent } from './account/client-verification/client-verification.component';
import { KycModule } from '../kyc/kyc.module';
import { ControlSummaryComponent } from 'src/app/main/pages/clients-management/account/account-summary/control-summary.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AccountConsultationComponent,
    AccountFilterComponent,
    AccountListComponent,
    AccountDetailsComponent,
    NewAccountComponent,
    ExpiredDocumentComponent,
    ClientVerificationComponent,
    ControlSummaryComponent,
    EspecesAccountComponent
  ],
  imports: [
    KycModule,
    NgbModule,
    ClientsManagementRoutingModule,
    ContactsManagementModule,
    ActivityManagementModule,
    DocumentsManagementModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FileUploadModule,
    MaterialModule,
    ChartModule,
    ModalModule.forRoot()
  ],
  exports: [
    ExpiredDocumentComponent
  ],
  providers: [
    GedService,
    AttachmentService,
    HistoryActivityService,
    WatchlistService,
    CompteEspService
  ]
})
export class ClientsManagementModule { }
