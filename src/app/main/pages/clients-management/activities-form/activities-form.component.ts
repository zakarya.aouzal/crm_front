import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountPMModel, AccountPMPayloadModel, AccountPPModel, AccountPPPayloadModel, AccountType, GenericItemModel } from 'src/app/main/models';
import { ReferenceService } from 'src/app/main/services';

@Component({
  selector: 'app-activities-form',
  templateUrl: './activities-form.component.html',
  styleUrls: ['./activities-form.component.scss']
})
export class ActivitiesFormComponent implements OnInit {

  @Input()
  mode: 'ADD' | 'EDIT' = 'ADD';

  @Input()
  set accountInfo(value: { accountType: AccountType, accountPP: AccountPPModel, accountPM: AccountPMModel }) {
    this.accountType = null;
    this.accountPP = null;
    this.accountPM = null;
    if (value) {
      this.accountType = value.accountType;
      this.accountPP = value.accountPP;
      this.accountPM = value.accountPM;
      this.createFormGroup();
      this.initFormGroup();
    }
  }

  // Form group
  accountPPFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // loaidng
  loading: boolean = false;

  // List
  typeProduits:GenericItemModel[] = [];
  sectActivitesList: GenericItemModel[] = [];
  horizonPlacementList: GenericItemModel[] = [];
  objectifInvList: GenericItemModel[] = [];
  profilInvList: GenericItemModel[] = [];
  originesFondsList: GenericItemModel[] = [];
  niveauInvList: GenericItemModel[] = [];
  categorieList: GenericItemModel[] = [];

  universInvtList: GenericItemModel[] = [];
  instrumentsList: GenericItemModel[] = [];
  selectedTypeProduit: string = '';
  selectedInstruments: string[] = [];

  // Account
  accountType: AccountType = null;
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;

  constructor(private fb: FormBuilder,private referenceService:ReferenceService) { }

  ngOnInit(): void {
    this.referenceService.getTypeProduits().subscribe(value => this.typeProduits = value);
    this.referenceService.getSectActivitesList().subscribe(value => this.sectActivitesList = value);
    this.referenceService.getHorizonPlacement().subscribe(value => this.horizonPlacementList = value);
    this.referenceService.getObjectifInvList().subscribe(value => this.objectifInvList = value);
    this.referenceService.getProfilInvList().subscribe(value => this.profilInvList = value);
    this.referenceService.getOriginesFondsList().subscribe(value => this.originesFondsList = value);
    this.referenceService.getNiveauInvList().subscribe(value => this.niveauInvList = value);
    this.referenceService.getCategories().subscribe(value => this.categorieList = value);
  }

  typeProduitsLoad(event): void {
    this.selectedTypeProduit = event[0].value;
    this.referenceService.getUniversInvList(this.selectedTypeProduit).subscribe(value => this.universInvtList = value);
  }

  universInvtLoad(event): void {
    this.selectedInstruments = [];
    event.forEach(el => this.selectedInstruments.push(el.value));
    this.referenceService.getInstrumentsList(this.selectedTypeProduit, this.listJoin(this.selectedInstruments)).subscribe(value => this.instrumentsList = value);
  }

  private listJoin(list): string {
    return list?.join(',');
  }
  private listUnjoin(str): [] {
    return str?.split(',');
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.accountPPFormGroup = null;
    this.accountPMFormGroup = null;
    switch (this.accountType) {
      case AccountType.PP:
        this.accountPPFormGroup = this.fb.group({
          profession: [null, [Validators.required]],
          secActivite: [null],
          capaciteFinanciere: [null, [Validators.required]],
          origineFonds: [null],
          niveauInv: [null],
          typeProduit:[null],
          produitSouhaites: [null],
          horizonPlacement: [null],
          secteurSouhaites: [null],
          objectifInvestissement: [null],
          profilInvestissement: [null],
          nationaliteUSA: [null],
          compteBancaireUSA: [null],
          adresseUSA: [null],
          greenCardUSA: [null],
          idFiscalUSA: [null],
          ligneTelephoneUSA: [null]
        });
        if (this.mode === 'ADD') {
          this.accountPPFormGroup.enable();
        } else {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        this.accountPMFormGroup = this.fb.group({
          categorie: [null],
          secActivite: [null],
          capaciteFinanciere: [null, [Validators.required]],
          produitSouhaites: [null],
          niveauInv: [null],
          typeProduit:[null, [Validators.required]],
          secteurSouhaites: [null],
          horizonPlacement: [null],
          profilInvestissement: [null],
          objectifInvestissement: [null],
          paysResidenceFiscale: [null],
          compteBancaireUSA: [null],
          adresseUSA: [null],
          villeFiscaleUSA: [null],
          idFiscalUSA: [null],
          ligneTelephoneUSA: [null],
          giin: [null]
        });
        if (this.mode === 'ADD') {
          this.accountPMFormGroup.enable();
        } else {
          this.accountPMFormGroup.disable();
        }
        break;
    }
  }

  private loadUniversInstru(type: string, produits: string): void {
    this.referenceService.getUniversInvList(type).subscribe(value => this.universInvtList = value);
    this.referenceService.getInstrumentsList(type, produits).subscribe(value => this.instrumentsList = value)
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.loadUniversInstru(this.accountPP.typeProduit, this.accountPP.produitSouhaites);
          this.accountPPFormGroup.patchValue({
            profession: this.accountPP.profession,
            secActivite: this.accountPP.secActivite,
            capaciteFinanciere: this.accountPP.capaciteFinanciere,
            origineFonds: this.accountPP.origineFonds,
            niveauInv: this.accountPP.niveauInv,
            typeProduit:this.accountPP.typeProduit,
            produitSouhaites: this.listUnjoin(this.accountPP.produitSouhaites),
            horizonPlacement: this.accountPP.horizonPlacement,
            secteurSouhaites: this.accountPP.secteurSouhaites,
            objectifInvestissement: this.accountPP.objectifInvestissement,
            profilInvestissement: this.accountPP.profilInvestissement,
            nationaliteUSA: this.accountPP.nationaliteUSA,
            compteBancaireUSA: this.accountPP.compteBancaireUSA,
            adresseUSA: this.accountPP.adresseUSA,
            greenCardUSA: this.accountPP.greenCardUSA,
            idFiscalUSA: this.accountPP.idFiscalUSA,
            ligneTelephoneUSA: this.accountPP.ligneTelephoneUSA
          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.loadUniversInstru(this.accountPM.typeProduit, this.accountPM.produitSouhaites);
          this.accountPMFormGroup.patchValue({
            categorie: this.accountPM.categorie,
            secActivite: this.accountPM.secActivite,
            capaciteFinanciere: this.accountPM.capaciteFinanciere,
            typeProduit:this.accountPM.typeProduit,
            produitSouhaites: this.listUnjoin(this.accountPM.produitSouhaites),
            niveauInv: this.accountPM.niveauInv,
            secteurSouhaites: this.accountPM.secteurSouhaites,
            horizonPlacement: this.accountPM.horizonPlacement,
            profilInvestissement: this.accountPM.profilInvestissement,
            objectifInvestissement: this.accountPM.objectifInvestissement,
            paysResidenceFiscale: this.accountPM.paysResidenceFiscale,
            compteBancaireUSA: this.accountPM.compteBancaireUSA,
            adresseUSA: this.accountPM.adresseUSA,
            villeFiscaleUSA: this.accountPM.villeFiscaleUSA,
            idFiscalUSA: this.accountPM.idFiscalUSA,
            ligneTelephoneUSA: this.accountPM.ligneTelephoneUSA,
            giin: this.accountPM.giin
          });
        }
        break;
    }
  }

  /** ON ENABLE FORM */
  onEnableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.enable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.enable();
        }
    }
  }

  /** ON DISABLE FORM */
  onDisableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.disable();
        }
    }
  }

  /** ON RESET FORM */
  onResetForm() {
    this.showValidationsMsg = false;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.reset();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.reset();
        }
    }
  }

  /** CHECK FORM GROUP IS VALID OR NOT */
  isValidForm(): boolean {
    let isValid: boolean = false;
    switch (this.accountType) {
      case AccountType.PP:
        isValid = this.accountPPFormGroup && this.accountPPFormGroup.valid;
        break;
      case AccountType.PM:
        isValid = this.accountPMFormGroup && this.accountPMFormGroup.valid;
    }
    if (!isValid) {
      this.showValidationsMsg = true;
    }
    return isValid;
  }

  /** GENERATE PAYLOAD */
  generatePayload(): AccountPPPayloadModel | AccountPMPayloadModel {
    let payload: AccountPPPayloadModel | AccountPMPayloadModel = null;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          payload = {
            ...new AccountPPPayloadModel(),
            profession: this.accountPPFormGroup.get("profession").value,
            secActivite: this.accountPPFormGroup.get("secActivite").value,
            capaciteFinanciere: this.accountPPFormGroup.get("capaciteFinanciere").value,
            origineFonds: this.accountPPFormGroup.get("origineFonds").value,
            niveauInv: this.accountPPFormGroup.get("niveauInv").value,
            produitSouhaites: this.listJoin(this.accountPPFormGroup.get("produitSouhaites").value),
            typeProduit: this.accountPPFormGroup.get("typeProduit").value,
            horizonPlacement: this.accountPPFormGroup.get("horizonPlacement").value,
            secteurSouhaites: this.accountPPFormGroup.get("secteurSouhaites").value,
            objectifInvestissement: this.accountPPFormGroup.get("objectifInvestissement").value,
            profilInvestissement: this.accountPPFormGroup.get("profilInvestissement").value,
            nationaliteUSA: this.accountPPFormGroup.get("nationaliteUSA").value,
            compteBancaireUSA: this.accountPPFormGroup.get("compteBancaireUSA").value,
            adresseUSA: this.accountPPFormGroup.get("adresseUSA").value,
            greenCardUSA: this.accountPPFormGroup.get("greenCardUSA").value,
            idFiscalUSA: this.accountPPFormGroup.get("idFiscalUSA").value,
            ligneTelephoneUSA: this.accountPPFormGroup.get("ligneTelephoneUSA").value
          }
          if (this.mode === 'EDIT' && this.accountPP) {
            payload.code = this.accountPP.code;
          }
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          payload = {
            ... new AccountPMPayloadModel(),
            categorie: this.accountPMFormGroup.get("categorie").value,
            secActivite: this.accountPMFormGroup.get("secActivite").value,
            capaciteFinanciere: this.accountPMFormGroup.get("capaciteFinanciere").value,
            produitSouhaites: this.listJoin(this.accountPMFormGroup.get("produitSouhaites").value),
            niveauInv: this.accountPMFormGroup.get("niveauInv").value,
            typeProduit: this.accountPMFormGroup.get("typeProduit").value,
            secteurSouhaites: this.accountPMFormGroup.get("secteurSouhaites").value,
            horizonPlacement: this.accountPMFormGroup.get("horizonPlacement").value,
            profilInvestissement: this.accountPMFormGroup.get("profilInvestissement").value,
            objectifInvestissement: this.accountPMFormGroup.get("objectifInvestissement").value,
            paysResidenceFiscale: this.accountPMFormGroup.get("paysResidenceFiscale").value,
            compteBancaireUSA: this.accountPMFormGroup.get("compteBancaireUSA").value,
            adresseUSA: this.accountPMFormGroup.get("adresseUSA").value,
            villeFiscaleUSA: this.accountPMFormGroup.get("villeFiscaleUSA").value,
            idFiscalUSA: this.accountPMFormGroup.get("idFiscalUSA").value,
            ligneTelephoneUSA: this.accountPMFormGroup.get("ligneTelephoneUSA").value,
            giin: this.accountPMFormGroup.get("giin").value
          }
          if (this.mode === 'EDIT' && this.accountPM) {
            payload.code = this.accountPM.code;
          }
        }
    }
    return payload;
  }

}
