import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../../../shared/components/page-not-found/page-not-found.component';
import { AccountConsultationComponent } from './account/account-consultation/account-consultation.component';
import { AccountDetailsComponent } from './account/account-details/account-details.component';
import {ClientVerificationComponent} from "./account/client-verification/client-verification.component";


const routes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: {
        label: 'BREADCRUMBS.CLIENT_MANAGEMENT.TITLE'
      }
    },
    children: [
      {
        path: 'client-consultation',
        component: AccountConsultationComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.CLIENT_MANAGEMENT.CLIENT_CONSULTATION'
          }
        },
      }, {
        path: 'clients-verification',
        component: ClientVerificationComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.CLIENT_MANAGEMENT.CLIENT_VERIFICATION'
          }
        },
      }, {
        path: 'client-details/:accountType/:accountCode',
        component: AccountDetailsComponent,
        data: {
          breadcrumb: {
            label: 'BREADCRUMBS.CLIENT_MANAGEMENT.CLIENT_DETAILS'
          }
        },
      }, {
        path: '',
        redirectTo: 'client-consultation',
        pathMatch: 'full'
      }, {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ClientsManagementRoutingModule { }
