import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from '../../../../shared/models';

export const COMPTES_ESP_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
      {
        name: 'banque',
        title: 'Banque',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.COMPTE_ESP.BANQUE'
        }
      }, {
        name: 'ville',
        title: 'Ville',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.COMPTE_ESP.VILLE'
        }
      }, {
        name: 'agence',
        title: 'Agence',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.COMPTE_ESP.AGENCE'
        }
      },
      // {
      //   name: 'descriptionFonds',
      //   title: 'Desc Fonds',
      //   format: DataTableColumnsFormat.STRING,
      //   data: {
      //     i18nValue:'COLUMNS.COMPTE_ESP.DESCRIPTION_FONDS'
      //   }
      // },
      {
        name: 'compte_esp',
        title: 'compte_esp',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.COMPTE_ESP.COMPTE_ESP'
        }
      },
      {
        name: 'depositaire',
        title: 'depositaire',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.COMPTE_ESP.DEPOSITAIRE'
        }
      },
      {
        name: 'compte_titre',
        title: 'compte_titre',
        format: DataTableColumnsFormat.STRING,
        data: {
          i18nValue:'COLUMNS.COMPTE_ESP.COMPTE_TITRE'
        }
      },
      
    ], actions: []
};
