import { Component, Input, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { CompteEspModel, ContactFilterModel } from 'src/app/main/models';
import { CompteEspService } from 'src/app/main/services';
import { DataTableSettingsModel } from 'src/app/shared/models';
import {COMPTES_ESP_LIST_SETTINGS} from './comptes-esp-list.settings'

@Component({
  selector: 'app-especes-account',
  templateUrl: './especes-account.component.html',
  styleUrls: ['./especes-account.component.scss']
})
export class EspecesAccountComponent implements OnInit {



  compteEspList:CompteEspModel[];
  loading: boolean = false;
  _enableActions: boolean = false;
  settings: DataTableSettingsModel = COMPTES_ESP_LIST_SETTINGS;

  @Input()
  set filter(value: { filterBody: ContactFilterModel }) {

    this.compteEspList = [];
    if (!value) return;

    if (value.filterBody.accountCode) {
      this.loadListEspecesAccounts(value.filterBody.accountCode);
    }
  }

  constructor( private compteEspService:CompteEspService) { }

  ngOnInit() {
  }

  loadListEspecesAccounts(code:string){
    this.compteEspService.loadListEspecesAccounts(code).pipe(finalize(() => {
      this.loading = false;
    })).subscribe(data=>{
      if(data){
        this.compteEspList=data
      }
    });
  }

}
