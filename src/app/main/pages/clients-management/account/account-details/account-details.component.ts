import { Component, Inject, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import {
  AccountPMModel,
  AccountPMPayloadModel,
  AccountPPModel,
  AccountPPPayloadModel,
  AccountType,
  ActivityGlobalFilterModel
} from 'src/app/main/models';
import { AccountService } from 'src/app/main/services';
import { ActivitiesFormComponent } from '../../activities-form/activities-form.component';
// import { ActivitiesHistoryComponent } from '../../activities-history/activities-history.component';
// import { AccountContactListComponent } from '../../contact/account-contact-list/account-contact-list.component';
// import { AddContactComponent } from "../../contact/add-contact/add-contact.component";
// import { IncidentFormComponent } from "../../incident/incident-form/incident-form.component";
// import { IncidentListComponent } from '../../incident/incident-list/incident-list.component';
// import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {environment} from "../../../../../../environments/environment";
import { AccountContactListComponent } from '../../../contacts-management/contact/account-contact-list/account-contact-list.component';
import { ActivitiesHistoryComponent, SearchDto } from '../../../activity-management/activities-history/activities-history.component';
import { IncidentListComponent } from '../../../activity-management/incident/incident-list/incident-list.component';
import { DomSanitizer } from '@angular/platform-browser';
import { AddContactComponent } from '../../../contacts-management/contact/add-contact/add-contact.component';
import { IncidentFormComponent } from '../../../activity-management/incident/incident-form/incident-form.component';
import { DataTableSettingsModel } from 'src/app/shared/models';
import { TaskListComponent } from '../../../activity-management/task/task-list/task-list.component';
import { EventListComponent } from '../../../activity-management/event/event-list/event-list.component';
import { OpportuniteListComponent } from '../../../activity-management/opportunite/opportunite-list/opportunite-list.component';
import { TASK_LIST_SETTINGS } from '../../../activity-management/task/task-list/task-list.settings';
import { EVENT_LIST_SETTINGS } from '../../../activity-management/event/event-list/event-list.settings';
import { INCIDENT_LIST_SETTINGS } from '../../../activity-management/incident/incident-list/incident-list.settings';
import { OPPORTUNITE_LIST_SETTINGS } from '../../../activity-management/opportunite/opportunite-list/opportunite-list.settings';
import {NewContactComponent} from '../../../contacts-management/contact/new-contact/new-contact.component';
import {PasserOrdreComponent} from '../../../carnet-ordre-management/carnet-ordre/passer-ordre/passer-ordre.component';


@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements OnInit, OnDestroy {



  searchDto:SearchDto={date:new Date(),compte:""};

//settings

  settings_task: DataTableSettingsModel = TASK_LIST_SETTINGS;

  settings_event: DataTableSettingsModel = EVENT_LIST_SETTINGS;

  settings_incident: DataTableSettingsModel = INCIDENT_LIST_SETTINGS;

  settings_opportunite: DataTableSettingsModel = OPPORTUNITE_LIST_SETTINGS;


 // components intance

  @ViewChild(AccountContactListComponent)
  accountContactListComponent: AccountContactListComponent;

  @ViewChild(ActivitiesHistoryComponent)
  activitiesHistoryComponent: ActivitiesHistoryComponent;

  @ViewChild(IncidentListComponent)
  incidentComponent: IncidentListComponent;


  @ViewChild(TaskListComponent)
  taskListComponent: TaskListComponent;

  @ViewChild(EventListComponent)
  evenListComponent: EventListComponent;

  @ViewChild(OpportuniteListComponent)
  opportuniteListComponent: OpportuniteListComponent;

  @ViewChild(IncidentListComponent)
  incidentListComponent: IncidentListComponent;



  // account
  accountType: AccountType = null;
  accountCode: string = null;
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;

  // subscription
  routeSubscription: Subscription = null;

  // dialog window
  accountDialogRef: any = null;
  accountDialogCloseSubscription: Subscription = null;

  incidentDialogRef: any = null;
  incidentDialogCloseSubscription: Subscription = null;

  // loading
  loadingAccount: boolean = false;
  loadingForm: boolean = false;

  enableForm: boolean = false;

  // image
  photoImg: string = './assets/img/avatar.png';
  apiUrl: string = environment.apiEndPoint;

  constructor(private activatedRoute: ActivatedRoute,
    private accountService: AccountService,
    @Inject('pipeParameters') public pipeParameters: any,
     private dialog: MatDialog,
    private toastrService: ToastrService,
    private router:Router) {

    this.routeSubscription = this.activatedRoute.paramMap.subscribe(params => {
      this.accountType = params.get('accountType') ? AccountType[params.get('accountType').toUpperCase()] : null;
      this.accountCode = params.get('accountCode') ? params.get('accountCode').toUpperCase() : null;



      if (this.accountType && this.accountCode) {
        switch (this.accountType) {
          case AccountType.PM:
            this.loadAccountPMByCode(this.accountCode);
            break;
          case AccountType.PP:
            this.loadAccountPPByCode(this.accountCode);
            break;
        }
        this.photoImg = `${this.apiUrl}/comptes/${this.accountCode}/photo`;
      }
    });

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
    if (this.accountDialogCloseSubscription) {
      this.accountDialogCloseSubscription.unsubscribe();
    }
    if (this.incidentDialogCloseSubscription) {
      this.incidentDialogCloseSubscription.unsubscribe();
    }
  }

  /** ON ADD CONTACT */
  onAddContact(): void {
    // this.accountDialogRef = this.dialog.open(AddContactComponent, {
    //   data: {
    //     code: this.accountCode,
    //     type: this.accountType,
    //     mode: 'AFFECT'
    //   }
    // });

    // this.accountDialogCloseSubscription = this.accountDialogRef.afterClosed()
    //   .subscribe(reload => {
    //     /** REFRESH CONTACTS LIST */
    //     if (reload === true) {
    //       if (this.accountContactListComponent) {
    //         this.accountContactListComponent.loadContacts(this.accountCode);
    //       }
    //     }
    //   });
  }

  onListcomptes(): void {
    this.router.navigate(['/pages/account-management/account-consultation']);
  }

  /** ON NEW INCIDENT */
  onNewIncident(): void {
    // this.incidentDialogRef = this.dialog.open(IncidentFormComponent, {
    //   width: '50vw',
    //   data: { accountCode: this.accountCode, editEnabled: false }
    // });

    // this.incidentDialogCloseSubscription = this.incidentDialogRef.afterClosed()
    //   .subscribe(reload => {
    //     if (reload === true) {
    //       if (this.incidentComponent) {
    //         this.incidentComponent.loadIncidents();
    //       }
    //     }
    //   });
  }

  /** LOAD ACCOUNT PP BY CODE */
  loadAccountPPByCode(accountCode: string): void {
    this.accountPP = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPPByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;
      }))
      .subscribe(response => {
        this.accountPP = response;
        console.log('accouuuuntPP: ', this.accountPP);
      },error => this.router.navigate(['/pages/account-management/account-consultation']))
  }

  /** LOAD ACCOUNT PM BY CODE */
  loadAccountPMByCode(accountCode: string): void {
    this.accountPM = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPMByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;
      }))
      .subscribe(response => {
        this.accountPM = response;
      }, error => this.router.navigate(['/pages/account-management/account-consultation']))
  }

  /** GET BACKGROUND COLOR FOR ACCOUNT INFO */
  getBackgroundColor(): string {
    let selectedColor: string = '#c7d2d752'
    ;
    const colorProspect: string = '#fff7e4';
    const colorClient: string = '#f0f9e6';
    if (this.loadingAccount) {
      return selectedColor;
    }
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP) {
          if (this.accountPP.typeCompte === 'P') {
            selectedColor = colorProspect;
          } else if (this.accountPP.typeCompte === 'C') {
            selectedColor = colorClient;
          }
        }
        break;
      case AccountType.PM:
        if (this.accountPM) {
          if (this.accountPM.typeCompte === 'P') {
            selectedColor = colorProspect;
          } else if (this.accountPM.typeCompte === 'C') {
            selectedColor = colorClient;
          }
        }
        break;
    }
    return selectedColor;
  }

  /** ON SUBMIT */
  // onSubmitForm(): void {

  //   // generate payload
  //   let generalInformationsPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  //   let identityInformationPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  //   let activitiesPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  //   let finalPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;

  //   if (this.generalInformationsFormComponent && this.identityInformationFormComponent && this.activitiesFormComponent
  //     && this.generalInformationsFormComponent.isValidForm() && this.identityInformationFormComponent.isValidForm() && this.activitiesFormComponent.isValidForm()) {
  //     generalInformationsPayload = this.generalInformationsFormComponent.generatePayload();
  //     identityInformationPayload = this.identityInformationFormComponent.generatePayload();
  //     activitiesPayload = this.activitiesFormComponent.generatePayload();
  //   }

  //   if (generalInformationsPayload && identityInformationPayload && activitiesPayload) {
  //     finalPayload = {
  //       ...activitiesPayload,
  //       ...identityInformationPayload,
  //       ...generalInformationsPayload
  //     }
  //   } else {
  //     return;
  //   }

  //   switch (this.accountType) {
  //     case AccountType.PP: {
  //       // update
  //       this.loadingForm = true;
  //       this.accountService.updateAccountPP(finalPayload as AccountPPPayloadModel)
  //         .pipe(finalize(() => {
  //           this.loadingForm = false;
  //         }))
  //         .subscribe(response => {
  //           if (response && response.valid) {
  //             this.toastrService.success("Fiche compte modifié avec succés");
  //             this.onDisableForm();
  //             this.loadAccountPPByCode(this.accountCode);
  //           } else {
  //             this.toastrService.warning("Un problème est survenu lors de modification de fiche compte");
  //           }
  //         })
  //       break;
  //     }
  //     case AccountType.PM: {
  //       // update
  //       this.loadingForm = true;
  //       this.accountService.updateAccountPM(finalPayload as AccountPMPayloadModel)
  //         .pipe(finalize(() => {
  //           this.loadingForm = false;
  //         }))
  //         .subscribe(response => {
  //           if (response && response.valid) {
  //             this.toastrService.success("Fiche compte modifié avec succés");
  //             this.onDisableForm();
  //             this.loadAccountPMByCode(this.accountCode);
  //           } else {
  //             this.toastrService.warning("Un problème est survenu lors de modification de fiche compte");
  //           }
  //         })
  //     }
  //   }

  // }

  /** ON ENABLE FORM */
  // onEnableForm() {
  //   this.generalInformationsFormComponent.onEnableForm();
  //   this.identityInformationFormComponent.onEnableForm();
  //   this.activitiesFormComponent.onEnableForm();
  //   this.enableForm = true;
  // }


  /** ON DISABLE FORM */
  // onDisableForm() {
  //   this.generalInformationsFormComponent.onDisableForm();
  //   this.identityInformationFormComponent.onDisableForm();
  //   this.activitiesFormComponent.onDisableForm();
  //   this.enableForm = false;
  // }

  /** ON INIT FORM */
  // onInitForm() {
  //   this.generalInformationsFormComponent.initFormGroup();
  //   this.identityInformationFormComponent.initFormGroup();
  //   this.activitiesFormComponent.initFormGroup();
  // }


  /** REFRESH TASK LIST */
  refreshTaskList() {

    if (this.taskListComponent) {
      this.taskListComponent.loadGlobalListOfTasks();

    }
  }

  /** REFRESH EVENT LIST */
  refreshEventList() {

    if (this.evenListComponent){
      this.evenListComponent.loadGlobalListOfEvents();
    }

  }

  /** REFRESH INCIDENT LIST */
  refreshIncidentList() {

    if (this.incidentListComponent){
      this.incidentListComponent.loadGlobalListOfIncidents();
    }
  }

  /** REFRESH OPPORTUNITE LIST */
  refreshOpportuniteList() {

    if (this.opportuniteListComponent){
      this.opportuniteListComponent.loadGlobalListOfOpportunites();
    }
  }


  /** REFRESH HISTORY ACTIVITY LIST */
  refreshHistoryActivityList() {

    if (this.activitiesHistoryComponent) {
      this.activitiesHistoryComponent.loadNextHistoryActivity(this.searchDto);
      this.activitiesHistoryComponent.loadPreviousHistoryActivity(this.searchDto);
    }
  }

  /** for chart  */
  visible:boolean=false;
  onVisible(){
    this.visible=!this.visible;
  }

  passerOrdre(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'passerOrdre-popup' ;
    dialogConfig.autoFocus = true ;
    switch (this.accountType) {
      case AccountType.PP:
        dialogConfig.data = { type : this.accountPP.typeCompte ,code : this.accountPP.code , nom : this.accountPP.nom , prenom :  this.accountPP.prenom }
        break;
      case AccountType.PM:
        dialogConfig.data = { type : this.accountPM.typeCompte , code : this.accountPP.code  , raisonSociale : this.accountPM.raisonSociale }
        break;
    }
    this.dialog.open(PasserOrdreComponent, dialogConfig)
  }

}
