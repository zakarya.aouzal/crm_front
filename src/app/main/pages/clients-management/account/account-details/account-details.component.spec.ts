import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, ParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject, of } from 'rxjs';
import { AccountType, AccountPPModel, AccountPMModel } from 'src/app/main/models';
import { AccountService } from 'src/app/main/services';
import { environment } from 'src/environments/environment';
import { AccountDetailsComponent } from './account-details.component';


fdescribe('AccountDetailsComponent', () => {
  let component: AccountDetailsComponent;
  let fixture: ComponentFixture<AccountDetailsComponent>;
  let activatedRouteData = new Subject<ParamMap>();
  let activatedRouteMock = { paramMap: activatedRouteData.asObservable() };
  let accountService: AccountService = null;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountDetailsComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        AccountService,
        { provide: 'pipeParameters', useValue: environment.pipeParameters },
        {
          provide: ActivatedRoute,
          useValue: activatedRouteMock
        },
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    accountService = TestBed.inject(AccountService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should assign \'accountType\' &&  \'accountCode\' when route is resolved', async(() => {
    // account PP
    activatedRouteData.next(convertToParamMap({ accountType: AccountType.PP, accountCode: 'PP_1002' }));
    expect(component.accountType).toEqual(AccountType.PP);
    expect(component.accountCode).toEqual('PP_1002');
    // account PM
    activatedRouteData.next(convertToParamMap({ accountType: AccountType.PM, accountCode: 'PM_1002' }));
    expect(component.accountType).toEqual(AccountType.PM);
    expect(component.accountCode).toEqual('PM_1002');
  }));

  it(`'loadAccountPPByCode' should call service 'accountService.getAccountPPByCode'`, () => {
    const response: AccountPPModel = null;
    const accountCode: string = 'PP_1002';
    spyOn(accountService, 'getAccountPPByCode').and.returnValue(of(response));
    component.loadAccountPPByCode(accountCode);
    expect(accountService.getAccountPPByCode).toHaveBeenCalledWith(accountCode);
    expect(component.accountPP).toEqual(response);
  });

  it(`'loadAccountPMByCode' should call service 'accountService.getAccountPMByCode'`, () => {
    const response: AccountPMModel = null;
    const accountCode: string = 'PM_1002';
    spyOn(accountService, 'getAccountPMByCode').and.returnValue(of(response));
    component.loadAccountPMByCode(accountCode);
    expect(accountService.getAccountPMByCode).toHaveBeenCalledWith(accountCode);
    expect(component.accountPM).toEqual(response);
  });

});
