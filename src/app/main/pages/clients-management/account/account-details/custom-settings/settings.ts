import { DataTableActions, DataTableColumnsFormat, DataTableSettingsModel } from '../../../../../../shared/models';

export const TASK_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'ID',
            format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.CODE'
          // }
        }, {
            name: 'subject',
            title: 'Sujet',
            format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.SUJET'
          // },
          style:{
            orientation:"left"
              }
        }, {
            name: 'description',
            title: 'Description',
            format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.DESCRIPTION'
          // },
          style:{
            orientation:"left"
              }
        }, {
            name: 'type',
            title: 'Type',
            format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.TYPE'
          // },
          style:{
            orientation:"left"
              }
        }, {
            name: 'dateInsert',
            title: 'Date',
            format: DataTableColumnsFormat.DATE,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.DATE_CREATION'
          // }
        },{
            name: 'compte_id',
            title: 'Compte',
            format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.COMPTE'
          // }
        }, {
            name: 'status',
            title: 'Status',
            format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.TASK_LIST.STATUT'
          // },
          style:{
            orientation:"left"
              }
        }
    ]
};




export const EVENT_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'ID',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.CODE'
      // }
    }, {
      name: 'subject',
      title: 'Sujet',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.SUJET'
      // },
      style:{
        orientation:"left"
      }
    }, {
      name: 'description',
      title: 'Desc',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.DESCRIPTION'
      // },
      style:{
        orientation:"left"
      }
    }, {
      name: 'start_date',
      title: 'Début',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.DATE_DEBUT'
      // }
    }, {
      name: 'end_date',
      title: 'Fin',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.DATE_FIN'
      // }
    },  {
      name: 'compte_id',
      title: 'Compte',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.COMPTE'
      // }
    }, {
      name: 'type',
      title: 'Type',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.TYPE'
      // }
    }, {
      name: 'status',
      title: 'Status',
      format: DataTableColumnsFormat.STRING,
      // data: {
      //   i18nValue: 'COLUMNS.EVENT_LIST.STATUT'
      // },
      style:{
        orientation:"left"
      }
    }
  ]
};

export const INCIDENT_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
      {
          name: 'code',
          title: 'Code',
          format: DataTableColumnsFormat.STRING,
          // data:{
          //   i18nValue:'COLUMNS.INCIDENT_LIST.CODE'
          // }
      }, {
          name: 'objet',
          title: 'Incident',
          format: DataTableColumnsFormat.STRING,
          // data:{
          //   i18nValue:'COLUMNS.INCIDENT_LIST.INCIDENT'
          // },
          style:{
            orientation:"left"
              }
      }, {
          name: 'description',
          title: 'Description',
          format: DataTableColumnsFormat.STRING,
          // data:{
          //   i18nValue:'COLUMNS.INCIDENT_LIST.DESCRIPTION'
          // },
          style:{
            orientation:"left"
              }
      }, {
          name: 'dateDeclaration',
          title: 'Date Déclaration',
          format: DataTableColumnsFormat.DATE,
          // data:{
          //   i18nValue:'COLUMNS.INCIDENT_LIST.DATE_DECLARATION'
          // }
      }, {
          name: 'type',
          title: 'Type de ticket',
          format: DataTableColumnsFormat.STRING,
          // data:{
          //   i18nValue:'COLUMNS.INCIDENT_LIST.TYPE'
          // },
          style:{
            orientation:"left"
              }
      }, {
          name: 'gestionnaire',
          title: 'Responsable',
          format: DataTableColumnsFormat.STRING,
      // data:{
      //   i18nValue:'COLUMNS.INCIDENT_LIST.GESTIONNAIRE'
      // }
    }, {
          name: 'compte',
          title: 'Compte',
          format: DataTableColumnsFormat.STRING,
      // data:{
      //   i18nValue:'COLUMNS.INCIDENT_LIST.COMPTE'
      // }
      }, {
          name: 'statut',
          title: 'Statut',
          format: DataTableColumnsFormat.STRING,
      // data:{
      //   i18nValue:'COLUMNS.INCIDENT_LIST.STATUT'
      // },
      style:{
        orientation:"left"
          }
      }, {
          name: 'priorite',
          title: 'Priorité',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.INCIDENT_LIST.PRIORITE'
          // },
          style: {
              colors: [
                  {
                      colorValue: 'red',
                      conditionValue: 'HAUTE'
                  }, {
                      colorValue: 'orange',
                      conditionValue: 'MOYENNE'
                  }, {
                      colorValue: 'blue',
                      conditionValue: 'BASSE'
                  }
              ],orientation:"left"
          }
      }
  ]
};
export const OPPORTUNITE_LIST_SETTINGS: DataTableSettingsModel = {
  columns: [
      {
          name: 'code',
          title: 'Code',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.CODE'
          // }
      }, {
          name: 'subject',
          title: 'Sujet',
          format: DataTableColumnsFormat.STRING,
          data: {
            i18nValue:'COLUMNS.OPPORTUNITE_LIST.SUJET'
          }
      }, {
          name: 'probabilite',
          title: 'Probabilité',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.PROBABILITE'
          // }
      }, {
          name: 'estimation',
          title: 'Estimation',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.ESTIMATION'
          // },
          style:{
            orientation:"right"
              }
      }, {
          name: 'description',
          title: 'Description',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.DESCRIPTION'
          // },
          style:{
            orientation:"left"
              }
      }, {
          name: 'dateInsert',
          title: 'Date de création',
          format: DataTableColumnsFormat.DATE,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.DATE_CREATION'
          // }
      }, {
          name: 'operateur_saisie',
          title: 'Gestionnaire',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.GESTIONNAIRE'
          // }
      }, {
        name: 'type',
        title: 'Type',
        format: DataTableColumnsFormat.STRING,
        // data: {
        //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.TYPE'
        // },
        style:{
          orientation:"left"
            }
      }, {
          name: 'compte_id',
          title: 'Compte',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.COMPTE'
          // }
      }, {
          name: 'status',
          title: 'Statut',
          format: DataTableColumnsFormat.STRING,
          // data: {
          //   i18nValue:'COLUMNS.OPPORTUNITE_LIST.STATUT'
          // },
          style:{
            orientation:"left"
              }
      }
  ]
};
