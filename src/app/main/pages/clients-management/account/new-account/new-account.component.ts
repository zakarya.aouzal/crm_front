import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AccountPMPayloadModel, AccountPPPayloadModel, AccountType, CompteContactPayload, ContactModel, RegisterAccountPayloadModel } from 'src/app/main/models';
import { AccountService, GedService } from 'src/app/main/services';
import { ActivitiesFormComponent } from '../../activities-form/activities-form.component';
import { IdentityInformationFormComponent } from '../identity-information-form/identity-information-form.component';
import { TranslateService } from '@ngx-translate/core';
import { AddContactComponent } from '../../../contacts-management/contact/add-contact/add-contact.component';
import { CreateContactComponent } from '../../../contacts-management/contact/create-contact/create-contact.component';
import { EditContactComponent } from '../../../contacts-management/contact/edit-contact/edit-contact.component';
import { DocumentComponent } from '../../../documents-management/document/document.component';


@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements OnInit, OnDestroy {



  @ViewChild(IdentityInformationFormComponent)
  identityInformationFormComponent: IdentityInformationFormComponent;

  @ViewChild(ActivitiesFormComponent)
  activitiesFormComponent: ActivitiesFormComponent;

  @ViewChild(DocumentComponent)
  attachmentFormComponent: DocumentComponent;

  // list
  readonly steps = [
    { id: 1, label: 'NOUVEAU_COMPTE.STEPS.INFO_COMPTE' },
    { id: 2, label: 'NOUVEAU_COMPTE.STEPS.ACTIVITY' },
    { id: 3, label: 'NOUVEAU_COMPTE.STEPS.CONTACTS' },
    { id: 4, label: 'NOUVEAU_COMPTE.STEPS.DOCUMENTS' },
    { id: 5, label: 'NOUVEAU_COMPTE.STEPS.VALIDATION' }
  ];
  readonly accountTypeList = [{ code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP") }, { code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM") }];

  // current step
  currentStep: { id: number, label: string } = null;

  // selected account type
  selectedAccountType: AccountType = AccountType.PP;

  // payload
  generalInformationsPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  identityInformationPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;
  activitiesPayload: AccountPPPayloadModel | AccountPMPayloadModel = null;

  // loading
  loading: boolean = false;

  // subscription
  addcontactMatDialogCloseSubscription: Subscription = null;
  newcontactMatDialogCloseSubscription: Subscription = null;
  editContactMatDialogCloseSubscription: Subscription = null;

  // list of selected contacts
  selectedContacts: ContactModel[] = [];
  contactAccountPayloadList: CompteContactPayload[] = [];

  constructor(private accountService: AccountService,
    private toastrService: ToastrService,
    private dialog: MatDialog,
    private translate:TranslateService,
    @Inject('pipeParameters') public pipeParameters: any,
    private gedService: GedService) { }

  ngOnInit(): void {
    this.currentStep = this.steps[0];
  }

  ngOnDestroy(): void {
    if (this.addcontactMatDialogCloseSubscription) {
      this.addcontactMatDialogCloseSubscription?.unsubscribe();
    }
    if (this.newcontactMatDialogCloseSubscription) {
      this.newcontactMatDialogCloseSubscription?.unsubscribe();
    }
    if (this.editContactMatDialogCloseSubscription) {
      this.editContactMatDialogCloseSubscription?.unsubscribe();
    }
  }

  /** CHANGE STEP WIZARD */
  onChangeStep(stepId: number) {
    if (!this.currentStep || !stepId) {
      return;
    }
    if (this.currentStep.id < stepId) {
      for (let i = 1; i < stepId; i++) {

        switch (i) {
          case 1: {
            // const generalInfoFormIsValid: boolean = this.generalInformationsFormComponent.isValidForm();
            const identityInfoFormIsValid: boolean = this.identityInformationFormComponent.isValidForm();
            if ( identityInfoFormIsValid) {
              // this.generalInformationsPayload = this.generalInformationsFormComponent.generatePayload();
              this.identityInformationPayload = this.identityInformationFormComponent.generatePayload();
            } else {
              this.currentStep = this.steps.find(item => item.id === i);
              return;
            }
            break;
          }
          case 2: {
            const activitiesFormIsValid: boolean = this.activitiesFormComponent.isValidForm();
            if (activitiesFormIsValid) {
              this.activitiesPayload = this.activitiesFormComponent.generatePayload();
              // show popup contacts
            } else {
              this.currentStep = this.steps.find(item => item.id === i);
              return;
            }
            break;
          }
        }
      }
    }
    this.currentStep = this.steps.find(item => item.id === stepId);
  }

  /** ON SUBMIT */
  onSubmit() {

    // generate payload
    let registerAccountPayload: RegisterAccountPayloadModel = new RegisterAccountPayloadModel();

    // account type
    registerAccountPayload.accountType = this.selectedAccountType;

    // account payload
    if (this.generalInformationsPayload && this.identityInformationPayload && this.activitiesPayload) {

      let accountPayload: AccountPPPayloadModel | AccountPMPayloadModel = {
        ...this.activitiesPayload,
        ...this.identityInformationPayload,
        ...this.generalInformationsPayload
      }

      switch (this.selectedAccountType) {
        case AccountType.PP: {
          registerAccountPayload.accountPP = accountPayload as AccountPPPayloadModel;
          break;
        }
        case AccountType.PM: {
          registerAccountPayload.accountPM = accountPayload as AccountPMPayloadModel;
        }
      }

    } else {
      return;
    }

    // account contact payload
    registerAccountPayload.contactAccounts = this.contactAccountPayloadList;

    // save
    this.loading = true;
    this.accountService.registerAccount(registerAccountPayload)
      .pipe(finalize(() => {
        this.onResetForm();
        this.currentStep = this.steps[0];
        this.loading = false;
      }))
      .subscribe(response => {
        if (response && response.valid) {
          this.toastrService.success("Compte ajouté avec succés");
          // upload attachments
          this.uploadAttachments(response.code);
          // upload compte photo
          if (this.identityInformationFormComponent.photoFile)
            this.uploadPhoto(response.code);
        } else {
          this.toastrService.warning("Un problème est survenu lors d'ajout de compte");
        }
      })

  }

  /** ON RESET FORM */
  onResetForm() {
    // if (this.generalInformationsFormComponent) {
    //   this.generalInformationsFormComponent.onResetForm();
    // }
    if (this.identityInformationFormComponent) {
      this.identityInformationFormComponent.onResetForm();
    }
    if (this.activitiesFormComponent) {
      this.activitiesFormComponent.onResetForm();
    }
  }

  /** ON ADD CONTACT */
  onAddContact() {
    const selectedList = this.selectedContacts.map(value => value?.code);
    const dialogRef = this.dialog.open(AddContactComponent, { data: { code: null, type: this.selectedAccountType, mode: 'ACCOUNT', selectedList } });
    this.addcontactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.selectedContacts.push(...dialogRef.componentInstance.selectedContacts);
        this.contactAccountPayloadList.push(...dialogRef.componentInstance.payloadList);
      }
    });
  }

  /** ON NEW CONTACT */
  onNewContact() {
    const dialogRef = this.dialog.open(CreateContactComponent, { data: { code: null, type: this.selectedAccountType } });
    this.newcontactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.selectedContacts.push(dialogRef.componentInstance.createdContact);
        this.contactAccountPayloadList.push(dialogRef.componentInstance.compteContactPayload);
      }
    });
  }

  /** ON EDIT CONTACT */
  // onViewEditContact(contact: ContactModel, viewMode: boolean) {
  //   const compteContact: CompteContactPayload = this.contactAccountPayloadList.filter(value => value.codeContact===contact.code)[0];
  //   const dialogRef: MatDialogRef<EditContactComponent> = this.dialog.open(EditContactComponent, {
  //     panelClass: 'activity-popup',
  //     data: {
  //       contact,
  //       compteContact,
  //       viewMode
  //     }
  //   });
  //   this.editContactMatDialogCloseSubscription = dialogRef.afterClosed().subscribe(result => {
  //     if (result === true) {
  //       // replace old values using filters!!!
  //       this.contactAccountPayloadList = this.contactAccountPayloadList.map(value => {
  //         return (value?.codeContact===dialogRef.componentInstance.compteContactPayload?.codeContact) ?
  //            dialogRef.componentInstance.compteContactPayload : value;
  //       });
  //     }
  //   });
  // }

  /** UPLOAD ATTACHMENTS */
  uploadAttachments(accountCode: string) {

    if (!accountCode || this.attachmentFormComponent.files.size === 0) {
      return;
    }

    let formData: FormData = new FormData();

    if (this.attachmentFormComponent.files) {
      for (let file of this.attachmentFormComponent.files) {
        formData.append('files', file[1], file[1].name);
      }
    }
    // upload
    this.gedService.uploadFilesForNewAccount(formData, accountCode)
      .subscribe(response => {
        if (!response || !response.valid) {
          this.toastrService.warning("Certains fichiers ne sont pas téléchargés correctement");
        }
      });

  }

  /** UPLOAD COMPTE PHOTO BY UPDATE */
  uploadPhoto(accountCode: string): void {
    const file = this.identityInformationFormComponent.photoFile;
    this.accountService.uploadPhoto(accountCode, file)
      .subscribe(response => {
        if (!response || !response.valid) {
          this.toastrService.warning("La photo n'a pas été chargé correctement");
        }
      })
  }

  onRemoveContact(code: string) {
    this.selectedContacts = this.selectedContacts.filter(value => value?.code !== code);
    this.contactAccountPayloadList = this.contactAccountPayloadList.filter(value => value?.codeContact !== code);
  }
}
