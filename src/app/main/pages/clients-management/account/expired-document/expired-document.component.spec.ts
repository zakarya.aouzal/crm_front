import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpiredDocumentComponent } from './expired-document.component';

describe('ExpiredDocumentComponent', () => {
  let component: ExpiredDocumentComponent;
  let fixture: ComponentFixture<ExpiredDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpiredDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpiredDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
