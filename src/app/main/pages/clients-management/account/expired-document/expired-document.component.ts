import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {AccountService} from "../../../../services";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import {
  AccountPPModel,
  AccountPPPayloadModel,
  CompteAlerte
} from "../../../../models";

@Component({
  selector: 'app-expired-document',
  templateUrl: './expired-document.component.html',
  styleUrls: ['./expired-document.component.scss']
})
export class ExpiredDocumentComponent implements OnInit {

  constructor(private fb: FormBuilder,
              private translate: TranslateService,
              private accountService: AccountService,
              // private dialogRef: MatDialogRef<ExpiredDocumentComponent>,
              // @Inject(MAT_DIALOG_DATA) public data: { alerte: CompteAlerte },
              private toastr: ToastrService) {
  }

  @Input()
  set alerte(value) {
    if (value) {
      this.compteAlerte = value;
      this.fillForm(this.compteAlerte);
    }
  }

  compteAlerte: CompteAlerte = null;

  loading: boolean = false;

  payloadModel: AccountPPPayloadModel = null;

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  @Output()
  emitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  viewMode: boolean = true;

  disableMail: boolean = false;

  accountPP: AccountPPModel = null;

  ngOnInit(): void {
    this.createForm();
    // this.fillForm(this.data?.alerte);
    this.fillForm(this.compteAlerte);
    // this.loadAccountPP(this.data?.alerte?.code);
    this.loadAccountPP(this.compteAlerte?.code);

    if (this.viewMode) {
      this.formGroup?.disable();
    }
  }

  createForm(): void {
    this.formGroup = this.fb.group({
      code: [null],
      accountType: [null],
      pieceIdentite: [null, [Validators.required]],
      numPI: [null, [Validators.required]],
      dateEmiPI: [null, [Validators.required]],
      dateExpPI: [null, [Validators.required]],
      emetteurPI: [null, [Validators.required]]
    });
  }

  private fillForm(alerte: CompteAlerte): void {
    if (this.formGroup) {
      this.formGroup?.patchValue({
        code: alerte?.code,
        accountType: alerte?.accountType,
        pieceIdentite: alerte?.pieceIdentite,
        numPI: alerte?.numPI,
        emetteurPI: alerte?.emetteurPI,
        dateEmiPI: alerte?.dateEmiPI,
        dateExpPI: alerte?.dateExpPI
      });
    }
  }

  onSubmit(): void {
    if (this.viewMode) {
      this.viewMode = false;
      this.formGroup?.enable();
      this.formGroup?.get('code')?.disable();
      return;
    }

    if (this.formGroup?.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    this.showValidationsMsg = false;

    this.payloadModel = this.generatePayload();

    this.accountService.updateAccountPP(this.payloadModel)
      .subscribe(value => {
        if (value.valid) {
          // this.dialogRef.close();
          this.emitter.emit(true);
          this.toastr.success("Document du compte modifié");
        } else {
          this.toastr.error("Erreur du serveur");
        }
      });
  }

  generatePayload(): AccountPPPayloadModel {
    let filter: AccountPPPayloadModel = null;
    if (this.formGroup) {
      filter = {
        ...Object.assign(new AccountPPPayloadModel(), this.accountPP),
        code: this.formGroup?.get('code').value,
        pieceIdentite: this.formGroup?.get('pieceIdentite').value,
        numPI: this.formGroup?.get('numPI').value,
        emetteurPI: this.formGroup?.get('emetteurPI').value,
        dateEmiPI: this.formGroup?.get('dateEmiPI').value,
        dateExpPI: this.formGroup?.get('dateExpPI').value
      }
    }
    return filter;
  }

  private loadAccountPP(code: string) {
    this.accountService.getAccountPPByCode(code)
      .subscribe(value => this.accountPP = value);
  }

  sendAlerteMail(): void {
    this.accountService.sendMail(this.compteAlerte)
      .subscribe(value => {
        if (value.valid) {
          this.disableMail = true;
          this.toastr.success('Mail envoyé au client');
        } else {
          this.toastr.success('Erreur du serveur');
        }
      })
  }
}
