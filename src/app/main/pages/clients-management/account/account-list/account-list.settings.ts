import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const ACCOUNT_PP_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
            }
        }, {
            name: 'nom',
            title: 'Nom',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.NOM'
            }
        }, {
            name: 'prenom',
            title: 'Prénom',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PRENOM'
            }
        }, {
            name: 'mobile1',
            title: 'Mobile',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.MOBILE'
            }
        }, {
            name: 'paysResidence',
            title: 'Pays',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PAYS_RESIDENCE'
            }
        }, {
            name: 'ville',
            title: 'Ville',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.VILLE'
            }
        }, {
            name: 'profession',
            title: 'Profession',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.PROFESSION'
            }
            
        },
        {
            name: 'operateur_saisie',
            title: 'Gestionnaire',
            format: DataTableColumnsFormat.STRING,
            data:{
                i18nValue:'COLUMNS.ACCOUNT_LIST.GESTIONNAIRE'
            }
        }    
    ], actions: [DataTableActions.VIEW]
};

export const ACCOUNT_PM_LIST_SETTINGS: DataTableSettingsModel = {
    columns: [
        {
            name: 'code',
            title: 'Code',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
            }
        }, {
            name: 'raisonSociale',
            title: 'Raison Sociale',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.RAISON_SOCIALE'
            }
        }, {
            name: 'rc',
            title: 'RC',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.RC'
            }
        }, {
            name: 'idFiscal',
            title: 'Id Fiscal',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.ID_FISCAL'
            }
        }, {
            name: 'ice',
            title: 'ICE',
            format: DataTableColumnsFormat.STRING,
            data: {
              i18nValue:'COLUMNS.ACCOUNT_LIST.ICE'
            }
        }
    ], actions: [DataTableActions.VIEW]
};
