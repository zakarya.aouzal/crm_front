import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AccountFilterModel, AccountPMModel, AccountPPModel } from 'src/app/main/models';
import { AccountService } from 'src/app/main/services';
import { ExportService } from 'src/app/shared/services';
import { AccountListComponent } from './account-list.component';


fdescribe('AccountListComponent', () => {
  let component: AccountListComponent;
  let fixture: ComponentFixture<AccountListComponent>;
  let accountService: AccountService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountListComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [AccountService, ExportService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    accountService = TestBed.inject(AccountService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`'loadListAccountPP' should call service 'accountService.getListAccountPP'`, () => {
    const response: AccountPPModel[] = [];
    const filter = new AccountFilterModel();
    spyOn(accountService, 'getListAccountPP').and.returnValue(of(response));
    component.loadListAccountPP(filter);
    expect(accountService.getListAccountPP).toHaveBeenCalledWith(filter);
    expect(component.accountPPList).toEqual(response);
  });

  it(`'loadListAccountPM' should call service 'accountService.getListAccountPM'`, () => {
    const response: AccountPMModel[] = [];
    const filter = new AccountFilterModel();
    spyOn(accountService, 'getListAccountPM').and.returnValue(of(response));
    component.loadListAccountPM(filter);
    expect(accountService.getListAccountPM).toHaveBeenCalledWith(filter);
    expect(component.accountPMList).toEqual(response);
  });

});
