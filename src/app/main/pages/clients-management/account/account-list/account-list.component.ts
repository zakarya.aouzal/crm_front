import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { AccountFilterModel, AccountPMModel, AccountPPModel, AccountType } from 'src/app/main/models';
import { AccountService } from 'src/app/main/services';
import { DataTableOutputActionsModel, DataTableSettingsModel, ToolbarOption } from 'src/app/shared/models';
import { ExportService } from 'src/app/shared/services';
import { ACCOUNT_PM_LIST_SETTINGS, ACCOUNT_PP_LIST_SETTINGS } from './account-list.settings';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {

  @Input()
  set filter(value: { filterBody: AccountFilterModel, accountType: AccountType }) {
    this.accountPPList = [];
    this.accountPMList = [];
    if (!value) return;
    this._filter = value.filterBody;
    this._accountType = value.accountType;
    if (this._filter && this._accountType) {
      switch (this._accountType) {
        case AccountType.PP:
          this.settings = ACCOUNT_PP_LIST_SETTINGS;
          this.loadListAccountPP(this._filter)
          break;
        case AccountType.PM:
          this.settings = ACCOUNT_PM_LIST_SETTINGS;
          this.loadListAccountPM(this._filter);
          break;
      }
    }
  }

  settings: DataTableSettingsModel = null;

  _filter: AccountFilterModel = null;

  _accountType: AccountType = null;

  accountPPList: AccountPPModel[] = [];

  accountPMList: AccountPMModel[] = [];

  loading: boolean = false;

  constructor(private AccountService: AccountService, private router: Router,
              private exportService: ExportService) { }

  ngOnInit(): void {
  }

  /** LOAD LIST OF ACCOUNTS PP */
  loadListAccountPP(filter: AccountFilterModel) {
    this.accountPPList = [];
    if (!filter) return;
    this.loading = true;
    this.AccountService.getListAccountPP(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.accountPPList = response;
      });
  }

  /** LOAD LIST OF ACCOUNTS PM */
  loadListAccountPM(filter: AccountFilterModel) {
    this.accountPMList = [];
    if (!filter) return;
    this.loading = true;
    this.AccountService.getListAccountPM(filter)
      .pipe(finalize(() => {
        this.loading = false;
      }))
      .subscribe(response => {
        this.accountPMList = response;
      });
  }

  /** ON ACTION */
  onAction(event: DataTableOutputActionsModel<AccountPMModel | AccountPPModel>) {
    if (event) {
      switch (event.actionType) {
        case 'VIEW':
          let id = event.item ? event.item.code : null;
          this.router.navigate(['/pages/clients-management/client-details', this._accountType, id]);
          break;
      }
    }
  }

  /** ON ACTION TOOLBAR */
  onActionToolbar(event: ToolbarOption) {
    switch (event) {
      case ToolbarOption.DownloadAsExcel:
        switch (this._accountType) {
          case AccountType.PP:
            if (this.accountPPList) {
              this.exportService.exportAsExcelFile(this.accountPPList, ACCOUNT_PP_LIST_SETTINGS, 'Liste des compte PP', 'comptes-pp');
            }
            break;
          case AccountType.PM:
            if (this.accountPMList) {
              this.exportService.exportAsExcelFile(this.accountPMList, ACCOUNT_PM_LIST_SETTINGS, 'Liste_des_compte_PM', 'comptes-pm');
            }
            break;
        }
        break;
      case ToolbarOption.DownloadAsPDF:
        switch (this._accountType) {
          case AccountType.PP:
            this.exportService.exportAsPDFFile(this.accountPPList, ACCOUNT_PP_LIST_SETTINGS, 'Liste des compte PP', 'comptes-pp');
            break;
          case AccountType.PM:
            this.exportService.exportAsPDFFile(this.accountPMList, ACCOUNT_PM_LIST_SETTINGS, 'Liste des compte PM', 'comptes-pm');
            break;
        }
        break;
    }
  }

}
