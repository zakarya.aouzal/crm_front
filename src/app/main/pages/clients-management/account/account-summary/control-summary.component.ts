import {Component, Inject, Input, OnDestroy, OnInit, Output, EventEmitter, ViewChild, TemplateRef, AfterViewInit, ChangeDetectorRef} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {
  AccountPMModel,
  AccountPMPayloadModel,
  AccountPPModel,
  AccountPPPayloadModel,
  AccountType, ActivityGlobalFilterModel, CompteContactPayload, ContactFilterModel, ContactModel,
  EventFilterModel,
  GenericItemModel,
  IStatusWatchlist,
  OpportuniteFilterModel,
  RegisterAccountPayloadModel,
  TaskFilterModel,
  WatchlistProcessDTO
} from 'src/app/main/models';
import {AccountService, GedService, ReferenceService} from 'src/app/main/services';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EditContactComponent} from "../../../contacts-management/contact/edit-contact/edit-contact.component";
import {AddContactComponent} from "../../../contacts-management/contact/add-contact/add-contact.component";
import { TaskListComponent } from '../../../activity-management/task/task-list/task-list.component';
import { EventListComponent } from '../../../activity-management/event/event-list/event-list.component';
import { OpportuniteListComponent } from '../../../activity-management/opportunite/opportunite-list/opportunite-list.component';
import { ContactListComponent } from '../../../contacts-management/contact/contact-list/contact-list.component';
import { DataTableOutputActionsModel, DataTableSettingsModel } from 'src/app/shared/models';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WatchListComponent } from '../../../prospects-management/onboarding/watch-list/watch-list.component';
import { WatchlistService } from 'src/app/main/services/kyc/account-management/watchlist/watchlist.service';
import { WATCHLIST_PM_LIST_SETTINGS, WATCHLIST_PP_LIST_SETTINGS } from '../../../prospects-management/onboarding/watch-list/watchlist-list.settings';


@Component({
  selector: 'app-control-summary',
  templateUrl: './control-summary.component.html',
  styleUrls: ['./control-summary.component.scss']
})
export class ControlSummaryComponent implements OnInit,AfterViewInit {

//list
watchlistStatusList:IStatusWatchlist[];

accountCode: string;
accountType: AccountType = AccountType.PP;

// filters
globalFilter: ActivityGlobalFilterModel = null;
taskFilter: TaskFilterModel = null;
eventFilter: EventFilterModel = null;
opportuniteFilter: OpportuniteFilterModel = null;
contactFilter:ContactFilterModel=null;

showAction:boolean=true
_hideComponent:string="";


@Input()
set dataInfo(value:{accountType:AccountType, accountCode: string}){

  this.accountType = value.accountType ? AccountType[value.accountType.toUpperCase()] : null;
  this.accountCode = value.accountCode ? value.accountCode.toUpperCase() : null;

  //initialise tabs

  this.globalFilter={...new ActivityGlobalFilterModel(),compte:this.accountCode,typeCompte:this.accountType};
  this.contactFilter={...new ContactFilterModel(),accountCode:this.accountCode,accountType:this.accountType};

  if (this.accountType && this.accountCode) {

// WAHTCH LIST 
this.watchlistStatusList=this.onWatchListProcessCall(this.accountCode,this.accountType);
    switch (this.accountType) {
      case AccountType.PP: {
        this. createAccountPPFormGroup();
        this.loadAccountPPByCode(this.accountCode);
        this.settings = WATCHLIST_PP_LIST_SETTINGS;

      }
        break;
      case AccountType.PM: {
        this. createaccountPMFormGroup();
        this.loadAccountPMByCode(this.accountCode);
        this.settings = WATCHLIST_PM_LIST_SETTINGS;
      }
        break;
    }

  }

};

@Input()
set hideComponent(value:string){

  if(value){
    this._hideComponent=value

  }
 

}




    motif:string="";

    // selected account type
    selectedAccountType: AccountType = AccountType.PP;


    // dialog window
    accountDialogRef: any = null;
    accountDialogCloseSubscription: Subscription = null;
    // control
    loading: boolean = false;
    loadingAccount: boolean = false;
    loadingForm: boolean = false;
    disableRejectButton:boolean=false;

    enableForm: boolean = false;

    // subscription
    routeSubscription: Subscription = null;


    mode:string = 'EDIT';


      // instances
      @ViewChild(TaskListComponent)
      taskListComponent: TaskListComponent;

      @ViewChild(EventListComponent)
      eventListComponent: EventListComponent;

      @ViewChild(OpportuniteListComponent)
      opportuniteListComponent: OpportuniteListComponent;

      @ViewChild(ContactListComponent)
      contactListComponent: ContactListComponent;


      @ViewChild(WatchListComponent)
      watchListComponent: WatchListComponent;

      @ViewChild('templateReject')
      templateRefRejct: TemplateRef<any>;

      @ViewChild('templateExport')
      templateRefExport: TemplateRef<any>;

      confirmationModalRef: BsModalRef;


        

  // account

  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;
 


  // Form group
  accountPPFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;

  settings: DataTableSettingsModel = null;


  constructor(
    @Inject('pipeParameters') public pipeParameters: any,
    private cdr: ChangeDetectorRef,
    private accountService: AccountService,
    private toastrService: ToastrService,
    //private el: ElementRef,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private referenceService: ReferenceService,
    private translate: TranslateService,
    private watchlistService:WatchlistService,
    private router: Router,
    private modalService: BsModalService,
    private activatedRoute: ActivatedRoute) {

//       this.routeSubscription = this.activatedRoute.paramMap.subscribe(params => {
//         this.accountType = params.get('accountType') ? AccountType[params.get('accountType').toUpperCase()] : null;
//         this.accountCode = params.get('accountCode') ? params.get('accountCode').toUpperCase() : null;

//         //initialise tabs

//         this.globalFilter={...new ActivityGlobalFilterModel(),compte:this.accountCode,typeCompte:this.accountType};
//         this.contactFilter={...new ContactFilterModel(),accountCode:this.accountCode,accountType:this.accountType};

//         if (this.accountType && this.accountCode) {

// // WAHTCH LIST 
// this.watchlistStatusList=this.onWatchListProcessCall(this.accountCode,this.accountType);
//           switch (this.accountType) {
//             case AccountType.PP: {
//               this. createAccountPPFormGroup();
//               this.loadAccountPPByCode(this.accountCode);
//               this.settings = WATCHLIST_PP_LIST_SETTINGS;

//             }
//               break;
//             case AccountType.PM: {
//               this. createaccountPMFormGroup();
//               this.loadAccountPMByCode(this.accountCode);
//               this.settings = WATCHLIST_PM_LIST_SETTINGS;
//             }
//               break;
//           }

//         }

//       });

      }
  ngAfterViewInit(): void {
    this.cdr.detectChanges();
  }
 

  ngOnInit() {

  }

  createAccountPPFormGroup() {
    this.accountPPFormGroup = this.fb.group({
      // general infos
      civilite: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      pieceIdentite: [null, [Validators.required]],
      numPi: [null, [Validators.required]],
      secteur: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      dateExpPI: [null, [Validators.required]],
      profession: [null, [Validators.required]],
      address: [null, [Validators.required]],
      nationality: [null, [Validators.required]],
      mobile: [null, [Validators.required]],
      email: [null, [Validators.required]],
      dateOfBirth: [null],
      fatherFullName: [null],
      motherFullName: [null],
      residenceCountry: [null],
      country: [null],
      city: [null],
      address2: [null],
      mobile2: [null],

      // activities
      capaciteFinanciere: [null],
      origineFonds: [null],
      niveauInv: [null],
      typeProduit:[null],
      produitSouhaites: [null],
      horizonPlacement: [null],
      secteurSouhaites: [null],
      objectifInvestissement: [null],
      profilInvestissement: [null],
      nationaliteUSA: [null],
      compteBancaireUSA: [null],
      adresseUSA: [null],
      greenCardUSA: [null],
      idFiscalUSA: [null],
      ligneTelephoneUSA: [null],

      // contacts

    });
    this.accountPPFormGroup.disable();
  }

  createaccountPMFormGroup() {
    this.accountPMFormGroup = this.fb.group({
      // general infos
      socialReason: [null, [Validators.required]],
      legalForm: [null, [Validators.required]],
      ice: [null, [Validators.required]],
      pays: [null, [Validators.required]],
      address: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      fixe: [null, [Validators.required]],
      email: [null, [Validators.required]],
      codePostale: [null, [Validators.required]],
      webSite: [null, [Validators.required]],
      gestionnaire: [null],

      // activities
      categorie: [null],
      secActivite: [null],
      capaciteFinanciere: [null],
      produitSouhaites: [null],
      niveauInv: [null],
      typeProduit:[null],
      secteurSouhaites: [null],
      horizonPlacement: [null],
      profilInvestissement: [null],
      objectifInvestissement: [null],
      paysResidenceFiscale: [null],
      compteBancaireUSA: [null],
      adresseUSA: [null],
      villeFiscaleUSA: [null],
      idFiscalUSA: [null],
      ligneTelephoneUSA: [null],
      giin: [null]
    });
    this.accountPMFormGroup.disable();
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.accountPPFormGroup.patchValue({
            civilite: this.accountPP.civilite,
            codePostale: this.accountPP.codePostale,
            pays: this.accountPP.paysResidence,
            pieceIdentite: this.accountPP.pieceIdentite,
            numPi: this.accountPP.numPI,
            secteur: this.accountPP.secActivite,
            lastName: this.accountPP.prenom,
            firstName: this.accountPP.nom,
            dateExpPI: this.accountPP.dateExpPI,
            profession: this.accountPP.profession,
            address: this.accountPP.adresse1,
            nationality: this.accountPP.nationalite,
            mobile: this.accountPP.mobile1,
            email: this.accountPP.email,
            dateOfBirth: this.accountPP.dateNaissance,
            fatherFullName: this.accountPP.nomPrenomPere,
            motherFullName: this.accountPP.nomPrenomMere,
            residenceCountry: this.accountPP.paysResidence,
            country: this.accountPP.paysResidence,
            city: this.accountPP.ville,
            address2: this.accountPP.adresse2,
            mobile2: this.accountPP.mobile2,

            capaciteFinanciere: this.accountPP.revenuAnnuel,
            origineFonds: this.accountPP.origineFonds,
            niveauInv: this.accountPP.niveauInv,
            typeProduit:this.accountPP.typeProduit,
            produitSouhaites: this.accountPP.produitSouhaites,
            horizonPlacement: this.accountPP.horizonPlacement,
            secteurSouhaites: this.accountPP.secteurSouhaites,
            objectifInvestissement: this.accountPP.objectifInvestissement,
            profilInvestissement: this.accountPP.profilInvestissement,
            nationaliteUSA: this.accountPP.nationaliteUSA,
            compteBancaireUSA: this.accountPP.compteBancaireUSA,
            adresseUSA: this.accountPP.adresseUSA,
            greenCardUSA: this.accountPP.greenCardUSA,
            idFiscalUSA: this.accountPP.idFiscalUSA,
            ligneTelephoneUSA: this.accountPP.ligneTelephoneUSA
          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.accountPMFormGroup.patchValue({
            socialReason: this.accountPM.raisonSociale,
            legalForm: this.accountPM.formeJuridique,
            ice: this.accountPM.ice,
            pays: this.accountPM.pays,
            address: this.accountPM.adresse1,
            phone: this.accountPM.mobile1,
            fixe: this.accountPM.fixe,
            email: this.accountPM.email,
            codePostale: this.accountPM.codePostale,
            webSite: this.accountPM.siteWeb,

            categorie: this.accountPM.categorie,
            secActivite: this.accountPM.secActivite,
            capaciteFinanciere: this.accountPM.resultatNet,
            typeProduit:this.accountPM.typeProduit,
            produitSouhaites: this.accountPM.produitSouhaites,
            niveauInv: this.accountPM.niveauInv,
            secteurSouhaites: this.accountPM.secteurSouhaites,
            horizonPlacement: this.accountPM.horizonPlacement,
            profilInvestissement: this.accountPM.profilInvestissement,
            objectifInvestissement: this.accountPM.objectifInvestissement,
            paysResidenceFiscale: this.accountPM.paysResidenceFiscale,
            compteBancaireUSA: this.accountPM.compteBancaireUSA,
            adresseUSA: this.accountPM.adresseUSA,
            villeFiscaleUSA: this.accountPM.villeFiscaleUSA,
            idFiscalUSA: this.accountPM.idFiscalUSA,
            ligneTelephoneUSA: this.accountPM.ligneTelephoneUSA,
            giin: this.accountPM.giin
          });
        }
    }
  }


   /** LOAD ACCOUNT PP BY CODE */
   loadAccountPPByCode(accountCode: string): void {

    this.accountPP = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPPByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;
        if(this.accountPP.flagExport=="N")this.disableRejectButton=true;
      }))
      .subscribe(response => {

        this.accountPP = response;
        this.initFormGroup();
      }, error => this.router.navigate(['/pages/prospects-management/prospects-consultation']))
  }

  /** LOAD ACCOUNT PM BY CODE */
  loadAccountPMByCode(accountCode: string): void {


    this.accountPM = null;
    if (!accountCode) return;
    this.loadingAccount = true;
    this.accountService.getAccountPMByCode(accountCode.toUpperCase())
      .pipe(finalize(() => {
        this.loadingAccount = false;
        if(this.accountPM.flagExport=="N")this.disableRejectButton=true;
      }))
      .subscribe(response => {
        this.accountPM = response;
        this.initFormGroup();
      }, error => this.router.navigate(['/pages/prospects-management/prospects-consultation']))
  }


  /** LOAD INCIDENTS LIST */


  validate(){
    this.confirmationModalRef = this.modalService.show(this.templateRefExport);
  }

  reject(){
    this.confirmationModalRef = this.modalService.show(this.templateRefRejct);
  }


  confirmDelete(event: DataTableOutputActionsModel<AccountPPModel>): void {
    if (event) {
      this.accountService.RejectProspect(this.accountCode,this.accountType,this.motif)
        .subscribe(value => {
          if (value) {
            this.router.navigate(['/pages/prospects-management/prospect-validation']);
            this.toastrService.success('Prospect Rejeté');
          } else {
            this.toastrService.error('Erreur pendant la Rejection')
          }
        });
    }
  }


  confirmExport(event: DataTableOutputActionsModel<AccountPPModel>): void {
    if (event) {
      this.accountService.ExportProspect(this.accountCode,this.accountType)
        .subscribe((value:any) => {
          if (value.valid) {
            this.router.navigate(['/pages/prospects-management/prospect-consultation']);
            this.toastrService.success('Prospect Exporté');
          } else {
            this.toastrService.error("Erreur pendant l'exportation")
          }
        });
    }
  }



  /** EXTERNE CALL */

onWatchListProcessCall(countCode:string,countType:string): IStatusWatchlist[] {
  
    this.loading=true
   const dto: WatchlistProcessDTO = {
     codeCompte: countCode,
     critere: {
       codeListe: "", // --- todo
       flag: true,
       flagTrait: ''
     }
   }
  
   switch (countType) {
     case AccountType.PP:
       this.watchlistService.traiterComptePPSimiltitudeByCode(dto).pipe(finalize(() => {
         this.loading = false;
         //his.disply=true;
       }))
         .subscribe(value => {this.watchlistStatusList=value; },
           error => this.toastrService.error("Erreur pendant le traitement de similtitude Watchlist"));
       break;
     case AccountType.PM:
       this.watchlistService.traiterComptePMSimiltitudeByCode(dto).pipe(finalize(() => {
         this.loading = false;
       }))
         .subscribe(value => this.watchlistStatusList=value,
           error => this.toastrService.error("Erreur pendant le traitement de similtitude Watchlist"));
       break;
   }
  
   return this.watchlistStatusList;
  }


  redirect(index: number) {
    switch (index) {

      case 1:
        this.router.navigate(['/pages/prospects-management/prospect-validation']);
        break;

      default:
        break;
    }
  }

  isStatutExport(): boolean {
    if (this.accountType && this.accountCode) {
      switch (this.accountType) {
        case AccountType.PP: {
          return this.accountPP?.flagExport==='O';
        }
        case AccountType.PM: {
          return this.accountPM?.flagExport==='O';
        }
      }
    }
  }
}
