import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AccountPMModel, AccountPPModel, AccountPPPayloadModel, AccountType, GenericItemModel } from 'src/app/main/models';
import { AccountPMPayloadModel } from '../../../../models';
import {ToastrService} from "ngx-toastr";
import {ReferenceService} from "../../../../services";

@Component({
  selector: 'app-identity-information-form',
  templateUrl: './identity-information-form.component.html',
  styleUrls: ['./identity-information-form.component.scss']
})
export class IdentityInformationFormComponent implements OnInit {

  @Input()
  mode: 'ADD' | 'EDIT' = 'ADD';

  @Input()
  set accountInfo(value: { accountType: AccountType, accountPP: AccountPPModel, accountPM: AccountPMModel }) {
    this.accountType = null;
    this.accountPP = null;
    this.accountPM = null;
    if (value) {
      this.accountType = value.accountType;
      this.accountPP = value.accountPP;
      this.accountPM = value.accountPM;
      this.createFormGroup();
      this.initFormGroup();
    }
  }

  // Form group
  accountPPFormGroup: FormGroup = null;
  accountPMFormGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // loading
  loading: boolean = false;

  // List
  pieceIdentiteList: GenericItemModel[] = [];

  // Account
  accountType: AccountType = null;
  accountPP: AccountPPModel = null;
  accountPM: AccountPMModel = null;

  // Avatar image
  photoImg: string = './assets/img/avatar.png';
  photoFile: File = null;

  constructor(private fb: FormBuilder,
              private referenceService: ReferenceService,
              @Inject('photoControl') public photoControl: any,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.referenceService.getTypesDocs().subscribe(value => this.pieceIdentiteList = value);
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.accountPPFormGroup = null;
    this.accountPMFormGroup = null;
    switch (this.accountType) {
      case AccountType.PP:
        this.accountPPFormGroup = this.fb.group({
          pieceIdentite: [null, [Validators.required]],
          numPI: [null, [Validators.required]],
          emetteurPI: [null, [Validators.required]],
          dateEmiPI: [null],
          dateExpPI: [null],
          tribImmatricule: [null],
          rc_rn: [null],
          numTaxePro: [null]
        });
        if (this.mode === 'ADD') {
          this.accountPPFormGroup.enable();
        } else {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        this.accountPMFormGroup = this.fb.group({
          autoriteAgrement: [null, [Validators.required]],
          idFiscal: [null, [Validators.required]],
          tribImmatricule: [null],
          rc: [null],
          numTaxePro: [null]
        });
        if (this.mode === 'ADD') {
          this.accountPMFormGroup.enable();
        } else {
          this.accountPMFormGroup.disable();
        }
        break;
    }
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPP && this.accountPPFormGroup) {
          this.accountPPFormGroup.patchValue({
            pieceIdentite: this.accountPP.pieceIdentite,
            numPI: this.accountPP.numPI,
            emetteurPI: this.accountPP.emetteurPI,
            dateEmiPI: this.accountPP.dateEmiPI,
            dateExpPI: this.accountPP.dateExpPI,
            tribImmatricule: this.accountPP.tribImmatricule,
            rc_rn: this.accountPP.rc_rn,
            numTaxePro: this.accountPP.numTaxePro
          });
        }
        break;
      case AccountType.PM:
        if (this.accountPM && this.accountPMFormGroup) {
          this.accountPMFormGroup.patchValue({
            autoriteAgrement: this.accountPM.autoriteAgrement,
            tribImmatricule: this.accountPM.tribImmatricule,
            idFiscal: this.accountPM.idFiscal,
            rc: this.accountPM.rc,
            numTaxePro: this.accountPM.numTaxePro
          });
        }
        break;
    }
  }

  /** CHECK FORM GROUP IS VALID OR NOT */
  isValidForm(): boolean {
    let isValid: boolean = false;
    switch (this.accountType) {
      case AccountType.PP:
        isValid = this.accountPPFormGroup && this.accountPPFormGroup.valid;
        break;
      case AccountType.PM:
        isValid = this.accountPMFormGroup && this.accountPMFormGroup.valid;
    }
    if (!isValid) {
      this.showValidationsMsg = true;
    }
    return isValid;
  }

  /** GENERATE PAYLOAD */
  generatePayload(): AccountPPPayloadModel | AccountPMPayloadModel {
    let payload: AccountPPPayloadModel | AccountPMPayloadModel = null;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          payload = {
            ...new AccountPPPayloadModel(),
            pieceIdentite: this.accountPPFormGroup.get("pieceIdentite").value,
            numPI: this.accountPPFormGroup.get("numPI").value,
            emetteurPI: this.accountPPFormGroup.get("emetteurPI").value,
            dateEmiPI: this.accountPPFormGroup.get("dateEmiPI").value,
            dateExpPI: this.accountPPFormGroup.get("dateExpPI").value,
            tribImmatricule: this.accountPPFormGroup.get("tribImmatricule").value,
            rc_rn: this.accountPPFormGroup.get("rc_rn").value,
            numTaxePro: this.accountPPFormGroup.get("numTaxePro").value
          }
          if (this.mode === 'EDIT' && this.accountPP) {
            payload.code = this.accountPP.code;
          }
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          payload = {
            ... new AccountPMPayloadModel(),
            autoriteAgrement: this.accountPMFormGroup.get("autoriteAgrement").value,
            tribImmatricule: this.accountPMFormGroup.get("tribImmatricule").value,
            idFiscal: this.accountPMFormGroup.get("idFiscal").value,
            rc: this.accountPMFormGroup.get("rc").value,
            numTaxePro: this.accountPMFormGroup.get("numTaxePro").value
          }
          if (this.mode === 'EDIT' && this.accountPM) {
            payload.code = this.accountPM.code;
          }
        }
    }
    return payload;
  }

  /** ON ENABLE FORM */
  onEnableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.enable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.enable();
        }
    }
  }

  /** ON DISABLE FORM */
  onDisableForm() {
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.disable();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.disable();
        }
    }
  }

  /** ON RESET FORM */
  onResetForm() {
    this.showValidationsMsg = false;
    switch (this.accountType) {
      case AccountType.PP:
        if (this.accountPPFormGroup) {
          this.accountPPFormGroup.reset();
        }
        break;
      case AccountType.PM:
        if (this.accountPMFormGroup) {
          this.accountPMFormGroup.reset();
        }
    }
    if (this.photoFile) {
      this.photoFile = null;
      this.photoImg = '/assets/img/avatar.png';
    }
  }

  /** PHOTO SELECTION */
  onFileSelected(event) {
    this.photoFile = event.target.files[0];
    if (!this.photoFile)
      return;

    let imgWidth, imgHeight;
    let img = new Image();
    img.src = URL.createObjectURL(this.photoFile);
    return new Promise<any>((resolve, reject) => {
      img.onload = (e: any) => {
        imgHeight = e.path[0].height;
        imgWidth = e.path[0].width;
        // run control on photo
        if ((imgHeight === this.photoControl?.defaultHeight) && (imgWidth === this.photoControl?.defaultWidth)) {
          this.photoSubmit(this.photoFile);
        } else {
          this.toastr.warning('l\'image ne respecte pas les dimensions recommendés');
        }
      }
    });
  }

  photoSubmit(photo: File): void {
    const reader = new FileReader();
    reader.onload = () => this.photoImg = reader.result as any;
    reader.readAsDataURL(photo);
  }
}
