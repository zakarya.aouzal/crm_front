import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentityInformationFormComponent } from './identity-information-form.component';

describe('IdentityInformationFormComponent', () => {
  let component: IdentityInformationFormComponent;
  let fixture: ComponentFixture<IdentityInformationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentityInformationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentityInformationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
