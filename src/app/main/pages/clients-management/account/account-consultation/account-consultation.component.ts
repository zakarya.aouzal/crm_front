import { Component, OnInit } from '@angular/core';
import { AccountFilterModel, AccountType } from 'src/app/main/models';

@Component({
  selector: 'app-account-consultation',
  templateUrl: './account-consultation.component.html',
  styleUrls: ['./account-consultation.component.scss']
})
export class AccountConsultationComponent implements OnInit {

  filter: { filterBody: AccountFilterModel, accountType: AccountType } = null;

  constructor() { }

  ngOnInit(): void {
  }

}
