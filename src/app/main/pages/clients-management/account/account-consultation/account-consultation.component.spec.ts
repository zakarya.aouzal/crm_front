import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountConsultationComponent } from './account-consultation.component';

describe('AccountConsultationComponent', () => {
  let component: AccountConsultationComponent;
  let fixture: ComponentFixture<AccountConsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountConsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
