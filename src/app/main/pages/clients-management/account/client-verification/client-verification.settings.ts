import {DataTableActions, DataTableColumnsFormat, DataTableSettingsModel} from 'src/app/shared/models';

export const COMPTE_PP_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
      }
    }, {
      name: 'nom',
      title: 'Nom',
      format: DataTableColumnsFormat.STRING,
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.NOM'
      }
    }, {
      name: 'prenom',
      title: 'Prénom',
      format: DataTableColumnsFormat.STRING,
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.PRENOM'
      }
    }, {
      name: 'dateInsert',
      title: 'Date Créat.',
      format: DataTableColumnsFormat.STRING,
      style: {
        textColor: '#FD7272'
      },
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.DATE_INSERT'
      }
    }, {
      name: 'dateModification',
      title: 'Date Modif.',
      format: DataTableColumnsFormat.STRING,
      style: {
        textColor: '#FD7272'
      },
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.DATE_MODIFICATION'
      }
    }
  ], actions: [DataTableActions.VIEW]
};

export const COMPTE_PM_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ACCOUNT_LIST.CODE'
      }
    }, {
      name: 'raisonSociale',
      title: 'Raison Sociale',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.ACCOUNT_LIST.RAISON_SOCIALE'
      }
    }, {
      name: 'dateInsert',
      title: 'Date Créat.',
      format: DataTableColumnsFormat.STRING,
      style: {
        textColor: '#FD7272'
      },
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.DATE_INSERT'
      }
    }, {
      name: 'dateModification',
      title: 'Date Modif.',
      format: DataTableColumnsFormat.STRING,
      style: {
        textColor: '#FD7272'
      },
      data:{
        i18nValue:'COLUMNS.ACCOUNT_LIST.DATE_MODIFICATION'
      }
    }
  ], actions: [DataTableActions.VIEW]
};

export const EXPIRED_DOCS_SETTINGS: DataTableSettingsModel = {
  columns: [
    {
      name: 'code',
      title: 'Code',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.COMPTE_ALERTE_LIST.ACCOUNT'
      }
    }, {
      name: 'pieceIdentite',
      title: 'Piece identité',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.COMPTE_ALERTE_LIST.PIECE_IDENTITE'
      }
    }, {
      name: 'numPI',
      title: 'Num PI',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.COMPTE_ALERTE_LIST.NUMPI'
      }
    }, {
      name: 'emetteurPI',
      title: 'Emetteur PI',
      format: DataTableColumnsFormat.STRING,
      data: {
        i18nValue:'COLUMNS.COMPTE_ALERTE_LIST.EMETTEUR_PI'
      }
    // }, {
      // name: 'dateEmiPI',
      // title: 'Date emi PI',
      // format: DataTableColumnsFormat.DATE,
      // data: {
      //   i18nValue:'COLUMNS.COMPTE_ALERTE_LIST.DATE_EMI_PI'
      // }
    }, {
      name: 'dateExpPI',
      title: 'Date exp PI',
      format: DataTableColumnsFormat.DATE,
      style: {
        textColor: '#fd7272'
      },
      data: {
        i18nValue:'COLUMNS.COMPTE_ALERTE_LIST.DATE_EXP_PI'
      }
    }
  ], actions: [DataTableActions.VIEW]
};
