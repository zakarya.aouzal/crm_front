import {Component, OnInit} from '@angular/core';
import {DataTableOutputActionsModel, DataTableSettingsModel} from "../../../../../shared/models";
import {AccountPMModel, AccountPPModel, CompteAlerte} from "../../../../models";
import {AccountService} from "../../../../services";
import {Router} from "@angular/router";
import {COMPTE_PM_SETTINGS, COMPTE_PP_SETTINGS, EXPIRED_DOCS_SETTINGS} from "./client-verification.settings";
import {finalize} from "rxjs/operators";

@Component({
  selector: 'app-client-verification',
  templateUrl: './client-verification.component.html',
  styleUrls: ['./client-verification.component.scss']
})
export class ClientVerificationComponent implements OnInit {

  // TABLE
  loading: boolean = true;
  showExpiredDocs: boolean = false;

  // TABLE SETTINGS
  accountPPSettings: DataTableSettingsModel = COMPTE_PP_SETTINGS;
  accountPMSettings: DataTableSettingsModel = COMPTE_PM_SETTINGS;
  expiredDocsSettings: DataTableSettingsModel = EXPIRED_DOCS_SETTINGS;

  // DATA
  accountPPList: AccountPPModel[] = [];
  accountPMList: AccountPMModel[] = [];
  compteAlerteList: CompteAlerte[] = [];
  compteAlerte: CompteAlerte = null;

  constructor(private accountService: AccountService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loadAccountsToUpdate();
    this.loadExpiredDocuments();
  }

  public loadAccountsToUpdate(): void {
    this.loading = true;
    this.accountService.getAccountsToUpdate()
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {
        this.accountPPList = value.comptesPP;
        this.accountPMList = value.comptesPM;
        console.log('kamal', this.accountPMList)
      });
  }

  public loadExpiredDocuments(): void {
    this.loading = true;
    this.accountService.getExpiredDocumentsAccount()
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {
        this.compteAlerteList = value;
        
      });
  }

  onAction(event: DataTableOutputActionsModel<AccountPMModel | AccountPPModel | CompteAlerte>, type: ('PP' | 'PM' | 'DOCS')) {
    if (event) {
      if (event?.item && type === 'DOCS') {
        this.showExpiredDocument(event.item);
      } else {
        switch (event.actionType) {
          case 'VIEW': {
            let id = event.item ? event.item.code : null;
            const redirect_URL: string = '/pages/prospects-management/prospect-details';
            this.router.navigate([redirect_URL, type, id]);
            break;
          }
        }
      }
    }
  }

  private showExpiredDocument(item): void {
    this.showExpiredDocs = true;
    this.compteAlerte = item;
  }

  public closeExpiredDocument(): void {
    this.showExpiredDocs = false;
  }

  onDoneUpdateExpiredDocument(event: boolean) {
    if (event) {
      this.loadExpiredDocuments();
    }
    this.showExpiredDocs = false;
    this.compteAlerte = null;
  }
}
