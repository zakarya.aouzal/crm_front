import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { GenericItemModel } from 'src/app/main/models';
import { ReferenceService } from 'src/app/main/services';
import { AccountFilterComponent } from './account-filter.component';

fdescribe('AccountFilterComponent', () => {
  let component: AccountFilterComponent;
  let fixture: ComponentFixture<AccountFilterComponent>;
  let referenceService: ReferenceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountFilterComponent],
      providers: [FormBuilder, ReferenceService],
      imports: [HttpClientTestingModule, ReactiveFormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    referenceService = TestBed.inject(ReferenceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should create formGroup`, () => {
    component.createFormGroup();
    expect(component.formGroup).not.toEqual(null);
  });

  it(`should init formGroup`, () => {
    component.initFormGroup();
    expect(component.formGroup).not.toEqual(null);
    let accountType = component.accountTypeList.find(item => item.code === 'PP');
    expect(component.formGroup.get('accountType').value).toEqual(accountType);
  });

  it(`should generate payload`, () => {

    // account type PP
    let accountTypePP = component.accountTypeList.find(item => item.code === 'PP');

    component.formGroup.reset();
    component.formGroup.get('accountType').setValue(accountTypePP);
    component.formGroup.get('firstName').setValue('soufiane');
    component.formGroup.get('lastName').setValue('oubalkass');
    component.formGroup.get('accountManager').setValue('manager x');
    component.formGroup.get('phoneNumber').setValue('0626470877');
    component.formGroup.get('creationDate').setValue(new Date('2021/05/18'));
    component.formGroup.get('country').setValue('maroc');
    component.formGroup.get('city').setValue('casablanca');

    let payloadAccountPP = component.generatePayload();

    expect(payloadAccountPP).not.toBe(null);
    expect(payloadAccountPP.prenom).toBe('soufiane');
    expect(payloadAccountPP.nom).toBe('oubalkass');
    expect(payloadAccountPP.raisonSociale).toBe(undefined);
    expect(payloadAccountPP.gestionnaire).toBe('manager x');
    expect(payloadAccountPP.mobile1).toBe('0626470877');
    expect(payloadAccountPP.dateInsert).toEqual(new Date('2021/05/18'));
    expect(payloadAccountPP.paysResidence).toBe('maroc');
    expect(payloadAccountPP.ville).toBe('casablanca');

    // account type PM
    let accountTypePM = component.accountTypeList.find(item => item.code === 'PM');

    component.formGroup.reset();
    component.formGroup.get('accountType').setValue(accountTypePM);
    component.formGroup.get('socialReason').setValue('social x');
    component.formGroup.get('accountManager').setValue('manager x');
    component.formGroup.get('phoneNumber').setValue('0626470877');
    component.formGroup.get('creationDate').setValue(new Date('2021/05/18'));
    component.formGroup.get('country').setValue('maroc');
    component.formGroup.get('city').setValue('casablanca');

    let payloadAccountPM = component.generatePayload();

    expect(payloadAccountPM).not.toBe(null);
    expect(payloadAccountPM.prenom).toBe(undefined);
    expect(payloadAccountPM.nom).toBe(undefined);
    expect(payloadAccountPM.raisonSociale).toBe('social x');
    expect(payloadAccountPM.gestionnaire).toBe('manager x');
    expect(payloadAccountPM.mobile1).toBe('0626470877');
    expect(payloadAccountPM.dateInsert).toEqual(new Date('2021/05/18'));
    expect(payloadAccountPM.paysResidence).toBe('maroc');
    expect(payloadAccountPM.ville).toBe('casablanca');
  });

  it(`'loadAccountManagers' should call service 'referenceService.getListAccountManager'`, () => {
    const response: GenericItemModel[] = [
      {
        code: "ASMAAN",
        value: "Asmaa nantis"
      }, {
        code: "ILILI",
        value: "OMAR ILILI"
      }];
    spyOn(referenceService, 'getListAccountManager').and.returnValue(of(response));
    component.loadAccountManagers();
    expect(referenceService.getGestionnaireFronts).toHaveBeenCalled();
    expect(component.accountManagerList).toEqual(response);
  });

  it(`'loadCountries' should call service 'referenceService.getListCountries'`, () => {
    const response: GenericItemModel[] = [{
      code: "EG",
      value: "EGYPTE"
    }, {
      code: "AE",
      value: "EMIRATS ARABES"
    }];
    spyOn(referenceService, 'getListCountries').and.returnValue(of(response));
    component.loadCountries();
    expect(referenceService.getPaysList).toHaveBeenCalled();
    expect(component.countryList).toEqual(response);
  });

});
