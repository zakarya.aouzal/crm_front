import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {AccountFilterModel, AccountType, Flag, FlagExport, GenericItemModel} from 'src/app/main/models';
import {ReferenceService} from 'src/app/main/services';
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-account-filter',
  templateUrl: './account-filter.component.html',
  styleUrls: ['./account-filter.component.scss']
})
export class AccountFilterComponent implements OnInit {

  @Output()
  submitted = new EventEmitter<{ filterBody: AccountFilterModel, accountType: AccountType }>();

  @Input()
  isFlagExport: boolean = false;

  // Form group
  formGroup: FormGroup = null;
  showValidationsMsg: boolean = false;

  // List
  accountTypeList = [{code: AccountType.PP, label:  this.translate.instant("NOUVEAU_COMPTE.PP")}, {code: AccountType.PM, label:  this.translate.instant("NOUVEAU_COMPTE.PM")}];
  gestionnaireList: GenericItemModel[] = [];
  countryList: GenericItemModel[] = [];

  constructor(private fb: FormBuilder, private referenceService: ReferenceService,private translate:TranslateService) { }

  ngOnInit(): void {
    this.createFormGroup();
    this.initFormGroup();
    this.onSubmit();
    this.loadCountries();
    this.loadGestionnaires();
  }

  /** CREATE FORM GROUP */
  createFormGroup() {
    this.formGroup = this.fb.group({
      accountType: [null, [Validators.required]],
      firstName: [null, [Validators.pattern(environment.regexPattern.alphabetic)]],
      lastName: [null, [Validators.pattern(environment.regexPattern.alphabetic)]],
      socialReason: [null],
      gestionnaire: [null],
      phoneNumber: [null, [Validators.pattern(environment.regexPattern.phoneNumber)]],
      creationDate: [null],
      country: [null],
      city: [null]
    });
  }

  /** INIT FORM GROUP */
  initFormGroup() {
    this.showValidationsMsg = false;
    if (this.formGroup) {
      this.formGroup.reset();
      let accountType = this.accountTypeList.find(item => item.code === 'PP');
      this.formGroup.get('accountType').setValue(accountType);
    }
  }

  /** SUBMIT EVENT */
  onSubmit() {
    if (this.formGroup.invalid) {
      this.showValidationsMsg = true;
      return;
    }
    const filter = this.generatePayload();
    let accountType: AccountType = this.formGroup.get('accountType').value ? this.formGroup.get('accountType').value.code : null;
    if (filter && accountType) {
      if (this.isFlagExport) filter.flagExport = FlagExport.Exported;
      this.submitted.emit({ filterBody: filter, accountType: accountType });
    }
  }

  /** GENERATE PAYLOAD */
  generatePayload(): AccountFilterModel {
    let filter: AccountFilterModel = null;

    if (this.formGroup) {

      filter = {
        ...new AccountFilterModel(),
        gestionnaire: this.formGroup.get('gestionnaire').value,
        mobile1: this.formGroup.get('phoneNumber').value,
        dateInsert: this.formGroup.get('creationDate').value,
        paysResidence: this.formGroup.get('country').value,
        ville: this.formGroup.get('city').value
      }

      let accountType = this.formGroup.get('accountType').value ? this.formGroup.get('accountType').value.code : null;

      switch (accountType) {
        case 'PP':
          filter.nom = this.formGroup.get('lastName').value;
          filter.prenom = this.formGroup.get('firstName').value;
          break;
        case 'PM':
          filter.raisonSociale = this.formGroup.get('socialReason').value;
          break;
      }

    }

    return filter;
  }

  /** GET LIST OF COUNTRIES */
  loadCountries() {
    this.referenceService.getPaysList()
      .subscribe(response => {
        this.countryList = response;
      })
  }

  /** GET LIST OF ACCOUNT MANAGERS */
  loadGestionnaires() {
    this.referenceService.getGestionnaireFronts()
      .subscribe(response => {
        this.gestionnaireList = response;
      });
  }

}
