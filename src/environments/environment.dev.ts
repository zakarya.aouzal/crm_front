import { KeycloakConfig, KeycloakInitOptions } from 'keycloak-js';
import { environment as defaultEnvironment } from './environment.default';

const keycloakConfig: KeycloakConfig = {
    url: 'http://10.20.20.201:8081/auth',
    realm: 'Manar-Realm',
    clientId: 'crm-front'
};
const keycloakInitOptions: KeycloakInitOptions = {
  checkLoginIframe: false
};

export const environment = {
  ...defaultEnvironment,
  production: true,
  useHash: true,
  apiEndPoint: 'http://10.20.20.201:8080/backend-1.0',
  keycloak: {
    config: keycloakConfig,
    enableLogging: true,
    keycloakOptions: keycloakInitOptions,
  }
};

