export const environment = {
    production: false,
    defaultLanguage: 'fr',
    pipeParameters: {
        currency: {
            currencyCode: ' ',
            display: 'code',
            digitsInfo: '.2-2'
        },
        number: {
            digitsInfo: '.0-2'
        },
        percent: {
            digitsInfo: '.2-2'
        },
        date: {
            format: 'dd/MM/yyyy, HH:mm:ss',
            formatNoTime: 'dd/MM/yyyy'
        },
        ged: {
          totalFilesSize: 5
        }
    },
    photoControl: {
      defaultHeight: 450,
      defaultWidth: 350
    },
    regexPattern: {
        number: '^[0-9]*$',
        numberDecimal: '^-?[0-9]\\d*(\\.\\d*)?$',
        numberDecimalTwoPlaces: /^[0-9]+(\.[0-9]{1,2})?$/,
        alphabetic: '[a-zA-Z][a-zA-Z ]+',
        phoneNumber: /^[0-9]{10,10}?$/
    }
};
